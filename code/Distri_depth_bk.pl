#!/usr/bin/perl -w
#*Filename: Distri_depth_bk.pl
#*Description: distribution of depth and breakpoints along query sequences
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.08.02
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : <align> | $0 <q_id.depth_bk>

notes: align should be standard ConverAlign.pl output, with consensus as query, and reads as sbjct

output format:
q_id position depth breakpoints

USAGE

die $usage if (@ARGV >= 1);

my $cutoff = 50;  #remainning sequence at ends to define real breakpoints
my %depth;
my %all_bk;
my %real_bk;
my $id;

while (<stdin>) {
	chomp;
	my @a = split(/\s+/, $_);
	if (@a<10) {
		next;
	}
	my $sig = $a[2];
	my $id = $a[0];
	my $size = $a[4];
	my $begin = $a[5];
	my $end = $a[6];
	my $s_size = $a[7];
	my $s_begin = $a[8];
	my $s_end = $a[9];
	
	#initialization
	if (!defined $depth{$id}) {
		my @a;
		my @b;
		my @c;
		for (my $i=0; $i<$size; $i++) {
			$a[$i] = 0;
			$b[$i] = 0;
			$c[$i] = 0;
		}
		$depth{$id} = \@a;
		$all_bk{$id} = \@b;
		$real_bk{$id} = \@c;
	}
	for (my $i=$begin; $i<=$end; $i++) {
		$depth{$id}[$i]++;
	}
	$all_bk{$id}[$begin]++;
	$all_bk{$id}[$end]++;
	if ($s_begin >= $cutoff) {
		if ($sig eq "+") {
			$real_bk{$id}[$begin]++;
		}
		else {
			$real_bk{$id}[$end]++;
		}
	}
	if ($s_size - $s_end >= $cutoff) {
		if ($sig eq "+") {
			$real_bk{$id}[$end]++;
		}
		else {
			$real_bk{$id}[$begin]++;
		}
	}
}

foreach my $id (keys %depth) {
	my $file = "$id.depth_bk";
	open(F, ">$file") || die;
	my $size = @{$depth{$id}};
	for (my $i=0; $i<$size; $i++) {
		print F "$i\t$depth{$id}[$i]\t$all_bk{$id}[$i]\t$real_bk{$id}[$i]\n";
	}
	close F;
}
