#!/usr/bin/perl -w
#*Filename: screen_vector.pl
#*Description: split reads dataset; run pairwise cross_match alignment to screen vector sequences
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.07.04
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
		-r	STRING	reads file
		-v	STRING	vector file
		-n	INT	split the dataset into number of files, default=1
		-a	INT	number of parallel processors, default=1
		-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts, "r=s", "n=i", "a=i", "v=s", "h");

my $read_file = defined $opts{r}? $opts{r} : die "no reads file\n";
my $vector_file = defined $opts{v}? $opts{v} : die "no vector file\n";
die $usage if defined $opts{h};
my $Nfiles = defined $opts{n}? $opts{n} : 1;
my $Nprocess = defined $opts{a}? $opts{a} : 1;


#make working space
my $dir = "ReAS".time();
mkdir($dir) ||die "can not make dir $dir\n";

chdir($dir);

#
`split_fa_bygroup.pl ../$read_file $Nfiles 1`;

open (F_sh, ">sh") || die "can not write file sh\n";
for (my $j=1; $j<=$Nfiles; $j++) {
	print F_sh "cross_match.manyreads -screen -minmatch 14 leaf.$j.fa ../$vector_file 2>/dev/null &\n";
}
close F_sh;
`cat sh|multi_run2.sh -n $Nprocess -w 5`;
`cat *.screen >../$read_file\.screen`;
chdir("..");
`rm -rf $dir`;
