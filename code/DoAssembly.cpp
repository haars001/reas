/*
*Copyright 2005  BGI
*All right reserved
*
*Filename:  DoAssembly
*Description:  

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2005.04.20
*/

#include "Assembly.h"
#include "Utilities.h"
#include "ReAS.h"
#include <stdlib.h>

using namespace std;

extern int depth;
extern int bk_size;
extern char quiet;
extern int sub_multi;
extern int min_join;
extern int min_extend;
extern int max_extend;

//usage:
void usage(void)
{
	cout<<"Usage:	DoAssembly [options]\n"
		<<"	-i	STRING	working directory, default=.\n"
		<<"	-r	STRING	input sequence file, default=seg.fa\n"
		<<"	-l	STRING	input linkage information file, default=<stdin>\n"
		<<"	-f	text input of linkage information? default=[binary]\n"
		<<"	-t	INT	a subset of segments for multi-alignment to get consensus, default=-1(entire), suggest 200 if set\n"
		<<"	-b	STRING	input breakpoint file, default=seg.bk\n"
		<<"	-d	INT	depth threshold, default=6\n"
		<<"	-o	STRING	output consensus sequence file, default=consensus.fa\n"
		<<"	-p	STRING	prefix of consensus sequence names, default is NULL\n"
		<<"	-s	output log details, default=[no]\n"
		<<"Advanced:\n"
		<<"	-e	INT	size of remainning sequence at ends of raw reads when defining breakpoints, default=50bp\n"
		<<"	-j	INT	minimal depth for extension, default=depth/3\n"
		<<"	-u	INT minimal extension size, default=50\n"
		<<"	-v	INT	maximum extension size, default=200\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

char * work_dir = ".";
char * seq_file = "seg.fa";
char * bkpoint_file = "seg.bk";
char * link_file = "";  //"seg.link";
char binary = 1;
char * out_file = "consensus.fa";
string prefix = "";

#ifndef DINTERNAL_MUSCLE
//process for calling muscle
pid_t pid_muscle;
int fd_muscle[2];
int fd_muscle_r[2];
char * multi_fa = "multi.fa";
char * multi_aln = "multi.aln";
#endif

int main(int argc, char *argv[])
{
	//print usage
	if (argc == 1)
	{
			usage();
	}
	//[options]
	int c;
	while((c=getopt(argc,argv,"i:r:l:fb:d:t:e:o:p:j:u:v:sh")) != -1) {
			switch(c) {
					case 'i': work_dir = optarg; break;
					case 'r': seq_file = optarg; break;
					case 'l': link_file = optarg; break;
					case 'f': binary = 0;break;
					case 'b': bkpoint_file = optarg; break;
					case 'd': depth = atoi(optarg); break;
					case 'e': bk_size = atoi(optarg); break;
					case 't': sub_multi = atoi(optarg); break;
					case 'o': out_file = optarg; break;
					case 'p': prefix = optarg; break;
					case 's': quiet = 0; break;
					case 'j': min_join = atoi(optarg); break;
					case 'u': min_extend = atoi(optarg); break;
					case 'v': max_extend = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	if (chdir(work_dir))
	{
		cerr<<"can not jump to "<<work_dir<<endl;
		exit(1);
	}
	//
	ifstream fin_seq(seq_file);
	if (!fin_seq)
	{
		cerr<<"can not open seq "<<seq_file<<endl;
		exit(1);
	}
	ifstream fin_bk(bkpoint_file);
	if (!fin_bk)
	{
		cerr<<"can not open bk "<<bkpoint_file<<endl;
		exit(1);
	}
	ofstream fout_seq(out_file);
	if (!fout_seq)
	{
		cerr<<"can not open output "<<out_file<<endl;
		exit(1);
	}
#ifndef DINTERNAL_MUSCLE
	//pipe
	if ((pipe(fd_muscle) == -1) || (pipe(fd_muscle_r) == -1))
	{
		cerr<<"error at creating pipe for muscle"<<endl;
		exit(1);
	}
	//fork
	if ((pid_muscle = fork()) < 0)
	{
		cerr<<"error at fork"<<endl;
		exit(1);
	}
	else if (pid_muscle == 0)
	{
		char c;
		close(fd_muscle[1]);
		close(fd_muscle_r[0]);
		char command[100];
//		sprintf(command, "muscle -in %s -out %s -maxiters 2 -diags1 -quiet", multi_fa, multi_aln);
		sprintf(command, "muscle -in %s -out %s -maxiters 2 -quiet", multi_fa, multi_aln);  //if maxiters >=3, it's much slower
//		sprintf(command, "clustalw %s -OUTPUT=PIR", multi_fa);
		while (1)
		{
			read(fd_muscle[0], &c, 1);
			if (c == 'e') //exit
			{
//				sprintf(command, "rm %s  %s",multi_fa, multi_aln);
//				system(command);
				write(fd_muscle_r[1], &c, 1);
				close(fd_muscle[0]);
				close(fd_muscle_r[1]);
				exit(0);
			}
			system(command);
			write(fd_muscle_r[1], &c, 1);
		}
	}
	close(fd_muscle[0]);
	close(fd_muscle_r[1]);
#endif
	//
	Assembly *as = new(Assembly);
	SEQ seq;
	Rseqs(fin_seq, seq);
	fin_seq.close();
	outLog("read in seq");
	as->InBreakpoint(fin_bk);
	fin_bk.close();
	outLog("read in bk");
	//text input of linkage information
	if (!binary)
	{
		ifstream fin_link(link_file);
		if (fin_link)
		{
			as->InLink(fin_link);
		}
		else
		{
			as->InLink(cin);
		}
		fin_link.close();
	}
	else
	{
		FILE * fin_link = fopen(link_file, "rb");
		if (fin_link)
		{
			as->InLink_binary(fin_link);
		}
		else
		{
			as->InLink_binary(stdin);
		}
	}

	outLog("read in link");
	as->InSeq(seq);
	as->DoAssembly();
	as->OutConsensusSeq(fout_seq);

	fout_seq.close();
	//
#ifndef DINTERNAL_MUSCLE
	char end='e';
	write(fd_muscle[1], &end, 1);
	close(fd_muscle[1]);
	close(fd_muscle_r[0]);
#endif
	outLog("finished");
	return 0;
}
