/*
*Copyright 2005  BGI
*All right reserved
*
*Filename: N_matchreads
*Description:  input kmer2reads result, count number of shared k-mers between every 2 reads

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2005.04.20
*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<set>
#include<vector>
#include<map>
#include<time.h>
#include <stdlib.h>
#include <cmath>
#include <algorithm>

using namespace std;

//const:

//structure & class & function


//usage:
void usage(void)
{
	cout<<"Usage:	<stdin> | N_matchreads [options] <stdout>\n"
		<<"	-m	INT	threshold of number of shared high-depth kmers, default=8\n"
		<<"	-d	INT	depth, default from kmer2reads file\n"
		<<"	-f	FLOAT	fraction of subset in all, default=1\n"
		<<"	-r	INT	stepwise ratio, default no tax, output all\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	int K;
	int D=0;
	int M = 8;
	float F = 1;
	int R = 10000000;
	//[options]
	int c;
	while((c=getopt(argc,argv,"m:d:f:r:h")) != -1) {
			switch(c) {
					case 'm': M = atoi(optarg); break;
					case 'd': D = atoi(optarg); break;
					case 'f': F = atof(optarg); break;
					case 'r': R = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	int initial_time = time(NULL);
	map<unsigned int, vector<size_t> > read_mers;
	K = fgetc(stdin);
	int depth = fgetc(stdin);
	if (D == 0)
	{
		D=depth;
	}
	size_t R_part = (size_t)pow(double(2), 64-K*2) - 1;
	size_t M_part = ~R_part;
//	cout<<"M_part:  "<<hex<<M_part<<"   R_part:  "<<R_part<<endl;

	//read in information from kmer2reads file
	size_t merRead;
	unsigned int read_id;
	size_t mer;
	set<size_t> set_mers;
	size_t num_lines;
	fread(&num_lines,sizeof(size_t),1,stdin);
	unsigned int old_read = 0;
	while (fread(&merRead,sizeof(size_t),1,stdin))
	{
		read_id = (merRead & R_part);  //it's numbered from 0, so should +1 here
		read_id +=1;
		mer = (merRead & M_part)>>(sizeof(size_t)*8 - K*2);
//		cout<<"read:  "<<read_id<<"   mer:   "<<mer<<endl;
		if ((old_read != read_id) && (set_mers.size() >= M))
		{
			read_mers[old_read].resize(set_mers.size());
			copy(set_mers.begin(), set_mers.end(), read_mers[old_read].begin());
			sort(read_mers[old_read].begin(), read_mers[old_read].end());
			old_read = read_id;
			set_mers.clear();
		}
		set_mers.insert(mer);
	}
	if (set_mers.size() >= M)
	{
		read_mers[read_id].resize(set_mers.size());
		copy(set_mers.begin(), set_mers.end(), read_mers[read_id].begin());
		sort(read_mers[read_id].begin(), read_mers[read_id].end());
	}
	
	fprintf(stderr, "\nread in data:\t%10d sec\n", time(NULL)-initial_time);
	//randomly decide a fraction of F reads to output
	vector<char> flag;
	if (1 == F)
	{
		flag.clear();
		flag.resize(read_mers.size(), 1);
	}
	else
	{
		flag.clear();
		flag.resize(read_mers.size(), 0);
		for (unsigned int i=0; i<read_mers.size() * F; i++)
		{
			flag[i]=1;
		}
		srandom((size_t)time(NULL));
		for (unsigned int i=0; i<read_mers.size(); i++)
		{
			int index_1 = random() % read_mers.size();
			int index_2 = random() % read_mers.size();
			char tmp = flag[index_1];
			flag[index_1] = flag[index_2];
			flag[index_2] = tmp;
		}
	}
	//
	int t1 = 0;
	set<unsigned int> to_remove;
	for (map<unsigned int, vector<size_t> >::iterator p = read_mers.begin(); p != read_mers.end(); p++)
	{
		if (!flag[t1])
		{
			to_remove.insert(p->first);
		}
		t1++;
	}
	for (set<unsigned int>::iterator p = to_remove.begin(); p != to_remove.end(); p++)
	{
		read_mers.erase(*p);
	}
	to_remove.clear();
	flag.clear();
	//count number of reads overlapping >= M kmers for each read
	int count=0;
	int sum = read_mers.size();
	map<unsigned int, unsigned int> num_overlap;
	for (map<unsigned int, vector<size_t> >::iterator p=read_mers.begin(); p!=read_mers.end(); p++)
	{
		unsigned int n=0;
		for (map<unsigned int, vector<size_t> >::iterator z=read_mers.begin(); z!=read_mers.end(); z++)
		{
			if (p->first == z->first)
			{
				continue;
			}
		/*	set<size_t> overlap;
			set_intersection(p->second.begin(), p->second.end(), z->second.begin(), z->second.end(), inserter(overlap, overlap.begin()));
			if (overlap.size() >= M)
			{
				n++;
			}*/
			unsigned int shared = 0;
			vector<size_t>::iterator d = p->second.begin();
			vector<size_t>::iterator h = z->second.begin();
			while ((d != p->second.end()) && (h != z->second.end()))
			{
				if (*d == *h)
				{
					shared++;
					if (shared >= M)
					{
						break;
					}
					d++;
					h++;
				}
				else if (*d > *h)
				{
					h++;
				}
				else
				{
					d++;
				}
			}
			if (shared >= M)
			{
				n++;
			}
			if ((10000000 <= R) && (n >= D))
			{
				break;
			}
		}
		if (n >= D)
		{
			num_overlap[p->first] = n;
		}
		count++;
		if (0 == count % 100)
		{
			float fr = (float)100*count/sum;
			int tt = time(NULL)-initial_time;
		//	cerr<<count<<"\t"<<fr<<"%\t"<<tt<<"sec";
			fprintf(stderr, "\r%10d\t%4.2f%\t%10d\t%10dsec", count, fr, sum, tt);
		}
	}
//	cerr<<endl;
	fprintf(stderr, "\n");
	//normalization to reduce reads for very high-depth repeats, so that to save a lot calculation
	//
	vector<unsigned int> bound;
	unsigned int order=0;
	unsigned int uplimit=1000000;
	unsigned a;
	while ((a = (unsigned int) pow(R, (float)order)) <= uplimit)
	{
		bound.push_back(a);
		order ++;
	}
	//
	map<unsigned int, vector<unsigned int> > range_IDs;
	for (map<unsigned int, unsigned int>::iterator p=num_overlap.begin(); p != num_overlap.end(); p++)
	{
		unsigned int group;
		for (int j=bound.size()-1; j>=0; j--)
		{
			if (p->second >= bound[j])
			{
				group = j;
				break;
			}
		}
		range_IDs[group].push_back(p->first);
	}
	//output
	sum=0;
	unsigned int picked=0;
	for (map<unsigned int, vector<unsigned int> >::iterator p=range_IDs.begin(); p!=range_IDs.end(); p++)
	{
		if (0 == p->first)
		{
			for (unsigned int i=0; i<p->second.size(); i++)
			{
				cout<<p->second[i]<<"\n";
			}
			sum += p->second.size();
			picked += p->second.size();
			cerr<<">="<<D<<"\t:\t"<<p->second.size()<<"\t"<<p->second.size()<<endl;
		}
		else
		{
			unsigned int rand = (unsigned int) (p->second.size() / pow(R, (float)p->first));
			sum += p->second.size();
			picked += rand;
			cerr<<">="<<pow(R, (float)p->first)*D<<"\t:\t"<<p->second.size()<<"\t"<<rand<<endl;
			//randomly decide which one to output
			flag.clear();
			flag.resize(p->second.size(), 0);
			for (unsigned int i=0; i<rand; i++)
			{
				flag[i]=1;
			}
			srandom((size_t)time(NULL));
			for (unsigned int i=0; i<p->second.size(); i++)
			{
				int index_1 = random() % p->second.size();
				int index_2 = random() % p->second.size();
				char tmp = flag[index_1];
				flag[index_1] = flag[index_2];
				flag[index_2] = tmp;
			}
			//
			for (unsigned int i=0; i<p->second.size(); i++)
			{
				if (flag[i])
				{
					cout<<p->second[i]<<"\n";
				}
			}
		}

	}
	cerr<<"total:\t\t"<<sum<<"\t"<<picked<<endl;
	int tt = time(NULL)-initial_time;
	cerr<<"time used:\t"<<tt<<"sec"<<endl;
	return 0;
}
