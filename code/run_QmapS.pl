#!/usr/bin/perl -w
#*Filename: run_QmapS.pl
#*Description: align query sequences with sbjct, convert into standard format
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.19
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
		-q	STRING	query sequence file
		-s	STRING	sbjct sequence file
		-w	split query or sbject file? default is query, switch to sbjct
		-n	INT	split file into number of files, default=1
		-a	INT	number of parallel processors, default=1
		-l	INT	size threshold of pairwise hits, default=100
		-i	FLOAT	identity threshold of pairwise hits, default=0.6
		-o	STRING	output file
		-t	INT	<0: cross_match |1: patternhunter |2:blastn>, pairwise alignment tool, default=0
		-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts, "q=s", "s=s", "w", "n=i", "a=i", "l=i", "i=s", "o=s", "t=i", "h");

die $usage if defined $opts{h};
my $query_file = defined $opts{q}? $opts{q} : "";
my $sbjct_file = defined $opts{s}? $opts{s} : "";
my $out_file = defined $opts{o}? $opts{o} : "";
my $switch = defined $opts{w}? 1: 0;   #1: sbjct; 0: query
my $Nfiles = defined $opts{n}? $opts{n} : 1;
my $Nprocess = defined $opts{a}? $opts{a} : 1;
my $hit_size = defined $opts{l}? $opts{l} : 100;
my $hit_ident = defined $opts{i}? $opts{i} : 0.6;
my $align_tool = defined $opts{t}? $opts{t} : 0;

#make working space
my $dir = "tmp".time();
mkdir($dir) ||die "can not make dir $dir\n";

chdir($dir);

#
if ($switch) {
	`split_fa_bygroup.pl ../$sbjct_file $Nfiles 1`;
}
else {
	`split_fa_bygroup.pl ../$query_file $Nfiles 1`;
}

open (Fout, ">sh") || die "can not write file sh\n";

if (0 == $align_tool) {
	for (my $i=1; $i<=$Nfiles; $i++) {
		if ($switch) {
			print Fout "cross_match.manyreads -masklevel 101 ../$query_file leaf.$i.fa 2>/dev/null |ConvertAlign.pl -s $hit_size -i $hit_ident -t 0 >$i.align &\n";
		}
		else {
			print Fout "cross_match.manyreads -masklevel 101 leaf.$i.fa ../$sbjct_file 2>/dev/null |ConvertAlign.pl -s $hit_size -i $hit_ident -t 0 >$i.align &\n";
		}
	}
}
elsif (2 == $align_tool) {
	if ($switch) {
		`seqSize.pl ../$query_file q.size`;
		for (my $i=1; $i<=$Nfiles; $i++) {
			`formatdb -i leaf.$i.fa -p F`;
			print Fout "seqSize.pl leaf.$i.fa $i.size\n";
			print Fout "blastall -p blastn -d leaf.$i.fa -i ../$query_file -e 1e-1 -F F -m8 |ConvertAlign.pl -s $hit_size -i $hit_ident -t 2 q.size $i.size >$i.align &\n";
		}
	}
	else {
		`seqSize.pl ../$sbjct_file s.size`;
		`formatdb -i ../$sbjct_file -p F`;
		for (my $i=1; $i<=$Nfiles; $i++) {
			print Fout "seqSize.pl leaf.$i.fa $i.size\n";
			print Fout "blastall -p blastn -i leaf.$i.fa -d ../$sbjct_file -e 1e-1 -F F -m8 |ConvertAlign.pl -s $hit_size -i $hit_ident -t 2 $i.size s.size >$i.align &\n";
		}
	}
}

close Fout;
`cat sh|multi_run2.sh -n $Nprocess -w 2\n`;
for (my $i=1; $i<=$Nfiles; $i++) {
	`cat $i.align >>../$out_file`;
}
chdir("../");
`rm -rf $dir`;
