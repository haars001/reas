#!/usr/bin/perl -w
#*Filename: run_SegLink.pl
#*Description: fine pairwise alignment for high-depth segments, remove LCS overlap, output linkage information between segments
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.11
#

use strict;
use Getopt::Long;
use Cwd qw(cwd);

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
		-r	STRING	sequence file, default=seg.fa
		-n	INT	split the dataset into number of files, default=1
		-a	INT	number of parallel processors, default=1
		-s	INT	size threshold of pairwise hits, default=100
		-i	FLOAT	identity threshold of pairwise hits, default=0.6
		-j	INT	threshold of none-LCS overlap, default = 50
		-t	INT	<0: cross_match |1: patternhunter |2:blastn>, pairwise alignment tool, default=0
		-o	STRING	output sequence linkage file, default=seg.link
		-f	text format output, default is binary output
		-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts, "r=s", "n=i", "a=i", "s=i", "i=s", "j=i", "t=i", "o=s", "f", "h");

die $usage if defined $opts{h};
my $read_file = defined $opts{r}? $opts{r} : "seg.fa";
my $link_file = defined $opts{o}? $opts{o} : "seg.link";
my $binary = defined $opts{f}? 0 : 1;
my $Nfiles = defined $opts{n}? $opts{n} : 1;
my $Nprocess = defined $opts{a}? $opts{a} : 1;
my $hit_size = defined $opts{s}? $opts{s} : 100;
my $hit_ident = defined $opts{i}? $opts{i} : 0.6;
my $nonLCS = defined $opts{j}? $opts{j} : 50;
my $align_tool = defined $opts{t}? $opts{t} : 0;

#make working space
my $dir = "ReAS".time();
mkdir($dir) ||die "can not make dir $dir\n";
unless(-e $dir and -d $dir or mkdir $dir) {
        die "$! cannot make dir $dir for $0 in ".cwd."\n";
}
chdir($dir);

#
`split_fa_bygroup.pl ../$read_file $Nfiles 1`;

open (Fout, ">sh") || die "can not write file sh\n";
for (my $i=1; $i<=$Nfiles; $i++) {
	print Fout "dust leaf.$i.fa 50 >$i.mask &\n";
}
`cat sh|multi_run2.sh -n $Nprocess -w 2\n`;
close Fout;
`cat *.mask >seg.mask.fa`;

my $N_out_file = $Nprocess * 10;
my $add = 0;
#
open (Fout, ">sh") || die "can not write file sh\n";
for (my $i=1; $i<=$Nfiles; $i++) {
	for (my $j=$i; $j<=$Nfiles; $j++) {
		if (0 == $align_tool) {
			my $curr = $add % $N_out_file;
			$add++;
			if ($binary) {
				print Fout "cross_match.manyreads leaf.$j.fa leaf.$i.fa -masklevel 101 -minmatch 14 -minscore 30 2>/dev/null |ConvertAlign.pl -s $hit_size -i $hit_ident -t $align_tool |rmLCSlink.pl -m seg.mask.fa -j $nonLCS |align_to_binary >>$curr.link &\n";
			}
			else {
				print Fout "cross_match.manyreads leaf.$j.fa leaf.$i.fa -masklevel 101 -minmatch 14 -minscore 30 2>/dev/null |ConvertAlign.pl -s $hit_size -i $hit_ident -t $align_tool |rmLCSlink.pl -m seg.mask.fa -j $nonLCS >>$curr.link &\n";
			}
		}
	}
}
close Fout;
`cat sh|multi_run2.sh -n $Nprocess -w 2`;
`rm -f ../seg.link` if (-e "../$link_file");
for (my $i=0; $i<$N_out_file && $i <$add; $i++) {
	`cat $i.link >>../$link_file`;
}
chdir("../");
`rm -rf $dir`;
