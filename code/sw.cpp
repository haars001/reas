/*****************************************************************************
 * #   Copyright (C) 1993-1998 by Phil Green.                          
 * #   All rights reserved.                           
 * #                                                                           
 * #   This software is part of a beta-test version of the swat/cross_match/phrap 
 * #   package.  It should not be redistributed or
 * #   used for any commercial purpose, including commercially funded
 * #   sequencing, without written permission from the author and the
 * #   University of Washington.
 * #   
 * #   This software is provided ``AS IS'' and any express or implied
 * #   warranties, including, but not limited to, the implied warranties of
 * #   merchantability and fitness for a particular purpose, are disclaimed.
 * #   In no event shall the authors or the University of Washington be
 * #   liable for any direct, indirect, incidental, special, exemplary, or
 * #   consequential damages (including, but not limited to, procurement of
 * #   substitute goods or services; loss of use, data, or profits; or
 * #   business interruption) however caused and on any theory of liability,
 * #   whether in contract, strict liability, or tort (including negligence
 * #   or otherwise) arising in any way out of the use of this software, even
 * #   if advised of the possibility of such damage.
 * #                                                                         
 * #*****************************************************************************/

//Revised by LIN Hao(linhao@ict.ac.cn) for ReAS. 2006-07-19

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "sw.h"

#define FINDALIGN

#define PENALTY -2
#define GAP_INIT -10
#define GAP_EXT -2
#define END_GAP -1

#define INIT_FLAG 0x0001
#define EXT_FLAG 0x0002
#define DEL_FLAG 0x0004
#define INS_FLAG 0x0008
#define END_FLAG 0x0010
#define PEN_FLAG 0x0020

#define  NALLOC  63000


void sw::set_mats()
{
  int i;
  for (i = 0; i < 256; i++) alph_mat[i] = isalpha(i) ? toupper(i) : 0;
  if (parameters->DNA_flag) {
    alph_mat['-'] = 'N';
    for (i = 0; i < 256; i++) comp_mat[i] = i;
    comp_mat['A'] = 'T';
    comp_mat['a'] = 't';
    comp_mat['C'] = 'G';
    comp_mat['c'] = 'g';
    comp_mat['G'] = 'C';
    comp_mat['g'] = 'c';
    comp_mat['T'] = 'A';
    comp_mat['t'] = 'a';
    comp_mat['y'] = 'r';
    comp_mat['Y'] = 'R';
    comp_mat['r'] = 'y';
    comp_mat['R'] = 'Y';
    comp_mat['M'] = 'K';
    comp_mat['m'] = 'k';
    comp_mat['K'] = 'M';
    comp_mat['k'] = 'm';
    comp_mat['H'] = 'D';
    comp_mat['D'] = 'H';
    comp_mat['B'] = 'V';
    comp_mat['V'] = 'B';
    comp_mat['h'] = 'd';
    comp_mat['d'] = 'h';
    comp_mat['b'] = 'v';
    comp_mat['v'] = 'b';
  }
}

void sw::set_params(char * score_matrix, int min_score)
{
	unsigned all_flag = 0;
	int gap_flag, c;
	int i;
	
	parameters = (Parameters *)malloc(sizeof(Parameters));
//	parameters = (Parameters *)our_alloc(sizeof(Parameters));
	parameters->matrix = 0; parameters->nw_flag = 0; parameters->penalty = PENALTY;
	parameters->default_qual = 15;
	parameters->minscore = min_score;

	/* set up scoring parameters */
	if (parameters->matrix == 0 && !(all_flag & PEN_FLAG)) {
//		parameters->matrix = (char*)malloc(strlen(DEFAULT_MATRIX) + 1);
		parameters->matrix = (char*)our_alloc(strlen(score_matrix) + 1);
		strcpy(parameters->matrix, score_matrix);
	}
	gap_flag = set_score_mat(parameters->matrix, parameters->penalty);
	if (!(all_flag & INIT_FLAG)) parameters->gap_init = gap_flag ? get_gap_init() : GAP_INIT;
	if (!(all_flag & EXT_FLAG)) parameters->gap_ext = gap_flag ? get_gap_ext() : GAP_EXT;
	if (!(all_flag & INS_FLAG)) parameters->ins_gap_ext = parameters->gap_ext;
	if (!(all_flag & DEL_FLAG)) parameters->del_gap_ext = parameters->gap_ext;
	set_gap_penalties(parameters->gap_init, parameters->gap_ext, parameters->ins_gap_ext,
			parameters->del_gap_ext, parameters->end_gap);

	parameters->DNA_flag = (get_alphsize() < 20);
	set_mats();
//	set_table_mats();
}

/* N.B. following needs to be tested for non-unix systems */
void sw::fatalError(char *message)
{
  fprintf(stderr, "\nFATAL ERROR: %s\n", message);
  exit(1);
}

void sw::notify(char *message)
{
  fprintf(stderr, message);
  if (!strcmp(message, " Done\n")) check_alloc_list();
  fflush(stderr);
  fflush(stdout);
}

FILE * sw::fopenRead(char *fileName)
{
  FILE *fp;
//  FILE *fopen();

  if(!(fp = fopen(fileName,"r"))) {
    fprintf(stderr, "\n ERROR: FILE %s NOT FOUND \n",fileName);
    exit(1);
  }
  else return fp;
}

FILE * sw::fopenWrite(char *fileName)
{
  FILE *fp;
//  FILE *fopen();

  if(!(fp = fopen(fileName,"w"))) {
    fprintf(stderr, "\n ERROR: FILE %s NOT AVAILABLE FOR WRITING \n",fileName);
    exit(1);
  }
  else return fp;
}

FILE * sw::fopenAppend(char *fileName)
{
  FILE *fp;
//  FILE *fopen();

  if(!(fp = fopen(fileName,"a"))) {
    fprintf(stderr, "\n ERROR: FILE %s NOT AVAILABLE FOR WRITING \n",fileName);
    exit(1);
  }
  else return fp;
}

char * sw::our_alloc(unsigned int nbytes)
{
	HEADER *p;
	HEADER *q;
	ALLOC nunits;
 
	nunits = 1 +(nbytes + sizeof(HEADER) - 1)/ sizeof(HEADER);
	if (!(q = allocp)) {
		base.s.ptr = allocp = q = &base;
		base.s.size = 0;
	}
	for (p = q->s.ptr;;q = p, p = p->s.ptr) {
 		if (p->s.size >= nunits){
			if(p->s.size == nunits) q->s.ptr = p->s.ptr;
			else {
				p->s.size -= nunits;
				p += p->s.size;
				p->s.size = nunits;
			}
			allocp = q;
			return ((char *)(p + 1));
		}
		if (p == allocp)
			if (!(p = morecore(nunits))) return 0;
	}
}

void sw::check_alloc_list()
{
	long int num_blocks, num_ends, num_units;
	HEADER *p, *q;

	p = allocp;
	num_blocks = num_ends = num_units = 0;
	do {
	  q = p->s.ptr;
	  if (p->s.size < 0)
	    fatalError(" our_alloc linked list corruption (check_alloc_list1)");
	  num_units += p->s.size;
	  if (q > p && p + p->s.size > q)
	    fatalError(" our_alloc linked list corruption (check_alloc_list2)");
	  if (q < p) num_ends++;
	  p = q;
	  num_blocks++;
	} while (p != allocp);
	if (num_ends != 1)
	    fatalError(" our_alloc linked list corruption (check_alloc_list3)");
	/* fprintf(stderr, "Total space allocated: %.3f Mbytes; currently free: %.3f Mbytes in %d blocks\n",
		total_allocated / 1000000.0, num_units * sizeof(HEADER) / 1000000.0, num_blocks); */
}

void sw::find_frags()
{
	SHORT num_frags;
	HEADER *p;

	for (p = allocp->s.ptr, num_frags = 1; p != allocp;
            p = p->s.ptr, num_frags++);
        fprintf(stderr, "\n number of allocation fragments = %d", num_frags);
}


HEADER * sw::morecore(unsigned int nu)
{
//	char *malloc();
	char *cp;
	HEADER *up;
	ALLOC rnu;
   
	rnu = NALLOC * ((nu + NALLOC - 1) / NALLOC);
	cp = (char*) malloc(rnu * sizeof(HEADER));
	if (!cp) {
	  fprintf(stderr, "\n\n%.3f Mbytes requested but unavailable", 
		  rnu * sizeof(HEADER) / 1000000.0);
	  check_alloc_list();
	  fatalError("REQUESTED MEMORY UNAVAILABLE");
	}
	total_allocated += rnu * sizeof(HEADER);
	/* fprintf(stderr, "%.3f Mbytes allocated -- total %.3f Mbytes\n", 
		rnu * sizeof(HEADER) / 1000000.0, total_allocated / 1000000.0); */
	up = (HEADER *)cp;
	up->s.size = rnu;
	our_free((char *)(up + 1));
	return allocp;
}

SHORT sw::our_free(char *ap)
{
	HEADER *p;
	HEADER *q;

	if (!ap) return 0; 

	p = (HEADER *)ap - 1;
	for (q = allocp; !(p > q && p < q->s.ptr); q = q->s.ptr)
		if (q>= q->s.ptr && (p > q || p < q->s.ptr)) break;

	if ( p + p->s.size == q->s.ptr){
		p->s.size += q->s.ptr->s.size;
		p->s.ptr = q->s.ptr->s.ptr;
	} 
	else if (p + p->s.size > q->s.ptr && p <= q->s.ptr) {
	  fatalError(" our_alloc linked list corruption (our_free1)");
	}
	else p->s.ptr = q->s.ptr;
	if (q + q->s.size == p){
		q->s.size += p->s.size;
		q->s.ptr = p->s.ptr;
	} 
	else if (q + q->s.size > p && p >= q) {
	  fatalError(" our_alloc linked list corruption (our_free2)");
	}
	else q->s.ptr = p;
	allocp = q;
        return 0;
}



/* Following routine reads in score matrix, and sets gap penalties; if file_name is 0 or the empty
 string, it is assumed that the matrix is for nucleotides (upper or lower
 case) and mismatch penalties are set to mismatch, except for N and X (which
 have penalties equal to 0 and -1 respectively). mismatch is ignored when
 file_name is not empty */

int sw::get_alphsize()
{
  return alphsize;
}

int sw::get_gap_init()
{
  return gap_init;
}

int sw::get_gap_ext()
{
  return gap_ext;
}

int sw::get_ins_gap_ext()
{
  return ins_gap_ext;
}

int sw::get_del_gap_ext()
{
  return del_gap_ext;
}

void sw::set_gap_penalties(int gap_init0, int gap_ext0, int ins_gap_ext0, 
			int del_gap_ext0, int end_gap0)
{
  gap_init = gap_init0;
  gap_ext = gap_ext0;
  ins_gap_ext = ins_gap_ext0;
  del_gap_ext = del_gap_ext0;
  end_gap = end_gap0;
}


int sw::set_score_mat(char *file_name, int mismatch)
{
  FILE *fp;
  char buffer[501];
  int i, j, k, n_lines, alph_flag, file_flag, gap_flag;
  char letter;
  float frequency;
  double total;
  double pre_freqs[256];

  file_flag = file_name && file_name[0];
  if (score_mat) {
//    for (i = 0; i < n_rows; i++) free(score_mat[i]);
    for (i = 0; i < n_rows; i++) our_free((char*)score_mat[i]);
    our_free((char*)score_mat);
  }
  gap_flag = 0;
  if (row_labels) our_free(row_labels);
  if (file_flag) {
    freqs = 0;
    freq_flag = 0;
    for (i = 0; i < 256; i++) pre_freqs[i] = 0;
    fp = fopenRead(file_name);
    n_rows = alph_flag = n_lines = 0;
    while (fgets(buffer, 500, fp)) {
      if (!alph_flag) n_lines++;
      for (i = 0; buffer[i] && isspace(buffer[i]); i++);
      if (!buffer[i] || buffer[i] == '#') continue;
      if (!strncmp(buffer + i, "GAP", 3)) {
		gap_flag = 1;
		if (2 != sscanf(buffer + i + 3, "%d %d", &gap_init, &gap_ext))
		  fatalError("Score matrix format: GAP line incorrect");
      }
      else if (!strncmp(buffer + i, "FREQS", 5)) {
		freq_flag = 1;
		k = i + 5; 
		do {
		  while (buffer[k] && isspace(buffer[k])) k++;
		  if (!buffer[k]) break;
		  if (1 != sscanf(buffer + k, "%c ", &letter))
		    fatalError("Incorrect score matrix format -- FREQS line");
		  for (k++; buffer[k] && isspace(buffer[k]); k++);
		  if (1 != sscanf(buffer + k, "%f", &frequency))
		    fatalError("Incorrect score matrix format -- FREQS line");
		  while (buffer[k] && !isspace(buffer[k])) k++;
		  pre_freqs[letter] = frequency;
		} while (buffer[k]);
      }
      else if (!alph_flag) {
	for (alphsize = 0; buffer[i]; i++) 
	  if (!isspace(buffer[i])) {
	    if ((isalpha(buffer[i]) || buffer[i] == '*') && alphsize < 256)
	      alphabet[alphsize++] = buffer[i]; /* toupper(buffer[i]); */
	    else fatalError("Incorrect score matrix format-- reading column labels");
	  }
	alph_flag = 1;
      }
      else n_rows++;
    }
    rewind(fp);
    for (i = 0; i < n_lines; i++) fgets(buffer, 500, fp);
  }
  else {
    alphsize = n_rows = 6;
    strcpy(alphabet,"ACGTNX");
    gap_flag = 1;
    gap_init = mismatch - 2;
    gap_ext = mismatch - 1;
  }

  for (i = 1; i < alphsize; i++)
    for (j = 0; j < i; j++)
      if (alphabet[i] == alphabet[j])
	fatalError("Incorrect score matrix format -- duplicate column labels");

  for (i = 0; i < 256; i++) col_convert[i] = alphsize - 1;
  for (i = 0; i < alphsize; i++) col_convert[alphabet[i]] = i;
 
  if (!freqs) {
    freqs = (double *)our_alloc(256 * sizeof(double));
    log_freqs = (double *)our_alloc(256 * sizeof(double));
    for (i = 0; i < 256; i++) freqs[i] = 0;

    if (freq_flag) {
      for (i = 0; i < 256; i++) {
	freqs[col_convert[i]] = pre_freqs[i];
      }
    }
    else if (alphsize < 20) {
      freqs[col_convert['A']] = .25;
      freqs[col_convert['C']] = .25;
      freqs[col_convert['G']] = .25;
      freqs[col_convert['T']] = .25;
    }
    else {
      freqs[col_convert['A']] = .0756;
      freqs[col_convert['C']] = .0171;
      freqs[col_convert['D']] = .0531;
      freqs[col_convert['E']] = .0633;
      freqs[col_convert['F']] = .0408;
      freqs[col_convert['G']] = .0688;
      freqs[col_convert['H']] = .0224;
      freqs[col_convert['I']] = .0574;
      freqs[col_convert['K']] = .0596;
      freqs[col_convert['L']] = .0934;
      freqs[col_convert['M']] = .0214;
      freqs[col_convert['N']] = .0456;
      freqs[col_convert['P']] = .0493;
      freqs[col_convert['Q']] = .0404;
      freqs[col_convert['R']] = .0517;
      freqs[col_convert['S']] = .0721;
      freqs[col_convert['T']] = .0578;
      freqs[col_convert['V']] = .0654;
      freqs[col_convert['W']] = .0127;
      freqs[col_convert['Y']] = .0322;
    }
	total = 0;
    for (i = 0; i < 256; i++) total += freqs[i];
    if (total) {
      for (i = 0; i < 256; i++) {
	freqs[i] /= total;
      }
    }
    
    for (i = 0; i < 256; i++) log_freqs[i] = freqs[i] ? log(freqs[i]) : 0.0;
  }


/* N.B. NEED CODE FOR PROTEINS; AND FOR PROFILES !!!  */

  score_mat = (int **)our_alloc(n_rows * sizeof(int *));
  row_labels = (char *)our_alloc(n_rows * sizeof(char));
  for (i = 0; i < n_rows; i++) {
    row_labels[i] = alphabet[i]; /* default */
    score_mat[i] = (int *)our_alloc(alphsize * sizeof(int));
    if (file_flag) {
	  do {
	  	fgets(buffer, 500, fp);
		  for (k = 0; buffer[k] && isspace(buffer[k]); k++);
			} while (!buffer[k] || buffer[k] == '#');
		if (isalpha(buffer[k]) || buffer[k] == '*') {
			row_labels[i] = buffer[k];
			k++;
	        }
		else if (i >= alphsize)
		fatalError("Incorrect score matrix format -- too many rows");
		for (j = 0; j < alphsize; j++) {
			while (buffer[k] && isspace(buffer[k])) k++;
				if (!buffer[k] || 1 != sscanf(buffer + k,"%d",&score_mat[i][j]))
					  fatalError("Incorrect score matrix format -- while reading rows");
					  while (buffer[k] && !isspace(buffer[k])) k++;
				      }
		}
	else {
/*      printf("\n"); */
      for (j = 0; j < alphsize; j++) {
	if (toupper(alphabet[j]) == 'N' || toupper(alphabet[i]) == 'N')
	  score_mat[i][j] = 0;
	else if (alphabet[j] == 'X' || alphabet[i] == 'X')
	  score_mat[i][j] = mismatch - 1; /* was 1 -- is this right? */
	else if (toupper(alphabet[i]) == toupper(alphabet[j]))
	  score_mat[i][j] = 1;
/*	else if (islower(alphabet[i]) || islower(alphabet[j]))
	  score_mat[i][j] = gap_ext / 2; */
	else score_mat[i][j] = mismatch;
/*	printf("%3d ",score_mat[i][j]); */
      }
    }
  }
  if (file_flag) fclose(fp); 

  profile_flag = 0;
  for (i = 1; i < n_rows && !profile_flag; i++)
    for (j = 0; j < i && !profile_flag; j++)
      if (row_labels[i] == row_labels[j]) profile_flag = 1;

  if (!profile_flag) {
    for (i = 0; i < 256; i++) row_convert[i] = n_rows - 1;
    for (i = 0; i < n_rows; i++) row_convert[row_labels[i]] = i;
  }   
  n_places = alphsize + 1 + 10;

  get_lambda();

/*  printf("\n%s\n",alphabet); */
  return gap_flag;
}

void sw::print_score_mat()
{
  int i, j;

  printf("\n ");
  for (i = 0; i < alphsize; i++) printf("   %c",  alphabet[i]);
  for (i = 0; i < n_rows; i++) {
    printf("\n%c",  row_labels[i]);
    for (j = 0; j < alphsize; j++) {
      printf(" %3d", score_mat[i][j]);
    }
  } 
}

void sw::print_background_freqs()
{
  int i;

  printf(" Assumed background frequencies:\n ");
  for (i = 0; i < alphsize; i++) printf("%c: %.3f  ",  alphabet[i], freqs[i]);
  return;
}

Profile * sw::make_profile_from_seq(char *seq, int nw_flag)
{
  Profile *profile;
  int i, j, k, length;
  char *strchr();
  int offset, max_score_cutoff, packed_length;

  for (length = 0; seq[length]; length++);
  if (profile_flag) {
    if (length != n_rows)
	fatalError("Query sequence does not match profile matrix row labels");
    for (i = 0; i < n_rows; i++)
      if (seq[i] != row_labels[i])
	fatalError("Query sequence does not match profile matrix row labels");
  }


  profile = (Profile *)our_alloc(sizeof(Profile));
  profile->length = length;
  profile->seq = (char *)our_alloc((length + 1) * sizeof(char));
  strcpy(profile->seq, seq);
  profile->maxstu_vec = (int *)our_alloc((length + 1) * sizeof(int));
  profile->t_vec = (int *)our_alloc((length + 1) * sizeof(int));
  for (j = 0; j < length; j++) profile->maxstu_vec[j] = profile->t_vec[j] = 0; 
  profile->maxstu_vec[length] = -1; /* sentinel for end of vector in smith-waterman
				  algorithm -- ; can never occur
				  with actual scores */
  profile->t_vec[length] = 0; /* sentinel (in comb w/ maxstu_vec[j] in n-w algorthm,
			    since never should exceed maxstu_vec with actual
			    scores */
  profile->convert = (int *)our_alloc(256 * sizeof(int));
  for (i = 0; i < 256; i++) profile->convert[i] = col_convert[i];
/*
  for (i = 1, j = 0; i < 256; i++) N.B. strchr(alphabet,0) is non-null !
    profile->convert[i] = strchr(alphabet, i) ?
      strchr(alphabet, i) - alphabet : alphsize - 1;
*/
  /* previously used toupper(i) instead of i */
  profile->scores = (int **)our_alloc(alphsize * sizeof(int *));
/*  profile->score_pos = (int **)our_alloc(alphsize * sizeof(int *)); */
  profile->max_entry = profile->min_entry = 0;
//printf("alphsize: %d\t length: %d\n", alphsize, length);
  for (i = 0; i < alphsize; i++) {
    profile->scores[i] = (int *)our_alloc((1 + length) * sizeof(int));
/*    profile->score_pos[i] = (int *)our_alloc((1 + length) * sizeof(int)); */
    for (j = k = 0; j < length; j++) {
      profile->scores[i][j] = 
	profile_flag ? score_mat[j][i] : score_mat[row_convert[seq[j]]][i];
/*
		printf("hah%d\t\n", profile->scores[i][j]);
		fflush(stdout);
*/
      if (nw_flag) profile->scores[i][j] -= gap_ext + gap_ext;
/* last term is adjustment for needleman-wunsch algorithm, to allow assumption
   that gap_ext = 0 */
/* N.B. ADJUSTMENTS NEEDED FOR INS_GAP_EXT, DEL_GAP_EXT */
      if (profile->scores[i][j] > profile->max_entry)
	profile->max_entry = profile->scores[i][j];
      if (profile->scores[i][j] < profile->min_entry)
	profile->min_entry = profile->scores[i][j];
/*      if (profile->scores[i][j] > 0) profile->score_pos[i][k++] = j; */
    }
    profile->scores[i][length] = 0;
/*    profile->score_pos[i][k] = length; */
  }
//XXX
/*
printf("print profile->scores, alphsize: %d\n", alphsize);
for (i = 0; i < alphsize; i++) {
printf("i: %d\n", i);
	for (j = 0; j < length; j++) {
		printf("%d\t", profile->scores[i][j]);
	}
	printf("\n");
}
*/
  profile->lambda = lambda;
  profile->log_freqs = log_freqs;
  profile->gap_init = gap_init;
  profile->gap_ext = gap_ext;
  profile->ins_gap_ext = ins_gap_ext;
  profile->del_gap_ext = del_gap_ext;
  /* temporary settings */
  profile->qbegin_gap_init = end_gap;
  profile->qbegin_gap_ext = end_gap;
  profile->qend_gap_init = end_gap;
  profile->qend_gap_ext = end_gap;

  profile->sbegin_gap_init = end_gap;
  profile->sbegin_gap_ext = end_gap;
  profile->send_gap_init = end_gap;
  profile->send_gap_ext = end_gap;
/*  profile->score_mat = score_mat; */
 
  profile->packed_length = packed_length = (length + 8) / 8; /* +8, rather than +7, to allow
								for shift of last byte to next
								iteration (in fast_smith_wat) */
  offset = 1;
  if (profile->min_entry < 0 && offset < -profile->min_entry) offset = -profile->min_entry;
  if (profile->gap_init < 0 && offset < -profile->gap_init) offset = -profile->gap_init;
  if (profile->gap_ext < 0 && offset < -8 * profile->gap_ext) offset = -8 * profile->gap_ext;
  profile->poly_offset = profile->poly_one = 0;
  profile->max_score_cutoff = 0;
  profile->poly_gap_init = 0;
  profile->poly_gap_ext = 0;
  max_score_cutoff = 127 - profile->max_entry;
/* N.B. COULD BE 255 - profile->max_entry -- IF WEREN'T USING 1 BIT FOR A POSITIVE T
   INDICATOR */
  for (i = 0; i < 8; i++) {
    profile->poly_offset <<= 8;
    profile->poly_offset += offset;
    profile->poly_one <<= 8;
    profile->poly_one += 1;
    profile->max_score_cutoff <<= 8;
    profile->max_score_cutoff += max_score_cutoff;
    profile->poly_gap_init <<= 8;
    profile->poly_gap_init += -gap_init;
    profile->poly_gap_ext <<= 8;
    profile->poly_gap_ext += -gap_ext;
  }
/* N.B. ASSUMING gap_init, gap_ext are NON-POSITIVE */
  profile->packed_maxstu_vec = (unsigned long int *)our_alloc((2 * packed_length + 2) * sizeof(unsigned long int));
  profile->packed_scores = (unsigned long int **)our_alloc(alphsize * sizeof(unsigned long int *));
  for (i = 0; i < alphsize; i++) {
    profile->packed_scores[i] = (unsigned long int *)our_alloc((1 + packed_length) * sizeof(unsigned long int));

    for (j = 0; j < packed_length * 8; j++) {
      profile->packed_scores[i][j / 8] <<= 8;
      profile->packed_scores[i][j / 8] += offset + (j < length ? profile->scores[i][j] : 0);
    }
    profile->packed_scores[i][packed_length] = 0;
  }
/*
  fprintf(stderr, "\n%d %d %d %d %d\n", gap_init, gap_ext, offset, packed_length, max_score_cutoff);
  fprintf(stderr, "\n%ld %ld %ld %d %ld\n", profile->poly_gap_init & 255, profile->poly_gap_ext & 255, profile->poly_offset & 255, profile->packed_length & 255, profile->max_score_cutoff & 255);
*/
  return profile;
}

void sw::find_segs(Profile *profile, int i_alph, int j_start, int j_end, 
		int minscore, char *seq, char *id, int screen)
{
  int j;
  int max, cum, min, j_min, j_max, j_min_best;

  max = min = cum = 0;
  j_min = j_start - 1;
  for (j = j_start; j <= j_end; j++) {
    cum += profile->scores[i_alph][j];
    if (cum < min) {
      min = cum;
      j_min = j;
    }
    else if (cum - min > max) {
      max = cum - min;
      j_min_best = j_min + 1;
      j_max = j;
    }
  }
  if (max >= minscore) {
    printf("\n%s   %c   Score: %4d   Residues: %5d - %5d", 
	   id, alphabet[i_alph], max, j_min_best + 1, j_max + 1);
    if (screen) {
      for (j = j_min_best; j <= j_max; j++) seq[j] = 'X';
    }
    find_segs(profile, i_alph, j_start, j_min_best, minscore, seq, id, screen);
    find_segs(profile, i_alph, j_max + 1, j_end, minscore, seq, id, screen);
  }
}
/*
void free_profile(Profile *profile)
{
  int i;
  
  if (NULL == profile) {
	return;
  }
  for (i = 0; i < alphsize; i++) {
	if (!profile->scores[i]) {
	   free(profile->scores[i]);
	}
	if (!profile->packed_scores[i]) {
	   free(profile->packed_scores[i]);
	}
  }
  if (!profile->scores) {
	  free(profile->scores); 
  }
  if (!profile->convert) {
  free(profile->convert); 
  }
  if (!profile->t_vec) {
  free(profile->t_vec); 
 }
  if (!profile->maxstu_vec) {
  free(profile->maxstu_vec); 
  }
  if (!profile->seq) {
  free(profile->seq); 
  }
  if (!profile->packed_maxstu_vec) {
  free(profile->packed_maxstu_vec);
  }
  if (!profile->packed_scores) {
  free(profile->packed_scores);
  }
  if (!profile) {
  free(profile); 
  }
}
*/
void sw::free_profile(Profile *profile)
{
  int i;
  
  if (NULL == profile) {
	return;
  }
  for (i = 0; i < alphsize; i++) {
	if (profile->scores[i]) {
    our_free((char*)profile->scores[i]);
//	   free(profile->scores[i]);
	}
	if (profile->packed_scores[i]) {
    our_free((char*)profile->packed_scores[i]);
//	   free(profile->packed_scores[i]);
	}
  }
  if (profile->scores) {
  our_free((char*)profile->scores);
//	  free(profile->scores); 
  }
  if (profile->convert) {
  our_free((char*)profile->convert);
//  free(profile->convert); 
  }
  if (profile->t_vec) {
  our_free((char*)profile->t_vec);
//  free(profile->t_vec); 
  }
  if (profile->maxstu_vec) {
  our_free((char*)profile->maxstu_vec);
//  free(profile->maxstu_vec); 
  }
  if (profile->seq) {
  our_free((char*)profile->seq);
//  free(profile->seq); 
  }
  if (profile->packed_maxstu_vec) {
  our_free((char*)profile->packed_maxstu_vec);
//  free(profile->packed_maxstu_vec);
  }
  if (profile->packed_scores) {
  our_free((char*)profile->packed_scores);
//  free(profile->packed_scores);
  }
  if (profile) {
  our_free((char*)profile);
//  free(profile); 
  }
}

/*
void free_profile(Profile *profile)
{
  int i;

  for (i = 0; i < alphsize; i++) {
    our_free((char*)profile->scores[i]);
    our_free((char*)profile->packed_scores[i]);
  }
  our_free((char*)profile->scores);
  our_free((char*)profile->convert);
  our_free((char*)profile->t_vec);
  our_free((char*)profile->maxstu_vec);
  our_free((char*)profile->seq);
  our_free((char*)profile->packed_maxstu_vec);
  our_free((char*)profile->packed_scores);
  our_free((char*)profile);
  return;
}
*/
void sw::get_lambda()
{
  double sum, lambda_lower, lambda_upper;

  lambda_lower = 0.0;
  lambda = 0.5;
  for (;;) {
    sum = get_sum();
    if (sum >= 1.0) break;
    lambda_lower = lambda;
    lambda *= 2.0; 
  } 
  lambda_upper = lambda;
  while (lambda_upper - lambda_lower > .00001) {
    lambda = (lambda_lower + lambda_upper) / 2.0;
    sum = get_sum();
    if (sum >= 1.0) lambda_upper = lambda;
    else lambda_lower = lambda;
  } 
/*
  fprintf(stderr,"\n lambda: %f  sum: %f", lambda, sum);
*/
}

double sw::get_sum()
{
  int i, j;
  double total;
  double check;

  check = 0;
  total = 0;
  for (i = 0; i < alphsize; i++)
    for (j = 0; j < alphsize; j++)
      if (freqs[i] && freqs[j]) {
	total += freqs[i] * freqs[j] * exp(lambda * score_mat[i][j]);
	check += freqs[i] * freqs[j];
      }
  if (check > 1.001 || check < .999) fatalError("frequency sums");
  return total;
}


/* should recheck complexity adjustment procedure: it is probably not optimal! */
void sw::set_complexity_adjust(double factor)
{
  complexity_adjust = 1;
  complexity_factor = factor;
}

void sw::unset_complexity_adjust()
{
  complexity_adjust = 0;
}

void sw::warnings_on()
{
  warnings = 1;
}

void sw::warnings_off()
{
  warnings = 0;
}

void sw::init_alignment_globals(Profile *q_p, char *db_e_seq, int db_e_len, int q_l, 
			int s_l, int b_w, int r_e, int l_e)
{
  subject_end = query_end = subject_start = query_start = 0;
  mismatches = insertions = deletions = 0;
  
  q_profile = q_p;
  db_entry_seq = db_e_seq;
  db_entry_length = db_e_len;
  band_width = b_w;
  r_edge_orig = r_e;
  l_edge_orig = l_e;
  q_left = q_l;
  s_left = s_l;
  gap_init = q_profile->gap_init;
  ins_gap_ext = q_profile->ins_gap_ext;
  del_gap_ext = q_profile->del_gap_ext;
}

void sw::alloc_strings()
{
  int i;
  int t_len;
  
  t_len = db_entry_length + q_profile->length;
  if (t_len > string_len) {
    if (string_len) { 
      /* only free output strings when need a new one -- so
	 are available for use between initial comparison and
	 subsequent one */
      our_free(string1);
      our_free(string2);
      our_free(string3);
    }
    string1 = (char *)our_alloc(t_len * sizeof(char));
    string2 = (char *)our_alloc(t_len * sizeof(char));
    string3 = (char *)our_alloc(t_len * sizeof(char));
    string_len = t_len;
  }
  string_end = -1;

  if (complexity_adjust)
    for (i = 0; i < 256; i++) complexity_counts[i] = 0;
}

void sw::alignment_from_direc(char **direc, char **mdir, char *mdiri, int *add_max_score)
{
  char edge_flag, d;
  int i, j, k;
  int s, score_check, cum_neg, min_cum_neg;
  char u_ext, t_ext; /* flags for whether current u and t are
			gap extending */
  int pos_score;
  
  edge_flag = 0;
  
  alloc_strings();
  
  u_ext = t_ext = 0;
  score_check = pos_score = cum_neg = min_cum_neg = 0;
  subject_end = (mdir - direc) + 1;
  query_end = (mdiri - *mdir) + 1;
  
  for (i = mdir - direc, j =  mdiri - *mdir, k = 0;
       i >= s_left && j >= q_left && (d = direc[i][j]); k++) {
    
    if (band_width && (j - i < l_edge_orig + 2 || j - i > r_edge_orig - 2))
      edge_flag = 1;
    
    string2[k] = string1[k] = '-';
    
    if (t_ext) {
      d = 1 + (d & 12);
    }
    if (u_ext) {
      d = 2 + (d & 12);
    }
    if (!(d & 2)) t_ext = d & 4;
    if (!(d & 1)) u_ext = d & 8;
    
    if (d & 1) { 
      string2[k] = db_entry_seq[i];
      i--;
    }
    if (d & 2) {
      string1[k] = q_profile->seq[j];
      j--;
    }
    string3[k] = ' ';
    if (string1[k] == '-') {
      s = string1[k - 1] == '-' ? ins_gap_ext : gap_init;
      score_check += s;
      string3[k] = '-';
      insertions++;
    }
    else if (string2[k] == '-') {
      s = string2[k - 1] == '-' ? del_gap_ext : gap_init;
      score_check += s;
      string3[k] = '-';
      deletions++;
    }
    else /* if (string2[k] != '-' && string1[k] != '-') */ {
      s = q_profile->scores[q_profile->convert[string2[k]]][j + 1];
      score_check += s;
      if (complexity_adjust) {
	pos_score += s;
/*
	complexity_counts[q_profile->convert[string1[k]]] += 1;
*/
	complexity_counts[q_profile->convert[string2[k]]] += 1;
      }
      if (s > 0) {
	string3[k] = string2[k] == string1[k] ? string1[k] : '+';
/*
	if (complexity_adjust) {
	  pos_score += s;
	  complexity_counts[q_profile->convert[string2[k]]] += 1;
	}
*/
      }
      else mismatches++; /* previously only was set if s < 0 */
      
    }
    cum_neg += s;
    if (cum_neg > 0) cum_neg = 0;
    else if (min_cum_neg > cum_neg) min_cum_neg = cum_neg;
  }
  if (score_check != *add_max_score) 
    fprintf(stderr, "\nSCORE DISCREPANCY: initial %d, from alignment %d  %d %d",
	    *add_max_score, score_check, q_left, s_left);
  
  i += 2;
  j += 2;
  subject_start = i;
  query_start = j;
//  printf("query_start: %i, subject_start: %i\n", query_start, subject_start);
  string_end = k - 1;
  
  if (complexity_adjust) {
    *add_max_score = complexity_correct(*add_max_score, pos_score);
/*
    fprintf(stderr, "\nmin_cum_neg = %d", min_cum_neg);
*/
  }
/*  if ((band_width || q_left) && edge_flag && warnings)
    fprintf(stderr, "\nWARNING: Possible band edge overflow: score %d",
	    *add_max_score);
*/  score = *add_max_score;
}

int sw::complexity_correct(int orig_score, int pos_score)
{
  int i, adj_score, old_adj_score, t_counts, n_letters;
  double t_factor, t_sum;
  
  t_sum = 0;
  t_factor = 0;
  for (i = t_counts = n_letters = 0; i < 256; i++) {
    if (complexity_counts[i]) {

      if (q_profile->log_freqs[i]) {
	t_factor += complexity_counts[i] * log((double)complexity_counts[i]);
	t_sum += complexity_counts[i] * q_profile->log_freqs[i];
	t_counts += complexity_counts[i];
	n_letters++;
      }
/*
      fprintf(stderr, "\n%d %d %f", i, complexity_counts[i], q_profile->log_freqs[i]);
*/
    }
  }
  
  t_factor -=  t_counts * log((double)t_counts);
  t_sum -= t_factor;

/*  t_factor /=  t_counts * log(complexity_factor); */
  t_factor /=  t_counts * log(n_letters > 1 / complexity_factor ? 1. / n_letters : complexity_factor);

  old_adj_score = int (orig_score + t_factor * pos_score - pos_score + .5);
  adj_score = int (orig_score + t_sum / q_profile->lambda + .999);

  if (adj_score < 0) adj_score = 0;
/*
  fprintf(stderr, "\nORIG, old/new ADJUSTED SCORES: %d, %d / %d  t_factor %f  pos_score %d  n_letters %d", 
	  orig_score, old_adj_score, adj_score, t_factor, pos_score, n_letters);  
  fprintf(stderr, "\nlambda %f, t_sum %f", q_profile->lambda, t_sum);
*/
/*
    fprintf(stderr, "\nScore increase: %d %d", orig_score, adj_score);
    for (i = 0; i < 256; i++) 
      if (complexity_counts[i])
	fprintf(stderr, "\n %d %d", i, complexity_counts[i]);
*/
  if (adj_score > orig_score) {
    fprintf(stderr, "\nScore increase: %d %d", orig_score, adj_score);
    for (i = 0; i < 256; i++) 
      if (complexity_counts[i])
	fprintf(stderr, "\n %c %d", (char)i, complexity_counts[i]);
  }
  return adj_score;
}

void sw::print_alignment(Profile *profile) 
{
  int i, j, k, k0, alen;

  if (!score) return; /* variable values may be undefined in this case */
  i = subject_start;
  j = query_start;
  for (alen = string_end; alen >= 0; alen -= 50) {
    printf("\n\nSubject %5d ",i);
    for (k = alen; k > alen - 50 && k >= 0; k--) {
      printf("%c",string2[k]);
      if (string2[k] != '-') i++;
    }
    printf(" %d\n              ", i - 1);
    for (k = alen; k > alen - 50 && k >= 0; k--) printf("%c",string3[k]);
    printf("\nQuery   %5d ", j);
    for (k = alen; k > alen - 50 && k >= 0; k--) {
      printf("%c",string1[k]);
      if (string1[k] != '-') j++;
    }
    printf(" %d", j - 1);
  }
}
void sw::print_alignment_one_line(Profile *profile)
{
	int k;
	if (!score) return;
	for (k = string_end; k >= 0; --k) putchar(string2[k]);
	putchar('\n');
	for (k = string_end; k >= 0; --k) putchar(string3[k]);
	putchar('\n');
	for (k = string_end; k >= 0; --k) putchar(string1[k]);
	putchar('\n');
}

void sw::get_stats(int *add_query_start, int *add_query_end, int *add_subject_start, 
	int *add_subject_end, int *add_mismatches, int *add_insertions, int *add_deletions)
{
//	printf("internal parameters: %i, %i, %i, %i\n", query_start, query_end, subject_start, subject_end);
  *add_query_start = query_start;
  *add_query_end = query_end;
  *add_subject_start = subject_start;
  *add_subject_end = subject_end;
  *add_mismatches = mismatches;
  *add_insertions = insertions;
  *add_deletions = deletions;
}


/* Efficient implementation of Smith-Waterman algorithm */

/*
   If function compiled with the flag -DFINDALIGN, alignments will be printed out;
   otherwise, only the score is computed and returned (latter version is
   appropriate for database searches, as it is faster).
   The defs of the key variables (using matrix notation, altho the matrix
   itself is not actually stored) are as follows. We imagine the columns
   of the matrix (indexed by j) as corresponding to the elements of the
   query sequence (or profile), while the rows correspond to the elements
   of the subject (database) sequence.
   s_ij is the score of the best alignment that ends at positions i (in subject)
   and j (in the query), AND that aligns i with j:  y_j
                                                    x_i
   t_ij is score of best alignment ending at i and j that has i at the
     end of the alignment, opposite a gap in the query.    y_j   -
                                                                x_i
   u_ij is score of best alignment ending at i and j that has j at the end of the
     alignment, opposite a gap in the subject.       y_j
                                                 x_i  -

   Then s_ij = max( max (s_{i-1,j-1},t_{i-1,j-1},u_{i-1,j-1}) + score(x_i,y_j), 0)
     t_ij = max(s_{i-1,j} + gap_init, t_{i-1,j} + gap_ext, u_{i-1,j} + gap_init)
     u_ij = max(s_{i,j-1} + gap_init, t_{i,j-1} + gap_init, u_{i,j-1} + gap_ext) 
   (Note: don't need to max t and u with 0 (?) )
   The following efficiency simplifications are made:
     (i) 
         store max (s,t,u) + gap_init, rather than s or max (s,t,u).
         Formula for t_ij is then max(maxstu_{i-1,j}, t_{i-1,j} + gap_ext);
	 similarly for u_ij. This saves an addition or two. -- NB: in current version
	 this no longer valid. Instead only initiate new t and u when max + gap_init > 0.
     (ii) only store a single row at a time of maxstu; maxstu_ij in the
         rightmost 16 bits of each entry in this vector. t_i+1 j is stored
         in the leftmost 16 bits and only extracted when necessary (since t is
	 usually 0, this is inexpensive).
         Only need a single value of u at a time, so don't need to
	 store it in a vector. 
     (iii) entries in score(x_i, y_j) are accessed as vectors (in j) which
         have all scores for a fixed residue (the current x_i) against residues
	 in j. This reduces matrix access overhead, and has the side benefit that
	 one can use exactly the same function when the query is a profile
	 instead of a sequence (altho in that case one might want to
	 force a global alignment rather than a local one); the variable names
	 reflects this. 
      (iv) the inner loop is coded quite efficiently, so that only 8
         instructions are carried out in most iterations (at least when gap_init is
	 reasonably large, relative to score matrix entries). These are:
	   2 memory retrieves  (maxstu and score).
	   2 pointer increments (""       "").
	   1 addition: maxstu + score.
	   3 tests: (next stored) maxstu < 0, and (current) maxstu > 0.
             1 additional test is carried out when maxstu > 0.
	     the first test subsumes the tests for the end of the current row and
	     for t being positive.
	   1 memory store (maxstu).
*/
/* 5/11/94: added changes to allow banded smith-waterman search.
   band_width: the width of the band. Generally an odd no. (so that band
   is symmetric around a diagonal), or 0 (original non-banded search).
   offset defines the central diagonal; it is
   (index of position in query) - (index of corresponding position in
   subject).
   5/17/94: allow for scores exceeding 2^15 - 1, by checking for max-score
     exceeding 32000, and (in quick version) returning immediately, or (in full
     version) reducing all scores in row, and keeping track of amount of
     reduction, and adding back later.
   5/23/94: in full version, return # mismatches, insertions, deletions (relative
     to query -- i.e. an insertion is an insertion in the subject, or a deletion
     in the query); "mismatch" is a negative score.
   6/1/94: Changed treatment of banded searches: now pass l_edge and r_edge
     instead of band_width and offset. For non-banded search, put r_edge
     < l_edge.
   6/24/06: Revised by LIN Hao for C++ compatiable
 */

#if defined(FINDALIGN)
int sw::full_smith_waterman(Profile *q_profile, char *db_entry_seq, int db_entry_length,
			int l_edge, int r_edge, int q_left, int q_right, int s_left,
			int s_right, int minscore, int *add_orig_score)
#else
int sw::smith_waterman(Profile *q_profile, char *db_entry_seq, int db_entry_length,
		   int l_edge, int r_edge, int q_left, int q_right, int s_left,
		   int s_right)
#endif
/*
     Profile *q_profile;
     char *db_entry_seq;
     int db_entry_length;
     int l_edge, r_edge; /* left and right edges of band (in first row) -- origin 0 */
 //    int q_left, q_right, s_left, s_right; /* boundaries of the query and 
//*/					      subject sequences to be used in this search (origin 1) */
{
  int band_width, r_edge_orig, l_edge_orig;
  int i, gap_init, q_len, s_len;
  int *maxstu_vec;
  char s_replace, revise_edges;
  char *seq;
  register int maxstu_above_el, u, t, maxstu, t_gap_ext, u_gap_ext,
     temp, max_score, n_gap_init;
  register int *score_vec, *maxstu_above;

#ifdef FINDALIGN
  char **direc, **dir, **mdir;
  register char *diri,*mdiri;
  register char d;
  char u_ext; /* flags for whether current u and t are gap extending */
  int alloc_size;
  char excess_flag;
  int excess, score;

  *add_orig_score = 0;
#endif

  l_edge_orig = l_edge;
  r_edge_orig = r_edge;
  s_len = (s_right <= 0 || s_right > db_entry_length) ? 
    db_entry_length : s_right; 

  q_len = (q_right <= 0 || q_right > q_profile->length) ?
    q_profile->length : q_right; 

  /* return s_left, q_left to origin 0 */

  s_left = (s_left > 1 && s_left <= s_len) ? s_left - 1 : 0;
  q_left = (q_left > 1 && q_left <= q_len) ? q_left - 1 : 0;
    
  band_width = r_edge - l_edge + 1;
  if (band_width <= 0) {
    band_width = 0;
    l_edge = q_left;
    r_edge = q_len - 1;
  }
  else {
    if (l_edge + s_len > q_len) s_len = q_len - l_edge;
    l_edge += s_left;
    if (l_edge >= q_len || r_edge + s_len <= q_left) 
      return 0; /* band does not intersect rectangle */
    r_edge += s_left;
    if (r_edge < q_left) {
      s_left += q_left - r_edge;
      l_edge += q_left - r_edge;
      r_edge = q_left;
    }
    if (r_edge > q_len - 1) r_edge = q_len - 1;
  }

  seq = db_entry_seq;
  s_replace = seq[s_len];
  seq[s_len] = 0; /* create artificial end for seq; replace later */
  seq += s_left;

  max_score = 0; 

  gap_init = q_profile->gap_init;
  t_gap_ext = q_profile->ins_gap_ext;
  u_gap_ext = q_profile->del_gap_ext;
  n_gap_init = -gap_init;
  maxstu_vec = q_profile->maxstu_vec;

  init_alignment_globals(q_profile, db_entry_seq, db_entry_length, band_width, r_edge_orig, l_edge_orig, 
			 q_left, s_left);

#ifdef FINDALIGN
  dir = direc = (char **)our_alloc(s_len * sizeof(char *));
  for (i = 0; i < s_len; i++) dir[i] = 0;
  dir += s_left;
  alloc_size = band_width ? band_width + 2 : q_len;
  excess = excess_flag = 0;
#endif

  revise_edges = (band_width || q_left);

  for (i = 0; i <= r_edge; i++) maxstu_vec[i] = 0;
  maxstu_vec[i] = -1;
/* Orig. version -- prior to band_width changes 
    for (maxstu_above = maxstu_vec; *maxstu_above != -1; maxstu_above++)
      *maxstu_above = 0;
*/

  for (; *seq; seq++) {

#if defined(FINDALIGN)
    if (excess_flag) { /* reduce scores to prevent overflow -- should not
			  interfere with alignment information */
      max_score -= 16000;
      excess += 16000; 
      excess_flag = 0;
      for (maxstu_above = maxstu_vec; *maxstu_above != -1; maxstu_above++) {
		maxstu_above_el = *maxstu_above;
		t = 0;
		if (maxstu_above_el < 0) {
		  maxstu_above_el = -maxstu_above_el;
		  t = maxstu_above_el >> 16;
		  maxstu_above_el &= 65535;
		}
		t = t < 16000 ? 0 : t - 16000;
		maxstu_above_el = maxstu_above_el < 16000 ? 0 : maxstu_above_el - 16000;
		*maxstu_above = t > 0 ? -((t << 16) + maxstu_above_el) : maxstu_above_el;
      }
    }
#endif
    score_vec = q_profile->scores[q_profile->convert[*seq]];
    maxstu_above = maxstu_vec;
    maxstu_above_el = 0; 

#ifdef FINDALIGN
    diri = (char *)our_alloc(alloc_size * sizeof(char));
    *dir = diri - l_edge;
    if (l_edge < q_left) diri = *dir + q_left;
/* N.B. *dir always points to 0 position; diri to l_edge or q_left, whichever
   is greater */
#endif

    if (revise_edges)  {
      if (l_edge > q_left) {
		score_vec += l_edge;
		maxstu_above += l_edge;

		maxstu_above_el = *(maxstu_above - 1);
		if (maxstu_above_el < 0) {
		  maxstu_above_el = -maxstu_above_el;
		  maxstu_above_el &= 65535;
		}
      }
      else {
		score_vec += q_left;
		maxstu_above += q_left;
      }
	    
      if (band_width) {
		maxstu_vec[r_edge + 1] = -1;
		if (maxstu_vec[r_edge] == -1) maxstu_vec[r_edge] = 0;
		if (r_edge < q_len - 1) r_edge++;
		l_edge++;
      }
    }

  zero_u:
    for(;;) {
      maxstu = maxstu_above_el;
      maxstu_above_el = *maxstu_above;
      if (maxstu_above_el >= 0) { /* maxstu_above_el >= 0  -- so t is 0 */
		maxstu += *score_vec++;
		if (maxstu <= 0) {
#ifdef FINDALIGN
		  d = 0; 
#endif
		  *maxstu_above++ = 0;
		} else { 
#ifdef FINDALIGN
		  d = 3; 
#endif
		  if (maxstu > n_gap_init) {
	    	t = u = maxstu + gap_init;
		    goto transition0;
		  } else {
		    *maxstu_above++ = maxstu;
		  }
		}
      } else {
		maxstu_above_el = -maxstu_above_el;
		if (maxstu_above_el == 1)
		  goto next_row; /* -1 is sentinel for end of row */

/* decompose maxstu_above_el into t and maxstu parts */
		maxstu += *score_vec++;
		t = maxstu_above_el >> 16;
		maxstu_above_el &= 65535;

#ifdef FINDALIGN
		d = maxstu_above_el + gap_init != t ? 7 : 3;
	  /* set 4's bit -- to indicate that extending gap */
#endif
		if (maxstu > t) {
		  if (maxstu > n_gap_init) {
		    t += t_gap_ext;
	    	u = maxstu + gap_init;
		    if (t < u) t = u;
		    goto transition0;
		  }
		} else { 
#ifdef FINDALIGN
		  d -= 2; /* remove two's bit */
#endif
	  	  maxstu = t; 
/* note no new maximum in score is possible, and no transition to positive
   u is necessary (because would imply adjacent gaps on opposite strands);
   so test for maxstu > n_gap_init is unnecessary */
		} 
		t += t_gap_ext;
		*maxstu_above++ = t > 0 ? -((t << 16) + maxstu) : maxstu;
      }
#ifdef FINDALIGN
      *diri++ = d; 
#endif
    }
  transition0:
#ifdef FINDALIGN
    u_ext = 0; 
#endif

  transition: /* transition (to case u positive) has occurred */
    if (max_score < maxstu) { /* N.B. This placement only allows detection of
				 scores exceeding -gap_init */
      max_score = maxstu;  
#ifdef FINDALIGN
      mdir = dir; 
      mdiri = diri; 
#endif
      if (max_score > 32000) {
#if defined(FINDALIGN)
		excess_flag = 1;
#else
		goto return_code; /* this sufficient for rapid database searches */
#endif
/*	printf("\nWARNING: Score exceeds 32000; alignment truncated\n"); 
#ifdef FINDALIGN
	*diri++ = d; 
#endif
	goto finish;
*/
      }
    }

    *maxstu_above++ = -((t << 16) + maxstu);

#ifdef FINDALIGN
    *diri++ = d; 
#endif

/* loop for case u positive */
    do {
      maxstu = maxstu_above_el + *score_vec++;
      maxstu_above_el = *maxstu_above;
      if (maxstu_above_el < 0) {
		maxstu_above_el = -maxstu_above_el;
		if (maxstu_above_el == 1)
		  goto next_row; /* -1 is sentinel for end of row */

		/* decompose maxstu_above_el into t and maxstu parts */
		t = maxstu_above_el >> 16;
		maxstu_above_el &= 65535;
	
#ifdef FINDALIGN
		d = u_ext ? 11 : 3; /* set 8's bit -- to indicate that extending u gap */
		if (maxstu_above_el + gap_init != t) d += 4;
		  /* set 4's bit -- to indicate that extending t gap */
#endif
		if (maxstu > t) {
		  if (maxstu > u) {
		    if (maxstu > n_gap_init) {
	    	  temp = maxstu + gap_init; 
		      t += t_gap_ext;
		      if (t < temp) t = temp; 
	    	  u += u_gap_ext;
		      if (u < temp) {
			u = temp;
			goto transition0;
		      }
#ifdef FINDALIGN
		      u_ext = 1;
#endif
	    	  goto transition;
		    }
		  } else {
		    maxstu = u; 
#ifdef FINDALIGN
		    d -= 1; 
#endif
		  }
		} else { 
		  if (u > t) {
		    maxstu = u; 
#ifdef FINDALIGN
	    	d -= 1; 
#endif
		  } else {
		    maxstu = t; 
#ifdef FINDALIGN
		    d -= 2; 
#endif
		  }
		} 
		t += t_gap_ext;
		*maxstu_above++ = t > 0 ? -((t << 16) + maxstu) : maxstu;
	  } else { /* maxstu_above_el >= 0  -- so t is 0; and maxstu >= u, which is positive */
#ifdef FINDALIGN
		d = u_ext ? 11 : 3;
#endif
		if (maxstu > u) {
		  if (maxstu > n_gap_init) {
	    	t = maxstu + gap_init; 
		    u += u_gap_ext;
		    if (u < t) {
	    	  u = t;
		      goto transition0;
		    }
#ifdef FINDALIGN
	    	u_ext = 1;
#endif
		    goto transition;
		  }
		  *maxstu_above++ = maxstu;
		} else {
		  *maxstu_above++ = u;
#ifdef FINDALIGN
		  d -= 1; 
#endif
		}
      }
#ifdef FINDALIGN
      u_ext = 1;
      *diri++ = d; 
#endif
      u += u_gap_ext;
    } while (u > 0);
    goto zero_u;

  next_row:
#ifdef FINDALIGN
    dir++;
#endif
	continue;
  }

#ifdef FINDALIGN
  max_score += excess;
  *add_orig_score = score = max_score;
//  printf("maxscore: %i, minscore: %i\n", max_score, minscore);
  if (max_score && max_score >= minscore) {
    alignment_from_direc(direc, mdir, mdiri, &score);
//    alignment_from_direc(mdir, mdiri, direc, &score);
    max_score = score;
  }

  if (revise_edges) {
    l_edge = band_width ? l_edge_orig : q_left;
    for (i = 0; i < s_len; i++) {
      if (direc[i]) {
		our_free(direc[i] + l_edge);
      }
      if (band_width) l_edge++;
    }
  } else for (i = 0; i < s_len; i++) {
    our_free(direc[i]);
  }
//XXX
  our_free((char*)direc);
#endif

 return_code:
  db_entry_seq[s_len] = s_replace;
  return max_score;
}


/*
extern Parameters *parameters;
extern int full_smith_waterman(Profile *q_profile, char *db_entry_seq,
		int db_entry_length, int l_edge, int r_edge, int q_left,
		int q_right, int s_left, int s_right, int minscore, int *add_orig_score);
extern Profile *make_profile_from_seq(char *seq, int nw_flag);
void free_profile(Profile *profile);
extern void get_stats(int *add_query_start, int *add_query_end,int *add_subject_start,
		int *add_subject_end, int *add_mismatches, int *add_insertions, int *add_deletions);
extern void get_params(int argc, char *argv[], char *align_file);
*/

AlignResult sw::revised_swat(Profile *q_profile, char *seq2)
//void revised_pswat(char *seq1, char *seq2)
{
	int i, j;
	int seq2_len;
	int max_score1, max_score2;
	int orig_score = 0;
	char c;
	int qs1, qe1, ss1, se1, mis1, ins1, del1;
	float iden1;
	AlignResult align_result;

	if (NULL == q_profile) {
		printf("Profile of sequence 1 is NULL.\n");
		exit(0);
	}
	if (NULL == seq2) {
		printf("sequence 2 is NULL.\n");
		exit(0);
	}
	
//Build profile for sequence 1 and reverse sequence of sequence 1	

//Doing pairwise alignment
	seq2_len = strlen(seq2);
	
	//pick the max score between seq1 vs seq2 and rev_seq1 vs seq2
	
	max_score1 = full_smith_waterman(q_profile, seq2, seq2_len, 1, 0, 0, 0, 0, 0, parameters->minscore, &orig_score);
	get_stats(&qs1, &qe1, &ss1, &se1, &mis1, &ins1, &del1);

	int alignLength = qe1-qs1+1+ins1;
	iden1 = (alignLength - mis1 - ins1 - del1) / double(alignLength);
//	printf("ident : %i, %i, %i, %i\n", max_score1, mis1, ins1, del1);
	align_result.iden = iden1;
	align_result.q_start = qs1 - 1;
	align_result.q_end = qe1 - 1;
	align_result.q_len = q_profile->length;
	align_result.s_start = ss1 - 1;
	align_result.s_end = se1 - 1;
	align_result.s_len = seq2_len;

	//cerr<<"inside sw: "<<align_result->iden<<"\t"<<align_result->q_start<<"\t"<<align_result->q_end<<endl;

	return align_result;
}
