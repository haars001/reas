#ifndef _ASSEMBLY_H_
#define _ASSEMBLY_H_

#include "ReAS.h"
#include "LocalAlign.h"

class Assembly
{
public:
	//input breakpoint information
	Assembly();
	void InBreakpoint(ifstream & fin);
	//read in linkage information among segments inside cluster
	void InLink(istream & fin);
	void InLink_binary(FILE *);
	//get sequences
	void InSeq(SEQ &);
	//search paths from the graph and do assembly
	void DoAssembly();
	void ChainInfor(unsigned int);
	set<unsigned int> ChildLink(unsigned int q_id, unsigned int sig);
	void ListSeq(set<unsigned int> & id);
	void UpdateVisitedSet();
	//get consensus sequence and matrix from multi-alignment
	void MultiAlign();
	int HighDbound(MATRIX & matrix, char LR);
	float AverageIdent(MATRIX & matrix, int left, int right);
	int DecideNtType_strict(vector<unsigned int>, char);
	int DecideNtType_loose(vector<unsigned int>, char);
	//build matrix for multi-alignment
	MATRIX BuildMatrix(SEQ & seq);
	//get consensus from matrix, update matrix on the same time
	void MatrixSeq_strict(MATRIX & m, string & con);
	void MatrixSeq_loose(MATRIX & m, string & con);
	void BiggestCluster();
	set<unsigned int> AddSubVertex();

	//get consensus sequence by vote
	string Consensus();
	//solve cycle problem
	void DeCycle();   //at left or right or both ends?
	void OutConsensusSeq(ofstream &);
	//join two sequence and update position information
	void Join2Seq(unsigned int q_id, unsigned int s_id);

protected:
	map<char, int> ch_int;
	map<int, char> int_ch;
	set<char> char_set;
	//input raw data: high-depth segments
	BLOCK bk_point;
	LINK link;
	SEQ_SIZE seq_size;
	SEQ seq;
	//
	map<unsigned int, char> chain; //how each segment projected on the consensus? 0: +; 1: -
	set<unsigned int> biggestCluster;          //biggest cluster among child sequences
	set<unsigned int> vertex;                  //the set of IDs of child sequences
	set<unsigned int> distant_extend;          //the set of IDs of distand extension >max_extend
	unsigned int num;                          //order of the assmebled m_seq
	SEQ child_seq;
	SEQ m_seq;   //consensus sequence of multi-alignment for a group of segments
	CONSENSUS m_matrix;  //corresponding matrix of m_seq
	map<unsigned int, unsigned int> num_seq;   //number of segments attended multi-alignment, which generated the m_seq
	set<unsigned int> visited;   //vertex visited during one time of path search
	set<unsigned int> unsearched;                       //set of unsearched vertex.
	LocalAlign a;
	map<unsigned int, unsigned int> cycle_L;   //cycle at left? 0:no cycle; 1:direct; 2:inverted
	map<unsigned int, unsigned int> cycle_R;
	map<unsigned int, unsigned int> num_frag;  //number of fragments assembled the consensus;
	map<unsigned int, unsigned int> total_seg; //total size of segments formed the consensus;
	//
	vector<int> positions;
//	unsigned int q_join_begin;   //beginning position on query of last joinning
	MATRIX matrix;
	CONSENSUS con_matrix;
	SEQ con_seq;
	//
};
#endif
