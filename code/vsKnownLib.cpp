/*
*Copyright 2005  BGI
*All right reserved
*
*Filename:  vsKnownLib
*Description:  

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2006.10.05
*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include <stdlib.h>
#include <algorithm>

using namespace std;

//const:

//structure & class & function


//usage:
void usage(void)
{
	cout<<"Usage:	vsKnownLib [options] <stdout>\n"
		<<"	-s	STRING	file of known TE sizes\n"
		<<"	-a	STRING	alignment, with assembled sequences as query\n"
		<<"	-h	help\n"
		<<"output format: ID | Size | AllHits_Coverage | BiggestSeq_Coverage | BiggestHit_Coverage"
		<<endl;
	exit(1);
};

struct hit
{
	int begin;
	int end;
};
bool less_begin(const hit & a, const hit & b)
{
	return a.begin<b.begin;
};
int add_hits(vector<hit> & h)
{
	int sum = 0;
	int old_begin=0;
	int old_end=0;
	sort(h.begin(), h.end(), less_begin);
	for (vector<hit>::iterator p=h.begin(); p!=h.end(); p++)
	{
		if (p->begin > old_end)
		{
			sum += old_end-old_begin+1;
			old_begin = p->begin;
			old_end = p->end;
		}
		else if (p->end > old_end)
		{
			old_end = p->end;
		}
	}
	sum += old_end-old_begin+1;
	return sum;
};

int main(int argc, char *argv[])
{
	char * size_file;
	char * align_file;
	//print usage
	if (argc == 1)
	{
			usage();
	}
	//[options]
	int c;
	while((c=getopt(argc,argv,"s:a:h")) != -1) {
			switch(c) {
					case 's': size_file = optarg; break;
					case 'a': align_file = optarg; break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	ifstream fin_size(size_file);
	if (!fin_size)
	{
		cerr<<"failed to open "<<size_file<<endl;
		exit(1);
	}
	ifstream fin_align(align_file);
	if (!fin_align)
	{
		cerr<<"failed to open "<<align_file<<endl;
		exit(1);
	}
	//
	map<string, int> TE_size;
	string id;
	int len;
	map<string, map<string, vector<hit> > > align;
	while (!fin_size.eof())
	{
		fin_size>>id;
		if (fin_size.eof())
		{
			break;
		}
		fin_size>>len;
		TE_size[id] = len;
	}
	fin_size.close();
	//
	while (!fin_align.eof())
	{
		string a_id, b_id, s;
		char ch[1000];
		hit h;
		fin_align>>a_id;
		if (fin_align.eof())
		{
			break;
		}
		fin_align>>b_id>>s>>s>>s>>s>>s>>s>>h.begin>>h.end;
		fin_align.getline(ch, 1000);
		align[b_id][a_id].push_back(h);
	}
	fin_align.close();
	////summary and output
	int sum_allhits=0;
	int sum_bestseq=0;
	int sum_besthit=0;
	map<string, int> q_allhits;
	map<string, int> q_bestseq;
	map<string, int> q_besthit;

	for (map<string, map<string, vector<hit> > >::iterator p=align.begin(); p!=align.end(); p++)
	{
		//best hit
		int besthit = 0;
		for (map<string, vector<hit> >::iterator z=p->second.begin(); z!=p->second.end(); z++)
		{
			for (vector<hit>::iterator y=z->second.begin(); y!=z->second.end(); y++)
			{
				if (y->end-y->begin+1 > besthit)
				{
					besthit = y->end-y->begin+1;
				}
			}
		}
		//best seq
		int bestseq=0;
		for (map<string, vector<hit> >::iterator z=p->second.begin(); z!=p->second.end(); z++)
		{
			int seq = add_hits(z->second);
			if (seq>bestseq)
			{
				bestseq = seq;
			}
		}
		//all hits
		int allhits;
		vector<hit> all;
		for (map<string, vector<hit> >::iterator z=p->second.begin(); z!=p->second.end(); z++)
		{
			all.insert(all.end(), z->second.begin(), z->second.end());
			allhits = add_hits(all);
		}
		//
		q_besthit[p->first] = besthit;
		q_bestseq[p->first] = bestseq;
		q_allhits[p->first] = allhits;

		sum_besthit += besthit;
		sum_bestseq += bestseq;
		sum_allhits += allhits;
	}
	//output
	cout<<"TE_id\tSize\tCoverage(allhits)\tCoverage(bestseq)\tCoverage(besthit)\n";
	int sum_length = 0;
	for (map<string, int>::iterator p=TE_size.begin(); p!=TE_size.end(); p++)
	{
		sum_length += p->second;
		float r_allhits = 0;
		float r_bestseq = 0;
		float r_besthit = 0;
		if (q_allhits.end() !=  q_allhits.find(p->first))
		{
			r_allhits = q_allhits[p->first] / float(p->second);
			r_bestseq = q_bestseq[p->first] / float(p->second);
			r_besthit = q_besthit[p->first] / float(p->second);
		}
		cout<<p->first<<"\t"<<p->second<<"\t"<<r_allhits<<"\t"<<r_bestseq<<"\t"<<r_besthit<<endl;
	}
	cout<<"Summary:\t"<<sum_length<<"\t"<<sum_allhits/float(sum_length)<<"\t"<<sum_bestseq/float(sum_length)<<"\t"<<
		sum_besthit/float(sum_length)<<endl;
	return 0;
}

