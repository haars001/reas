#!/usr/bin/perl -w
#*Filename: geneMask.pl
#*Description: calculate fraction of gene region masked by TE library on genome
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.06.03
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 <genome.mask> <psl> <stdout>

notes: output format:
ID  cDNA_size  aligned_cDNA  genomic_size  mask_exon  mask_intron  mask_all

USAGE
die $usage if (@ARGV<1);
#************************************************

my ($maskFile, $pslFile) = @ARGV;
open(F_mask, $maskFile) || die $usage;
open(F_psl, $pslFile) || die $usage;

my %mask;
my $id;
while (<F_mask>) {
	chomp;
	if (/^>(\S+)/) {
		$id = $1;
	}
	else {
		$mask{$id} .= $_;
	}
}
close F_mask;

my $sum_fullSize;
my $sum_intron;
my $sum_exon;
my $sum_all;
my $sum_mask_intron;
my $sum_mask_exon;
my $sum_mask_all;

while (<F_psl>) {
	chomp;
	my @a = split(/\s+/, $_);
	if (@a<21) {
		next;
	}
	my $q_id = $a[9];
	my $s_id = $a[13];
	my $start = $a[15];
	my $end = $a[16];
	my @exon_size = split(/,/, $a[18]);
	my @exon_start = split(/,/, $a[20]);
	my $num_exon = $a[17];
	#
	my $n_exon = 0;
	my $n_all = $end-$start;
	my $mask_all = 0;
	my $mask_exon = 0;
	for (my $i=$start; $i<$end; $i++) {
		my $s = substr($mask{$s_id}, $i, 1);
		if (($s eq "x") || ($s eq "X") || ($s eq "n") || ($s eq "N")) {
			$mask_all++;
		}
	}
	for (my $j=0; $j<$num_exon; $j++) {
		$n_exon += $exon_size[$j];
		for (my $i=0; $i<$exon_size[$j]; $i++) {
			my $s = substr($mask{$s_id}, $i+$exon_start[$j], 1);
			if (($s eq "x") || ($s eq "X") || ($s eq "n") || ($s eq "N")) {
				$mask_exon++;
			}
		}
	}
	my $n_intron = $n_all - $n_exon;
	my $mask_intron = $mask_all - $mask_exon;
	my $ratio_exon = $mask_exon/$n_exon;
	my $ratio_intron;
	if ($n_intron == 0) {
		$ratio_intron = 0;
	}
	else {
		$ratio_intron = $mask_intron/$n_intron;
	}
	my $ratio_all = $mask_all/$n_all;
	print "$q_id\t$a[10]\t$n_exon\t$n_all\t$ratio_exon\t$ratio_intron\t$ratio_all\n";
	#
	$sum_fullSize += $a[10];
	$sum_intron += $n_intron;
	$sum_exon += $n_exon;
	$sum_all += $n_all;
	$sum_mask_intron += $mask_intron;
	$sum_mask_exon += $mask_exon;
	$sum_mask_all += $mask_all;
}
my $ratio_exon = $sum_mask_exon/$sum_exon;
my $ratio_intron;
if ($sum_intron == 0) {
	$ratio_intron = 0;
}
else {
	$ratio_intron = $sum_mask_intron/$sum_intron;
}
my $ratio_all = $sum_mask_all/$sum_all;
print "Sum:\t$sum_fullSize\t$sum_exon\t$sum_all\t$ratio_exon\t$ratio_intron\t$ratio_all\n";
