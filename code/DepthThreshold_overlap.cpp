/*
    Description: 
 	Compute the depth theshold of overlapping, according to false positive
    Author: 
    	Hao LIN (linhao@ict.ac.cn)
    Date:
    	2006-05-10
	Modified by Li Ruiqiang (lirq@genomics.org.cn) at 2006-05-12
*/
	
#include<unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

void usage(void)
{
	printf("Usage:	DepthThreshold [options]\n");
	printf("	-c	double	coverage of WGS, default = 6\n");
	printf("	-r	int	average length of reads, default = 500bp\n");
	printf("	-o	int	overlap size among two reads, default = 100bp\n");
	printf("	-p	double	false positive rate, default = 0.001\n");
	printf("	-h	help\n");
	exit(1);
}
double pbinorm(int n, int k, double p)
{
	double i;
	double prob;
	
	prob = 0.0;
	if (k > n - k) {
		k = n - k;
	}
	for (i = k + 1; i <= n; i++) {
		prob = prob + log(i);
	}
	for (i = 1; i <= n - k; i++) {
		prob = prob - log(i);
	}
	prob += k * log(p) + (n - k)*log(1 -p); 
	prob = exp(prob);
	
	return prob;
}

int main(int argc, char* argv[])
{
	double cov = 6;
	int read_len = 500;
	int seg_len = 100;
	double fp_thresh = 0.001;
	double prob;
	int k;
	int i;

	int c;
	while((c=getopt(argc,argv,"c:r:o:p:h")) != -1) {
			switch(c) {
					case 'c': cov = atof(optarg); break;
					case 'r': read_len = atoi(optarg); break;
					case 'o': seg_len = atoi(optarg); break;
					case 'p': fp_thresh = atof(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	
	if (fp_thresh >= 1) {
		printf("fp should be in range [0.0, 1.0)\n");
		exit(0);
	}
	prob = 0.0;
	k = -1;
	while (prob < 1 - fp_thresh) {
		k++;
		prob += pbinorm(2*(read_len - seg_len), k, (cov / read_len));
	}
	printf("Parameters:\n");
	printf("WGS:\t%lf X\n", cov);
	printf("Read:\t%d bp\n",read_len);
	printf("Overlap:\t%d bp\n", seg_len);
	printf("FP rate:%lf \n\n",fp_thresh);
	printf("Depth threshold:\t%d\n", k);

	return 0;
}
