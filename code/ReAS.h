#ifndef _REAS_H_
#define _REAS_H_

#include<unistd.h>
#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include<set>
#include<list>
#include<time.h>
//#include<hash_map>

using namespace std;

struct BOUND
{
	unsigned int size;
	unsigned int begin;
	unsigned int end;
};

typedef unsigned int kmer_t; // 32-bit
typedef unsigned int read_t; // 32-bit

typedef map<read_t, BOUND> BLOCK;

typedef map<read_t, string> SEQ;

struct Q
{
	int size;
	vector<unsigned int> depth;
	vector<unsigned int> breakpoint;
};
typedef map<read_t, Q> DEPTH_BK;

struct JOIN
{
	char type; //0: containing; 1: left joining, extension < max_extend; 2: right joining, extension < max_extend;
			   //               3: left joining, other long extension;   4: right joining, other long extension
	char flag; //0: +; 1: -
};
typedef map<read_t, map<read_t, JOIN> > LINK;

typedef map<read_t, unsigned int> SEQ_SIZE;

typedef vector< vector<read_t> > MATRIX;
typedef map<unsigned int, MATRIX> CONSENSUS;

typedef map<kmer_t, set<read_t> > index_kmer_read;
typedef map<read_t, set<kmer_t> > index_read_kmer;

struct hit
{
	unsigned int left;
	unsigned int right;
};
typedef map<read_t, set<read_t> > READ_RELATION;

struct align_inf
{
	unsigned int q_id;
	unsigned int s_id;
	char flag;
	float ident;
	unsigned int q_length;
	unsigned int q_begin;
	unsigned int q_end;
	unsigned int s_length;
	unsigned int s_begin;
	unsigned int s_end;
};

#endif
