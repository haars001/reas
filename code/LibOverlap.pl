#!/usr/bin/perl
#*Copyright 2003.08  BGI Liruiqiang
#*All right reserved
#*Filename: LibOverlap.pl
#*Abstract: count fraction of genome masked by either of the two libraries
#*Version : 1.0
#*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
#*Time:    2006.06.03
#

use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 <masked.lib1>  <masked.lib2>  <stdout>

Note: Output format: genome size | by both | by only 1 | by only 2 | by either

USAGE

die $usage if (@ARGV<1);
#************************************************

my ($Lib1File, $Lib2File, $D) = @ARGV;

open(F1, $Lib1File) || die;
open(F2, $Lib2File) || die;

my %Mask_1 = ();
my %Mask_2 = ();

#
my $name;
#
while (<F1>) {
	chomp;
	if (/^>(\S+)/) {
		$name = $1;
	}
	else {
		$Mask_1{$name} .= $_;
	}
}
close F1;
#
while (<F2>) {
	chomp;
	if (/^>(\S+)/) {
		$name = $1;
	}
	else {
		$Mask_2{$name} .= $_;
	}
}
close F2;
#
my $sum = 0;
my $uniq_1 = 0;
my $uniq_2 = 0;
my $common = 0;

foreach $id (keys %Mask_1) {
	$sum += length($Mask_1{$id});
	for (my $i=0; $i<length($Mask_1{$id}); $i++) {
		my $a = substr($Mask_1{$id}, $i, 1);
		my $b = substr($Mask_2{$id}, $i, 1);
		my $m_1 = 0;
		my $m_2 = 0;
		if (($a eq "X") || ($a eq "x") || ($a eq "N") || ($a eq "n")) {
			$m_1 = 1;
		}
		if (($b eq "X") || ($b eq "x") || ($b eq "N") || ($b eq "n")) {
			$m_2 = 1;
		}
		if ($m_1 && $m_2) {
			$common++;
		}
		elsif ($m_1) {
			$uniq_1++;
		}
		elsif ($m_2) {
			$uniq_2++;
		}
	}
}
#
print "Genome Size\tOverlap\tLib 1 unique\tLib 2 unique\tAdded\n";
my $ratio_common = $common/$sum;
my $ratio_uniq_1 = $uniq_1/$sum;
my $ratio_uniq_2 = $uniq_2/$sum;
my $ratio_add = ($common+$uniq_1+$uniq_2)/$sum;
print "$sum\t$ratio_common\t$ratio_uniq_1\t$ratio_uniq_2\t$ratio_add\n";
