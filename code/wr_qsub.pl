#!/usr/bin/perl -w
use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options] <stdout>
	-com	STRING	command file
	-n	INT	number of cpus, default=1
	-mem	STRING	default=1000mb
	-walltime	STRING	default=100:00:00
	-h	help

notes:

USAGE
#************************************************

my %opts;
GetOptions(\%opts , "com=s", "n=i", "mem=s" , "walltime=s" , "h");

die $usage if defined $opts{h};

my $n = defined $opts{n}? $opts{n} : 1;
my $mem = defined $opts{mem}? $opts{mem} : "1000mb";
my $walltime = defined $opts{walltime}? $opts{walltime} : "100:00:00";
my $com_file = defined $opts{com}? $opts{com} : die;

my $s = `wc $com_file`;
$s =~ /^\s*(\d+)\s+/;
my $num_task = $1;

my $n_ele = int($num_task/$n);

open (F_com, $com_file)||die;
my $c=0;
my $i=1;
open (F_sh, ">$i.sh") ||die;
while (<F_com>) {
	print F_sh $_;
	$c++;
	if ((0 == $c % $n_ele) && ($i<$n)) {
		$i++;
		close F_sh;
		open (F_sh, ">$i.sh") || die;
	}
}
close F_sh;
for ($i=1; $i<=$n; $i++) {
	print "qsub -l mem=$mem -l walltime=$walltime $i.sh\n";
}
