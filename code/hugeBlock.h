/******************************************************************
** hugeBlock.h
** Copyright (c) BGI 2003
** Author:Li Heng <liheng@genomics.org.cn>
** Program Date :2003-1-28 13:57
** Modifier:Li Shengting <lishengting@genomics.org.cn>
** Last Modified:2003-12-16 18:00
** Description:
** Version:1.0 create version
******************************************************************/
#ifndef HUGEBLOCK_H_
#define HUGEBLOCK_H_

#include <stdlib.h>
#include <malloc.h>
#include <cstring>

typedef unsigned int int_32;

struct qentry {
	size_t left, right;
};

template<class TYPE>
class HugeBlock
{
	TYPE **base;
	TYPE *tail,*max;
	size_t basic_len,mask,lsize;
	int nbase,bbit;
	int sorted;
public:
	HugeBlock(void) { 
		HugeBlock(10);
	};
	HugeBlock(int block_bit) {
		nbase=0;
		bbit=block_bit;
		basic_len=(size_t(1)<<bbit);///sizeof(TYPE);
		//if (!basic_len) basic_len=1;
		mask=(size_t(~0))>>(sizeof(size_t)*8-bbit);
		base=NULL;
		max=tail=NULL;
		lsize=0;
		sorted=0;
	};
	HugeBlock(int block_bit,int nb) { //block_bit:block bits num; nb: block num
		bbit=block_bit;
		basic_len=(size_t(1)<<bbit);///sizeof(TYPE);
		//if (!basic_len) basic_len=1;
		nbase=nb;
		mask=(size_t(~0))>>(sizeof(size_t)*8-bbit);
		if (!(base=(TYPE**)malloc(sizeof(TYPE*)*nbase))) {
			fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE*)*nbase);
			exit(1);
		}
		for(int i=0;i<nbase;i++) {
			if (!(base[i]=(TYPE*)malloc(sizeof(TYPE)*basic_len))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE)*basic_len);
				exit(1);
			}
		}
		tail=base[0];
		max=base[nbase-1]+basic_len;
		lsize=0; 
		sorted=0;
	};
	~HugeBlock(void) {
		freeall();
	};
	void freeall(void) {
		for(int i=0;i<nbase;i++) {
			free(base[i]);
			base[i]=NULL;
		}
		free(base);
		nbase=0;base=NULL;
		max=tail=NULL;
		lsize=0;
	};
	void free0(void) {
		int needFree;
		for(int i=0;i<nbase;i++) {
			needFree=1;
			if (base[i]!=NULL && base[i][0]==0 && base[i][basic_len-1]==0) {
				for (int j=1;j<basic_len-1;j++) {
					if (base[i][j]!=0) {
						needFree=0;
						break;
					}
				}
				if (needFree) {
					free(base[i]);
					base[i]=NULL;
				}
			}
		}
	};
	void fill0(void) {
		for(int i=0;i<nbase;i++)
			memset(base[i],0,sizeof(TYPE)*basic_len);
		tail=max; //init with all unit fill 0
		lsize=basic_len*nbase; //full
	};
	void input(FILE *inf,size_t len){
		if (len>nbase*basic_len) {
			int tmp=nbase;
			nbase=len % basic_len ? len/basic_len+1 : len/basic_len;
			if (!(base=(TYPE**)malloc(sizeof(TYPE*)*nbase))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE*)*nbase);
				exit(1);
			}
			for(int i=tmp;i<nbase;i++) {
				if (!(base[i]=(TYPE*)malloc(sizeof(TYPE)*basic_len))) {
					fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE)*basic_len);
					exit(1);
				}
			}
		}
		for(int i=0;i<len/basic_len;i++) {
			fread(base[i],sizeof(TYPE),basic_len,inf);
		}
		if (len % basic_len) {
			fread(base[len/basic_len],sizeof(TYPE),len % basic_len,inf);
			tail=base[len/basic_len]+len % basic_len;
			max=base[len/basic_len]+basic_len;
		}else
			tail=max=base[len/basic_len-1]+basic_len;
		if (lsize<len) lsize=len;
	};
	void inText(FILE *inf,size_t len){
		size_t i;
		size_t value;
		if (len>nbase*basic_len) {
			int tmp=nbase;
			nbase=len % basic_len ? len/basic_len+1 : len/basic_len;
			if (!(base=(TYPE**)malloc(sizeof(TYPE*)*nbase))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE*)*nbase);
				exit(1);
			}
			for(i=tmp;i<nbase;i++) {
				if (!(base[i]=(TYPE*)malloc(sizeof(TYPE)*basic_len))) {
					fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE)*basic_len);
					exit(1);
				}
			}
		}
		fill0();
		while (fscanf(inf,"%lu:%lu",&i,&value)==2) {
			get(i)=(TYPE)value;
		}
		if (len % basic_len) {
			tail=base[len/basic_len]+len % basic_len;
			max=base[len/basic_len]+basic_len;
		}else
			tail=max=base[len/basic_len-1]+basic_len;
		if (lsize<len) lsize=len;
	};

	void inCompact(FILE *inf,size_t len){
		if (len>nbase*basic_len) {
			int tmp=nbase;
			nbase=len % basic_len ? len/basic_len+1 : len/basic_len;
			if (!(base=(TYPE**)malloc(sizeof(TYPE*)*nbase))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE*)*nbase);
				exit(1);
			}
			for(int i=tmp;i<nbase;i++) {
				if (!(base[i]=(TYPE*)malloc(sizeof(TYPE)*basic_len))) {
					fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE)*basic_len);
					exit(1);
				}
			}
		}
		size_t highbit,first,lasth,i,tmp;
		size_t compactMask;
		int llen;
		int list_size=256;
		int_32 *lowbit=(int_32 *)malloc(sizeof(int_32)*list_size);
		char type;
		fread(&compactMask,sizeof(size_t),1,inf);
		fread(&type,sizeof(type),1,inf);
		//printf("\n==  %lu,%lu\n",compactMask,len);
		tmp=~(compactMask|(size_t(~compactMask)>>1));
		i=0;
		while (i<len) {
			fread(&first,sizeof(size_t),1,inf);
			if (type && (first & tmp)) {
				get(i++)=first;
			}else{
				highbit=first & compactMask;
				llen=int_32(first & size_t(~compactMask));
				//printf("%lu,%lu->",first,llen);
				if (llen>list_size) {
					list_size=llen;
					lowbit=(int_32 *)malloc(sizeof(int_32)*list_size);
				}
				fread(lowbit,sizeof(int_32),llen,inf);
				for (int j=0;j<llen;j++) {
					//printf("%lu\t",lowbit[j]);
					get(i+j)=highbit | lowbit[j];
				}
				//printf("\n");
				i+=llen;
			}
		}
		//printf("%lu\n",i);
		if (len % basic_len) {
			tail=base[len/basic_len]+len % basic_len;
			max=base[len/basic_len]+basic_len;
		}else
			tail=max=base[len/basic_len-1]+basic_len;
		if (lsize<len) lsize=len;
	};

	void output(FILE *outf,int drop) {
		for(int i=0;i<nbase-1;i++) {
			fwrite(base[i],sizeof(TYPE),basic_len,outf);
			if (drop) {
				free(base[i]);
				base[i]=NULL;
			}
		}
		fwrite(base[nbase-1],sizeof(TYPE),lsize-basic_len*(nbase-1),outf);
		if (drop) {
			free(base[nbase-1]);
			base[nbase-1]=NULL;
			free(base);
			nbase=0;base=NULL;
			max=tail=NULL;
			lsize=0;
		}
	};
	void outText(FILE *outf,int drop) {
		size_t tmp=0;
		for(int i=0;i<nbase;i++) {
			for (int j=0;j<basic_len;j++) {
				if (tmp<lsize) {
					if (base[i][j]) 
						fprintf(outf,"%lu:%d\n",tmp,base[i][j]);
					tmp++;
				}else{
					break;
				}
			}
			if (drop) {
				free(base[i]);
				base[i]=NULL;
			}
		}
		if (drop) {
			free(base);
			nbase=0;base=NULL;
			max=tail=NULL;
			lsize=0;
		}
	};

	void outCompact(FILE *outf,int drop,size_t compactMask,size_t maxlow) {
		if (!sorted)
			quick_sort();
		if (maxlow>int_32(~0)) {
			output(outf,drop);
			return;
		}
		//fprintf(stderr,"OK0\n");
		size_t highbit,first,lasth,tmp,tmp2;
		int len;
		int list_size=256;
		int_32 *lowbit;
		char type;

		if (!(lowbit=(int_32 *)malloc(sizeof(int_32)*list_size))) {
			fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(int_32)*list_size);
			exit(1);
		}
		//fprintf(stderr,"OK1\n");
		lasth=base[0][0] & compactMask;
		tmp2=~(compactMask|(size_t(~compactMask)>>1));
		len=0;
		tmp=0;
		fwrite(&compactMask,sizeof(size_t),1,outf);
		if (maxlow > size_t(~compactMask)>>1)
			type=0;
		else
			type=1;
		//fprintf(stderr,"OK2----%d\n",type);
		fwrite(&type,sizeof(type),1,outf);
		for(int i=0;i<nbase;i++) {
			for (int j=0;j<basic_len;j++) {
				if (tmp<lsize) {
					highbit=base[i][j] & compactMask;
					if (lasth!=highbit) {
						if (type && (len==1)) {
							first=lasth | lowbit[0] | tmp2;
							fwrite(&first,sizeof(size_t),1,outf);
						}else{
							first=lasth | len;
							fwrite(&first,sizeof(size_t),1,outf);
							fwrite(lowbit,sizeof(int_32),len,outf);
						}
						len=0;
						lasth=highbit;
					}
					if (len>=list_size) {
						list_size=len+256;
						if (!(lowbit=(int_32 *)realloc(lowbit,sizeof(int_32)*list_size))) {
							fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(int_32)*list_size);
							exit(1);
						}
					}
					lowbit[len++]=int_32(base[i][j] & size_t(~compactMask));
					tmp++;
				}else{
					break;
				}
			}
			if (drop) {
				free(base[i]);
				base[i]=NULL;
			}
		}
		//fprintf(stderr,"OK3\n");
		if (type && (len==1)) {
			first=lasth | lowbit[0] | tmp2;
			fwrite(&first,sizeof(size_t),1,outf);
		}else{
			first=lasth | len;
			fwrite(&first,sizeof(size_t),1,outf);
			fwrite(lowbit,sizeof(int_32),len,outf);
		}
		//fprintf(stderr,"OK4\n");
		if (drop) {
			free(base);
			nbase=0;base=NULL;
			max=tail=NULL;
			lsize=0;
		}
	};

	inline void append(TYPE a) {
		if(tail==max) { //full,need extend
			nbase++;
			if (!(base=(TYPE**)realloc(base,sizeof(TYPE*)*nbase))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE*)*nbase);
				exit(1);
			}
			if (!(base[nbase-1]=(TYPE*)malloc(sizeof(TYPE)*basic_len))) {
				fprintf(stderr,"Can't allocate %lu bytes memory!\n",sizeof(TYPE)*basic_len);
				exit(1);
			}
			memset(base[nbase-1],0,sizeof(TYPE)*basic_len);
			tail=base[nbase-1];
			max=tail+basic_len;
		}else if (!(lsize % basic_len))	{
			tail=base[lsize/basic_len];
		}
		*tail++=a;
		lsize++;
	};
	inline TYPE &operator[](size_t x) {
		return(base[x>>bbit][x&mask]); 
	};
	inline TYPE &get(size_t x) { 
		return(base[x>>bbit][x&mask]); 
	};
	void shift(size_t i,size_t n,TYPE &tmp)
	{
		static size_t k;
		tmp=get(i);
		while(1) {
			k=(i<<1)+1;
			if(k>=n) { get(i)=tmp;return; }
			if(k<n-1 && get(k)>get(k+1)) k++;
			if(get(k)<tmp) { get(i)=get(k);i=k; }
				else { get(i)=tmp;return; }
		}
	};
	void heap_sort(void) // dump sort
	{
		ssize_t i;
		TYPE tmp;

		for(i=(lsize>>1)-1;i>=0;i--) shift(i,lsize,tmp);
		for(i=lsize-1;i>0;i--) {
			tmp=get(i);get(i)=**base;**base=tmp;
			shift(0,i,tmp);
		}
		sorted=1;
	};
	inline size_t size(void) { 
		return(lsize); 
	};

#define THRESHOLD 9
#define key(a) (a)

	inline void swap(TYPE &ele1, TYPE &ele2) {
		TYPE tmp;
		tmp=ele1;
		ele1=ele2;
		ele2=tmp;
	};
	void inssort(int size)
	{
		int i,j,tmp;
		for (i =0; i<size; i++)
			for (j =i; (j>0)&& (key(get(j)) < key(get(j-1))); j--)
				swap(get(j),get(j-1));
	};
	void qsort_part(size_t i,size_t j)
	{
		size_t listsize =j-i+1;
		struct qentry Stack[1000];
		size_t top=0;
		TYPE pivot;
		size_t pivotindex,l,r;
		//size_t count=0;
		//char str[256];
	
		Stack[top].left =i;
		Stack[top].right =j;
		while (top < size_t(~0)) {
			/* Pop Stack */
			/*
			if (!(count%1000000)) {
				sprintf(str,"Pop Stack %dM times,top %lu",count/1000000,top);
				cal_time(str);
			}
			count++;
			*/
			
			i =Stack[top].left;
			j =Stack[top--].right;
			/* Findpivot */
			pivotindex =(i+j)/2;
			pivot=key(get(pivotindex));
			swap(get(pivotindex), get(j)); /* Stick pivot at end */
			/* Patition */
			l =i-1;
			r =j;
			do {
				while (key(get(++l)) < pivot);
				while (r && (key(get(--r)) > pivot));
				//fprintf(stderr,"l:%lu r:%lu\n",l,r);
				swap(get(l), get(r));
			} while (l < r);
			swap(get(l), get(r)); /* Undo final swap */
			swap(get(l), get(j)); /* Put pivot value in place */
			/* Load up Stack */
			if ((l-i) > THRESHOLD) {  /* Left partition */
				Stack[++top].left =i;
				//fprintf(stderr,"top++: %lu\n",top);
				Stack[top].right = l-1;
			};
			if ((j-l) > THRESHOLD) {  /* Right partition */
				Stack[++top].left = l+1;
				//fprintf(stderr,"top++: %d\n",top);
				Stack[top].right =j;
			}
		}
		inssort (listsize);
	};

	void quick_sort(void)
	{
		qsort_part(0,lsize-1);
		sorted=1;
	};
/*
	inline int cmp(TYPE * a,TYPE * b) {
		return *a<*b?-1:(*a==*b?0:1);
	}

	void sort()
	{
		for(int i=0;i<nbase-1;i++) {
			qsort(base[i],basic_len,sizeof(TYPE),cmp);
			base[i]=NULL;
		}
		qsort(base[nbase-1],tail-base[nbase-1],sizeof(TYPE),cmp);
		sorted=1;
	};
*/
	// friend class iHugeBlock;
};

template<class TYPE>
class iHugeBlock
{
	HugeBlock<TYPE> &hb;
	TYPE *sp,*max,*tmp;
	int n;
public:
	iHugeBlock(HugeBlock<TYPE> &h){
		hb=h;sp=hb.base[0];n=0;
		max=hb.base[0]+h.basic_len;
	};
	~iHugeBlock(void) {};
	inline TYPE &operator[](size_t x) {return(hb.base[x>>hb.bbit][x&hb.mask]);};
	inline TYPE *operator++(void) {
		sp++;
		if(sp==max) { n++;sp=hb.base[n];max=sp+hb.basic_len;	}
		return(sp);
	};
	inline TYPE *operator++(int) {
		tmp=sp;sp++;
		if(sp==max) { n++;sp=hb.base[n];max=sp+hb.basic_len; }
		return(tmp);
	}
	inline TYPE &operator*(void) { return(*sp); };
};

#endif
