#!/usr/bin/perl -w
#*Filename: run_DoAssembly.pl
#*Description: run assembly for each cluster
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.10.11
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
	-dir	STRING	directory of clusters, default =.
	-depth	INT	depth threshold, default=6
	-subset	INT	a subset of segments for multi-alignment to get consensus, default=-1(entire), suggest 200 if set
	-con	STRING	output consensus sequence file, default="consensus.fa"
	-pre	STRING	prefix of consensus sequence names, default =""
	-log	output log details, default=[no]
	-a	INT	number of parallel processors, default=1
	-h	help

notes:

USAGE
#************************************************

my $seq_file = "seg.fa";
my $bk_file = "seg.bk";
my $link_file = "seg.link";
my $cluster = "cluster_";

my %opts;
GetOptions(\%opts , "dir=s", "depth=i", "subset=i", "con=s", "pre:s", "log", "a=i", "h");

die $usage if defined $opts{h};
my $dir = defined $opts{dir}? $opts{dir} : ".";
my $depth = defined $opts{depth}? $opts{depth} : 6;
my $subset = defined $opts{subset}? " -t $opts{subset} " : "";
my $con = defined $opts{con}? $opts{con} : "consensus.fa";
my $pre = defined $opts{pre}? "$opts{pre}" : "";
my $log = defined $opts{log}? "-s" : "";
my $Nprocess = defined $opts{a}? $opts{a} : 1;

chdir $dir ||die;

open (f_sh, ">sh") || die;
foreach my $e (`ls`) {
	chomp $e;
	if ($e =~ /^$cluster(\d+)$/) {
		my $g = $1;
		print f_sh "DoAssembly -i $e -r $seq_file -l $link_file -b $bk_file $subset -d $depth -o $con -p $pre$g\_ $log &\n";
	}
}
close f_sh;
`cat sh | multi_run2.sh -n $Nprocess -w 2`;
foreach (`ls |grep $cluster`) {
	chomp;
	`cat $_/$con >>$con`;
}
