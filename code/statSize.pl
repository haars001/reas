#!/usr/bin/perl -w
#*Filename: statSize.pl
#*Description: 
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.07.04
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : <seq> | $0 <stdout>
	-h	help

notes: gives the total size over all sequences.

USAGE
#************************************************

my %opts;
GetOptions(\%opts , "h");

die $usage if defined $opts{h};

my $n_seq = 0;
my $total_size = 0;

while (<stdin>) {
	if (/^>/) {
		$n_seq++;
	}
	else {
		chomp;
		$total_size += length $_;
	}
}
print "Num_of_seqs\tTotal_size\tAvg_size\n";
my $avg_size = $total_size/$n_seq;
print "$n_seq\t$total_size\t$avg_size\n";
