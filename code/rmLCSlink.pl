#!/usr/bin/perl -w
#*Filename: rmLCSlink.pl
#*Description: remove links by only LCS region overlap
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.11
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat <align> | $0 [options] <stdout>
	-m	STRING	low-complexity masked (by dust or seg) sequences file
	-j	INT	threshold of none-LCS overlap, default = 100
	-h	help

notes:

USAGE
die $usage if (@ARGV<1);

my %opts;
GetOptions(\%opts, "m=s", "j=i", "h");

my $mask_file = defined $opts{m}? $opts{m} : "";
my $nonLCS = defined $opts{j}? $opts{j} : 100;
#************************************************
open (fin_mask, $mask_file) || die "can not open masked segments file\n";

my %mask;
my $id;
my $seq = "";
while (<fin_mask>) {
	chomp;
	if (/^>(\S+)/) {
		if ($seq ne "") {
			my @s = ($seq =~ /([NX]{50,})/g);
			my $n = @s;
			if ($n >= 1) {
				my @a;
				$a[0] = index($seq, $s[0]);
				$a[1] = index($seq, $s[$n-1]) + length($s[$n-1]);
				$mask{$id} = \@a;
			}
		}
		$id = $1;
		$seq = "";
	}
	else {
		$seq .= $_;
	}
}
if ($seq ne "") {
	my @s = ($seq =~ /([NX]{50,})/g);
	my $n = @s;
	if ($n >= 1) {
		my @a;
		$a[0] = index($seq, $s[0]);
		$a[1] = index($seq, $s[$n-1]) + length($s[$n-1]);
		$mask{$id} = \@a;
	}
}
close fin_mask;
#
while (<stdin>) {
	my $link = $_;
	chomp;
	my @a=split(/\s+/, $_);
	my $q_id = $a[0];
	my $s_id = $a[1];
	my $q_begin = $a[5];
	my $q_end = $a[6];
	my $s_begin = $a[8];
	my $s_end = $a[9];
	if ((defined $mask{$q_id}) && (defined $mask{$s_id})) {
		#overlap between hit and LCS region on query and sbject:
		my $left;
		my $right;
		#query:
		$left = $mask{$q_id}[0] > $q_begin? $mask{$q_id}[0] : $q_begin;
		$right = $mask{$q_id}[1] < $q_end? $mask{$q_id}[1] : $q_end;
		if ((($q_end-$q_begin+1) - ($right-$left+1) <$nonLCS) && (($right-$left+1) > ($q_end-$q_begin+1) * 0.5)) {
			next;
		}
		#sbject;
		$left = $mask{$s_id}[0] > $s_begin? $mask{$s_id}[0] : $s_begin;
		$right = $mask{$s_id}[1] > $s_end? $mask{$s_id}[1] : $s_end;
		if ((($s_end-$s_begin+1) - ($right-$left+1) <$nonLCS)  && (($right-$left+1) > ($s_end-$s_begin+1) * 0.5)) {
			next;
		}
	}
	print $link;
}
