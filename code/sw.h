#ifndef _SW_H_
#define _SW_H_
/*****************************************************************************
#   Copyright (C) 1993-1999 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/


#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace std;

typedef struct profile {
  double lambda; /* calculated lambda for score matrix */
  double *log_freqs; /* log residue freqs for score matrix */
  int gap_init, gap_ext, ins_gap_ext, del_gap_ext; /* gap initiating and gap extension penalties;
			    assumed constant across profile. Should be
			    negative numbers. gap_init is the penalty for
			    the first residue in a gap, gap_ext is the
			    penalty for each succeeding residue; ins_gap_ext
			    is for insertion (in subject rel to query),
			    del_gap_ext is for deletion (in subject rel to query)*/
  int qbegin_gap_init,qbegin_gap_ext,qend_gap_init,qend_gap_ext,
      sbegin_gap_init,sbegin_gap_ext,send_gap_init,send_gap_ext;
/* penalties at beginning, end of query and subject, for Needleman-Wunsch.
   e.g. qbegin penalties are for gaps in query preceding first residue in query */

  int length; /* length of profile */
  int *convert; /* array to convert letters (e.g. in target aa seq being
		   compared to the profile) to indices for score matrix */
  int **scores; /* array (of length 20, or size of alphabet)
		   of pointers to vectors of scores (corresponding to
		   rows in profile; each has length profile->length + 1) */
//  int **hist;
  int max_entry, min_entry; /* maximum positive entry, and minimum negative entries
			       (or 0, if none) in scores */
			       
/*  int **score_pos;  array of pointers to vectors of indices of positive
		      score positions in scores -- was used only with SPARSE */
  char *seq; /* characters in sequence which corresponds to the profile (if any) */
  int *maxstu_vec; /* array of length length + 1, used to hold row of scores,
		      in smith-waterman alg; last entry must be -1 */
  int *t_vec; /* array of length length + 1, used to hold row of t scores,
		 in smith-waterman alg; last entry must be > -1 */
/*  int **work3, **work4;  working space, must be of length profile->length */
/*  int **score_mat;  score matrix */

  int packed_length;
  unsigned long int poly_offset, poly_one, max_score_cutoff, poly_gap_init, poly_gap_ext;
/* N.B. NEED POLY_INS_GAP_EXT ETC. */
  unsigned long int *packed_maxstu_vec;
  unsigned long int **packed_scores;
} Profile;


typedef struct database {
  char *id_buffer, *descrip_buffer, *seq_buffer; /* buffers containing current entry (when file is being read) */
  int seq_length; /* length of current entry */
  int id_buffer_size, descrip_buffer_size, seq_buffer_size;
  int first_entry, last_entry; /* index (in list of all entries) of first and last entries
				  in this database */
  int num_entries; /* no. of entries in database 
			(before complementing) */
  int t_length; /* total length of entries (before complementing) */
  int in_memory; /* flag indicating whether in memory */
  FILE *file; /* FASTA file used */
  char *id_area, *descrip_area, *seq_area;
/* arrays containing ids, descriptions all residue sequences; first char is 0, 
 which is followed by each sequence in turn, each being terminated by 0 */
  int id_area_size, descrip_area_size, seq_area_size; /* size (in bytes) of seq_area */
  char *orig_qual_area, *adj_qual_area;
  struct align_info *align_array;
  int complements; /* flag indicating whether includes complements */
  struct database *next;
} Database;

typedef struct file {
  char *name;
  FILE *fp;
  struct file *next;
  struct database *db;
} File;

typedef struct _parameters {
	int gap_init;
	int gap_ext, ins_gap_ext, del_gap_ext;
	int end_gap;
	int minscore;
	int nw_flag, DNA_flag;
	int penalty;
	int default_qual;
//	float min_iden;
//	int min_block_size;
	char *matrix;
//	File *query_files;
//	File *subject_files;
} Parameters;

class AlignResult {
public:
	float iden;
	int q_start;
	int q_end;
	int q_len;
	int s_start;
	int s_end;
	int s_len;
};

/* N.B. The following is modified from Kernighan & Ritchie, The C Programming
Language, 1st edition */

typedef unsigned int ALLOC;
typedef short SHORT;

typedef double ALIGN;
union header {
	struct {
		union header *ptr;
		ALLOC size;
	} s;
	ALIGN  x;
};

typedef union header HEADER;


//
class sw
{
public:
	void set_mats();
	void set_params(char * score_matrix, int min_score);
	void fatalError(char *message);
	void check_alloc_list();
	void notify(char *message);
	FILE *fopenRead(char *fileName);
	FILE *fopenWrite(char *fileName);
	FILE *fopenAppend(char *fileName);
	char *our_alloc(unsigned int nbytes);
	void find_frags();
	HEADER *morecore(unsigned int nu);
	SHORT our_free(char *ap);
	int get_alphsize();
	int get_gap_init();
	int get_gap_ext();
	int get_ins_gap_ext();
	int  get_del_gap_ext();
	void set_gap_penalties(int gap_init0, int gap_ext0, int ins_gap_ext0, 
				int del_gap_ext0, int end_gap0);
	void get_lambda();
	int set_score_mat(char *file_name, int mismatch);
	void print_score_mat();
	void print_background_freqs();
	Profile * make_profile_from_seq(char *seq, int nw_flag);
	void find_segs(Profile *profile, int i_alph, int j_start, int j_end, 
			int minscore, char *seq, char *id, int screen);
	void free_profile(Profile *profile);
	double get_sum();
	void set_complexity_adjust(double factor);
	void unset_complexity_adjust();
	void warnings_on();
	void warnings_off();
	void init_alignment_globals(Profile *q_p, char *db_e_seq, int db_e_len, int q_l, 
				int s_l, int b_w, int r_e, int l_e);
	void alloc_strings();
	int complexity_correct(int orig_score, int pos_score);
	void alignment_from_direc(char **direc, char **mdir, char *mdiri, int *add_max_score);
	void print_alignment(Profile *profile);
	void print_alignment_one_line(Profile *profile);
	void get_stats(int *add_query_start, int *add_query_end, int *add_subject_start, 
		int *add_subject_end, int *add_mismatches, int *add_insertions, int *add_deletions);
	AlignResult revised_swat(Profile *q_profile, char *seq2);
	int full_smith_waterman(Profile *q_profile, char *db_entry_seq, int db_entry_length,
				int l_edge, int r_edge, int q_left, int q_right, int s_left,
				int s_right, int minscore, int *add_orig_score);
protected:
	HEADER base;
	HEADER *allocp;	
	long int total_allocated;
Parameters *parameters;
char comp_mat[256], alph_mat[256];
 char alphabet[256];
 int alphsize;
// int alphsize = 6;
 char *row_labels;
 int n_rows, n_places;
 int **score_mat;
 int gap_init, gap_ext, ins_gap_ext, del_gap_ext, end_gap;
 int row_convert[256], col_convert[256];
 int profile_flag;
 double *freqs, *log_freqs;
 double lambda;
 int freq_flag;
 /* the following retrieve additional information -- after the initial call */
/*BUG 0 SCORE ALIGNMENTS DO NOT HAVE INFORMATION APPROPRIATELY SET */
 char *string1, *string2, *string3;
 int string_end, string_len;
 int query_start, query_end, subject_start, subject_end;
 int mismatches, insertions, deletions;
 char warnings, complexity_adjust;
 double complexity_factor;
 int complexity_counts[256];
 int score;
 Profile *q_profile;
 char *db_entry_seq;
 int db_entry_length;
 int q_left, s_left; /* boundaries of the query and 
			      subject sequences to be used -- origin 0 */
 int band_width, r_edge_orig, l_edge_orig;
// int gap_init, ins_gap_ext, del_gap_ext;
};

#endif
