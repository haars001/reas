#include "Utilities.h"
#include "ReAS.h"
#include<math.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>    // std::set_difference, std::sort

extern int K;
extern int D;
extern int depth;
extern int hit_size;
extern float hit_ident;
extern int bk_size;
extern string align_tool;
extern int over_size;
extern int min_extend;
extern int max_extend;
extern char quiet;
extern int nonlcs_size;
extern float child_ident;

//read in seq
int Rseqs (istream & f, SEQ & seq)
{
	char ch[10000];
	string s;
	read_t id;
	while (!f.eof())
	{
		s.clear();
		f>>s;
		if (s[0] == '>')
		{
			s.erase(0,1);
			id = atoi(s.c_str());
		}
		else
		{
			seq[id] += s;
		}
		f.getline(ch,10000);
	}
	for (SEQ::iterator p=seq.begin(); p!=seq.end(); p++)
	{
		p->second.resize(p->second.size());
	}
	return 0;
}
//read in seq with IDs in the list
int Rlist_seq (istream & f, set<read_t> & reads, SEQ & seq)
{
	char ch[10000];
	string s;
	read_t id;
	while (!f.eof())
	{
		s.clear();
		f>>s;
		if (s[0] == '>')
		{
			s.erase(0,1);
			id = atoi(s.c_str());
		}
		else
		{
			if (reads.find(id) != reads.end())
			{
				seq[id] += s;
			}
		}
		f.getline(ch,10000);
	}
	for (SEQ::iterator p=seq.begin(); p!=seq.end(); p++)
	{
		p->second.resize(p->second.size());
	}
	return 0;
}
//read in seq with IDs not in the list
int Rnonlist_seq (istream & f, set<read_t> & reads, SEQ & seq)
{
	char ch[10000];
	string s;
	read_t id;
	while (!f.eof())
	{
		s.clear();
		f>>s;
		if (s[0] == '>')
		{
			s.erase(0,1);
			id = atoi(s.c_str());
		}
		else
		{
			if (reads.find(id) == reads.end())
			{
				seq[id] += s;
			}
		}
		f.getline(ch,10000);
	}
	for (SEQ::iterator p=seq.begin(); p!=seq.end(); p++)
	{
		p->second.resize(p->second.size());
	}
	return 0;
}
int InBreakpoint(BLOCK & bk_point, ifstream & fin)
{
	BOUND b;
	unsigned int q_id;
	while (!fin.eof())
	{
		fin>>q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>b.size>>b.begin>>b.end;
		bk_point[q_id] = b;
	}
	return 0;
}
//define high-depth segment from reads
int HighDseg (istream & fin, BLOCK & blocks)
{
	map<read_t, vector<hit> > hits;
	map<read_t, unsigned int> seq_sizes;
	hit h;
	unsigned int q_id;
	char ch[1000];
	string ts;

	//read in alignment result to get coverage and breakpoint information
	int q_size;
	float ident;

	while (!fin.eof())
	{
		//revised at 2006-10-09 to read hit for each sbjct
		fin>>q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>ts>>ts>>ident>>q_size>>h.left>>h.right;
		fin.getline(ch, 1000);
		if ((ident < hit_ident) || (h.right-h.left+1 < hit_size))
		{
			continue;
		}
		hits[q_id].push_back(h);
		seq_sizes[q_id]=q_size;
	}
	//
	BOUND bound;
	for (map<read_t, vector<hit> >::iterator p=hits.begin(); p!=hits.end(); p++)
	{
		if (HighDseg_2(seq_sizes[p->first], p->second, bound))
		{
			blocks[p->first] = bound;
		}
	}
	return 0;
}
//define high-depth segment from reads
int HighDseg_2 (const int size, vector<hit> & hits, BOUND & bound)
{
	Q e;
	e.size = size;
	e.depth.clear();
	e.depth.resize(size, 0);
	e.breakpoint.clear();
	e.breakpoint.resize(size, 0);
	for (vector<hit>::iterator p=hits.begin(); p!=hits.end(); p++)
	{
		for (int i=p->left; i<=p->right; i++)
		{
			e.depth[i]++;
		}
		e.breakpoint[p->left]++;
		e.breakpoint[p->right]++;
	}

	int border = (hit_size/4 < 50? hit_size/4 : 50); //search for the highest depth position in the internal region, skip near-ends

	//define high-depth block for each read
	//find position with max depth
	int index=0;
	int max = 0;
	for (int i=border; i<e.size-border; i++)
	{
		if (e.depth[i] > max)
		{
			max = e.depth[i];
			index = i;
		}
	}
	if (max < depth)
	{
		return 0;
	}
	bound.size = size;
	bound.begin = 0;
	bound.end = e.size-1;
	//left boundary
	for (int i=index; i>=0; i--)
	{
		if (e.depth[i] < depth)
		{
			bound.begin = i+1;
			break;
		}
		int s_bk=0;
		int max_bk=0;
		int index_max_bk=i;
		for (int j=i; (j>i-10) && (j>=3); j--)
		{
			s_bk += e.breakpoint[j];
			if (e.breakpoint[j] > max_bk)
			{
				max_bk=e.breakpoint[j];
				index_max_bk=j;
			}
		}
		if ((s_bk >= e.depth[index_max_bk]*0.5) && (s_bk >= e.depth[index_max_bk] - depth))
		{
			bound.begin = index_max_bk;
			break;
		}
	}
	//right boundary
	for (int i=index+1; i<e.size; i++)
	{
		if (e.depth[i] < depth)
		{
			bound.end = i-1;
			break;
		}
		int s_bk=0;
		int max_bk=0;
		int index_max_bk=i;
		for (int j=i; (j<i+10) && (j<e.size-3); j++)
		{
			s_bk += e.breakpoint[j];
			if (e.breakpoint[j] > max_bk)
			{
				max_bk=e.breakpoint[j];
				index_max_bk=j;
			}
		}
		if ((s_bk >= e.depth[index_max_bk]*0.5) && (s_bk >= e.depth[index_max_bk] - depth))
		{
			bound.end = index_max_bk;
			break;
		}
	}
	//check high-depth block size
	if (bound.end-bound.begin+1 < hit_size)
	{
		return 0;
	}
//	cout<<"block size:\t"<<bound.end-bound.begin+1<<endl;
	return 1;
}
//define high-depth segment for each query, since input is ordered as query, 
//so deal with each query, and needn't to save intermediate result
int HighDseg_3 (istream & fin, BLOCK & blocks)
{
	vector<hit> hits;
	BOUND bound;
	int t;
	string ts;
	unsigned int q_id, old_q_id=0;
	char c;
	char ch[1000];

	//read in alignment, while deal with and generate breakpoint information on the same time
	int q_size, old_q_size;
	hit h;
	float ident;

	while (!fin.eof())
	{
		fin>>q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>ts>>ts>>ident>>q_size>>h.left>>h.right;
		fin.getline(ch, 1000);
		if (old_q_id == 0)
		{
			old_q_id = q_id;
			old_q_size = q_size;
		}
		if (q_id != old_q_id)
		{
			if (!hits.empty() && HighDseg_2(old_q_size, hits, bound))
			{
				blocks[old_q_id] = bound;
			}
			old_q_id = q_id;
			old_q_size = q_size;
			hits.clear();
		}
		if ((ident < hit_ident) || (h.right-h.left+1 < hit_size))
		{
			continue;
		}
		hits.push_back(h);
	}
	if (!hits.empty() && HighDseg_2(old_q_size, hits, bound))
	{
		blocks[old_q_id] = bound;
	}
	return 1;
}
//output sequence with provided boundaries
int OutBoundSeq(SEQ & seq, BLOCK & blocks, ostream & fout)
{
	BLOCK::iterator p = blocks.begin();
	while (p != blocks.end())
	{
		string s = seq[p->first].substr(p->second.begin, p->second.end - p->second.begin+1);
		fout<<">"<<p->first<<endl;
		fout<<s<<endl;
		p++;
	}
	return 0;
}
//output breakpoint information
int OutBreakpoint(BLOCK & blocks, ostream & fout)
{
	BLOCK::iterator p = blocks.begin();
	while (p != blocks.end())
	{
		fout<<p->first<<"\t"<<p->second.size<<"\t"
		<<p->second.begin<<"\t"<<p->second.end<<endl;
		p++;
	}
	return 0;
}

string ComplemSeq(string seq)
{
	string compl_seq;
	for (int i=seq.size()-1; i>=0; i--)
	{
		switch (seq[i])
		{
		case 'A' : compl_seq.push_back( 'T'); break;
		case 'G' : compl_seq.push_back('C'); break;
		case 'C' : compl_seq.push_back('G'); break;
		case 'T' : compl_seq.push_back('A'); break;
		default : compl_seq.push_back(seq[i]); break;
		}
	}
	return compl_seq;
}

char * ComplemSeq(char * seq)
{
	string s = seq;
	int length = s.size();
	int j=0;
	for (int i=length-1; i>=0; i--,j++)
	{
		switch (seq[i])
		{
		case 'A' : s[j] = 'T'; break;
		case 'G' : s[j] = 'C'; break;
		case 'C' : s[j] = 'G'; break;
		case 'T' : s[j] = 'A'; break;
		default : s[j] = seq[i]; break;
		}
	}
	char * compl_seq = new char[length+1];
	strcpy(compl_seq, s.c_str());
	return compl_seq;
}
int IDseq(set<unsigned int> & id, SEQ & seq, SEQ & idSeq)
{
	for (set<unsigned int>::iterator p = id.begin(); p != id.end(); p++)
	{
		idSeq[*p] = seq[*p];
	}
	return 0;
}

//convert text to binary format for pairwise alignment
int ConvertAlign_binary (istream & fin, FILE * fout)
{
	align_inf a;
	size_t d;

	while (!fin.eof())
	{
		fin>>a.q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>a.s_id>>a.flag>>a.ident>>a.q_length>>a.q_begin>>a.q_end>>a.s_length>>a.s_begin>>a.s_end;
		if (ConvertAlign_binary_2(a, d))
		{
			fwrite(&d, sizeof(size_t), 1, fout);
		}
	}
	return 0;
}

//convert text to binary format for pairwise alignment
bool ConvertAlign_binary_2 (align_inf & a, size_t & d)
{
	int flag_int;
	int join_type;
	int distant;
	
	//flag
	if (a.flag == '+')
	{
		flag_int = 0;
	}
	else
	{
		flag_int = 1;
	}

	/*
		join types:
		0:	equal
		1:	q contains s
		2:	s contains q
		3:	q left join s,
		4:	q right join s,

		distant joining?
		0:	neither is distant
		1:	q is distant
		2:	s is distant
		3:	both are distant
	
	*/
	distant = 0;

	if ((a.q_begin < min_extend) && (a.q_length-a.q_end-1 < min_extend) && (a.s_begin < min_extend) && (a.s_length-a.s_end-1 < min_extend))
	{
		join_type = 0;
	}
	else if ((a.s_begin < min_extend) && (a.s_length-a.s_end-1 < min_extend))
	{
		join_type = 1;
	}
	else if ((a.q_begin < min_extend) && (a.q_length-a.q_end-1 < min_extend))
	{
		join_type = 2;
	}
	else if (!flag_int)
	{
		if ((a.q_begin >= min_extend) && (a.q_length-a.q_end-1 < min_extend) && (a.s_begin < min_extend) && (a.s_length-a.s_end-1 >= min_extend))
		{
			join_type = 4;
			if ((a.q_begin >= max_extend) && (a.s_length-a.s_end-1 >= max_extend))
			{
				distant = 3;
			}
			else if ((a.q_begin < max_extend) && (a.s_length-a.s_end-1 >= max_extend))
			{
				distant = 2;
			}
			else if ((a.q_begin >= max_extend) && (a.s_length-a.s_end-1 < max_extend))
			{
				distant = 1;
			}
			else
			{
				distant = 0;
			}
		}
		else if ((a.q_begin < min_extend) && (a.q_length-a.q_end-1 >= min_extend) && (a.s_begin >= min_extend) && (a.s_length-a.s_end-1 < min_extend))
		{
			join_type = 3;
			if ((a.q_length-a.q_end-1 >= max_extend) && (a.s_begin >= max_extend))
			{
				distant = 3;
			}
			else if ((a.q_length-a.q_end-1 < max_extend) && (a.s_begin >= max_extend))
			{
				distant = 2;
			}
			else if ((a.q_length-a.q_end-1 >= max_extend) && (a.s_begin < max_extend))
			{
				distant = 1;
			}
			else
			{
				distant = 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else if (flag_int)
	{
		if ((a.q_begin >= min_extend) && (a.q_length-a.q_end-1 < min_extend) && (a.s_begin >= min_extend) && (a.s_length-a.s_end-1 < min_extend))
		{
			join_type = 4;
			if ((a.q_begin >= max_extend) && (a.s_begin >= max_extend))
			{
				distant = 3;
			}
			else if ((a.q_begin < max_extend) && (a.s_begin >= max_extend))
			{
				distant = 2;
			}
			else if ((a.q_begin >= max_extend) && (a.s_begin < max_extend))
			{
				distant = 1;
			}
			else
			{
				distant = 0;
			}
		}
		else if ((a.q_length-a.q_end-1 >= min_extend) && (a.q_begin < min_extend) && (a.s_begin < min_extend) && (a.s_length-a.s_end-1 >= min_extend))
		{
			join_type = 3;
			if ((a.q_length-a.q_end-1 >= max_extend) && (a.s_length-a.s_end-1 >= max_extend))
			{
				distant = 3;
			}
			else if ((a.q_length-a.q_end-1 < max_extend) && (a.s_length-a.s_end-1 >= max_extend))
			{
				distant = 2;
			}
			else if ((a.q_length-a.q_end-1 >= max_extend) && (a.s_length-a.s_end-1 < max_extend))
			{
				distant = 1;
			}
			else
			{
				distant = 0;
			}
		}
		else
		{
			return 0;
		}
	}

	//convert to binary data type
	//q_id(29 bit) + s_id(29 bit) + flag(1 bit) + type(3 bit) + distant (2 bit) = 64 bit
	size_t size_qid = (size_t)pow(double(2), 29) - 1;
	size_t size_sid = (size_t)pow(double(2), 29) - 1;
	size_t size_flag = (size_t)pow(double(2), 1) - 1;
	size_t size_type = (size_t)pow(double(2), 3) - 1;
	size_t size_distant = (size_t)pow(double(2), 2) - 1;

	d = ((size_qid & a.q_id) << 35) | ((size_sid & a.s_id) << 6) | ((size_flag & flag_int) << 5) 
		| ((size_type & join_type) << 2) | (size_distant & distant);
	return 1;
}
//print log
void outLog(char * s)
{
	if (!quiet)
	{
		cout<<s<<endl;
	}
}

inline kmer_t ComplemMer(const kmer_t mer)
{
	kmer_t compl_mer = 0;
	for (int i=0; i<K; i++)
	{
		int c = (mer>>(i*2)) & 3;
		compl_mer = (compl_mer<<2) + (3-c); 
	}
	return compl_mer;
}

inline kmer_t DigitalMer(const string sm)
{
	kmer_t mer = 0;
	for (int i = 0; i != sm.size(); i++)
	{
		int ele;
		switch(sm[i])
		{
			case 'A' : ele = 0; break;
			case 'G' : ele = 1; break;
			case 'C' : ele = 2; break;
			case 'T' : ele = 3; break;
			default : return 0;
		}
		mer = (mer<<2)+ele;
	}
	return mer;
}

//input kmer2reads, output read ids of containing >=M kmers
set<read_t> RepeatReads(FILE * fin, const int M)
{
	K = fgetc(fin);
	D = fgetc(fin);
	size_t R_part = (size_t)pow(double(2), 64-K*2) - 1;
	size_t M_part = ~R_part;
	//
	size_t merRead;
	read_t read_id;
	map<read_t, unsigned int> num_mers;
	size_t count = 0;
	size_t num_lines;
	fread(&num_lines,sizeof(size_t),1,fin);
	while (fread(&merRead,sizeof(size_t),1,fin))
	{
		count++;
//		cout<<merRead<<endl;
		read_id = (merRead & R_part);  //it's numbered from 0, so should +1 here
		read_id +=1;
		if (num_mers.end() == num_mers.find(read_id))
		{
			num_mers[read_id] = 1;
		}
		else
		{
			num_mers[read_id]++;
		}
	}
	set<read_t> reads;
	for (map<read_t, unsigned int>::iterator p=num_mers.begin(); p!=num_mers.end(); p++)
	{
		int n_m = p->second;
		if (n_m >= M)
		{
			reads.insert(p->first);
		}
	}
	return reads;
}

//check whether the kmer is a simple kmer
inline bool isSimple_kmer(const kmer_t mer)
{
	map<int, int> occur;
	for (int i=1; i<4; i++)
	{
		occur.clear();
		size_t u_tl = (size_t)pow((double)4,i) - 1;
		for (int j=0; j<=K-i; j++)
		{
			int uni = (mer>>(j*2))&u_tl;
			if (occur.find(uni) == occur.end())
			{
				occur[uni] = 1;
			}
			else
			{
				occur[uni]++;
			}
		}
		for (map<int,int>::iterator pm=occur.begin(); pm!=occur.end(); pm++)
		{
			if (pm->second > K*3/4/i)
			{
				return 1;  //simple
			}
		}
	}
	return 0;  //not simple
}

//check whether the read is a repeat containing read
bool isRepeatRead(const read_t read_id, const set<read_t> & reads)
{
	if (reads.find(read_id) == reads.end())
	{
		return 0;  //not exist
	}
	else
	{
		return 1;  //does exist
	}
}
//read Mer-Reads index file(binary file)
int RMerIndex(FILE * fin, const set<read_t> & reads, index_kmer_read & mer_reads, index_read_kmer & read_mers)
{
	//update Kmer size, it's a global parameter
	K = fgetc(fin);
	D = fgetc(fin);
	size_t R_part = (size_t)pow(double(2), 64-K*2) - 1;
	size_t M_part = ~R_part;
	//
	size_t merRead;
	kmer_t mer_id;
	read_t read_id;
	size_t count = 0;
	while (fread(&merRead,sizeof(size_t),1,fin))
	{
		count++;
		mer_id = (merRead & M_part) >> (sizeof(size_t)*8 - K*2);
		read_id = merRead & R_part;
		read_id+=1;   //it's numbered from 0, so should +1 here

		//is it a simple k-mer?
		if (isSimple_kmer(mer_id))
		{
			continue;
		}
		if (!isRepeatRead(read_id, reads))
		{
			continue;
		}
		mer_reads[mer_id].insert(read_id);
		read_mers[read_id].insert(mer_id);
	}
//	cout<<"read in "<<count<<" mer-reads index"<<endl;
	return 1;
}


int RandomSubset(set<read_t> & reads, const float fraction)
{
	if (1 == fraction)
	{
		return 1;
	}
	set<read_t> new_reads;
	vector<char> flag;
	flag.resize(reads.size(), 0);
	for (unsigned int i=0; i<reads.size() * fraction; i++)
	{
		flag[i]=1;
	}
	srandom((size_t)time(NULL));
	for (unsigned int i=0; i<reads.size(); i++)
	{
		int index_1 = random() % reads.size();
		int index_2 = random() % reads.size();
		char tmp = flag[index_1];
		flag[index_1] = flag[index_2];
		flag[index_2] = tmp;
	}
	int count=0;
	for (set<read_t>::iterator p=reads.begin(); p!=reads.end(); p++)
	{
		if (flag[count])
		{
			new_reads.insert(*p);
		}
		count++;
	}
	reads = new_reads;
	return 1;
}

//randomize the order
vector<read_t> RandomOrder(set<read_t> & reads)
{
	vector<read_t> t(reads.size());
	copy(reads.begin(), reads.end(), t.begin());
	int total = t.size();
	srandom((size_t)time(NULL));
	for (int i=0; i<total; i++)
	{
		int index_1 = random() % total;
		int index_2 = random() % total;
		read_t tmp = t[index_1];
		t[index_1] = t[index_2];
		t[index_2] = tmp;
	}
	return t;
}

//randomize the order
void RandomOrder(vector<read_t> & reads)
{
	vector<read_t> t(reads.size());
	copy(reads.begin(), reads.end(), t.begin());
	int total = t.size();
	srandom((size_t)time(NULL));
	for (int i=0; i<total/2; i++)
	{
		int index_1 = random() % total;
		int index_2 = random() % total;
		read_t tmp = t[index_1];
		t[index_1] = t[index_2];
		t[index_2] = tmp;
	}
	reads = t;
}
//calculate time used
time_t calculate_time()
{
	static time_t old=time(NULL);
	return time(NULL) - old;
}

//find masked region
int find_mask(istream & f, BLOCK & b, set<read_t> & entire_simple)
{
	string simple = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN";
	BOUND a;
	char c;
	char ch[1000];
	string s;
	read_t id;
	f>>c;
	while (!f.eof())
	{
		f>>id;
		f.getline(ch,1000);
		s.clear();
		f>>c;
		while ((c != '>') && (!f.eof()))
		{
			s.push_back(c);
			f>>c;
		}
		a.size = s.size();
		a.begin = s.find(simple);
		a.end = s.rfind(simple);
		//if entirely compounded of simple sequence which was masked
		if (a.size - a.begin - a.end < hit_size)
		{
			entire_simple.insert(id);
			continue;
		}
		//keep simple sequence region information with size >= hit_size, but ignore shorter ones
		if (a.end-a.begin >=hit_size)
		{
			b[id] = a;
		}
	}
	return 0;
}

//is the aligned region almost entirely composed of simple sequence
bool isEntireSimpleAlign(BLOCK & b, align_inf & a)
{
	int left;
	int right;
	//on query
	left = b[a.q_id].begin > a.q_begin?  b[a.q_id].begin : a.q_begin;
	right = b[a.q_id].end < a.q_end? b[a.q_id].end : a.q_end;
	if (((a.q_end-a.q_begin) - (right-left) < nonlcs_size) && (right-left+1 > (a.q_end-a.q_begin+1) *0.5))
	{
		return 1;
	}
	//on sbjct
	left = b[a.s_id].begin > a.s_begin? b[a.s_id].begin : a.s_begin;
	right = b[a.s_id].end < a.s_end? b[a.s_id].end : a.s_end;
	if (((a.s_end-a.s_begin) - (right-left) < nonlcs_size) && (right-left+1 > (a.s_end-a.s_begin+1) *0.5))
	{
		return 1;
	}
	//not entirely simple
	return 0;
}

//is high-identity parent-child relationship? so that will needn't to do pairwise alignment for the child sequence next time
read_t isChild(align_inf & a)
{
	if (a.ident < child_ident)
	{
		return 0;
	}
	//only need to check for the sbjct sequence
	if ((a.s_begin < 10) && (a.s_length - a.s_end-1 < 10))
	{
		return a.s_id;
	}
	return 0;
}

//generate seeds
set<string> generate_seed(string & s)
{
	int seed_size = 10;
	int step = 10;
	set<string> seeds;
	for (int i=0; i<s.size()-seed_size; i+=step)
	{
		string e = s.substr(i, seed_size);
		seeds.insert(e);
	}
	return seeds;
}

bool matched_mers(set<string> & v, string & s, int threshold)
{
	int num_match = 0;
	for (set<string>::iterator p=v.begin(); p!=v.end(); p++)
	{
		if (s.find(*p) != s.size())
		{
			num_match++;
			if (num_match >= threshold)
			{
				break;
			}
		}
	}
	if (num_match >= threshold)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int read_to_seed(SEQ & seq, index_read_kmer & read_mer, index_kmer_read & mer_reads_direct, index_kmer_read & mer_reads_compl)
{
	int step = 10;
	for (SEQ::iterator p=seq.begin(); p!=seq.end(); p++)
	{
		for (int i=0; i<p->second.size()-K; i+=step)
		{
			string e = p->second.substr(i, K);
			kmer_t m = DigitalMer(e);
			if (isSimple_kmer(m))
			{
				continue;
			}
			kmer_t c = ComplemMer(m);
			read_mer[p->first].insert(m);
			mer_reads_direct[m].insert(p->first);
			mer_reads_compl[c].insert(p->first);
		}
	}
	return 0;
}
unsigned int cluster_link(map<read_t, unsigned int> & id_cluster, map<unsigned int, set<read_t> > & cluster_ids, FILE * fin)
{
	size_t d;
	read_t q_id;
	read_t s_id;
	size_t part_qid = (size_t)pow(double(2), 29)-1;
	size_t part_sid = (size_t)pow(double(2), 29)-1;
	map<read_t, set<read_t> > r;  //relationship
	while (fread(&d, sizeof(size_t), 1, fin))
	{
		q_id = part_qid & (d>>35);
		s_id = part_sid & (d>>6);
		r[q_id].insert(s_id);
		r[s_id].insert(q_id);
	}
	//clustering
	int n=0;
	while (!r.empty())
	{
		set<read_t> old_list;
		set<read_t> new_list;
		new_list.insert(r.begin()->first);
		while (new_list.size())
		{
			old_list.insert(new_list.begin(),new_list.end());
			set<read_t> tmp_list;
			for (set<read_t>::iterator p=new_list.begin(); p!=new_list.end(); p++)
			{
				tmp_list.insert(r[*p].begin(), r[*p].end());
				r.erase(*p);
			}
			new_list.clear();
			set_difference(tmp_list.begin(),tmp_list.end(),old_list.begin(),old_list.end(),inserter(new_list, new_list.begin()));
		}
		n++;
		cluster_ids[n] = old_list;
	}
	for (map<unsigned int, set<read_t> >::iterator p=cluster_ids.begin(); p!=cluster_ids.end(); p++)
	{
		for (set<read_t>::iterator z=p->second.begin(); z!=p->second.end(); z++)
		{
			id_cluster[*z] = p->first;
		}
	}
	return cluster_ids.size();
/*		if ((id_cluster.end() != id_cluster.find(q_id)) && (id_cluster.end() != id_cluster.find(s_id)))
		{
			//unite two clusters, with query cluster id. 
			if (id_cluster[q_id] != id_cluster[s_id])
			{
				for (list<read_t>::iterator p=cluster_ids[id_cluster[s_id]].begin(); p!=cluster_ids[id_cluster[s_id]].end(); p++)
				{
					id_cluster[*p] = id_cluster[q_id];
				}
				cluster_ids[id_cluster[q_id]].insert(cluster_ids[id_cluster[q_id]].end(),cluster_ids[id_cluster[s_id]].begin(), cluster_ids[id_cluster[s_id]].end());
				cluster_ids.erase(id_cluster[q_id]);
			}
		}
		else if (id_cluster.end() != id_cluster.find(q_id))
		{
			id_cluster[s_id] = id_cluster[q_id];
			cluster_ids[id_cluster[s_id]].insert(cluster_ids[id_cluster[s_id]].end(), s_id);
		}
		else if (id_cluster.end() != id_cluster.find(s_id))
		{
			id_cluster[q_id] = id_cluster[s_id];
			cluster_ids[id_cluster[q_id]].insert(cluster_ids[id_cluster[q_id]].end(), q_id);
		}
		else {
			n++;
			id_cluster[q_id] = n;
			id_cluster[s_id] = n;
			cluster_ids[n].insert(cluster_ids[n].end(), q_id);
			cluster_ids[n].insert(cluster_ids[n].end(), s_id);
		}
	}
	//adjust cluster numbers
	vector<unsigned int> c;
	for (map<unsigned int, list<read_t> >::iterator p=cluster_ids.begin(); p!=cluster_ids.end(); p++)
	{
		c.push_back(p->first);
	}
	sort(c.begin(), c.end());
	map<unsigned int, unsigned int> a;
	for (int i=0; i<c.size(); i++)
	{
		a[c[i]] = i+1;
	}
	map<unsigned int, list<read_t> > tmp;
	for (map<unsigned int, list<read_t> >::iterator p=cluster_ids.begin(); p!=cluster_ids.end(); p++)
	{
		tmp[a[p->first]] = p->second;
	}
	cluster_ids = tmp;
	for (map<read_t, unsigned int>::iterator p=id_cluster.begin(); p!=id_cluster.end(); p++)
	{
		p->second = a[p->second];
	}
	return c.size();*/
}
int separate_link(map<unsigned int, list<size_t> > & sep_link, map<read_t, unsigned int> & id_cluster, FILE * fin)
{
	size_t d;
	read_t q_id;
	size_t part_qid = (size_t)pow(double(2), 29)-1;
	while (fread(&d, sizeof(size_t), 1, fin))
	{
		size_t a=d;
		q_id = part_qid & (d>>35);
		sep_link[id_cluster[q_id]].insert(sep_link[id_cluster[q_id]].end(), a);
	}
	return 0;
}
