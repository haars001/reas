/*
*Filename:  Faster_SegLink
*Description:  faster version of getting pairwise linkage information between segments. It read data into memory, use sw to perform 
pairwise alignment. pthread technique was used. similar with Faster_HighDseg, it speeded up a lot, but requires huge memory. run_SegLink.pl
is the alternative choice.

*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2006.07.22
*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include<pthread.h>
#include "ReAS.h"
#include "Utilities.h"
#include "sw.h"

using namespace std;

extern int hit_size;
extern int nonlcs_size;
extern float hit_ident;
extern int bk_size;
extern int min_extend;
extern int max_extend;
extern char * score_matrix;
extern float child_ident;
extern bool avoid_child_align;

int num_cpus = 1;

FILE * fout_link = fopen("seg.link", "w");

BLOCK lcs_mask;
SEQ seg;
index_kmer_read mer_reads;
index_read_kmer read_mers;
set<read_t> seg_ids;
set<read_t> pure_child;  //the set of read ids which are totally covered by the other reads.
int total_seg;

//mutex
pthread_mutex_t mutex_seg_ids;
pthread_mutex_t mutex_read_seq;
pthread_mutex_t mutex_child;
pthread_mutex_t mutex_output;
pthread_mutex_t mutex_mer_reads;


void * sw_link(void *)
{
	sw s;
	s.set_params(score_matrix, 20);

	while (1)
	{
		pthread_mutex_lock(&mutex_seg_ids);
		if (seg_ids.empty())
		{
			pthread_mutex_unlock(&mutex_seg_ids);
			break;
		}
		if (0 == (total_seg-seg_ids.size()) % 100)
		{
			float percent = float(total_seg-seg_ids.size())*100/total_seg;
			cout<<"Finished:\t"<<total_seg-seg_ids.size()<<" sequences,\t"<<percent<<"%\tTotal:\t"<<total_seg<<"\t"<<calculate_time()<<"sec"<<endl;
		}
		read_t id = *seg_ids.begin();
		seg_ids.erase(seg_ids.begin());
		pthread_mutex_unlock(&mutex_seg_ids);

		//skip those which are totally covered by another seg
		bool t = 0;
		pthread_mutex_lock(&mutex_child);
		if (pure_child.end() != pure_child.find(id))
		{
			t = 1;
		}
		pthread_mutex_unlock(&mutex_child);
		if (t)
		{
			continue;
		}
		//
		set<read_t> sharing_reads;
		//
		pthread_mutex_lock(&mutex_mer_reads);
		for (set<kmer_t>::iterator p=read_mers[id].begin(); p!=read_mers[id].end(); p++)
		{
			sharing_reads.insert(mer_reads[*p].begin(), mer_reads[*p].end());
		}
		pthread_mutex_unlock(&mutex_mer_reads);
		//pairwise alignment
		vector<size_t> bin_link;
		pthread_mutex_lock(&mutex_read_seq);
		char * query = new char[seg[id].size()+1];
		strcpy(query, seg[id].c_str());
		pthread_mutex_unlock(&mutex_read_seq);
		Profile * q_profile;
		q_profile = s.make_profile_from_seq(query, 0);
		for (set<read_t>::iterator p=sharing_reads.begin(); p!=sharing_reads.end(); p++)
		{
			//save time, only align pairs which s_id > q_id
			if (*p <= id)
			{
				continue;
			}
			char flag = '+';
			pthread_mutex_lock(&mutex_read_seq);
			char *sbjct = new char[seg[*p].size()+1];
			char *sbjct_compl = new char[seg[*p].size()+1];
			strcpy(sbjct, seg[*p].c_str());
			strcpy(sbjct_compl, ComplemSeq(seg[*p]).c_str());
			pthread_mutex_unlock(&mutex_read_seq);

			AlignResult align_result = s.revised_swat(q_profile, sbjct);
			AlignResult align_comple = s.revised_swat(q_profile, sbjct_compl);

			delete(sbjct);
			delete(sbjct_compl);

			if ((align_result.iden * (align_result.q_end - align_result.q_start +1)) 
				< (align_comple.iden * (align_comple.q_end - align_comple.q_start +1)))
			{
				flag = '-';
				align_result = align_comple;
				int s_start = align_result.s_len - align_result.s_end-1;
				int s_end = align_result.s_len - align_result.s_start-1;
				align_result.s_start = align_result.s_start = s_start;
				align_result.s_end = align_result.s_end;
			}
			if ((align_result.iden < hit_ident) || (align_result.q_end - align_result.q_start+1 <hit_size))
			{
				continue;
			}
			align_inf a;
			a.q_id = id;
			a.s_id = *p;
			a.flag = flag;
			a.ident = align_result.iden;
			a.q_length = align_result.q_len;
			a.q_begin = align_result.q_start;
			a.q_end = align_result.q_end;
			a.s_length = align_result.s_len;
			a.s_begin = align_result.s_start;
			a.s_end = align_result.s_end;
			if (isEntireSimpleAlign(lcs_mask, a))
			{
				continue;
			}
			//convert to binary output format
			size_t d;
			if (ConvertAlign_binary_2(a, d))
			{
				bin_link.push_back(d);
			}
			if (avoid_child_align && isChild(a))
			{
				pthread_mutex_lock(&mutex_child);
				pure_child.insert(isChild(a));
				pthread_mutex_unlock(&mutex_child);
			}
		}
		s.free_profile(q_profile);
		delete(query);

		if (bin_link.size() > 0)
		{
			pthread_mutex_lock(&mutex_output);
			for (vector<size_t>::iterator p = bin_link.begin(); p != bin_link.end(); p++)
			{
				fwrite(&(*p), sizeof(size_t), 1, fout_link);
			}
			pthread_mutex_unlock(&mutex_output);
		}
	}
}

int faster_SegLink(FILE * f_kmer, istream & f_seg, istream & f_mask)
{
	set<read_t> entire_simple;
	find_mask(f_mask, lcs_mask, entire_simple);
	Rnonlist_seq (f_seg, entire_simple, seg);
	entire_simple.clear();
	for (SEQ::iterator p=seg.begin(); p!=seg.end(); p++)
	{
		seg_ids.insert(p->first);
	}
	total_seg = seg_ids.size();
	cout<<"Total number of segments:\t"<<seg_ids.size()<<"\t"<<calculate_time()<<"sec"<<endl;
	if (seg_ids.empty())
	{
		return(1);
	}
	RMerIndex(f_kmer, seg_ids, mer_reads, read_mers);
	cout<<"Read in kmer2reads index:\t"<<calculate_time()<<"sec"<<endl;

	//pthread
	//initialize mutex
	if (pthread_mutex_init(&mutex_seg_ids, NULL))
	{
		cerr<<"failed to initialize mutex_seg_ids"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_read_seq, NULL))
	{
		cerr<<"failed to initialize mutex_read_seq"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_output, NULL))
	{
		cerr<<"failed to initialize mutex_output"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_child, NULL))
	{
		cerr<<"failed to initialize mutex_child"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_mer_reads, NULL))
	{
		cerr<<"failed to initialize mutex_mer_reads"<<endl;
		exit(1);
	}
	//create
	vector<pthread_t> pthread_ids(num_cpus);
	for (int i=0; i<num_cpus; i++)
	{
		if (pthread_create(&pthread_ids[i], NULL, sw_link, NULL))
		{
			cerr<<"creat pthread error"<<endl;
			exit (1);
		}
	}

	//join
	for (int i=0; i<num_cpus; i++)
	{
		pthread_join(pthread_ids[i], NULL);
	}
	return 0;
}


//usage:
void usage(void)
{
	cout<<"Usage:	Faster_SegLink [options]\n"
		<<"	-x	STRING	kmer2reads file, default=<stdin>\n"
		<<"	-r	STRING	segment sequences file, default=seg.fa\n"
		<<"	-m	STRING	low-complexity masked sequences file, default=seg.mask.fa\n"
		<<"	-o	STRING	output sequence linkage file, default=seg.link\n"
		<<"	-a	INT	number of parallel processors, default=1\n"
		<<"Advanced:\n"
		<<"	-p	BOOL	best sensitivity or fastest speed? default=0(sensitive)\n"
		<<"	-s	INT	size threshold of pairwise hits, default=100\n"
		<<"	-i	FLOAT	identity threshold of pairwise hits, default=0.7\n"
		<<"	-j	INT	threshold of none-LCS overlap, default = 50\n"
		<<"	-c	STRING	score matrix for pariwise alignment, default=mat70\n"
		<<"	-z	FLOAT	child identity, default=0.95\n"
		<<"	-u	INT	minimal extention size, default=50\n"
		<<"	-v	INT	minimal extention size, default=200\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	FILE * fin_kmer2reads = stdin;
	char * file_reads = "seg.fa";
	char * file_mask = "seg.mask.fa";
	//[options]
	int c;
	while((c=getopt(argc,argv,"x:r:m:p:s:i:j:a:c:o:z:u:v:h")) != -1) {
			switch(c) {
					case 'x': fin_kmer2reads=fopen(optarg, "r"); break;
					case 'r': file_reads = optarg; break;
					case 'm': file_mask = optarg; break;
					case 'p': avoid_child_align = optarg; break;
					case 's': hit_size = atoi(optarg); break;
					case 'i': hit_ident = atof(optarg); break;
					case 'j': nonlcs_size = atoi(optarg); break;
					case 'a': num_cpus = atoi(optarg); break;
					case 'c': score_matrix = optarg; break;
					case 'o': fout_link = fopen(optarg, "w"); break;
					case 'z': child_ident = atof(optarg); break;
					case 'u': min_extend = atoi(optarg); break;
					case 'v': max_extend = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	ifstream fin_reads(file_reads);
	if (!fin_reads)
	{
		cerr<<"failed to open file "<<file_reads<<endl;
		exit(1);
	}
	ifstream fin_mask(file_mask);
	if (!fin_mask)
	{
		cerr<<"failed to open file "<<file_mask<<endl;
		exit(1);
	}
	if (!fout_link)
	{
		cerr<<"failed to open file seg.link"<<endl;
		exit(1);
	}
	if (!fin_kmer2reads)
	{
		cerr<<"failed to open file kmer2reads"<<endl;
		exit(1);
	}
	//
	faster_SegLink(fin_kmer2reads, fin_reads, fin_mask);
	fin_reads.close();
	fin_mask.close();
	fclose(fin_kmer2reads);
	fclose(fout_link);
	//
	return 0;
}
