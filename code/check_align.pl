#!/usr/bin/perl -w
#*Filename: check_align.pl
#*Description: 
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.07.06
#

use strict;

while (<stdin>) {
	if (/^\d+\t\d+\t[\+\-]\t\d+(\.\d+)?\t\d+\t\d+\t\d+\t\d+\t\d+\t\d+\n$/) {
		print $_;
	}
}
