#!/usr/bin/perl -w
#*Filename: reas_all.pl
#*Description: the integrated pipeline
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.09.12
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
	-read	STRING	reads file, fasta format
	-k	INT	k-mer size, default=17
	-d	INT	depth threshold, default=6
	-m	INT	threshold of number of high-depth k-mers when defining repeat, default=1
	-subset	FLOAT	randomly pick up a fraction of reads for assembly, default=1(all)
	-n	INT	split the data file into number of pieces, default=1
	-pa	INT	number of parallel processors, default=1
	-bound	STRING	output repeat boundaries file, default="seg.bk"
	-link	STRING	output bigraph linkage file, default="seg.link"
	-multi	INT	a subset for multi-alignment, default=-1(all), suggest 1000 if set
	-output	STRING	output final consensus file, default="consensus.fa"
	-prefix	STRING	prefix of consensus sequence name, default=""
	-log		output log details, default=[not]
	-seqsize INT minimal length of input sequences to use


Advanced options:

	-size	INT	size threshold of pairwise hits, default=100
	-ident	FLOAT	identity threshold of pairwise hits, default=0.6
	-nonlcs	INT	size threshold of non-LCS overlap, default=50
	-end	INT	size of remaining sequence at ends when defining breakpoints, default=50bp
	-min_depth	INT	minimal depth at extension, default=depth/3
	-min_extend	INT	minimal extension size, default=50
	-max_extend	INT	maximum extension size, default=200


	-h	help
notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts, "read=s", "k=i", "d=i", "m=i", "subset=s", "n=i", "pa=i", "bound=s", "link=s", "multi=i", "output=s",
"prefix:s", "log", "size=i", "ident=s", "nonlcs=i", "end=i", "min_depth=i", "min_extend=i", "max_extend=i", "seqsize=i", "h");

die $usage if defined $opts{h};
my $read_file = defined $opts{read}? $opts{read} : die();
my $k = defined $opts{k} ? $opts{k} : 17;
my $d = defined $opts{d} ? $opts{d} : 6;
my $m = defined $opts{m} ? $opts{m} : 1;
my $fraction = defined $opts{subset}? $opts{subset} : 1;
my $n = defined $opts{n}? $opts{n} : 1;
my $pa = defined $opts{pa}? $opts{pa} : 1;
my $bound_file = defined $opts{bound}? $opts{bound} : "seg.bk";
my $link_file = defined $opts{link}? $opts{link} : "seg.link";
my $sub_multi = defined $opts{multi}? "-t $opts{multi}" : "";
my $output = defined $opts{output}? $opts{output} : "consensus.fa";
my $prefix = defined $opts{prefix}? "-p $opts{prefix}" : "";
my $log = defined $opts{log}? "-s" : "";
my $size = defined $opts{size}? $opts{size} : 100;
my $seqsize = defined $opts{seqsize}? $opts{seqsize} : 100;
my $ident = defined $opts{ident}? $opts{ident} : 0.6;
my $nonlcs = defined $opts{nonlcs}? $opts{nonlcs} : 50;
my $end = defined $opts{end}? $opts{end} : 50;
my $min_depth = defined $opts{min_depth}? "-j $opts{min_depth}" : "";
my $min_extend = defined $opts{min_extend}? "-u $opts{min_extend}" : "";
my $max_extend = defined $opts{max_extend}? "-v $opts{max_extend}" : "";

my $clean_read_file = "reads.clean.fa";
my $nmers_file = "read.N_mers";
my $hd_read_id_file = "HD_reads.id";
my $hd_read_file = "HD_reads.fa";
my $seg_file = "seg.fa";
`cat $read_file |CleanData.pl -size=$seqsize |rename.pl >$clean_read_file`;
`kmer_num -k $k $clean_read_file| kmer2reads -d $d $clean_read_file |N_mers -m $m >$nmers_file`;
`cat $nmers_file |cut -f1 |RandomList.pl $fraction >$hd_read_id_file`;
`cat $clean_read_file |pickListSeq.pl $hd_read_id_file >$hd_read_file`;
`run_HighDseg.pl -r $hd_read_file -n $n -a $pa -b $bound_file $size $ident -d $d`;
`cat $bound_file |cutSeg.pl $hd_read_file >$seg_file`;
`run_SegLink.pl -r $hd_read_file -n $n -a $pa $size $ident $nonlcs -o $link_file`;
`DoAssembly -r $seg_file -l $link_file $sub_multi -b $bound_file -d $d -o $output $prefix  $log -e $end $min_depth $min_extend $max_extend`;
