#!/usr/bin/perl -w
#*Filename: ConvertAlign.pl
#*Description: convert pairwise alignment into the standard format, and setting threshold to remove low-quality alignment
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.11
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat align.input | $0 [options] <stdout>
	-s	INT	size cutoff of hits, default=100bp
	-i	FLOAT	identity threshold of pairwise hits, default=0.6
	-t	INT	<0: cross_match |1: patternhunter |2: blastn> alignment tool, default=0
		if 2, <query.size> <sbjct.size>
	-h	help

notes: output format:
q_id, s_id, +/-, ident, q_size, q_begin, q_end, s_size, s_begin, s_end

positions counted from 0.

USAGE
#************************************************

my %opts;
GetOptions(\%opts , "s:i" , "i:s" , "t:s", "r", "h");

die $usage if defined $opts{h};
my $hit_size = defined $opts{s}? $opts{s} : 100;
my $hit_ident = defined $opts{i}? $opts{i} : 0.6;
my $align_tool = defined $opts{t}? $opts{t} : 0;
my $reas  = defined $opts{r}? 1 : 0;

my $bound = 50; #special for reas, disregard unjoinable link

my $q_id;
my $q_size;
my $ident;
my $flag;
my $q_begin;
my $q_end;
my $s_id;
my $s_size;
my $s_begin;
my $s_end;

if (0 == $align_tool) {
	while (<stdin>) {
		if (/\(\d+\)/) {
			$_ =~ s/^\s+//;
			my @a=split(/\s+/, $_);
			$ident = 1-($a[1] + $a[2] + $a[3])/100;
			if ($ident < $hit_ident) {
				next;
			}
			#
			$a[7] =~ s/\(//;
			$a[7] =~ s/\)//;
			$q_id = $a[4];
			$q_begin = $a[5] - 1;
			$q_end = $a[6] - 1;
			$q_size = $a[6] + $a[7];
			if ("C" eq $a[8]) {
				$flag = "-";
				$s_id = $a[9];
				$s_begin = $a[12] - 1;
				$s_end = $a[11] - 1;
				$a[10] =~ s/\(//;
				$a[10] =~ s/\)//;
				$s_size = $a[10] + $a[11];
			}
			else
			{
				$flag = "+";
				$s_id = $a[8];
				$s_begin = $a[9] - 1;
				$s_end = $a[10] - 1;
				$a[11] =~ s/\(//;
				$a[11] =~ s/\)//;
				$s_size = $a[11] + $a[10];
			}
			#
			if (($q_end-$q_begin+1 < $hit_size) && ($s_end-$s_begin+1 < $hit_size)) {
				next;
			}
			if ($q_id eq $s_id) {
				next;
			}
			if (!$reas) {
				print "$q_id\t$s_id\t$flag\t$ident\t$q_size\t$q_begin\t$q_end\t$s_size\t$s_begin\t$s_end\n";
			}
			else {
				if ("+" eq $flag) {
					if ((($q_size-$q_end >= $bound) && ($s_size-$s_end >= $bound)) || (($q_begin >= $bound) && ($s_begin >= $bound))) {
						next;
					}
				}
				elsif ("-" eq $flag) {
					if ((($q_begin >= $bound) && ($s_size-$s_end >= $bound)) || (($q_size-$q_end >= $bound) && ($s_begin >= $bound))) {
						next;
					}
				}
				else {
					next;
				}
				print "$q_id\t$s_id\t$flag\t$ident\t$q_size\t$q_begin\t$q_end\t$s_size\t$s_begin\t$s_end\n";
			}
		}
	}
}
elsif (1 == $align_tool) {
	while (<stdin>) {
		my @a=split(/\s+/, $_);
		$ident = $a[2]/100;
		if ($ident < $hit_ident) {
			next;
		}
		#
		$q_id = $a[0];
		$s_id = $a[1];
		if ($q_id eq $s_id) {
			next;
		}
		$q_size = $a[4];
		$s_size = $a[5];
		if ($a[6] > $a[7]) {
			$flag = "-";
			$q_begin = $a[7] - 1;
			$q_end = $a[6] - 1;
		}
		else {
			$flag = "+";
			$q_begin = $a[6] - 1;
			$q_end = $a[7] - 1;
		}
		$s_begin = $a[8] - 1;
		$s_end = $a[9] - 1;
		#
		if (($q_end-$q_begin+1 < $hit_size) && ($s_end-$s_begin+1 < $hit_size)) {
			next;
		}
		#
		print "$q_id\t$s_id\t$flag\t$ident\t$q_size\t$q_begin\t$q_end\t$s_size\t$s_begin\t$s_end\n";	
	}
}
elsif (2 == $align_tool) {
	my $query_size_file = shift;
	my $sbjct_size_file = shift;
	my %query_size;
	my %sbjct_size;
	open (f_q, $query_size_file) || die $usage;
	open (f_s, $sbjct_size_file) || die $usage;
	while (<f_q>) {
		if (/^(\S+)\s+(\d+)/) {
			$query_size{$1} = $2;
		}
	}
	close f_q;
	while (<f_s>) {
		if (/^(\S+)\s+(\d+)/) {
			$sbjct_size{$1} = $2;
		}
	}
	close f_s;
	while (<stdin>) {
		my @a = split(/\s+/, $_);
		if (($a[3] < $hit_size) || ($a[2]*0.01 < $hit_ident)) {
			next;
		}
		$q_id = $a[0];
		$s_id = $a[1];
		$ident = $a[2]*0.01;
		$q_size = $query_size{$q_id};
		$s_size = $sbjct_size{$s_id};
		$q_begin = $a[6]-1;
		$q_end = $a[7]-1;
		if ($a[8] < $a[9]) {
			$s_begin = $a[8]-1;
			$s_end = $a[9]-1;
			$flag = "+";
		}
		else {
			$s_begin = $a[9]-1;
			$s_end = $a[8]-1;
			$flag = "-";
		}
		print "$q_id\t$s_id\t$flag\t$ident\t$q_size\t$q_begin\t$q_end\t$s_size\t$s_begin\t$s_end\n";
	}
}
else {
	print "sorry, didn't support this input format now.\n";
	exit 1;
}
