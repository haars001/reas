#!/usr/bin/perl
#*Copyright 2003.03  BGI prj0310
#*All right reserved
#*Filename: seqSize.pl
#*Abstract
#*Version : 1.0
#*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
#*Time:    2003.03.29
#

if (@ARGV != 2)
{
	printf "Usage: seqSize.pl  InSeq.file  OutSize.file\n";
	exit 1;
}
open (fin,$ARGV[0]) || die;
open (fout,">$ARGV[1]") || die;

my $name = "";
my $size;

while (<fin>) {
	if (/>(\S+)/) {
		if ($name ne "") {
			printf fout "$name\t$size\n";
		}
		$name = $1;
		$size = 0;
	}
	else
	{
		chomp();
		$size += length($_);
	}
}
if ($name ne "") {
	printf fout "$name\t$size\n";
}
#
close fin;
close fout;
