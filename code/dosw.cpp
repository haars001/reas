#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "sw.h"

using namespace std;

int main(int argc, char *argv[])
{
	char seq1[1000];
	char seq2[1000];
	char seq1_name[256];
	char seq2_name[256];
//	char align_file[256];
//	FILE *pfalign;
	AlignResult * align_result;
	Profile *q_profile;
	
	sw  s;
//	s = new(sw);
	s.set_params("mat50", 0);

	strcpy(seq1_name, "seq1");
	strcpy(seq2_name, "seq2");

	strcpy(seq1, "ACTGAGTCAAGTTCAATGCAAAAADDFFGGGG");
	strcpy(seq2, "ACTGAGTCAAGTTACAATGCATAAAADDFFGGGG");
//You need the following two steps and the free_profile(q_profile) step when seq1 is finished
	q_profile = s.make_profile_from_seq(seq1, 0);
	align_result = s.revised_swat(q_profile, seq2);

	printf("%f\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n", align_result->iden, seq1_name, seq2_name, align_result->q_len, align_result->q_start, align_result->q_end, 
			align_result->s_len, align_result->s_start, align_result->s_end);
//	printf("%f\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n", align_result.iden, seq1_name, seq2_name, align_result.q_len, align_result.q_start, align_result.q_end, 
//			align_result.s_len, align_result.s_start, align_result.s_end);
	s.free_profile(q_profile);
	//delete align_result;
	return 0;

}
