#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include "LocalAlign.h"

using namespace std;

int main(int argc, char *argv[])
{
	string q_id, q_seq, s_id, s_seq;
	char c;
	char ch[1000];
	ifstream fin(argv[1]);
	fin>>c>>q_id>>q_seq;
	fin>>c>>s_id>>s_seq;
	fin.close();
	cout<<q_seq<<endl;
	cout<<s_seq<<endl<<endl;
	LocalAlign a;
//	a.DoAlign(q_seq, s_seq);
	a.InSeq(q_seq, s_seq);
	a.Initialize_ff_dd_ii_path();
	a.Score();
	a.Repath();
	cout<<a.OutPident()<<"\t"<<q_id<<"\t"<<q_seq.size()<<"\t"<<a.OutQueryBegin()<<"\t"<<a.OutQueryEnd()
		<<"\t"<<s_id<<"\t"<<s_seq.size()<<"\t"<<a.OutSbjctBegin()<<"\t"<<a.OutSbjctEnd()<<endl;
	cout<<a.OutQueryAlign()<<endl;
	cout<<a.OutSbjctAlign()<<endl<<endl;
    return 0;
}
