/*
Use pthread to do pairwise alignment of difining repeat segments. It's a faster version compare with run_HighDseg.pl, 
since only about 100~500 hits are needed to defined boundaries. But it needs a huge memory.
*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include<pthread.h>
#include "ReAS.h"
#include "Utilities.h"
#include "sw.h"
#include <algorithm>

using namespace std;

extern int K;
extern int D;
extern int M;
extern int depth;
extern int hit_size;
extern float hit_ident;
extern float sub_fraction;
extern int max_hits;
extern char * score_matrix;

int num_cpus = 1;
//bool store_align;

SEQ read_seq;
set<read_t> hd_reads;
index_kmer_read mer_reads;
index_read_kmer read_mers;
BLOCK blocks;
READ_RELATION tried_align;
map<read_t, vector<hit> > known_hits;
int total_reads;

//mutex
pthread_mutex_t mutex_hd_reads;
pthread_mutex_t mutex_read_seq;
pthread_mutex_t mutex_para;
pthread_mutex_t mutex_mer_reads;
pthread_mutex_t mutex_tried_align;
pthread_mutex_t mutex_known_hits;
pthread_mutex_t mutex_blocks;

//
//for each read, get the reads sharing kmers with it, do pairwise alignment to get sufficient hits, then define the repeat boundaries
void * sw_bound(void *)
{
	sw s;
	s.set_params(score_matrix, 30);
	//
	while (1)
	{
		pthread_mutex_lock(&mutex_hd_reads);
		if (hd_reads.empty())
		{
			pthread_mutex_unlock(&mutex_hd_reads);
			break;
		}
		if (0 == (total_reads-hd_reads.size()) % 100)
		{
			float percent = float(total_reads-hd_reads.size())*100/total_reads;
			cout<<"Finished:\t"<<total_reads-hd_reads.size()<<" reads,\t"<<percent<<"%\tTotal:\t"
			<<total_reads<<"\t"<<calculate_time()<<"sec"<<endl;
		}
		read_t read_id = *hd_reads.begin();
		hd_reads.erase(hd_reads.begin());
		pthread_mutex_unlock(&mutex_hd_reads);
		
		//get shared reads
		set<read_t> sharing_reads;
		pthread_mutex_lock(&mutex_mer_reads);
		for (set<kmer_t>::iterator p=read_mers[read_id].begin(); p!=read_mers[read_id].end(); p++)
		{
//			sharing_reads.insert(mer_reads[*p].begin(), mer_reads[*p].end());
			set_union(sharing_reads.begin(), sharing_reads.end(), mer_reads[*p].begin(), mer_reads[*p].end(), inserter(sharing_reads, sharing_reads.begin()));
		}
		pthread_mutex_unlock(&mutex_mer_reads);

		//remove known status (either aligned already, or proved unalignable). and remove itself
		if (sharing_reads.find(read_id) != sharing_reads.end())
		{
			sharing_reads.erase(read_id);
		}
		set<read_t> tmp_set;
		pthread_mutex_lock(&mutex_tried_align);
		if (tried_align.find(read_id) != tried_align.end())
		{
			set_difference(sharing_reads.begin(), sharing_reads.end(), tried_align[read_id].begin(), tried_align[read_id].end(), inserter(tmp_set, tmp_set.begin()));
			sharing_reads = tmp_set;
			tried_align.erase(read_id);
		}
		pthread_mutex_unlock(&mutex_tried_align);
		
		//get known hits
		vector<hit> hit_this_read;
		pthread_mutex_lock(&mutex_known_hits);
		if (known_hits.find(read_id) != known_hits.end())
		{
			hit_this_read = known_hits[read_id];
			known_hits.erase(read_id);
		}
		pthread_mutex_unlock(&mutex_known_hits);
		//
		if ((hit_this_read.size() + sharing_reads.size()) < depth)
		{
			continue;
		}
		
		//do pairwise alignment
		if ((hit_this_read.size() < max_hits) && (!sharing_reads.empty()))
		{
			pthread_mutex_lock(&mutex_read_seq);
			char * query = new char[read_seq[read_id].size()+1];
			strcpy(query, read_seq[read_id].c_str());
			pthread_mutex_unlock(&mutex_read_seq);
			
			Profile * q_profile;
			q_profile = s.make_profile_from_seq(query, 0);
			vector<read_t> randomOrdered = RandomOrder(sharing_reads);
			for (vector<read_t>::iterator p=randomOrdered.begin(); p!=randomOrdered.end(); p++)
			{
				//try direct and complementary chain
				pthread_mutex_lock(&mutex_read_seq);
				char *sbjct = new char[read_seq[*p].size()+1];
				char *sbjct_compl = new char[read_seq[*p].size()+1];
				strcpy(sbjct, read_seq[*p].c_str());
				strcpy(sbjct_compl, ComplemSeq(read_seq[*p]).c_str());
				pthread_mutex_unlock(&mutex_read_seq);
				
				AlignResult align_result = s.revised_swat(q_profile, sbjct);
				AlignResult align_comple = s.revised_swat(q_profile, sbjct_compl);

				if ((align_result.iden * (align_result.q_end - align_result.q_start +1)) 
					< (align_comple.iden * (align_comple.q_end - align_comple.q_start +1)))
				{
					align_result = align_comple;
					int s_start = align_result.s_len - align_result.s_end-1;
					int s_end = align_result.s_len - align_result.s_start-1;
					align_result.s_start = align_result.s_start = s_start;
					align_result.s_end = align_result.s_end;
				}
				if (align_result.iden >= hit_ident)
				{
					if (align_result.q_end - align_result.q_start +1 >= hit_size)
					{
						hit a;
						a.left = align_result.q_start;
						a.right = align_result.q_end;
						hit_this_read.push_back(a);
					}
					if (align_result.s_end - align_result.s_start +1 >= hit_size)
					{
						hit a;
						a.left = align_result.s_start;
						a.right = align_result.s_end;

						pthread_mutex_lock(&mutex_known_hits);
						known_hits[*p].push_back(a);
						pthread_mutex_unlock(&mutex_known_hits);
					}
				}
				delete(sbjct);
				delete(sbjct_compl);

				pthread_mutex_lock(&mutex_tried_align);
				tried_align[*p].insert(read_id);
				pthread_mutex_unlock(&mutex_tried_align);

				//after getting enough hits
				if (hit_this_read.size() >= max_hits)
				{
					break;
				}
			}
			s.free_profile(q_profile);
			delete(query);
		}
		//define high-depth boundary based on mapped hits
		pthread_mutex_lock(&mutex_read_seq);
		int q_size = read_seq[read_id].size();
		pthread_mutex_unlock(&mutex_read_seq);

		BOUND b;
		if (HighDseg_2(q_size, hit_this_read, b))
		{
			pthread_mutex_lock(&mutex_blocks);
			blocks[read_id] = b;
			pthread_mutex_unlock(&mutex_blocks);
		}
	}
}

int faster_HighDseg(FILE * fin_kmer2reads, istream & fin_reads)
{
	time_t used_time = calculate_time();
	hd_reads = RepeatReads(fin_kmer2reads, M);
	cout<<"Total number of high-depth reads:\t"<<hd_reads.size()<<"\t"<<calculate_time()<<"sec"<<endl;
	if (hd_reads.empty())
	{
		return(1);
	}
	RandomSubset(hd_reads, sub_fraction);
	cout<<"Randomly get a subset:\t"<<sub_fraction<<",\t"<<hd_reads.size()<<" reads\t"<<calculate_time()<<"sec"<<endl;
	total_reads = hd_reads.size();
	fseek(fin_kmer2reads, 0, SEEK_SET);
	RMerIndex(fin_kmer2reads, hd_reads, mer_reads, read_mers);
	cout<<"Read in kmer2reads index:\t"<<calculate_time()<<"sec"<<endl;
	//read in read sequences
	Rlist_seq(fin_reads, hd_reads, read_seq);
	cout<<"Read in read sequences:\t"<<calculate_time()<<"sec"<<endl;

	//pthread
	//initialize mutex
	if (pthread_mutex_init(&mutex_hd_reads, NULL))
	{
		cerr<<"failed to initialize mutex_hd_reads"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_read_seq, NULL))
	{
		cerr<<"failed to initialize mutex_read_seq"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_para, NULL))
	{
		cerr<<"failed to initialize mutex_para"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_tried_align, NULL))
	{
		cerr<<"failed to initialize mutex_tried_align"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_known_hits, NULL))
	{
		cerr<<"failed to initialize mutex_known_hits"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_blocks, NULL))
	{
		cerr<<"failed to initialize mutex_blocks"<<endl;
		exit(1);
	}
	if (pthread_mutex_init(&mutex_mer_reads, NULL))
	{
		cerr<<"failed to initialize mutex_mer_reads"<<endl;
		exit(1);
	}
	//create
	vector<pthread_t> pthread_ids(num_cpus);
	for (int i=0; i<num_cpus; i++)
	{
		if (pthread_create(&pthread_ids[i], NULL, sw_bound, NULL))
		{
			cerr<<"creat pthread error"<<endl;
			exit (1);
		}
	}

	//join
	for (int i=0; i<num_cpus; i++)
	{
		pthread_join(pthread_ids[i], NULL);
	}
	return 0;
}

//usage:
void usage(void)
{
	cout<<"Usage:	Faster_HighDseg [options]\n"
		<<"	-x	STRING	kmer2reads file, default=<stdin>\n"
		<<"	-r	STRING	reads file, default=reads.fa\n"
		<<"	-a	INT	number of parallel processors, default=1\n"
		<<"	-b	STRING	output breakpoints information file, default=seg.bk\n"
		<<"	-o	STRING	output high-depth segments file, default=seg.fa\n"
		<<"	-s	INT	size threshold of pairwise hits, default=100\n"
		<<"	-i	FLOAT	identity threshold of pairwise hits, default=0.7\n"
		<<"	-c	STRING	score matrix for pariwise alignment, default=mat70\n"
		<<"	-d	INT	depth cutoff of defining repeat, default=6\n"
		<<"	-m	INT	minimal number of hidh-depth kmers when picking up repeat containing reads, default=30\n"
		<<"	-f	FLOAT	randomly have a subset of reads for assembly, be sure the depth parameter should be adjusted synchronously. default=1\n"
		<<"	-n	INT	maximum number of hits needed to define repeat boundaries from reads, default=depth*20\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	FILE * fin_kmer2reads = stdin;
	char * file_reads = "reads.fa";
	char * file_bk = "seg.bk";
	char * file_seg = "seg.fa";

	//[options]
	int c;
	while((c=getopt(argc,argv,"x:r:a:b:o:s:i:c:d:m:f:n:h")) != -1) {
			switch(c) {
					case 'x': fin_kmer2reads = fopen(optarg, "r"); break;        
					case 'r': file_reads = optarg; break;
					case 'a': num_cpus = atoi(optarg); break;
					case 'b': file_bk = optarg; break;
					case 'o': file_seg = optarg; break;
					case 's': hit_size = atoi(optarg); break;
					case 'i': hit_ident = atof(optarg); break;
					case 'c': score_matrix = optarg; break;
					case 'd': depth = atoi(optarg); break;
					case 'm': M = atoi(optarg); break;
					case 'f': sub_fraction = atof(optarg); break;
					case 'n': max_hits = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	if (!fin_kmer2reads)
	{
		cerr<<"failed to open file kmer2reads"<<endl;
		exit(1);
	}
	ifstream fin_reads(file_reads);
	if (!fin_reads)
	{
		cerr<<"failed to open file "<<file_reads<<endl;
		exit(1);
	}
	ofstream fout_bk(file_bk);
	if (!fout_bk)
	{
		cerr<<"failed to open file "<<file_bk<<endl;
		exit(1);
	}
	ofstream fout_seg(file_seg);
	if (!fout_seg)
	{
		cerr<<"failed to open file "<<file_seg<<endl;
		exit(1);
	}
	faster_HighDseg(fin_kmer2reads, fin_reads);
	//output
	OutBreakpoint(blocks, fout_bk);
	OutBoundSeq(read_seq, blocks, fout_seg);

	//
	fclose(fin_kmer2reads);
	fin_reads.close();
	fout_bk.close();
	fout_seg.close();
	//
	return 0;
}
