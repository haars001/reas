#!/usr/bin/perl
#*Copyright 2005  BGI
#*All right reserved
#*Filename: picknonListSeq.pl
#*Description: pick sequences with IDs not in the list
#*Version : 1.0
#*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
#*Time:    2005.11.02
#

my ($listFile) = @ARGV;

open (F, $listFile) || die "can not open $listFile\n";

my %names;
while (<F>) {
	chomp;
	$names{$_} = 1;
}
close F;

my $flag;
while (<STDIN>) {
	if (/^>(\S+)/) {
		if (!defined $names{$1}) {
			$flag = 1;
		}
		else {
			$flag = 0;
		}
	}
	if ($flag) {
		printf $_;
	}
}
