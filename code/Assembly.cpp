#include <stdlib.h> 
#include <stdio.h> 
#include <time.h>
#include <math.h>
#include "Assembly.h"
#include "Utilities.h"
#include <algorithm>

extern int bk_size;
extern int depth;
extern float hit_ident;
extern int hit_size;
extern int min_extend;
extern int max_extend;
extern float avg_ident_bound;
extern int min_join;
extern string prefix;
extern char quiet;
extern int sub_multi;

#ifdef INTERNAL_MUSCLE
//external muscle interface function in muscle_reas.cpp file
extern int muscle_reas(SEQ & in_seq, SEQ & out_aln);

#else
//process for calling muscle
extern pid_t pid_muscle;
extern int fd_muscle[2];
extern int fd_muscle_r[2];
extern char * multi_fa;
extern char * multi_aln;
#endif

//
char log_s[10000];
Assembly::Assembly()
{
	ch_int['A'] = 0;
	ch_int['C'] = 1;
	ch_int['G'] = 2;
	ch_int['T'] = 3;
	ch_int['-'] = 4;
	//
	int_ch[0] = 'A';
	int_ch[1] = 'C';
	int_ch[2] = 'G';
	int_ch[3] = 'T';
	int_ch[4] = '-';
	//
	char_set.insert('A');
	char_set.insert('C');
	char_set.insert('G');
	char_set.insert('T');
	char_set.insert('-');
}
void Assembly::InSeq(SEQ & s)
{
	seq = s;
	for (SEQ::iterator p = seq.begin(); p != seq.end(); p++)
	{
		seq_size[p->first] = p->second.size();
	}
}
//input breakpoint information
void Assembly::InBreakpoint(ifstream & fin)
{
	BOUND b;
	unsigned int q_id;
	while (!fin.eof())
	{
		fin>>q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>b.size>>b.begin>>b.end;
		bk_point[q_id] = b;
		seq_size[q_id] = b.size;
	}
}
//read in linkage information among segments inside cluster
void Assembly::InLink_binary(FILE * fin)
{
	size_t d;
	unsigned int q_id, s_id;
	unsigned int flag;
	unsigned int join_type;
	unsigned int distant;

	JOIN join;

	size_t part_qid = (size_t)pow(double(2), 29)-1;
	size_t part_sid = (size_t)pow(double(2), 29)-1;
	size_t part_flag = (size_t)pow(double(2), 1)-1;
	size_t part_type = (size_t)pow(double(2), 3)-1;
	size_t part_distant = (size_t)pow(double(2), 2)-1;

	while (fread(&d, sizeof(size_t), 1, fin))
	{
		q_id = part_qid & (d>>35);
		s_id = part_sid & (d>>6);
		flag = part_flag & (d>>5);
		join_type = part_type & (d>>2);
		distant = part_distant & d;

//		cout<<q_id<<"\t"<<s_id<<"\t"<<flag<<"\t"<<join_type<<"\t"<<distant<<endl;
		if ((link.count(q_id) && link[q_id].count(s_id)) || (link.count(s_id) && link[s_id].count(q_id)))
		{
			continue;
		}
		/*
			flag:
			0:	'+'
			1:	'-'
			join types:
			0:	equal
			1:	q contains s
			2:	s contains q
			3:	q left join s,
			4:	q right join s,

			distant joining?
			0:	neither is distant
			1:	q is distant
			2:	s is distant
			3:	both are distant
		
		*/		
		if (0 == flag)
		{
			join.flag=0;   //'+'
			if (0 == join_type)
			{
				join.type = 0;
				link[q_id][s_id] = join;
				link[s_id][q_id] = join;
			}
			else if (1 == join_type)
			{
				join.type = 0;
				link[q_id][s_id] = join;
			}
			else if (2 == join_type)
			{
				join.type = 0;
				link[s_id][q_id] = join;
			}
			else if (3 == join_type)
			{
				if ((bk_point[q_id].begin >= bk_size) || (bk_point[s_id].size-bk_point[s_id].end-1 >= bk_size))
				{
					continue;
				}
				switch (distant)
				{
				case 0: {
							join.type = 1;
							link[q_id][s_id] = join;
							join.type = 2;
							link[s_id][q_id] = join; break;}
				case 1: {
							join.type = 1;
							link[q_id][s_id] = join;
							join.type = 4;
							link[s_id][q_id] = join; break;}
				case 2: {
							join.type = 3;
							link[q_id][s_id] = join;
							join.type = 2;
							link[s_id][q_id] = join; break;}
				case 3: {
							join.type = 3;
							link[q_id][s_id] = join;
							join.type = 4;
							link[s_id][q_id] = join; }
				}
			}
			else if (4 == join_type)
			{
				if ((bk_point[s_id].begin >= bk_size) || (bk_point[q_id].size-bk_point[q_id].end-1 >= bk_size))
				{
					continue;
				}
				switch (distant)
				{
				case 0: {
							join.type = 2;
							link[q_id][s_id] = join;
							join.type = 1;
							link[s_id][q_id] = join; break;}
				case 1: {
							join.type = 2;
							link[q_id][s_id] = join;
							join.type = 3;
							link[s_id][q_id] = join; break;}
				case 2: {
							join.type = 4;
							link[q_id][s_id] = join;
							join.type = 1;
							link[s_id][q_id] = join; break;}
				case 3: {
							join.type = 4;
							link[q_id][s_id] = join;
							join.type = 3;
							link[s_id][q_id] = join; }
				}
			}
		}
		else if (1 == flag)
		{
			join.flag=1;   //'-'
			if (0 == join_type)
			{
				join.type = 0;
				link[q_id][s_id] = join;
				link[s_id][q_id] = join;
			}
			else if (1 == join_type)
			{
				join.type = 0;
				link[q_id][s_id] = join;
			}
			else if (2 == join_type)
			{
				join.type = 0;
				link[s_id][q_id] = join;
			}
			else if (3 == join_type)
			{
				if ((bk_point[q_id].begin >= bk_size) || (bk_point[s_id].begin >= bk_size))
				{
					continue;
				}
				switch (distant)
				{
				case 0: {
							join.type = 1;
							link[q_id][s_id] = join;
							join.type = 1;
							link[s_id][q_id] = join; break;}
				case 1: {
							join.type = 1;
							link[q_id][s_id] = join;
							join.type = 3;
							link[s_id][q_id] = join; break;}
				case 2: {
							join.type = 3;
							link[q_id][s_id] = join;
							join.type = 1;
							link[s_id][q_id] = join; break;}
				case 3: {
							join.type = 3;
							link[q_id][s_id] = join;
							join.type = 3;
							link[s_id][q_id] = join; }
				}
			}
			else if (4 == join_type)
			{
				if ((bk_point[q_id].size-bk_point[q_id].end-1 >= bk_size) || (bk_point[s_id].size-bk_point[s_id].end-1 >= bk_size))
				{
					continue;
				}
				switch (distant)
				{
				case 0: {
							join.type = 2;
							link[q_id][s_id] = join;
							join.type = 2;
							link[s_id][q_id] = join; break;}
				case 1: {
							join.type = 2;
							link[q_id][s_id] = join;
							join.type = 4;
							link[s_id][q_id] = join; break;}
				case 2: {
							join.type = 4;
							link[q_id][s_id] = join;
							join.type = 2;
							link[s_id][q_id] = join; break;}
				case 3: {
							join.type = 4;
							link[q_id][s_id] = join;
							join.type = 4;
							link[s_id][q_id] = join; }
				}
			}
		}
	}
}

//read in linkage information among segments inside cluster
void Assembly::InLink(istream & fin)
{
	unsigned int q_id, s_id;
	unsigned int q_length, q_begin, q_end;
	unsigned int s_length, s_begin, s_end;
	char flag;
	float ident;
	JOIN join;
	char ch[5000];
	while (!fin.eof())
	{
		fin>>q_id;
		if (fin.eof())
		{
			break;
		}
		fin>>s_id;
		fin>>flag>>ident;
		fin>>q_length>>q_begin>>q_end;
		fin>>s_length>>s_begin>>s_end;  

	/*	//extra check to disregard wrong lines
		fin>>ch;
		if (fin.eof())
		{
			break;
		}
		if ((q_id = atoi(ch)) <= 0)
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if ((s_id = atoi(ch)) <= 0)
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>flag;
		if ((flag != '+') && (flag != '-'))
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		ident = atof(ch);
		if ((ident <= 0) || (ident >1))
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>q_length>>q_begin>>q_end;
		fin>>s_length>>s_begin>>s_end;
		fin>>ch;
		if ((q_length = atoi(ch)) <= 0)
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if (((q_begin = atoi(ch)) <= 0) || (q_begin >= q_length))
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if (((q_end = atoi(ch)) <= 0) || (q_end >= q_length))
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if ((s_length = atoi(ch)) <= 0)
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if (((s_begin = atoi(ch)) <= 0) || (s_begin >= s_length))
		{
			fin.getline(ch, 5000);
			continue;
		}
		fin>>ch;
		if (((s_end = atoi(ch)) <= 0) || (s_end >= s_length))
		{
			fin.getline(ch, 5000);
			continue;
		} */
		//
		fin.getline(ch,5000);

		if ((link.count(q_id) && link[q_id].count(s_id)) || (link.count(s_id) && link[s_id].count(q_id)))
		{
			continue;
		}
		
		if ('+' == flag)
		{
			join.flag=0;   //'+'
			if ((q_begin < min_extend) && (q_length-q_end-1 < min_extend))
			{
				join.type=0;
				link[s_id][q_id] = join;
			}
			if ((s_begin < min_extend) && (s_length-s_end-1 < min_extend))
			{
				join.type=0;
				link[q_id][s_id] = join;
				continue;
			}
			if ((q_begin >= min_extend) && (q_length-q_end-1 < min_extend) && (s_begin < min_extend) && (s_length-s_end-1 >= min_extend))
			{
				if (bk_point[s_id].begin >= bk_size)
				{
					continue;
				}
				if (bk_point[q_id].size-bk_point[q_id].end-1 >= bk_size)
				{
					continue;
				}
				if (q_begin < max_extend)
				{
					join.type = 1;
					link[s_id][q_id] = join;
				}
				else
				{
					join.type = 3;
					link[s_id][q_id] = join;
				}
				if (s_length-s_end-1 < max_extend)
				{
					join.type = 2;
					link[q_id][s_id] = join;
				}
				else
				{
					join.type = 4;
					link[q_id][s_id] = join;
				}
				continue;
			}
			if ((q_begin < min_extend) && (q_length-q_end-1 >= min_extend) && (s_begin >= min_extend) &&(s_length-s_end-1 < min_extend))
			{
				if (bk_point[q_id].begin >= bk_size)
				{
					continue;
				}
				if (bk_point[s_id].size-bk_point[s_id].end-1 >= bk_size)
				{
					continue;
				}
				if (s_begin < max_extend)
				{
					join.type=1;
					link[q_id][s_id] = join;
				}
				else
				{
					join.type=3;
					link[q_id][s_id] = join;
				}
				if (q_length-q_end-1 < max_extend)
				{
					join.type=2;
					link[s_id][q_id] = join;
				}
				else
				{
					join.type=4;
					link[s_id][q_id] = join;
				}
				continue;
			}
		}
		else if ('-' == flag)
		{
			join.flag=1;   //'-'
			if ((q_begin < min_extend) && (q_length-q_end-1 < min_extend))
			{
				join.type=0;
				link[s_id][q_id] = join;
			}
			if ((s_begin < min_extend) && (s_length-s_end-1 < min_extend))
			{
				join.type=0;
				link[q_id][s_id] = join;
				continue;
			}
			if ((q_begin >= min_extend) && (q_length-q_end-1 < min_extend) && (s_length-s_end-1 < min_extend) && (s_begin >= min_extend))
			{
				if (bk_point[s_id].size-bk_point[s_id].end-1 >= bk_size)
				{
					continue;
				}
				if (bk_point[q_id].size-bk_point[q_id].end-1 >= bk_size)
				{
					continue;
				}
				if (q_begin < max_extend)
				{
					join.type=2;
					link[s_id][q_id]=join;
				}
				else
				{
					join.type=4;
					link[s_id][q_id]=join;
				}
				if (s_begin < max_extend)
				{
					join.type=2;
					link[q_id][s_id]=join;
				}
				else
				{
					join.type=4;
					link[q_id][s_id]=join;
				}
				continue;
			}
			if ((q_begin < min_extend) && (q_length-q_end-1 >= min_extend) && (s_length-s_end-1 >= min_extend) && (s_begin < min_extend))
			{
				if (bk_point[q_id].begin >= bk_size)
				{
					continue;
				}
				if (bk_point[s_id].begin >= bk_size)
				{
					continue;
				}
				if (q_length-q_end-1 < max_extend)
				{
					join.type=1;
					link[s_id][q_id]=join;
				}
				else
				{
					join.type=3;
					link[s_id][q_id]=join;
				}
				if (s_length-s_end-1 < max_extend)
				{
					join.type=1;
					link[q_id][s_id]=join;
				}
				else
				{
					join.type=3;
					link[q_id][s_id]=join;
				}
				continue;
			}
		}
		else
		{
			cerr<<"unrecognizable flag: "<<flag<<endl;
		}
	}
//	ofstream fa("linkage");
//	for (LINK::iterator p=link.begin(); p!=link.end(); p++)
//	{
//		for (map<unsigned int, JOIN>::iterator pz = p->second.begin(); pz != p->second.end(); pz++)
//		{
//			fa<<p->first<<"\t"<<pz->first<<"\t"<<(int)pz->second.type<<"\t"<<(int)pz->second.flag<<endl;
//		}
//	}
//	fa.close();
}

//search paths from the graph and do assembly
void Assembly::DoAssembly()
{
	unsearched.clear();
	set<unsigned int> assembly_used;
	//generate the full list of to be searched seg IDs which existed in link list.
	for (LINK::iterator p=link.begin(); p!=link.end(); p++)
	{
		unsearched.insert(p->first);
	}
	//
	unsigned int N = 1;  //order of consensus sequences in the cluster
	while (!unsearched.empty())
	{
		sprintf(log_s, "remaining sequences:  %d", unsearched.size());
		outLog(log_s);
		vector<unsigned int> left;   //path, left link to
		vector<unsigned int> right;  //path, right link to
		num = 0;
		matrix.clear();
		chain.clear();

		unsigned int q_id;
		//find seg with maximum linkage in unsearched set
		unsigned int max = 0;
		set<unsigned int>::iterator p = unsearched.begin();
		set<unsigned int> to_remove;
		while (p != unsearched.end())
		{
			//if only a small number of sequences remained which link with 'q_id', then ignore it.
			int n = 0;
			for (map<unsigned int, JOIN>::iterator pi=link[*p].begin(); pi!=link[*p].end(); pi++)
			{
				if (assembly_used.end() == assembly_used.find(pi->first))
				{
					n++;
				}
			}

			if (n < depth)
			{
				to_remove.insert(*p);
			}
			else if (n > max)
			{
				max = n;
				q_id = *p;
			}
			p++;
		}
		if (0 == max)
		{
			break;
		}
		for (set<unsigned int>::iterator p=to_remove.begin(); p!=to_remove.end(); p++)
		{
			unsearched.erase(*p);
		}
		//initial consensus
		chain[q_id] = 0;
		ChainInfor(q_id);
		vertex.clear();
		child_seq.clear();
		vertex = ChildLink(q_id, 0);
		ListSeq(vertex);
		child_seq[q_id]=seq[q_id];
		sprintf(log_s, "%d  -  number of seq: %d", q_id, child_seq.size());
		outLog(log_s);
		num++;
		MultiAlign();
		sprintf(log_s, "initial:   %s", m_seq[num].c_str());
		outLog(log_s);
		right.push_back(num);
		num_seq[num] = child_seq.size();
		UpdateVisitedSet();

		//left extension
		outLog("to left");
		child_seq.clear();
		vertex.clear();
		vertex = ChildLink(q_id, 1);
		distant_extend.clear();
		distant_extend = ChildLink(q_id, 3);
		while (1)
		{
			sprintf(log_s, "Number of seqs:  %d",vertex.size());
			outLog(log_s);
			
			//
			biggestCluster.clear();
			BiggestCluster();
			for (set<unsigned int>::iterator p=biggestCluster.begin(); p != biggestCluster.end(); p++)
			{
				ChainInfor(*p);
			}
			sprintf(log_s, "Biggest Cluster:  %d",biggestCluster.size());
			outLog(log_s);
			
			set<unsigned int> added_vertex = AddSubVertex();
			sprintf(log_s, "Biggest Cluster after adding:  %d", biggestCluster.size()+added_vertex.size());
			outLog(log_s);
			//check if should be ended here? num of new sequence fished < min_join
			bool end = 0;   //if end, then still do this round of assembly so that have redundance at ends favorite for joining.
			set<unsigned int> new_vertex;
			set_difference(biggestCluster.begin(), biggestCluster.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
			set_difference(added_vertex.begin(), added_vertex.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
			sprintf(log_s, "New/All fished:  %d / %d", new_vertex.size(), biggestCluster.size()+added_vertex.size());
			outLog(log_s);
			set<unsigned int> unvisited_distant;
			set_difference(distant_extend.begin(), distant_extend.end(), visited.begin(), visited.end(), inserter(unvisited_distant, unvisited_distant.begin()));
			sprintf(log_s, "Unvisited distant ones:  %d", unvisited_distant.size());
			outLog(log_s);
			if (new_vertex.size() < min_join)
			{
				vertex.insert(distant_extend.begin(), distant_extend.end());
				biggestCluster.clear();
				BiggestCluster();
				for (set<unsigned int>::iterator p=biggestCluster.begin(); p != biggestCluster.end(); p++)
				{
					ChainInfor(*p);
				}
				sprintf(log_s, "Biggest Cluster:  %d", biggestCluster.size());
				outLog(log_s);
				set<unsigned int> added_vertex = AddSubVertex();
				sprintf(log_s, "Biggest Cluster after adding:  %d", biggestCluster.size()+added_vertex.size());
				outLog(log_s);
				new_vertex.clear();
				set_difference(biggestCluster.begin(), biggestCluster.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
				set_difference(added_vertex.begin(), added_vertex.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
				sprintf(log_s, "New/All fished:  %d / %d", new_vertex.size(), biggestCluster.size()+added_vertex.size());
				outLog(log_s);
				if (new_vertex.size() < min_join)
				{
					end = 1;
					set_union(biggestCluster.begin(), biggestCluster.end(), added_vertex.begin(), added_vertex.end(), inserter(new_vertex, new_vertex.begin()));
				}
			}
			if (new_vertex.empty())
			{
				break;
			}
			child_seq.clear();
			ListSeq(new_vertex);
//			for (SEQ::iterator p=child_seq.begin(); p != child_seq.end(); p++)
//			{
//				cout<<">"<<p->first<<endl;
//				cout<<p->second<<endl;
//			}
			num++;
			MultiAlign();
			sprintf(log_s, "%d\t%s", num, m_seq[num].c_str());
			outLog(log_s);
			if (m_seq[num].size() > 10)
			{
				left.push_back(num);
				num_seq[num] = child_seq.size();
			}
			//
			UpdateVisitedSet();
			//
			if (end)
			{
				break;
			}
			vertex.clear();
			distant_extend.clear();
			for (set<unsigned int>::iterator pi = biggestCluster.begin(); pi != biggestCluster.end(); pi++)
			{
				ChainInfor(*pi);
				vertex = ChildLink(*pi, 1);
				distant_extend = ChildLink(*pi, 3);
			}
		}

		//right extension
		outLog("to right");
		vertex.clear();
		vertex = ChildLink(q_id, 2);
		distant_extend.clear();
		distant_extend = ChildLink(q_id, 4);
		while (1)
		{
			sprintf(log_s, "Number of seqs:  %d",vertex.size());
			outLog(log_s);
			//
			biggestCluster.clear();
			BiggestCluster();
			for (set<unsigned int>::iterator p=biggestCluster.begin(); p != biggestCluster.end(); p++)
			{
				ChainInfor(*p);
			}
			sprintf(log_s,"Biggest Cluster:  %d",biggestCluster.size());
			outLog(log_s);
			set<unsigned int> added_vertex = AddSubVertex();
			
			sprintf(log_s,"Biggest Cluster after adding:  %d",biggestCluster.size()+added_vertex.size());
			outLog(log_s);
			//check if should be ended here? num of new sequence fished < depth/2
			bool end = 0;   //if end, then still do this round of assembly so that have redundance at ends favorite for joining.
			set<unsigned int> new_vertex;
			set_difference(biggestCluster.begin(), biggestCluster.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
			set_difference(added_vertex.begin(), added_vertex.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
			sprintf(log_s, "New/All fished:  %d / %d",new_vertex.size(),biggestCluster.size()+added_vertex.size());
			outLog(log_s);
			set<unsigned int> unvisited_distant;
			set_difference(distant_extend.begin(), distant_extend.end(), visited.begin(), visited.end(), inserter(unvisited_distant, unvisited_distant.begin()));
			sprintf(log_s, "Unvisited distant ones:  %d",unvisited_distant.size());
			outLog(log_s);
			if (new_vertex.size() < min_join)
			{
				vertex.insert(distant_extend.begin(), distant_extend.end());
				biggestCluster.clear();
				BiggestCluster();
				for (set<unsigned int>::iterator p=biggestCluster.begin(); p != biggestCluster.end(); p++)
				{
					ChainInfor(*p);
				}
				sprintf(log_s,"Biggest Cluster:  %d",biggestCluster.size());
				outLog(log_s);
				set<unsigned int> added_vertex = AddSubVertex();
				sprintf(log_s, "Biggest Cluster after adding:  %d",biggestCluster.size()+added_vertex.size());
				outLog(log_s);
				new_vertex.clear();
				set_difference(biggestCluster.begin(), biggestCluster.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
				set_difference(added_vertex.begin(), added_vertex.end(), visited.begin(), visited.end(), inserter(new_vertex, new_vertex.begin()));
				sprintf(log_s,"New/All fished:  %d / %d",new_vertex.size(),biggestCluster.size()+added_vertex.size());
				outLog(log_s);
				if (new_vertex.size() < min_join)
				{
					end = 1;
					set_union(biggestCluster.begin(), biggestCluster.end(), added_vertex.begin(), added_vertex.end(), inserter(new_vertex, new_vertex.begin()));
				}
			}
			if (new_vertex.empty())
			{
				break;
			}
			child_seq.clear();
			ListSeq(new_vertex);
//			for (SEQ::iterator p=child_seq.begin(); p != child_seq.end(); p++)
//			{
//				cout<<">"<<p->first<<endl;
//				cout<<p->second<<endl;
//			}
			num++;
			MultiAlign();
			sprintf(log_s, "%d\t%s",num,m_seq[num].c_str());
			outLog(log_s);
			if (m_seq[num].size() > 10)
			{
				right.push_back(num);
				num_seq[num] = child_seq.size();
			}
			//
			UpdateVisitedSet();
			//
			if (end)
			{
				break;
			}
			vertex.clear();
			distant_extend.clear();
			for (set<unsigned int>::iterator pi = biggestCluster.begin(); pi != biggestCluster.end(); pi++)
			{
				ChainInfor(*pi);
				vertex = ChildLink(*pi, 2);
				distant_extend = ChildLink(*pi, 4);
			}
		}
		//number of fragments assembled the consensus
		num_frag[N] = visited.size();
		//total size of fragments formed the consensus
		total_seg[N] = 0;
		for (set<unsigned int>::iterator p=visited.begin(); p!=visited.end(); p++)
		{
			total_seg[N] += seq[*p].size();
		}
		//join fragments and get consensus
//		ofstream f("m_seq.fa");
//		for (SEQ::iterator p=m_seq.begin(); p!=m_seq.end(); p++)
//		{
//			f<<">"<<p->first<<endl;
//			f<<p->second<<endl;
//		}
//		f.close();
		outLog("join fragments");
		matrix.clear();
		int q_begin, q_end, s_begin, s_end;
		string q_align, s_align;
		positions.clear();

		vector<unsigned int> join_order;  //ordered IDs from left to right for unempty seqs;
		for (vector<unsigned int>::reverse_iterator pz = left.rbegin(); pz!=left.rend(); pz++)
		{
			if (!m_matrix[*pz].empty())
			{
				join_order.push_back(*pz);
			}
		}
		for (vector<unsigned int>::iterator pz = right.begin(); pz!=right.end(); pz++)
		{
			if (!m_matrix[*pz].empty())
			{
				join_order.push_back(*pz);
			}
		}
		if (!join_order.empty())
		{
			//initialize with the first element
			matrix = m_matrix[join_order.front()];
			positions.resize(matrix.size());
			for (int i=0; i<positions.size(); i++)
			{
				positions[i] = i;
			}
			//joining
			for (int i=1; i<join_order.size(); i++)
			{
				Join2Seq(join_order[i-1], join_order[i]);
			}
		}
		outLog("joining finished");
		con_matrix[N] = matrix;
		//consensus:
		string con = Consensus();
		if (con.size()>=hit_size)
		{
			con_seq[N] = con;
			N++;
		}
		//update unsearched vertex set.
		set<unsigned int> tmp;
		set_difference(unsearched.begin(), unsearched.end(), visited.begin(), visited.end(), inserter(tmp, tmp.begin()));
		assembly_used.insert(visited.begin(), visited.end());
		unsearched = tmp;
		visited.clear();
	}
}

void Assembly::ChainInfor(unsigned int q_id)
{
	for (map<unsigned int, JOIN>::iterator p=link[q_id].begin(); p!=link[q_id].end(); p++)
	{
		if (chain.end() == chain.find(q_id))
		{
			continue;
		}
		if ((!chain[q_id] && !p->second.flag) || (chain[q_id] && p->second.flag))
		{
			chain[p->first] = 0;
		}
		else
		{
			chain[p->first] = 1;
		}
	}
}

set<unsigned int> Assembly::ChildLink(unsigned int q_id, unsigned int sig)
{
	int flag;
	if (!chain[q_id]) //direct order on consensus
	{
		flag = sig;
	}
	else   //complementary order
	{
		switch (sig)
		{
		case 0: flag = 0; break;
		case 1: flag = 2; break;
		case 3: flag = 4; break;
		case 2: flag = 1; break;
		case 4: flag = 3;
		}
	}
	set<unsigned int> v;
	map<unsigned int, JOIN>::iterator p=link[q_id].begin();
	while (p!=link[q_id].end())
	{
		if (flag == p->second.type)
		{
			v.insert(p->first);
		}
		p++;
	}
	return v;
}
void Assembly::ListSeq(set<unsigned int> & id)
{
	for (set<unsigned int>::iterator p=id.begin(); p!=id.end(); p++)
	{
		if (chain.end() == chain.find(*p))
		{
			cerr<<*p<<" not exist in chain\n";
			exit(1);
		}
		if (!chain[*p])
		{
			child_seq[*p] = seq[*p];
		}
		else
		{
			child_seq[*p] = ComplemSeq(seq[*p]);
		}
	}
}
void Assembly::UpdateVisitedSet()
{
	SEQ::iterator pm = child_seq.begin();
	while (pm != child_seq.end())
	{
		visited.insert(pm->first);
		pm++;
	}
}

//#define INTERNAL_MUSCLE

//get consensus sequence and matrix from multi-alignment
void Assembly::MultiAlign()
{
	//get a subset of segments for multi-alignment to get consensus if the entire set is too large
	SEQ sub_child_seq;
	if ((sub_multi < 0) || (child_seq.size() <= sub_multi))
	{
		sub_child_seq = child_seq;
	}
	else
	{
		int n = child_seq.size();
		vector<char> R(n, 0);
		for (int i=0; i<sub_multi; i++)
		{
			R[i]=1;
		}
		srand(time(NULL));
		for (int i=0; i<n; i++)
		{
			int index_1 = rand()%n;
			int index_2 = rand()%n;
			int tmp = R[index_1];
			R[index_1] = R[index_2];
			R[index_2] = tmp;
		}
		int count = 0;
		for (SEQ::iterator pz = child_seq.begin(); pz != child_seq.end(); pz++)
		{
			if (R[count])
			{
				sub_child_seq[pz->first]=pz->second;
			}
			count++;
		}
	}
	//do multi-alignment
	if (sub_child_seq.size() < 100)
	{
		SEQ s;
#ifdef INTERNAL_MUSCLE
		muscle_reas(sub_child_seq, s);
#else
		ofstream fout(multi_fa);
		for (SEQ::iterator p = sub_child_seq.begin(); p != sub_child_seq.end(); p++)
		{
			fout<<">"<<p->first<<endl;
			fout<<p->second<<endl;
		}
		fout.close();

//		system("muscle -in multi.fa -out multi.aln -maxiters 2 -diags1 -quiet");

		char tc = 'r';
		write(fd_muscle[1], &tc, 1);
		read(fd_muscle_r[0], &tc, 1); 
		ifstream fin(multi_aln);
		Rseqs(fin, s);
		fin.close();
#endif
		MATRIX m = BuildMatrix(s);
		string con;
		MatrixSeq_loose(m, con);

		m_seq[num] = con;
		m_matrix[num] = m;
	}
	else
	{
		//separate sequences into subsets, then do first round of multi-alignment inside subset
		int n_cut = (int)sqrt(double(sub_child_seq.size()));
		int n_cluster = sub_child_seq.size() / n_cut;
		CONSENSUS c;
		SEQ sub_s;
		SEQ::iterator p=sub_child_seq.begin();
		for (int i=0; i<n_cluster; i++)
		{
			SEQ s;
			SEQ tmp_seq;
			if (i <n_cluster-1)
			{
				for (int j=0; j<n_cut; j++)
				{
					tmp_seq[p->first] = p->second;
					p++;
				}
			}
			else
			{
				while (p!=sub_child_seq.end())
				{
					tmp_seq[p->first] = p->second;
					p++;
				}
			}
#ifdef INTERNAL_MUSCLE
			muscle_reas(tmp_seq, s);
#else
			ofstream fout(multi_fa);
			for (SEQ::iterator p = tmp_seq.begin(); p != tmp_seq.end(); p++)
			{
				fout<<">"<<p->first<<endl;
				fout<<p->second<<endl;
			}
			fout.close();
			//
//			system("muscle -in multi.fa -out multi.aln -maxiters 2 -diags1 -quiet");

			char tc = 'r';
			write(fd_muscle[1], &tc, 1);
			read(fd_muscle_r[0], &tc, 1);
			ifstream fin(multi_aln);

			Rseqs(fin, s);
			fin.close();
#endif
			MATRIX m = BuildMatrix(s);
			string con;
			MatrixSeq_strict(m, con);
			//
			if (!m.empty())
			{
				c[i] = m;
				sub_s[i] = con;
			}
		}
		//second round of multi-align for consensus sequences of subsets.
		{
			MATRIX m;
			string con;
			if (!sub_s.empty())
			{
				SEQ s;
#ifdef INTERNAL_MUSCLE
				muscle_reas(sub_s, s);
#else
				ofstream fout(multi_fa);
				for (SEQ::iterator p = sub_s.begin(); p != sub_s.end(); p++)
				{
					fout<<">"<<p->first<<endl;
					fout<<p->second<<endl;
				}
				fout.close();

	//			system("muscle -in multi.fa -out multi.aln -maxiters 2 -diags1 -quiet");

				char tc = 'r';
				write(fd_muscle[1], &tc, 1);
				read(fd_muscle_r[0], &tc, 1); 

				ifstream fin(multi_aln);

				Rseqs(fin, s);
				fin.close();
#endif
				m.clear();
				vector<unsigned int> a(5,0);
				m.insert(m.end(),s.begin()->second.size(),a);
				for (SEQ::iterator p = s.begin(); p != s.end(); p++)
				{
					int pos=0;
					for (int i=0; i<p->second.size(); i++)
					{
						if ('-' != p->second[i])
						{
							for (int j=0; j<5; j++)
							{
								m[i][j] += c[p->first][pos][j];
							}
							pos++;
						}
						else
						{
							m[i][4] += n_cut;
						}
					}
				}
				//
				MatrixSeq_loose(m, con);
			}
			m_seq[num] = con;
			m_matrix[num] = m;
		}
	}
}

//find high-depth boundary of multi-alignment matrix
int Assembly::HighDbound(MATRIX & m, char LR)  //LR? 0:left; 1:right
{
	if (!LR)
	{
		for (int i=0; i<m.size(); i++)
		{
			int n_acgt = m[i][0]+m[i][1]+m[i][2]+m[i][3];
			if (n_acgt >= m[i][4]*2)
			{
				return i;
			}
			else
			{
				//check whether the coming several positions are also mostly with acgt
				int n_pos = 0;
				int n_with = 0;
				for (int j=1; (j<10) && (j+i<m.size()); j++)
				{
					n_pos++;
					n_acgt = m[i+j][0]+m[i+j][1]+m[i+j][2]+m[i+j][3];
					if (n_acgt >= depth)
					{
						n_with ++;
					}
				}
				if (n_with >= n_pos * 0.6)
				{
					return i;
				}
			}
		}
	}
	else
	{
		for (int i=m.size()-1; i>=0; i--)
		{
			int n_acgt = m[i][0]+m[i][1]+m[i][2]+m[i][3];
			if (n_acgt >= m[i][4]*2)
			{
				return i;
			}
			else
			{
				//check whether the coming several positions are also mostly with acgt
				int n_pos = 0;
				int n_with = 0;
				for (int j=1; (j<10) && (i-j>0); j++)
				{
					n_pos++;
					n_acgt = m[i-j][0]+m[i-j][1]+m[i-j][2]+m[i-j][3];
					if (n_acgt >= depth)
					{
						n_with ++;
					}
				}
				if (n_with >= n_pos * 0.6)
				{
					return i;
				}
			}
		}
	}
}
//average identity for the range of left to right of matrix
float Assembly::AverageIdent(MATRIX & m, int left, int right) 
{
	float ident=0;
	for (int i=left; i<=right; i++)
	{
		int n_acgt = m[i][0]+m[i][1]+m[i][2]+m[i][3];
		int max = m[i][0];
		for (int j=1; j<4; j++)
		{
			if (m[i][j] > max)
			{
				max = m[i][j];
			}
		}
		ident += (float)max/n_acgt;
	}
	return ident/(right-left+1);
}
//vote nt for the position among A/C/G/T/-
int Assembly::DecideNtType_strict(vector<unsigned int> a, char flag)   //flag '1' internal multi-alignment region; '0' boundaries
{
	int n_acgt = a[0]+a[1]+a[2]+a[3];
	int n_gap = a[4];
	int index = 0;
	int max = a[0];
	for (int i=1; i<4; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
			index = i;
		}
	}
	if (flag)
	{
		if ((n_acgt >= depth*2) && (max >= n_acgt*0.6))
		{
			return index;  //acgt
		}
		else if (n_acgt < n_gap)
		{
			return 4;  //gap
		}
		else
		{
			return index;
		}
	}
	return index;
}
//vote nt for the position among A/C/G/T/-
int Assembly::DecideNtType_loose(vector<unsigned int> a, char flag)   //flag '1' internal multi-alignment region; '0' boundaries
{
	int n_acgt = a[0]+a[1]+a[2]+a[3];
	int n_gap = a[4];
	int index = 0;
	int max = a[0];
	for (int i=1; i<4; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
			index = i;
		}
	}
	if (flag)
	{
		if ((n_acgt >= depth*2) && (max >= n_acgt*0.6))
		{
			return index;  //acgt
		}
		else if (((n_acgt < depth) && (n_acgt < n_gap)) || ((n_acgt >= depth) && (n_acgt < n_gap*0.3)))
		{
			return 4;  //gap
		}
		else
		{
			return index;
		}
	}
	return index;
}
//build matrix for multi-alignment
MATRIX Assembly::BuildMatrix(SEQ & seq)
{
	MATRIX m;
	vector<unsigned int> a(5,0);
	m.insert(m.end(),seq.begin()->second.size(),a);
	for (SEQ::iterator p = seq.begin(); p != seq.end(); p++)
	{
		for (int i=0; i<p->second.size(); i++)
		{
			switch (p->second[i])
			{
			case 'A': m[i][0]++; break;
			case 'C': m[i][1]++; break;
			case 'G': m[i][2]++; break;
			case 'T': m[i][3]++; break;
			case '-': m[i][4]++;
			}
		}
	}
	return m;
}
//get consensus from matrix, update matrix on the same time
void Assembly::MatrixSeq_strict(MATRIX & m, string & con)
{
	int h_left = HighDbound(m, 0);   //boundary of n_acgt >= n_gap
	int h_right = HighDbound(m, 1);

	//if the consensus is too short, disregard it and return empty
	if (h_right-h_left < hit_size)
	{
		m.clear();
		con.clear();
		return;
	}
	int a_left;                      //boundary of high %identity
	int a_right;
	int win = 10;      //10bp window to get accurate boundary
	if ((h_left==0) || (AverageIdent(m, 0, h_left-1) >= avg_ident_bound))
	{
		a_left = 0;
	}
	else
	{
		a_left = 0;
		for (int i=h_left; i>=win; i-=win)
		{
			if (AverageIdent(m, i-win, i-1) < avg_ident_bound)
			{
				a_left = i;
				break;
			}
		}
	}
	if ((h_right==m.size()-1) || (AverageIdent(m, h_right+1, m.size()-1) >= avg_ident_bound))
	{
		a_right = m.size()-1;
	}
	else
	{
		a_right = m.size()-1;
		for (int i=h_right; i<m.size()-win; i+=win)
		{
			if (AverageIdent(m, i+1, i+win) < avg_ident_bound)
			{
				a_right = i;
				break;
			}
		}
	}
	//
	if (a_right-a_left < hit_size)
	{
		m.clear();
		con.clear();
		return;
	}
	MATRIX t;
	con.clear();
	for (int i=a_left; i<=a_right; i++)
	{
		int index;
		if ((i <= h_left) || (i >= h_right))
		{
			index = DecideNtType_strict(m[i],0);
		}
		else
		{
			index = DecideNtType_strict(m[i],1);
		}
		switch (index)
		{
		case 0: con.push_back('A'); t.push_back(m[i]); break;
		case 1: con.push_back('C'); t.push_back(m[i]); break;
		case 2: con.push_back('G'); t.push_back(m[i]); break;
		case 3: con.push_back('T'); t.push_back(m[i]); break;
		}
	}
	m=t;
}
//get consensus from matrix, update matrix on the same time
void Assembly::MatrixSeq_loose(MATRIX & m, string & con)
{
	int h_left = HighDbound(m, 0);   //boundary of n_acgt >= n_gap
	int h_right = HighDbound(m, 1);
	//if the consensus is too short, disregard it and return empty
	if (h_right-h_left < hit_size)
	{
		m.clear();
		con.clear();
		return;
	}
	int a_left;                      //boundary of high %identity
	int a_right;
	int win = 10;      //10bp window to get accurate boundary
	if ((h_left==0) || (AverageIdent(m, 0, h_left-1) >= avg_ident_bound))
	{
		a_left = 0;
	}
	else
	{
		a_left = 0;
		for (int i=h_left; i>=win; i-=win)
		{
			if (AverageIdent(m, i-win, i-1) < avg_ident_bound)
			{
				a_left = i;
				break;
			}
		}
	}
	if ((h_right==m.size()-1) || (AverageIdent(m, h_right+1, m.size()-1) >= avg_ident_bound))
	{
		a_right = m.size()-1;
	}
	else
	{
		a_right = m.size()-1;
		for (int i=h_right; i<m.size()-win; i+=win)
		{
			if (AverageIdent(m, i+1, i+win) < avg_ident_bound)
			{
				a_right = i;
				break;
			}
		}
	}
	//
	if (a_right-a_left < hit_size)
	{
		m.clear();
		con.clear();
		return;
	}
	MATRIX t;
	con.clear();
	for (int i=a_left; i<=a_right; i++)
	{
		int index;
		if ((i <= h_left) || (i >= h_right))
		{
			index = DecideNtType_loose(m[i],0);
		}
		else
		{
			index = DecideNtType_loose(m[i],1);
		}
		switch (index)
		{
		case 0: con.push_back('A'); t.push_back(m[i]); break;
		case 1: con.push_back('C'); t.push_back(m[i]); break;
		case 2: con.push_back('G'); t.push_back(m[i]); break;
		case 3: con.push_back('T'); t.push_back(m[i]); break;
		}
	}
	m=t;
}
void Assembly::BiggestCluster()
{
	//get the subset of segment relationship
	map<unsigned int, set<unsigned int> > rel;
	set<unsigned int> t;
	for (set<unsigned int>::iterator p = vertex.begin(); p != vertex.end(); p++)
	{
		t.clear();
		for (map<unsigned int, JOIN>::iterator pi = link[*p].begin(); pi != link[*p].end(); pi++)
		{
			if ((vertex.end() != vertex.find(pi->first)) && (0 == pi->second.type))
			{
				t.insert(pi->first);
//				cout<<*p<<"\t"<<pi->first<<endl;
			}
		}
		rel[*p] = t;
	}
	//clustering, single linkage
	vector<set<unsigned int> > cluster;
	set<unsigned int> c;
	set<unsigned int> remain = vertex;
	set<unsigned int> added;
	set<unsigned int> tmp;
	while (!remain.empty())
	{
		if (c.empty())
		{
			c.insert(*(remain.begin()));
			added = c;
			tmp = c;
		}
		for (set<unsigned int>::iterator pz=added.begin(); pz != added.end(); pz++)
		{
			set_union(tmp.begin(), tmp.end(), rel[*pz].begin(), rel[*pz].end(), inserter(tmp, tmp.begin()));
		}
		if (tmp.size() > c.size())
		{
			added.clear();
			set_difference(tmp.begin(), tmp.end(), c.begin(), c.end(), inserter(added, added.begin()));
			c=tmp;
		}
		else
		{	
			sprintf(log_s,"%d",c.size());
			outLog(log_s);
			cluster.push_back(c);
//			if (c.size() > vertex.size()/2)
//			{
//				break;
//			}
			set<unsigned int> t;
			set_difference(remain.begin(), remain.end(), c.begin(), c.end(), inserter(t, t.begin()));
			remain = t;
			c.clear();
		}
	}
	sprintf(log_s,"%d  clusters",cluster.size());
	//find biggest cluster
	int max = 0;
	for (vector<set<unsigned int> >::iterator p=cluster.begin(); p!=cluster.end(); p++)
	{
		if (p->size() > max)
		{
			max = p->size();
			biggestCluster=*p;
		}
	}
}
set<unsigned int> Assembly::AddSubVertex()
{
	set<unsigned int> tmp;
	for (set<unsigned int>::iterator p=biggestCluster.begin(); p != biggestCluster.end(); p++)
	{
		for (map<unsigned int, JOIN>::iterator pi=link[*p].begin(); pi != link[*p].end(); pi++)
		{
			if ((0 == pi->second.type) && (visited.end() == visited.find(pi->first)))
			{
				tmp.insert(pi->first);
			}
		}
	}
	set<unsigned int> add;
	set_difference(tmp.begin(), tmp.end(), biggestCluster.begin(), biggestCluster.end(), inserter(add, add.begin()));
	return add;
}

//join two sequence and update position information
void Assembly::Join2Seq(unsigned int q_id, unsigned int s_id)
{
	sprintf(log_s,"q %d  size: %d   s  %d   size:  %d",q_id,m_seq[q_id].size(),s_id,m_seq[s_id].size());
	outLog(log_s);

	int q_begin, q_end, s_begin, s_end;
	float ident;
	//do local alignment
	a.DoAlign(m_seq[q_id], m_seq[s_id]);
	string q_align, s_align;
	q_begin = a.OutQueryBegin();
	q_end = a.OutQueryEnd();
	s_begin = a.OutSbjctBegin();
	s_end = a.OutSbjctEnd();
	q_align = a.OutQueryAlign();
	s_align = a.OutSbjctAlign();
	ident = a.OutPident();
	sprintf(log_s, "%f\t%d\t%d\t%d\t%d",ident,q_begin,q_end,s_begin,s_end);
	outLog(log_s);
	sprintf(log_s, "%s",m_seq[q_id].c_str());
	outLog(log_s);
	sprintf(log_s, "%s\n",m_seq[s_id].c_str());
	outLog(log_s);
	sprintf(log_s,"%s\n%s\n",q_align.c_str(),s_align.c_str());
	outLog(log_s);
	if ((ident < 0.3) || (q_end-q_begin < 30))  //force joining
	{
		outLog("Warning: very poorly aligned");
		q_begin = m_seq[q_id].size() - 2;
		q_end = m_seq[q_id].size() - 1;
		s_begin = 0;
		s_end = 1;
		int last_pos = matrix.size();
		matrix.insert(matrix.end(), m_matrix[s_id].begin(), m_matrix[s_id].end());
		positions.clear();
		for (int z=0; z<m_matrix[s_id].size(); z++)
		{
			positions.push_back(last_pos+z);
		}
		return;
	}
	//
	vector<int> tmp_positions;

	//position of q_end can not be mapped on the previous consensus, have to disregard old consensus, initial "matrix" and positions
	if (positions[q_end] <0)
	{
		//initialize with sbject element
		matrix = m_matrix[s_id];
		positions.resize(matrix.size());
		for (int i=0; i<positions.size(); i++)
		{
			positions[i] = i;
		}
		return;
	}
	//else, go on
	//rm the unaligned region at 3' end of query
	if (q_end < m_seq[q_id].size()-1)
	{
		matrix.erase(matrix.begin()+positions[q_end]+1, matrix.end());
	}
	//compress the aligned region
	int query_pos = q_begin;
	int sbjct_pos = s_begin;
	int n=0;
	while (positions[query_pos] <0)
	{
		if (q_align[n] != '-')
		{
			if (s_align[n] != '-')
			{
				sbjct_pos++;
			}
			query_pos++;
		}
		else
		{
			sbjct_pos++;
		}
		n++;
	}
	for (int z=0; z<sbjct_pos; z++)
	{
		tmp_positions.push_back(-1);
	}
	int nInsertion = 0;  //number of inserted positions on matrix, so that to correct positions(query)
	int curr_pos = positions[query_pos];
	for (int j=n; j<q_align.size(); j++)
	{
		if (q_align[j] != '-')
		{
			if (s_align[j] != '-')
			{
				for (int m=0; m<5; m++)
				{
					matrix[positions[query_pos]+nInsertion][m] += m_matrix[s_id][sbjct_pos][m];
				}
				tmp_positions.push_back(curr_pos);
				sbjct_pos++;
			}
			else
			{
				matrix[positions[query_pos]+nInsertion][4] += num_seq[s_id];
			}
			curr_pos = positions[query_pos]+nInsertion;
			curr_pos++;
			query_pos++;
		}
		else
		{
			if (curr_pos < positions[query_pos] + nInsertion)
			{
				for (int m=0; m<5; m++)
				{
					matrix[curr_pos][m] += m_matrix[s_id][sbjct_pos][m];
				}
			}
			else
			{
				m_matrix[s_id][sbjct_pos][4] += num_seq[q_id];
				matrix.insert(matrix.begin()+curr_pos, m_matrix[s_id][sbjct_pos]);
				nInsertion++;
			}
			tmp_positions.push_back(curr_pos);
			curr_pos++;
			sbjct_pos++;
		}
	}
	//add the unaligned region at 3' end of sbjct to the end of final_matrix
	if (s_end < m_matrix[s_id].size()-1)
	{
		matrix.insert(matrix.end(), m_matrix[s_id].begin()+s_end+1, m_matrix[s_id].end());
		unsigned int last_pos = tmp_positions[s_end];
		for (int z=1; z<m_matrix[s_id].size()-s_end; z++)
		{
			tmp_positions.push_back(last_pos+z);
		}
	}
	//
	positions = tmp_positions;
}
//get consensus sequence by vote
string Assembly::Consensus()
{
	string con;
	con.clear();
	if (matrix.empty())
	{
		return con;
	}
	MATRIX t;
	int h_left = HighDbound(matrix, 0);
	int h_right = HighDbound(matrix, 1);
	int a_left = h_left;
	int a_right = h_right;
	for (int i=0; i<h_left; i++)
	{
		int n_acgt = matrix[i][0]+matrix[i][1]+matrix[i][2]+matrix[i][3];
		if (n_acgt >= matrix[i][4] * 0.25)
		{
			a_left = i;
			break;
		}
	}
	for (int i=matrix.size()-1; i>h_right; i--)
	{
		int n_acgt = matrix[i][0]+matrix[i][1]+matrix[i][2]+matrix[i][3];
		if (n_acgt >= matrix[i][4] * 0.25)
		{
			a_right = i;
			break;
		}
	}
	sprintf(log_s, "Boundaries:  %d\t%d\t%d\t%d\t%d",a_left,h_left,h_right,a_right,matrix.size());
	outLog(log_s);
	int index;
	if ((a_left<h_left) && (AverageIdent(matrix, a_left, h_left-1) >= avg_ident_bound))
	{
		for (int i=a_left; i<h_left; i++)
		{
			int n_acgt = matrix[i][0]+matrix[i][1]+matrix[i][2]+matrix[i][3];
			if (n_acgt < matrix[i][4] * 0.1)
			{
				index = 4;
			}
			else
			{
				index = DecideNtType_loose(matrix[i],0);
			}
			switch (index)
			{
			case 0: con.push_back('A'); t.push_back(matrix[i]); break;
			case 1: con.push_back('C'); t.push_back(matrix[i]); break;
			case 2: con.push_back('G'); t.push_back(matrix[i]); break;
			case 3: con.push_back('T'); t.push_back(matrix[i]); break;
			}
		}
	}
	for (int i=h_left; i<=h_right; i++)
	{
		index = DecideNtType_loose(matrix[i],1);
		switch (index)
		{
		case 0: con.push_back('A'); t.push_back(matrix[i]); break;
		case 1: con.push_back('C'); t.push_back(matrix[i]); break;
		case 2: con.push_back('G'); t.push_back(matrix[i]); break;
		case 3: con.push_back('T'); t.push_back(matrix[i]); break;
		}
	}
	if ((a_right>h_right) && (AverageIdent(matrix, h_right+1, a_right) >= avg_ident_bound))
	{
		for (int i=h_right; i<=a_right; i++)
		{
			int n_acgt = matrix[i][0]+matrix[i][1]+matrix[i][2]+matrix[i][3];
			if (n_acgt < matrix[i][4] * 0.1)
			{
				index = 4;
			}
			else
			{
				index = DecideNtType_loose(matrix[i],0);
			}
			switch (index)
			{
			case 0: con.push_back('A'); t.push_back(matrix[i]); break;
			case 1: con.push_back('C'); t.push_back(matrix[i]); break;
			case 2: con.push_back('G'); t.push_back(matrix[i]); break;
			case 3: con.push_back('T'); t.push_back(matrix[i]); break;
			}
		}
	}
//	for (int i=0; i<matrix.size(); i++)
//	{
//		int index = DecideNtType_loose(matrix[i],1);
//		switch (index)
//		{
//		case 0: con.push_back('A'); t.push_back(matrix[i]); break;
//		case 1: con.push_back('C'); t.push_back(matrix[i]); break;
//		case 2: con.push_back('G'); t.push_back(matrix[i]); break;
//		case 3: con.push_back('T'); t.push_back(matrix[i]); break;
//		}
//	}
	matrix=t;
	sprintf(log_s,"Consensus:   %d    %s",con.size(),con.c_str());
	outLog(log_s);
	return con;
}

void Assembly::OutConsensusSeq(ofstream & fout)
{
	for (SEQ::iterator p = con_seq.begin(); p != con_seq.end(); p++)
	{
		float mean_depth = total_seg[p->first] / (float)p->second.size();
		fout<<">"<<prefix<<p->first<<"     Size= "<<p->second.size()<<"    NumFrag= "<<num_frag[p->first]<<"    AvgDepth= "<<mean_depth<<endl;
		fout<<p->second<<endl;
	}
}
