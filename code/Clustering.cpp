/*
*Copyright 2006  BGI
*All right reserved
*
*Filename:  Clustering.cpp
*Description:  

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2006.10.11
*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include<list>
#include <sys/stat.h>
#include "Utilities.h"
#include "ReAS.h"
#include <stdlib.h>

using namespace std;

//const:
char * cluster_pre = "cluster_";
char * out_seq_file = "seg.fa";
char * out_bk_file = "seg.bk";
char * out_link_file = "seg.link";
//structure & class & function


//usage:
void usage(void)
{
	cout<<"Usage:	\n"
		<<"	-l	STRING	sequence linkage file, default=seg.link\n"
		<<"	-f	STRING	sequence file, default=seg.fa\n"
		<<"	-b	STRING	breakpoint file, default=seg.bk\n"
		<<"	-m	INT	minimal group size, default=1\n"
		<<"	-h	help\n"
		<<"notes: input should be in binary format\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	char * link_file="seg.link";
	char * seq_file="seg.fa";
	char * bk_file="seg.bk";
	int min_cluster = 1;
	//[options]
	int c;
	while((c=getopt(argc,argv,"l:f:b:m:h")) != -1) {
			switch(c) {
					case 'l': link_file = optarg; break;        
					case 'f': seq_file = optarg; break;
					case 'b': bk_file = optarg; break;
					case 'm': min_cluster = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//clustering
	FILE * fin = fopen(link_file, "rb");
	if (!fin)
	{
		cerr<<"can not open "<<link_file<<endl;
		exit(1);
	}
	ifstream f_seq(seq_file);
	if (!f_seq)
	{
		cerr<<"can not open "<<seq_file<<endl;
		exit(1);
	}
	ifstream f_bk(bk_file);
	if (!f_bk)
	{
		cerr<<"can not open "<<bk_file<<endl;
		exit(1);
	}
	//read in sequence file
	SEQ seq;
	Rseqs(f_seq, seq);
	f_seq.close();
	//read in breakpoint file
	BLOCK bk_point;
	InBreakpoint(bk_point, f_bk);
	f_bk.close();
	//
	map<read_t, unsigned int> id_cluster;
	map<unsigned int, set<read_t> > cluster_ids;
	unsigned int num_clusters = cluster_link(id_cluster, cluster_ids, fin);
	//read in linkage file
	fseek(fin, 0, SEEK_SET);
	map<unsigned int, list<size_t> > sep_link;
	separate_link(sep_link, id_cluster, fin);
	fclose(fin);
	//read in seq file

	//output
	char curr_dir[100];
	getcwd(curr_dir, 100);
	int sum_size=0;
	for (int i=1; i<=num_clusters; i++)
	{
		if (cluster_ids[i].size() < min_cluster)
		{
			continue;
		}
		char dir_name[100];
		sprintf(dir_name, "%s%d", cluster_pre, i);
		mkdir(dir_name, S_IRWXU);
		if (chdir(dir_name))
		{
			cerr<<"can not jump to "<<dir_name<<endl;
			exit(1);
		}
		//output
		//bk
		ofstream fout_bk(out_bk_file);
		for (set<read_t>::iterator p=cluster_ids[i].begin(); p!=cluster_ids[i].end(); p++)
		{
			fout_bk<<*p<<"\t"<<bk_point[*p].size<<"\t"<<bk_point[*p].begin<<"\t"<<bk_point[*p].end<<endl;
		}
		fout_bk.close();
		//seq
		ofstream fout_seq(out_seq_file);
		for (set<read_t>::iterator p=cluster_ids[i].begin(); p!=cluster_ids[i].end(); p++)
		{
			fout_seq<<">"<<*p<<endl;
			fout_seq<<seq[*p]<<endl;
		}
		fout_seq.close();
		//link
		FILE * fout = fopen(out_link_file, "wb");
		size_t d;
		for (list<size_t>::iterator p=sep_link[i].begin(); p!=sep_link[i].end(); p++)
		{
			d=*p;
			fwrite(&d, sizeof(size_t), 1, fout);
		}
		fclose(fout);
		chdir(curr_dir);
		cout<<dir_name<<" :\t"<<cluster_ids[i].size()<<endl;
		sum_size+=cluster_ids[i].size();
	}
	cout<<"Total:\t"<<sum_size<<endl;
	//
	return 0;
}
