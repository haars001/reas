#!/usr/bin/perl -w
#*Filename: rename.pl
#*Description: rename sequences by simplifying into numbers
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.04.17
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat input.fa | $0 stdout
        -h	help

notes:

USAGE
#************************************************

my %opts;
GetOptions(\%opts , "h");

die $usage if defined $opts{h};

my $n = 1;
while (<STDIN>) {
	if (/^>(\S+).*/) {
		print ">$n\n";
		$n++;
	}
	else {
		print $_;
	}
}
