#include "LocalAlign.h"
#include <stdlib.h>
#include <algorithm>

using namespace std;

LocalAlign::LocalAlign(void)
{
	_gpo = GPO;
	_gpe = GPE;
}

void LocalAlign::InSeq(string & query, string & sbjct)
{
	_s_query = query;
	_s_sbjct = sbjct;
}
void LocalAlign::Initialize_ff_dd_ii_path(void)
{
	for(int i=0;i<MAX_LEN_Q;i++)
	{
		_ff[i][0]=0;
		_ii[i][0]=_gpo;
		_path[i][0]=3;           //border
	}
	for(int j=0;j<MAX_LEN_S;j++)
	{
		_ff[0][j]=0;
		_dd[0][j]=_gpo;
		_path[0][j]=3;           //border
	}
}
inline int LocalAlign::ChangeCharToInt(const char c)
{
	for (int i=0; i<8; i++)
	{
		if (c==FORMAT[i])
		{
			return i;
		}
	}
	return 7;
}
void LocalAlign::Score()
{
	if(_s_query.size()>=MAX_LEN_Q)
	{
		cerr<<"_s_query.s.size()>=MAX_LEN_Q :  "<<_s_query.size()<<endl;
		exit(0);
	}
	if(_s_sbjct.size()>=MAX_LEN_S)
	{
		cerr<<"_s_sbjct.s.size()>=MAX_LEN_S :  "<<_s_sbjct.size()<<endl;
		exit(0);
	}
	//
	for(int i=1;i!=_s_query.size()+1;i++)
		for(int j=1;j!=_s_sbjct.size()+1;j++)
		{
			DATATYPE m=_dd[i-1][j]+_gpe;
			DATATYPE n=_ff[i-1][j]+_gpo+_gpe;
			if(m>=n)
				_dd[i][j]=m;
			else
				_dd[i][j]=n;
			m=_ii[i][j-1]+_gpe;
			n=_ff[i][j-1]+_gpo+_gpe;
			if(m>=n)
				_ii[i][j]=m;
			else
				_ii[i][j]=n;
			//score and memery path:
			DATATYPE v=_ff[i-1][j-1]+
			SMATRIX[ChangeCharToInt(_s_query[i-1])][ChangeCharToInt(_s_sbjct[j-1])];
	//		cout<<_s_query[i-1]<<"\t"<<_s_sbjct[j-1]<<"\t"<<ChangeCharToInt(_s_query[i-1])<<"\t"<<ChangeCharToInt(_s_sbjct[j-1])<<"\t"<<SMATRIX[ChangeCharToInt(_s_query[i-1])][ChangeCharToInt(_s_sbjct[j-1])]<<endl;
			if((v<=0)&&(_dd[i][j]<=0)&&(_ii[i][j]<=0))
			{
				_ff[i][j]=0;
				_path[i][j]=3;        //new path start
			}
			else if((_dd[i][j]>=v)
				&&(_dd[i][j]>=_ii[i][j]))
			{
				_ff[i][j]=_dd[i][j];
				_path[i][j]=-1;        //deletion
			}
			else if((_ii[i][j]>=v)
				&&(_ii[i][j]>_dd[i][j]))
			{
				_ff[i][j]=_ii[i][j];
				_path[i][j]=1;          //insertion
			}
			else if((v>_ii[i][j])&&(v>_dd[i][j]))
			{
				_ff[i][j]=v;
				_path[i][j]=0;         //match
			}
			else
			{
				cerr<<"lose something"<<endl;
				exit(0);
			}
		}
}
void LocalAlign::Repath(void)
{
	DATATYPE max_sc = 0;
	
	int n_l, n_h;
	//find max score in possible regions:
	for(int i=1; i<=_s_query.size(); i++)
	{
		for (int j=1; j<=_s_sbjct.size(); j++)
		{
			if (_ff[i][j] >= max_sc)
			{
				max_sc=_ff[i][j];
				n_l=i;
				n_h=j;
			}
		}
	}
	_q_end = n_l-1;
	_s_end = n_h-1;
	//
	_out_query.erase(_out_query.begin(),_out_query.end());
	_out_sbjct.erase(_out_sbjct.begin(),_out_sbjct.end());
	//repath
	while ((n_l>0) && (n_h>0) && (abs(_ff[n_l][n_h])>1e-6))    //whether repath to the start
	{
		if(_path[n_l][n_h]==0)
		{
			_out_query.push_back(_s_query[n_l-1]);
			_out_sbjct.push_back(_s_sbjct[n_h-1]);
			n_l--;
			n_h--;
		}
		else if(_path[n_l][n_h]==-1)
		{
			do
			{
				_out_query.push_back(_s_query[n_l-1]);
				_out_sbjct.push_back('-');
				n_l--;
			}while((n_l>0) && (abs(_dd[n_l+1][n_h]-_dd[n_l][n_h]-_gpe)<1e-6));
		}
		else if(_path[n_l][n_h]==1)
		{
			do
			{
				_out_query.push_back('-');
				_out_sbjct.push_back(_s_sbjct[n_h-1]);
				n_h--;
			}while((n_h>0) && (abs(_ii[n_l][n_h+1]-_ii[n_l][n_h]-_gpe)<1e-6));
		}
		else
		{
			cerr<<"error path "<<_path[n_l][n_h]<<"  --"<<n_l<<", "<<n_h<<endl;
			exit(0);
		}
	}
	_q_begin = n_l;
	_s_begin = n_h;

	reverse(_out_query.begin(), _out_query.end());
	reverse(_out_sbjct.begin(), _out_sbjct.end());
}
void LocalAlign::DoAlign(string & query, string & sbjct)
{
	InSeq(query, sbjct);
	Initialize_ff_dd_ii_path();
	Score();
	Repath();
}
float LocalAlign::OutPident(void)
{
	int ident=0;
	for(int i=0;i!=_out_query.size();i++)
	{
		if  ((_out_query[i]!='-') && (_out_sbjct[i]!='-') && (_out_query[i] == _out_sbjct[i]))					
		{
			ident++;

		}
	}
	_ident = (float)ident/_out_query.size();
	return _ident;
}
string LocalAlign::OutQueryAlign(void)
{
	return _out_query;
}
string LocalAlign::OutSbjctAlign(void)
{
	return _out_sbjct;
}
int LocalAlign::OutQueryBegin(void)
{
	return _q_begin;
}
int LocalAlign::OutQueryEnd(void)
{
	return _q_end;
}
int LocalAlign::OutSbjctBegin(void)
{
	return _s_begin;
}
int LocalAlign::OutSbjctEnd(void)
{
	return _s_end;
}
