/******************************************************************
** kmer2reads.cc
** Copyright (c) BGI 2003
** Author:Li Shengting <lishengting@genomics.org.cn>
** Program Date :2003-2-6 1:46
** Modifier:Li Shengting <lishengting@genomics.org.cn>
** Last Modified:2003-2-14 0:34
** Description:
** Version:1.00 create version
**		   1.02 add statistic function	
******************************************************************/
//#define SINGLE_ELEMENT
#ifdef SINGLE_ELEMENT
#include <set>
#endif
#include <unistd.h>
#include <malloc.h>
#include "common.h"
#include "fasta.h"
#include "hugeBlock.h"
#include <map>
#include <math.h>

using namespace std;

int compact;

static kmer_t get_start(uchar *buf,int len,int &i,int K)
// calculate the start k-mer. this will also be called if `N' is met.
{
	static int k;
	static kmer_t t;
	static uchar tmp;
	
	for(t=0,k=i;i<K+k && i<len;i++) {
		tmp=buf[i];
		if(tmp>=4) { k=i+1;t=0;continue; } // skip `N' and restart.
		t=(t<<2)|tmp;
	}
	if(i!=K+k) return(kmer_t(~0)); // fail to construct a k-mer
	return(t);
}
//added by Ruiqiang Li at 2006-10-17, to filter simple kmers
//rm simple kmers
int rmSimpleKmers(HugeBlock<kcount_t> &dl)
{
	size_t num = (size_t)pow((double)4, K);
	for (size_t kmer=0; kmer<num; kmer++)
	{
		if (dl[kmer]<D)
		{
			continue;
		}
		map<int, int> occur;
		for (int i=1; i<4; i++)
		{
			occur.clear();
			size_t u_tl = (size_t)pow((double)4,i) - 1;
			for (int j=0; j<=K-i; j++)
			{
				int uni = (kmer>>(j*2))&u_tl;
				if (occur.find(uni) == occur.end())
				{
					occur[uni] = 1;
				}
				else
				{
					occur[uni]++;
				}
			}
			for (map<int,int>::iterator pm=occur.begin(); pm!=occur.end(); pm++)
			{
				if (pm->second > K*3/4/i)
				{
					dl[kmer] = 0;  //simple
				}
			}
		}
	}
	return 0;
}
static inline int isright(HugeBlock<kcount_t> &dl,kmer_t kmer) {
	static kcount_t tmp;

	tmp=dl[kmer];

	//modified by Ruiqiang Li at 2006-05-04, so that will remain high-depth kmers
	if(tmp>=D) return(1);
	return(0);
}

int kmer2reads(int fin,FILE * outfile,HugeBlock<kcount_t> &dl)
{
	int i,len,buf_size=0;
	uchar *buf=NULL,tmp;
	kmer_t t,mask,t3;

	HugeBlock<slist_t> sl(BLOCK_BIT);
	mask=(kmer_t(~0))>>(sizeof(kmer_t)*8-K*2);
	size_t count=0;
	int t2=sizeof(slist_t)*8-K*2;
	//int j=0;
	while(1) {
		read(fin,&len,sizeof(len));
		if (len==0)	continue;
		if (len<0) break;
		if (buf_size<len) {
			buf=(uchar *)realloc(buf,len*2);
			buf_size=len*2;
		}
		read(fin,buf,len);

		i=0;
		t=get_start(buf,len,i,K);
#ifdef SINGLE_ELEMENT
		set<slist_t> check_set;
#endif
		slist_t e;
		while(i<len) {
			if (isright(dl,t)) {
				t3=reverse_kmer(t);
				e=(slist_t((t<t3)?t:t3)<<t2)|count;
#ifdef SINGLE_ELEMENT
				if (!check_set.count(e)) {
					sl.append(e);
					check_set.insert(e);
				}
#else
				sl.append(e);
				//printf("%lu->%lX\n",j++,e);
#endif
			}
			tmp=buf[i];
			if(tmp>=4) t=get_start(buf,len,i,K);
				else { t=((t<<2)|tmp)&mask;i++; }
		}
		if(t!=kmer_t(~0) && isright(dl,t)) {
			t3=reverse_kmer(t);
			e=(slist_t((t<t3)?t:t3)<<t2)|count;
			sl.append(e);
			//printf("%lu->%lX\n",j++,e);
		}
		count++;
	}
	cal_time("Build kmer-reads pair list");
	/*
	fputc(K,stdout);
	fputc(D,stdout);
	size_t num=sl.size();
	fwrite(&num,sizeof(num),1,stdout);
	sl.output(stdout,0);
	cal_time("Output no sort kmer-reads pair list to stdout");
	*/
	sl.quick_sort();
	//sl.heap_sort();
	cal_time("Sort kmer-reads pair list");
	/*
	fprintf(outfile,"%d\n%d\n",K,M);
	slist_t maskl=(slist_t(~0))<<(sizeof(slist_t)*8-2*K);
	slist_t maskh=(slist_t(~0))>>2*K;
	for (int i=0;i<sl.size();i++ ){
		fprintf(outfile,"%X -> %lu\n",(sl[i]&maskl)>>(sizeof(slist_t)*8-2*K),(sl[i])&maskh);
	}
	*/
	fputc(K,outfile);
	fputc(D,outfile);
	//num=sl.size();
	size_t num=sl.size();
	fwrite(&num,sizeof(num),1,outfile);
	if (compact) {
		sl.outCompact(outfile,1,(slist_t(~0))<<(sizeof(slist_t)*8-2*K),count);
	}else{
		sl.output(outfile,1);
	}
	cal_time("Output kmer-reads pair list");
	return count;
}

void usage (FILE *fp,int exit_code)
{
	fprintf(fp,"Program : kmer2reads\n");
	fprintf(fp,"Version : %.2f\n",VERSION);
	fprintf(fp,"Contact : liheng, lishengting or zhangy@genomics.org.cn\n\n");
	fprintf(fp,"Usage : kmer2reads [options] reads_fasta\n");
	fprintf(fp,"\t-o output file, [stdout]\n");
	fprintf(fp,"\t-c (switch,output compact format file)\n");
	fprintf(fp,"\t-d D value, [%u]\n",D);
	fprintf(fp,"\t-b number of bits of the size for a huge block, [%u]\n",BLOCK_BIT);
	fprintf(fp,"\t-i kmers number file, [stdin]\n");
	fprintf(fp,"\t-x (switch,input text format)\n");
	fprintf(fp,"\t-s statistic output file, [none]\n");
	fprintf(fp,"\t-h Display this usage infileormation.\n\n");

	exit (exit_code);
}

int main (int argc,char* argv[])
{
	FILE *outfile=NULL,*infile=NULL,*kmerfile=NULL,*statfile=NULL;
///////////////////////////////////////////////////////////////////////////////////////////////////
//Get options
///////////////////////////////////////////////////////////////////////////////////////////////////
	if(argc<2) usage(stderr,1);
	int text=0;
	int c;
	compact=0;
	while ((c=getopt (argc,argv,"hd:b:xi:o:s:c"))>=0) {
		switch (c) {
			case '?':
			case 'h':
				usage (stdout,0);
			case 'd':
				D=atoi(optarg);
				break;
			case 'b':
				BLOCK_BIT=atoi(optarg);
				break;
			case 'i':
				kmerfile=fopen(optarg,"r");
				break;
			case 'x':
				text=1;
				break;
			case 'o':
				outfile=fopen(optarg,"w");
				break;
			case 'c':
				compact=1;
				break;
			case 's':
				statfile=fopen(optarg,"w");
				break;
			default:
				usage (stderr,1);
		}
	}

	if (optind<argc) 
		infile=fopen(argv[optind],"r");
	else 
		infile=stdin;
///////////////////////////////////////////////////////////////////////////////////////////////////
//Main
///////////////////////////////////////////////////////////////////////////////////////////////////
	if (!outfile)
		outfile=stdout;
	if (!infile)
		syserr("Can't open input file!");
	if (!kmerfile)
		kmerfile=stdin;

	if (infile==kmerfile) {
		syserr("Reads fasta file can't equal with kmers number file!");
	}

	int fd[2];
	if (pipe(fd)<0)
		syserr("Pipe error!");

	pid_t pid;
	if ((pid=fork())>0) {
		cal_time("Start kmer2reads");
		if (text) {
			fscanf(kmerfile,"%d",&K);
		}else{
			K=fgetc(kmerfile);
		}
		HugeBlock<kcount_t> dl(BLOCK_BIT,1<<(2*K>BLOCK_BIT ? 2*K-BLOCK_BIT : 0));
		if (text) {
			dl.inText(kmerfile,size_t(1)<<2*K);
		}else{
			dl.input(kmerfile,size_t(1)<<2*K);
		}
		rmSimpleKmers(dl);
		fprintf(stderr,"%lu kmers number read!\n",dl.size());
		cal_time("Read kmers number from file");
		if (statfile) {
			size_t dn[256];
			memset(dn,0,sizeof(size_t)*256);
			for (size_t i=0;i<dl.size();i++) {
				dn[dl[i]]++;
			}
			for (int i=0;i<256;i++) {
				fprintf(statfile,"%d:%lu\n",i,dn[i]);
			}
			fclose(statfile);
			cal_time("Output statistic infomation");
		}
		int readsNum=kmer2reads(fd[0],outfile,dl);
		//fwrite(&readsNum,sizeof(readsNum),1,stdout);
		//fwrite(&readsNum,sizeof(readsNum),1,outfile);
		fprintf(stderr,"Total %d reads\n",readsNum);
		cal_time("Close");
	}else if (pid==0) {
		aFasta aread;
		char locus[LOCUS_LEN];
		int len;

		while ((len=read_fasta(infile,aread,locus))>=0)
		{
			write(fd[1],&len,sizeof(len));
			write(fd[1],aread.buf,len);
		}
		write(fd[1],&len,sizeof(len));
	}else{
		syserr("Can't fork()!");
	}

	fclose(kmerfile);
	fclose(outfile);
	fclose(infile);
	return 0;
}
