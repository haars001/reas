#ifndef FASTA_H_
#define FASTA_H_

#include<stdio.h>
#include<malloc.h>
#include"bio_table.h"

const int BASIC_BUFFER=4096;

class aFasta
{
	int max,sp;
public:
	aFasta(void) { max=0;sp=0;buf=0; };
	~aFasta(void) { free(buf);buf=0; };
	unsigned char *buf;
	inline void reset(void) { sp=0; };
	inline int get_len(void) { return(sp); };
	inline int size(void) { return sp; };
	inline void resize(int len)
	{
		if (len >= max) {
			max = len;
			buf = (unsigned char*)realloc(buf, max);
		}
		sp = len;
	}
	inline void add(unsigned char c) {
		if(sp==max) {
			max+=BASIC_BUFFER;
			buf=(unsigned char*)realloc(buf,max);
		}
		buf[sp++]=c;
	};
};

int read_fasta(FILE *fp,aFasta &buf,char *locus,bool bin=true);
int read_qual(FILE *fp, aFasta &buf, char *locus);

inline void reverse4(uchar *buf, int len)
{
	uchar tmp;
	for (int i = 0; i < len/2; ++i) {
		tmp = buf[i]; buf[i] = buf[len - i - 1]; buf[len - 1 - i] = tmp;
	}
}
inline void counter4(uchar *buf, int len)
{
	for (int i = 0; i < len/2; ++i)
		if (buf[i] < 4) buf[i] = (~buf[i]) & 3;
}

#endif
