#!/usr/bin/perl -w
#*Filename: AssignSeq.pl
#*Description: assign sequences into groups according to clustering information.
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.11
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat <clustering> | $0 [options]
	-s	STRING	whole sequences dataset
	-m	INT	minimal group size, will not output small groups, default = 20
	-p	STRING	prefix of directory names, default = "cluster_"
	-h	help

notes:

USAGE
#************************************************

my %opts;
GetOptions(\%opts, "c=s" , "s=s" , "m=i" , "p=s" , "h");

die $usage if defined $opts{h};
my $seq_file = defined $opts{s}? $opts{s} : "";
my $min_group = defined $opts{m}? $opts{m} : 20;
my $prefix = defined $opts{p}? $opts{p} : "cluster_";

open (fin_seq, $seq_file) || die "can not open $seq_file\n";

my %cluster;
my $c;
my $order;
while (<stdin>) {
	if (/^(\d+)\s+(\d+)/) {
		$order = $1;
		$c = $2;
	}
	if (!defined $cluster{$c}) {
		my @a;
		$cluster{$c} = \@a;
	}
	push(@{$cluster{$c}}, $order);
}
#read in sequences
my @seq_id;
my %seq_s;

my $id;
while (<fin_seq>) {
	if (/^>(\S+)/) {
		$id=$1;
		push(@seq_id, $id);
	}
	else {
		chomp;
		$seq_s{$id} .= $_;
	}
}
close fin_seq;
#assign
foreach my $e (keys %cluster) {
	if (@{$cluster{$e}} < $min_group) {
		next;
	}
	mkdir("$prefix$e") || die "can not mkdir $prefix$e\n";

	open ($e, ">$prefix$e/reads.fa") || die "can not open output file $prefix$e/reads.fa\n";
	foreach my $i (@{$cluster{$e}}) {
		$id = $seq_id[$i-1];
		print $e ">$id\n";
		print $e "$seq_s{$id}\n";
	}
	close $e;
}
