/******************************************************************
** common.cc
** Copyright (c) BGI 2003
** Author:Li Shengting <lishengting@genomics.org.cn>
** Program Date :2003-2-5 17:40
** Modifier:Li Shengting <lishengting@genomics.org.cn>
** Last Modified:2003-2-5 17:41
** Description:
** Version:1.0 create version
******************************************************************/
#include<time.h>
#include <string>
#include "common.h"
#include <strings.h>
using namespace std;

int K=17;
int D=12;
int M=40;
int min_M=8;
int BLOCK_BIT=23;
volatile size_t mem_size=0;


void syserr(char * str) {
	fprintf(stderr,"%s\n",str);
	exit(1);
}

kmer_t reverse_kmer(kmer_t kmer) {
	static int i;
	static kmer_t tmp;
	/*
	if (kmer>=kmer_t(1)<<(K<<1)) {
		fprintf(stderr,"%lu\n",kmer);
		syserr("kmer >= kmer_t(1)<<(2*K)!");
	}
	*/
	for(i=0,kmer=~kmer,tmp=0;i<K;i++) {
		tmp=(tmp<<2)|(kmer&0x3);
		kmer>>=2;
	}
	return(tmp);
}

void checkLicence() {
	char *hostname=getenv("HOSTNAME");
//	if (strcmp(hostname,"690a")) {
//		syserr("\nSorry,Licence not available!\nPlease contact with lishengting@genomics.org.cn!\n");
//	}
}

void cal_time(char * str) {
	static time_t cpu_clock,cpu_clock2;
	static int count=0;
	static FILE *clock_fp;

	time_t t;
	if(count==0) {
		time(&t);
#ifdef TIMEOUT_STDERR
		clock_fp=stderr;
#else
		char str_t[16];
		string file_name="run_";
		sprintf(str_t,"%u",t);
		file_name+=str_t;
		file_name.resize(file_name.length()-1);
		file_name+=".log";
		clock_fp=fopen(file_name.c_str(),"w+");
#endif
		fprintf(clock_fp,"\n%-50s%s\n",str,ctime(&t));
#ifndef TIMEOUT_STDERR
		fflush(clock_fp);
#endif
		time(&cpu_clock);
	} else if(!strncasecmp(str,"close",5)) {
		time(&t);
		fprintf(clock_fp,"%-50s%s\n","End",ctime(&t));
#ifndef TIMEOUT_STDERR
		fflush(clock_fp);
		fclose(clock_fp);
#endif
	} else {
		time(&cpu_clock2);
		fprintf(clock_fp,"%-50s%16ld secs\n",str,
			cpu_clock2-cpu_clock);
#ifndef TIMEOUT_STDERR
		fflush(clock_fp);
#endif
		cpu_clock=cpu_clock2;
	}
	count++;
}
