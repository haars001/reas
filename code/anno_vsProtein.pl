#!/usr/bin/perl -w
#*Filename: anno_vsProtein.pl
#*Description: annotate assembled consensus sequences with known TE-related proteins with blastx,
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.10.08
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options] <stdout>
	-query	STRING	query file, default=
	-gag	STRING	gag proteins, default=
	-pol	STRING	pol proteins, default=
	-rt	STRING	reverse transcriptase enzymes, default=
	-rh	STRING	ribonuclease H, default=
	-int	STRING	integrase enzymes, default=
	-trp	STRING	transposases, default=
	-env	STRING	envelope proteins, default=
	-e	FLOAT	e-value threshold, default=1e-5
	-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts , "query=s" , "gag=s" , "pol=s" , "rt=s" , "rh=s", "int=s", "trp=s", "env=s", "e=s", "h");

die $usage if defined $opts{h};
my $query = defined $opts{query}? $opts{query} : die;
my $gag_file = defined $opts{gag}? $opts{gag} : "~/database/TE/TEproteins/gag.061007.pep";
my $pol_file = defined $opts{pol}? $opts{pol} : "~/database/TE/TEproteins/pol.061007.nonRD.pep";
my $rt_file = defined $opts{rt}? $opts{rt} : "~/database/TE/TEproteins/reverse_transcriptase.061007.nonRD.pep";
my $int_file = defined $opts{int}? $opts{int} : "~/database/TE/TEproteins/integrase.061007.pep";
my $trp_file = defined $opts{trp}? $opts{trp} : "~/database/TE/TEproteins/transposases.061007.nonRD.pep";
my $env_file = defined $opts{env}? $opts{env} : "~/database/TE/TEproteins/env.061007.pep";
my $e = defined $opts{e}? $opts{e} : 1e-5;

my %des_gag = &read_des($gag_file);
my %des_pol = &read_des($pol_file);
my %des_rt = &read_des($rt_file);
my %des_int = &read_des($int_file);
my %des_trp = &read_des($trp_file);
my %des_env = &read_des($env_file);

my %align_gag;
my %align_pol;
my %align_rt;
my %align_int;
my %align_trp;
my %align_env;

my %e_gag;
my %e_pol;
my %e_rt;
my %e_int;
my %e_trp;
my %e_env;

#gag
foreach (`blastall -p blastx -i $query -d $gag_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_gag{$a[0]}) || ($e_gag{$a[0]} > $a[10])) {
		$align_gag{$a[0]} = $a[1];
		$e_gag{$a[0]} = $a[10];
	}
}

#pol
foreach (`blastall -p blastx -i $query -d $pol_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_pol{$a[0]}) || ($e_pol{$a[0]} > $a[10])) {
		$align_pol{$a[0]} = $a[1];
		$e_pol{$a[0]} = $a[10];
	}
}
#rt
foreach (`blastall -p blastx -i $query -d $rt_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_rt{$a[0]}) || ($e_rt{$a[0]} > $a[10])) {
		$align_rt{$a[0]} = $a[1];
		$e_rt{$a[0]} = $a[10];
	}
}
#int
foreach (`blastall -p blastx -i $query -d $int_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_int{$a[0]}) || ($e_int{$a[0]} > $a[10])) {
		$align_int{$a[0]} = $a[1];
		$e_int{$a[0]} = $a[10];
	}
}
#trp
foreach (`blastall -p blastx -i $query -d $trp_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_trp{$a[0]}) || ($e_trp{$a[0]} > $a[10])) {
		$align_trp{$a[0]} = $a[1];
		$e_trp{$a[0]} = $a[10];
	}
}
#env
foreach (`blastall -p blastx -i $query -d $env_file -m8 -e $e 2>/dev/null`) {
	chomp;
	my @a = split(/\s+/, $_);
	next if (@a < 12);
	if ((!defined $e_env{$a[0]}) || ($e_env{$a[0]} > $a[10])) {
		$align_env{$a[0]} = $a[1];
		$e_env{$a[0]} = $a[10];
	}
}
#

my @query_id = &get_ids($query);

print "ID\ttransposase\tE-value\tdescription\tgag\tE-value\tdescription\tpol\tE-value\tdescription\t";
print "reverse transcriptase\tE-value\tdescription\tintegrase\tE-value\tdescription\tenvelope\tE-value\tdescription\n";

foreach my $e (@query_id) {
	print $e;
	if (defined $align_trp{$e}) {
		print "\t$align_trp{$e}\t$e_trp{$e}\t$des_trp{$align_trp{$e}}";
	}
	else {
		print "\t\t\t";
	}
	#
	if (defined $align_gag{$e}) {
		print "\t$align_gag{$e}\t$e_gag{$e}\t$des_gag{$align_gag{$e}}";
	}
	else {
		print "\t\t\t";
	}
	#
	if (defined $align_pol{$e}) {
		print "\t$align_pol{$e}\t$e_pol{$e}\t$des_pol{$align_pol{$e}}";
	}
	else {
		print "\t\t\t";
	}
	#
	if (defined $align_rt{$e}) {
		print "\t$align_rt{$e}\t$e_rt{$e}\t$des_rt{$align_rt{$e}}";
	}
	else {
		print "\t\t\t";
	}
	#
	if (defined $align_int{$e}) {
		print "\t$align_int{$e}\t$e_int{$e}\t$des_int{$align_int{$e}}";
	}
	else {
		print "\t\t\t";
	}
	#
	if (defined $align_env{$e}) {
		print "\t$align_env{$e}\t$e_env{$e}\t$des_env{$align_env{$e}}";
	}
	else {
		print "\t\t\t";
	}
	print "\n";
}

sub get_ids {
	my $seq_file = shift;
	my @ids;
	my @s = `cat $seq_file | grep "^>"`;
	foreach (@s) {
		if (/^>(\S+)\s*/) {
			push(@ids, $1);
		}
	}
	return @ids;
}
sub read_des {
	my $seq_file = shift;
	my %des;
	my @a = `cat $seq_file | grep "^>"`;
	foreach (@a) {
		chomp;
		if (/^>(\S+)\s+(.+)$/) {
			$des{$1} = $2;
		}
	}
	return %des;
}

sub do_align {
	my ($q, $s, %align_id, %align_e) = @_;
	foreach (`blastall -p blastx -i $q -d $s -m8 -e $e 2>/dev/null`) {
		chomp;
		my @a = split(/\s+/, $_);
		next if (@a < 12);
		if ((!defined $align_e{$a[0]}) || ($align_e{$a[0]} > $a[10])) {
			$align_id{$a[0]} = $a[1];
			$align_e{$a[0]} = $a[10];
		}
	}
}
