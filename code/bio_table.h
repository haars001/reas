#ifndef BIO_TABLE_H_
#define BIO_TABLE_H_

typedef unsigned char uchar;
typedef unsigned long ulong;

const uchar DNA_a=0x01;
const uchar DNA_g=0x02;
const uchar DNA_c=0x04;
const uchar DNA_t=0x08;
const uchar DNA_r=DNA_a|DNA_g;
const uchar DNA_y=DNA_c|DNA_t;
const uchar DNA_k=DNA_g|DNA_t;
const uchar DNA_m=DNA_a|DNA_c;
const uchar DNA_s=DNA_g|DNA_c;
const uchar DNA_w=DNA_a|DNA_t;
const uchar DNA_b=DNA_g|DNA_c|DNA_t;
const uchar DNA_d=DNA_a|DNA_g|DNA_t;
const uchar DNA_h=DNA_a|DNA_c|DNA_t;
const uchar DNA_v=DNA_a|DNA_g|DNA_c;
const uchar DNA_n=DNA_a|DNA_t|DNA_g|DNA_c;
const uchar DNA_gap = 0x00;

const uchar DNA_A=0x00; 
const uchar DNA_G=0x01; 
const uchar DNA_C=0x02; 
const uchar DNA_T=0x03;
const uchar DNA_GAP = 0x04;

extern uchar gene_table1[128],gene_table2[16],gene_table3[16];
extern uchar gene_table4[128], gene_table5[5], gene_table6[16];
extern uchar gene_table7[128];
extern char protein_table[64];
extern uchar protein_table1[128];
extern uchar protein_table2[26];
extern uchar protein_table3[128];
extern uchar protein_table4[26];
extern double protein_table5[20];
extern uchar protein_table6[64];
extern char *protein_name[20];

#endif
