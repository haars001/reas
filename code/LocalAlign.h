#ifndef _LOCALALIGN_H_
#define _LOCALALIGN_H_

#include<vector>
#include<string>
#include<stdio.h>
#include<math.h>
#include<iostream>

using namespace std;

typedef int DATATYPE;
const string FORMAT= "ACGTYRNX";
//A  C  G  T  Y  R  N  X
const int SMATRIX[8][8] = {
	2, -2,  0, -2, -2,  1,  0, -2,
   -2,  2, -2,  0,  1, -2,  0, -2,
	0, -2,  2, -2, -2,  1,  0, -2,
   -2,  0, -2,  2,  1, -2,  0, -2,
   -2,  1, -2,  1,  1, -2,  0, -2,
	1, -2,  1, -2, -2,  1,  0, -2,
    0,  0,  0,  0,  0,  0,  0,  0,
   -2, -2, -2, -2, -2, -2,  0, -2
};
const int MAX_LEN_Q=5000;
const int MAX_LEN_S=5000;
const int SCORE=20;
const int OUTLEN=60;
const DATATYPE GPO=-10;
const DATATYPE GPE=-2;

//Local align one pair sequences:
class LocalAlign
{
public:
	LocalAlign(void);
	void InSeq(string & query, string & sbjct);
//	void SetParameters(void);
	void Initialize_ff_dd_ii_path(void);
	inline int  ChangeCharToInt(const char c);
	void Score(void);
	void Repath(void);
	void DoAlign(string & query, string & sbjct);
	float OutPident(void);
	string OutQueryAlign(void);
	string OutSbjctAlign(void);
	int OutQueryBegin(void);
	int OutQueryEnd(void);
	int OutSbjctBegin(void);
	int OutSbjctEnd(void);
protected:
	DATATYPE _smatrix[8][8];
	DATATYPE _gpo;
	DATATYPE _gpe;

	string _s_query;
	string _s_sbjct;

	DATATYPE _ff[MAX_LEN_Q][MAX_LEN_S];
	DATATYPE _dd[MAX_LEN_Q][MAX_LEN_S];
	DATATYPE _ii[MAX_LEN_Q][MAX_LEN_S];
	int   _path[MAX_LEN_Q][MAX_LEN_S];

	DATATYPE _score;
	
	float _ident;
	int _q_begin;
	int _q_end;
	int _s_begin;
	int _s_end;
	string _out_query;
	string _out_sbjct;
	vector<int> _out_n_query;
	vector<int> _out_n_sbjct;
	string _out_align;
};

#endif
