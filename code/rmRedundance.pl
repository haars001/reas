#!/usr/bin/perl -w
#*Filename: rmRedundance.pl
#*Description: remove redundance
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.06.29
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
	-inseq	STRING	input sequence file
	-outseq	STRING	output sequence file
	-overlap	FLOAT	fraction overlaped, default=0.8
	-end	INT	remainning sequences at ends allowed, default=200
	-ident	FLOAT	identity, default=0.8
	-h	help

notes:

USAGE
#************************************************

die $usage if (@ARGV<1);
my %opts;
GetOptions(\%opts , "inseq=s", "outseq=s", "overlap=s", "end=i", "ident=s", "h");

die $usage if defined $opts{h};
my $inseq = defined $opts{inseq}? $opts{inseq} : die;
my $outseq = defined $opts{outseq}? $opts{outseq} : die;
my $overlap = defined $opts{overlap}? $opts{overlap} : 0.8;
my $remain = defined $opts{end}? $opts{end} : 200;
my $ident = defined $opts{ident}? $opts{ident} : 0.8;

my %to_remove;

foreach (`cross_match.manyreads -masklevel 101 $inseq $inseq 2>/dev/null | ConvertAlign.pl`) {
	chomp;
	my @a = split(/\s+/, $_);
	if (@a != 10) {
		next;
	}
	if ($a[0] eq $a[1]) {
		next;
	}
	if ($a[3] < $ident) {
		next;
	}
	if ((($a[5] < $remain) && ($a[4]-$a[6]<$remain) && ($a[6]-$a[5]+1 >= $a[4]*$overlap)) ||
		(($a[8] < $remain) && ($a[7]-$a[9]<$remain) && ($a[9]-$a[8]+1 >= $a[7]*$overlap))) {
		if ($a[4] > $a[7]) {
			$to_remove{$a[1]} = "$a[7]\t$a[0]";
		}
		elsif ($a[4] == $a[7]) {
			if ((!defined $to_remove{$a[4]}) && (!defined $to_remove{$a[7]})) {
				$to_remove{$a[0]} = "$a[4]\t$a[1]";
			}
		}
		else {
			$to_remove{$a[0]} = "$a[4]\t$a[1]";
		}
	}
}

my $removed = "removed.id";
open (F_removed, ">$removed") || die;
foreach my $e (keys %to_remove) {
	print F_removed "$e\n";
}
close F_removed;
`cat $inseq |picknonListSeq.pl $removed >$outseq`;
