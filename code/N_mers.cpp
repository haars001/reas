/*
*Copyright 2005  BGI
*All right reserved
*
*Filename:  N_mers
*Description:  input read-kmer index file, output statistics of number of HD kmers each sequence have

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2005.05.20
*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<map>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<math.h>

using namespace std;

int M=1;

//usage:
void usage(void)
{
        cout<<"Usage:	<stdin> | N_mers [options] <stdout>\n"
            <<"	-m	INT	threshold of numer of HD kmers, default = 1 (output all)\n"
            <<"	-h	help\n"
            <<endl;
        exit(1);
};

int main(int argc, char *argv[])
{
	//[options]
	int c;
	while((c=getopt(argc,argv,"m:i:h")) != -1) {
			switch(c) {
					case 'm': M = atoi(optarg); break;  
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	int K = fgetc(stdin);
	int D = fgetc(stdin);
	size_t R_part = (size_t)pow(double(2), 64-K*2) - 1;
	size_t M_part = ~R_part;
//	cout<<"M_part:  "<<hex<<M_part<<"   R_part:  "<<R_part<<endl;
	//
	size_t merRead;
	unsigned int read_id;
	map<unsigned int, unsigned int> num_mers;
	size_t count = 0;
	size_t num_lines;
	fread(&num_lines,sizeof(size_t),1,stdin);
	while (fread(&merRead,sizeof(size_t),1,stdin))
	{
		count++;
//		cout<<merRead<<endl;
		read_id = (merRead & R_part);  //it's numbered from 0, so should +1 here
		read_id +=1;
		if (num_mers.end() == num_mers.find(read_id))
		{
			num_mers[read_id] = 1;
		}
		else
		{
			num_mers[read_id]++;
		}
	}
	//output
	for (map<unsigned int, unsigned int>::iterator p=num_mers.begin(); p!=num_mers.end(); p++)
	{
		int n_m = p->second;
		if (n_m >= M)
		{
			cout<<p->first<<"\t"<<n_m<<endl;
		}
	}
	return 0;
}
