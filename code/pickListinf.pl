#!/usr/bin/perl
#*Copyright 2003.10  BGI Liruiqiang
#*All right reserved
#*Filename: pickListinf.pl
#*Abstract: 
#*Version : 1.0
#*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
#*Time:    2003.10.05
#

if (@ARGV < 2) {
	printf "Usage: pickListinf.pl    list.Infile    Info.Infile   stdout\n";
	exit 1;
}

my ($list, $inf) = @ARGV;

my %a;

open (FList, $list) || die;
open (Finf,  $inf) || die;

while (<FList>) {
	chomp;
	$a{$_} = 1;
}

while (<Finf>) {
	if (/^(\S+)/) {
		if (defined $a{$1}) {
			printf $_;
		}
	}
}

close FList;
close Finf;
