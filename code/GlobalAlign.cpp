// lirq 2003.05.04

#include <vector>
#include<string>
#include<stdio.h>
#include<math.h>

using namespace std;

const int max_q = 1000;
const int max_s = 1000;

template <class DataType>
class CGlobalAlign
{
public:
	CGlobalAlign();
	~CGlobalAlign() {};
	void SetParams(DataType match = 1, DataType mismatch = -2, DataType gapo = -10, DataType gape = -2);
	void InSeq(string & query, string & sbjct);
	void InitializeM(void);
	void Score(void);
	void Repath(void);
	void DoAlign(string & query, string & sbjct);
	int OutIdents(void);
	float OutPident(void);
	string OutQueryAlign(void);
	string OutSbjctAlign(void);
private:
	string _query;
	string _sbjct;
	int _qSize;
	int _sSize;
	//
	DataType _match;
	DataType _mismatch;
	DataType _gapo;
	DataType _gape;
	//
	DataType  _ff[max_q][max_s];
	DataType  _dd[max_q][max_s];
	DataType  _ii[max_q][max_s];
	int  _path[max_q][max_s];
	//
	string _outQuery;
	string _outSbjct;
};
template <class DataType>
CGlobalAlign<DataType>::CGlobalAlign()
{
	_match = 1;
	_mismatch = -2;
	_gapo = -10;
	_gape = -2;
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::SetParams(DataType match , DataType mismatch , DataType gapo, DataType gape)
{
	_match = match;
	_mismatch = mismatch;
	_gapo = gapo;
	_gape = gape;
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::InSeq(string & query, string & sbjct)
{
	_query = query;
	_sbjct = sbjct;
	_qSize = _query.size();
	_sSize = _sbjct.size();
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::InitializeM()
{
	_ff[0][0]=0;
	_dd[0][0]=0+_gapo;
	_ii[0][0]=0+_gapo;

	int k;
	for (k=1; k<max_s; k++)
	{
		_ii[0][k]=_ii[0][k-1]+_gape;
		_ff[0][k]=_ii[0][k];
		_dd[0][k]=_ff[0][k]+_gapo+_gape;
	}
	for (k=1; k<max_q; k++)
	{
        _dd[k][0]=_dd[k-1][0]+_gape;
		_ff[k][0]=_dd[k][0];
		_ii[k][0]=_ff[k][0]+_gapo+_gape;
	}

	//path:              1-- insertion; -1--deletetion;  0--align
	_path[0][0]=3;
	for( k=1; k<max_s; k++)
	{
		_path[0][k]=1;
	}
	for( k=1; k<max_q; k++)
	{
		_path[k][0]=-1;
	}
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::Score()
{
	for( int i=1; i<=_qSize; i++)
	{
		for( int j=1; j<=_sSize; j++)
		{
			DataType m,n;
			m=_ff[i-1][j]+_gapo+_gape;
			n=_dd[i-1][j]+_gape;
			if (n>=m)
				_dd[i][j]=n;
			else
				_dd[i][j]=m;
			m=_ff[i][j-1]+_gapo+_gape;
			n=_ii[i][j-1]+_gape;
			if (n>=m)
				_ii[i][j]=n;
			else
				_ii[i][j]=m;
			//
			DataType sc;
			if (_query[i-1] == _sbjct[j-1])
			{
				sc = _match;
			}
			else
			{
				sc = _mismatch;
			}
			DataType b=_ff[i-1][j-1]+sc;
			if ((_dd[i][j] >= _ii[i][j]) && (_dd[i][j]>=b))
			{
				_ff[i][j]=_dd[i][j];
				_path[i][j]=-1;
			}
			else if(_ii[i][j]>=b)
			{
				_ff[i][j]=_ii[i][j];
				_path[i][j]=1;
			}
			else
			{
				_ff[i][j]=b;
				_path[i][j]=0;
			}
		}
	}
/*	for (int i=0; i<=_qSize; i++)
	{
		for (int j=0; j<=_sSize; j++)
		{
			cout<<_path[i][j]<<"\t";
		}
		cout<<endl;
	}*/
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::Repath()
{
	_outQuery.clear();
	_outSbjct.clear();
	//
	int n_l=_qSize;
	int n_h=_sSize;

	while (!((0==n_l) && (0==n_h)))
	{
		if (0==_path[n_l][n_h])
		{
			_outQuery.push_back(_query[n_l-1]);
			_outSbjct.push_back(_sbjct[n_h-1]);
			n_l--;
			n_h--;
		}
		else if (-1==_path[n_l][n_h])
		{
			do
			{
				_outQuery.push_back(_query[n_l-1]);
				_outSbjct.push_back('-');
				n_l--;
			}while ((0!=n_l) && (_dd[n_l+1][n_h]-_dd[n_l][n_h]-_gape == 0));
		}
		else if (1==_path[n_l][n_h])
		{
			do
			{
				_outQuery.push_back('-');
				_outSbjct.push_back(_sbjct[n_h-1]);
				n_h--;
			}while ((0!=n_h) && (_ii[n_l][n_h+1]-_ii[n_l][n_h]-_gape == 0));
		}
		else
		{
			cerr<<"error path "<<_path[n_l][n_h]<<"  ***   "<<n_l<<", "<<n_h<<endl;
			exit(0);
		}
	}
}
//***      ***
template <class DataType>
void CGlobalAlign<DataType>::DoAlign(string & query, string & sbjct)
{
	InSeq(string & query, string & sbjct);
	InitializeM();
	Score();
	Repath();
}
//
template <class DataType>
int CGlobalAlign<DataType>::OutIdents()
{
	int idents = 0;
	for (int i = 0; i != _outQuery.size(); i++)
	{
		if (_outQuery[i] == _outSbjct[i])
		{
			idents++;
		}
	}
	return idents;
}
template <class DataType>
float CGlobalAlign<DataType>::OutPident()
{
	int idents = 0;
	for (int i = 0; i != _outQuery.size(); i++)
	{
		if (_outQuery[i] == _outSbjct[i])
		{
			idents++;
		}
	}
	int min_len = (_qSize <= _sSize ? _sSize : _sSize);
	return (float)idents/min_len;
}
template <class DataType>
string CGlobalAlign<DataType>::OutQueryAlign()
{
	return _outQuery;
}
template <class DataType>
string CGlobalAlign<DataType>::OutSbjctAlign()
{
	return _outSbjct;
}
