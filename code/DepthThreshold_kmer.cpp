/*
    Description: 
 	Compute the depth threshold of k-mer according to false positive
    Author: 
    	Hao LIN (linhao@ict.ac.cn)
    Date:
    	2006-05-17
*/
	
#include<unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

void usage(void)
{
	printf("Usage:	DepthThreshold_kmer [options]\n");
	printf("	-c	double	coverage of WGS, default = 6\n");
	printf("	-k	int	k-mer size, default = 17\n");
	printf("	-p	double	false positive rate, default = 0.001\n");
	printf("	-h	help\n");
	exit(1);
}
double pbinorm(int n, int k, double p)
{
	double i;
	double prob;
	
	prob = 0.0;
	if (k > n - k) {
		k = n - k;
	}
	for (i = k + 1; i <= n; i++) {
		prob = prob + log(i);
	}
	for (i = 1; i <= n - k; i++) {
		prob = prob - log(i);
	}
	prob += k * log(p) + (n - k)*log(1 -p); 
	prob = exp(prob);
	
	return prob;
}

int main(int argc, char* argv[])
{
	double cov = 6;
	int read_len = 500;
	double fp_thresh = 0.001;
	double prob;
	int k=17;
	int i;

	int c;
	while((c=getopt(argc,argv,"c:k:p:h")) != -1) {
		switch(c) {
			case 'c': cov = atof(optarg); break;
			case 'k': k = atoi(optarg); break;
			case 'p': fp_thresh = atof(optarg); break;
			case 'h':usage();    //usage information
			case '?':usage();    //unrecognizable input
		}
	}
	
	if (fp_thresh >= 1) {
		printf("fp should be in range [0.0, 1.0)\n");
		exit(0);
	}
	prob = 0.0;
	i = -1;
	while (prob < 1 - fp_thresh) {
		i++;
		prob += pbinorm((read_len - k + 1), i, (cov / read_len));
	}
	printf("Parameters:\n");
	printf("WGS:\t%lf X\n", cov);
	printf("K-mer:\t%d bp\n", k);
	printf("FP rate:%lf \n\n",fp_thresh);
	printf("Depth threshold:\t%d\n", i);

	return 0;
}
