#include<string>

using  namespace std;

int K = 15;   //mer size, which was set during kmer_num|kmer2reads, will be updated by kmer2reads file
int D = 30;   //mer depth threshold, will be updated by kmer2reads file. so needn't revise these two parameters

int M = 30;   //minimal number of contained high-depth kmers, if say it possibly include a repeat segment

int depth = 6;    //depth cutoff of repeat
int hit_size = 100;       //minimum size of pairwise alignment hit
int nonlcs_size = 50;	//minimum size of align hit after removing lcs.
float hit_ident = 0.6;    //minimum identity of pairwise alignment hit
int bk_size = 50;         //size of remainning sequence at ends of raw reads when defining breakpoints of the internal high-depth segment
int min_extend = 50;       //minimum extension size out of parent seq each time, if smaller, will be defined as containing relationship
int max_extend = 200;      //maximum extension size out of parent seq each time, larger than this threshold will be defined as long distance joining
float avg_ident_bound = 0.7;  //minimal average identity at boundaries of multi-alignment when deciding keeping it or not
int min_join = (depth/3>0? depth/3 : 1);          //minimal depth during extension.
char quiet = 1;         //print log details out or not? 1: no
int sub_multi = -1;     //get a subset of sequences for multi-alignment to get consensus, default -1 use entire set.
float sub_fraction = 1;     //randomly pick up a fraction of reads to do assembly, rather than the whole set if sequencing coverage is real high.
int max_hits = depth*20;      //maximum number of hits are needed to define repeat boundaries, it's only effective when total hits > depth threshold.
char * score_matrix = "mat70";
float child_ident = 0.95;
bool avoid_child_align = 0;   //for a sequence which is fully covered by another sequence, you can align this child sequence with all others to get best sensitivity, or you can avoid the alignments to speed up.
