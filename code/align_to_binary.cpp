/*
*Copyright 2005  BGI
*All right reserved
*
*Filename:  align_to_binary
*Description:  convert alignment to binary format, output is special for Assembly program

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2006.07.10
*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include "Utilities.h"

using namespace std;

//const:

//structure & class & function


//usage:
void usage(void)
{
	cout<<"Usage:	<text align> | align_to_binary <binary_output>\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	//[options]
	int c;
	while((c=getopt(argc,argv,"h")) != -1) {
			switch(c) {
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	FILE * fout = stdout;
	ConvertAlign_binary(cin, fout);
	//
	fclose(fout);
	return 0;
}

