#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include "fasta.h"

static double prob_thres = 0.50;
static int min_qual = 13;

const int PROB_TABLE_SIZE = 102;
const int MAX_LOCUS_LEN = 80;

static double prob_table[PROB_TABLE_SIZE];

static void print_error(const char *s, int exit_code)
{
	fprintf(stderr, "Error(%d): %s\n", exit_code, s);
	exit(exit_code);
}
static void init_prob_table()
{
	prob_table[0] = 0.1;
	for (int i = 1; i < PROB_TABLE_SIZE; ++i)
		prob_table[i] = 1.0 - pow(10.0, -double(i)/10.0);
}
static inline int read_both(int fd, aFasta &buf, int *len, char *locus)
{
	if (!read(fd, len, sizeof(int))) return 0;
	read(fd, locus, sizeof(char) * (*len + 1));
	read(fd, len, sizeof(int));
	buf.resize(*len); 
	read(fd, buf.buf, sizeof(char) * (*len));
	return 1;
}
static inline void write_fasta(int fd, aFasta &buf, char *locus)
{
	int len;
	
	len = strlen(locus);
	write(fd, &len, sizeof(int));
	write(fd, locus, sizeof(char) * (len + 1));
	len = buf.get_len();
	write(fd, &len, sizeof(int));
	write(fd, buf.buf, sizeof(char) * len);
}
static void usage(FILE *fp, int exit_code)
{
	fprintf(fp, "\n");
	fprintf(fp, "Program : maskLow (mask low quality region in reads)\n");
	fprintf(fp, "Version : 0.1.0, on December 18, 2003\n");
	fprintf(fp, "Contact : liheng@genomics.org.cn\n\n");
	fprintf(fp, "Usage   : maskLow [-t thres] [-m min_qual] reads_fasta reads_qual\n\n");
	fprintf(fp, "Options : -t FNUM     probability threshold, default is %.2f\n", prob_thres);
	fprintf(fp, "          -m INUM     minimum quality score, default is %d\n", min_qual);
	fprintf(fp, "          -h          help\n\n");
	exit(exit_code);
}
static void mask_fasta(aFasta &buff, aFasta &bufq)
{
	int start = 0;
	register int i, j;
	double p = 1.0;
	bool flag = false;

	for (i = 0; i < buff.get_len(); ++i) {
		if (int(bufq.buf[i]) < min_qual) {
			if (!flag) {
				start = i;
				p = prob_table[bufq.buf[i]];
				flag = true;
			} else p *= prob_table[bufq.buf[i]];
		} else {
			if (flag && p < prob_thres) {
				for (j = start; j < i; ++j)
					buff.buf[j] = 'N';
			}
			flag = false;
		}
	}
	if (p < prob_thres) {
		for (j = start; j < i; ++j)
			buff.buf[j] = 'N';
	}
}

int main(int argc, char *argv[])
{
	int fdf[2], fdq[2]; // pipe for fasta file and qual file
	int c;
	pid_t childf, childq;

	while ((c = getopt(argc, argv, "t:m:") >= 0)) {
		switch(c) {
			case 'm': min_qual = uchar(atoi(optarg)); break;
			case 't': prob_thres = atof(optarg); break;
			default: usage(stderr, 1);
		}
	}
	if (optind + 1 >= argc) usage(stderr, 1);
	
	pipe(fdf); pipe(fdq);
	
	if ((childf = fork()) == 0) { // childf
		if ((childq = fork()) == 0) { // childq
			aFasta buf;
			char locus[MAX_LOCUS_LEN];
			FILE *fpf;
			int countq = 0;

			fpf = fopen(argv[optind], "r");
			if (!fpf) print_error("Cannot open fasta file", 4);
			close(fdf[0]); close(fdq[0]); close(fdq[1]);
			while (read_fasta(fpf, buf, locus, false) >= 0) {
				++countq;
				write_fasta(fdf[1], buf, locus);
			}
			fclose(fpf);
			close(fdf[1]);
			fprintf(stderr, "Number of sequences in quality file: %d\n", countq);
		} else { // child for qual
			aFasta buf;
			char locus[MAX_LOCUS_LEN];
			FILE *fpq;
			int countf = 0;
	
			fpq = fopen(argv[optind + 1], "r");
			if (!fpq) print_error("Cannot open qual file", 3);
			close(fdq[0]); close(fdf[0]); close(fdf[1]);
			while (read_qual(fpq, buf, locus) >= 0) {
				++countf;
				write_fasta(fdq[1], buf, locus);
			}
			fclose(fpq);
			close(fdq[1]);
			fprintf(stderr, "Number of sequences in fasta file: %d\n", countf);
		}
	} else { // parent
		close(fdq[1]); close(fdf[1]);
		
		register int i;
		int lenf, lenq;
		int tmpf = 0, tmpq = 0, c;
		char locusf[MAX_LOCUS_LEN], locusq[MAX_LOCUS_LEN];
		aFasta buff, bufq;
		
		init_prob_table();
		for (;;) {
			tmpf = read_both(fdf[0], buff, &lenf, locusf);
			tmpq = read_both(fdq[0], bufq, &lenq, locusq);
			if (!tmpf || !tmpq) break;
			if (strcmp(locusf, locusq))
				print_error("Please sort both fasta file and qual file by loci", 2);
			if (buff.size() != bufq.size())
				fprintf(stderr, "Warning: in %s, fasta length does not equal to quality length.\n", locusf);
			mask_fasta(buff, bufq);
			printf(">%s\n", locusf);
			for (i = 0; i < buff.size(); ++i)
				putchar(buff.buf[i]);
			putchar('\n');
		}
		if (tmpf != tmpq)
			fprintf(stderr, "Warning: fasta does not equal to qual: (%d, %d)\n", tmpf, tmpq);
		
		close(fdq[0]); close(fdf[0]);
		wait(&c);
	}
	return 0;
}
