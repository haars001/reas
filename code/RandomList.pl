#!/usr/bin/perl -w
#*Filename: RandomList.pl
#*Description: get a subset with random elements.
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.05.28
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : <InList> | $0 fraction <stdout>


USAGE
die $usage if (@ARGV<1);
#************************************************

my %inList;
while (<stdin>) {
	if (/^(\S+)/) {
		$inList{$1} = 0;
	}
}
#
my $total = keys %inList;

my $num = $total * $ARGV[0];

if ($total <= $num) {
	foreach my $i (keys %inList) {
		print "$i\n";
	}
	exit;
}
#
my @a;
for (my $i = 0; $i < $num; $i++) {
	$a[$i]=1;
}
for (my $i = $num; $i < $total; $i++) {
	$a[$i]=0;
}
for (my $i=0; $i<$total*10; $i++) {
	my $q = int(rand($total));
	my $s = int(rand($total));
	#change
	my $tmp = $a[$q];
	$a[$q] = $a[$s];
	$a[$s] = $tmp;
}

my $count=0;
foreach my $e (keys %inList) {
	if ($a[$count]) {
		print "$e\n";
	}
	$count++;
}

