#!/usr/bin/perl -w
#*Filename: join_fragments.pl
#*Description: pipeline to join fragments according to overlapping information
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.10.04
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
	-inseq	STRING	sequence file
	-outseq	STRING	output file
	-ident	FLOAT	identity threshold, default=0.9
	-end	INT	remainning sequence at ends, default=100
	-hit	INT	hit size, default=300
	-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts , "inseq=s" , "outseq=s" , "ident=s" , "hit=i" , "end=i", "h");

die $usage if defined $opts{h};
my $in_seq_file = defined $opts{inseq}? $opts{inseq} : die;
my $out_seq_file = defined $opts{outseq}? $opts{outseq} : die;
my $ident = defined $opts{ident}? $opts{ident} : 0.9;
my $end = defined $opts{end}? $opts{end} : 100;
my $hit = defined $opts{hit}? $opts{hit} : 300;

open(F_inseq, $in_seq_file) || die;
open(F_outseq, ">$out_seq_file") || die;

#input seq
my %seq;
my $id;
while (<F_inseq>) {
	chomp;
	if (/>(\S+)/) {
		$id = $1;
	}
	else {
		$seq{$id} .= $_;
	}
}
close F_inseq;

my $tmp_file = "tmp.align";
my $tmp_seq = "tmp.seq";

my %align;
my $goon = 1;

while ($goon) {
	$goon = 0;
	&run_align;
	&read_align;
	&rm_involved;
	&join_best;
}

#output
foreach my $e (keys %seq) {
	print F_outseq ">$e\n";
	print F_outseq "$seq{$e}\n";
}
close F_outseq;

`rm $tmp_seq $tmp_file`;


#############
sub run_align {
	open(F_tmp, ">$tmp_seq") || die;
	foreach my $e (keys %seq) {
		print F_tmp ">$e\n";
		print F_tmp "$seq{$e}\n";
	}
	close F_tmp;
	#pairwise alignment
	`cross_match.manyreads -masklevel 101 $tmp_seq $tmp_seq 2>/dev/null|ConvertAlign.pl >$tmp_file`;
}
sub read_align {
	%align = ();
	open(F_align, $tmp_file) || die;
	while (<F_align>) {
		chomp;
		if (/^(\S+)\s+(\S+)\s+(.+)$/) {
			my $q_id = $1;
			my $s_id = $2;
			my @a = split(/\s+/, $3);
			if (($a[1]<$ident) || ($a[4]-$a[3]<$hit)) {
				next;
			}
			if ((($a[3]>$end) && ($a[2]-$a[4]>$end)) || (($a[6]>$end) && ($a[5]-$a[7]>$end))) {
				next;
			}
			if (!defined $align{$q_id}) {
				my %b;
				$b{$s_id} = \@a;
				$align{$q_id} = \%b;
			}
			elsif (!defined $align{$q_id}{$s_id}) {
				$align{$q_id}{$s_id} = \@a;
			}
			elsif (($align{$q_id}{$s_id}[4] - $align{$q_id}{$s_id}[3]) < ($a[4] - $a[3])) {
				$align{$q_id}{$s_id} = \@a;
			}
		}
	}
	close F_align;
}

sub rm_involved {
	my %to_remove;
	foreach my $q (keys %align) {
		foreach my $s (keys %{$align{$q}}) {
			my @inf = @{$align{$q}{$s}};
			if (($inf[3] < $end) && ($inf[2]-$inf[4] < $end) && ($inf[6] < $end) && ($inf[5]-$inf[7] < $end)) {
				if ($inf[2] < $inf[5]) {
					$to_remove{$q} = 1;
				}
				elsif ($inf[2] > $inf[5]) {
					$to_remove{$s} = 1;
				}
				else {
#					$q =~ /\_(\d+)$/;
#					my $order_q = $1;
#					$s =~ /\_(\d+)$/;
#					my $order_s = $1;
#					if ($order_q < $order_s) {
#						$to_remove{$order_s} = 1;
#					}
#					else {
#						$to_remove{$order_q} = 1;
#					}
					if ($q gt $s) {
						$to_remove{$q} = 1;
					}
					else {
						$to_remove{$s} = 1;
					}
				}
			}
			elsif (($inf[3] < $end) && ($inf[2]-$inf[4] < $end)) {
				$to_remove{$q} = 1;
			}
			elsif (($inf[6] < $end) && ($inf[5]-$inf[7] < $end)) {
				$to_remove{$s} = 1;
			}
		}
	}
	foreach my $e (keys %to_remove) {
		delete $seq{$e};
	}
}

sub join_best {
	my %joined;
	while (1) {
		#find longest joinable hit
		my $best_hit = 0;
		my $best_q;
		my $best_s;
		foreach my $q (keys %align) {
			foreach my $s (keys %{$align{$q}}) {
				if (!defined $seq{$q} || !defined $seq{$s} || defined $joined{$q} || defined $joined{$s}) {
					next;
				}
				my @inf = @{$align{$q}{$s}};
				if ($inf[0] eq "+") {
					if ((($inf[2]-$inf[4]<$end) && ($inf[6]<$end)) || (($inf[3]<$end) && ($inf[5]-$inf[7]<$end))) {
						if ($inf[4] - $inf[3] > $best_hit) {
							$best_hit = $inf[4] - $inf[3];
							$best_q = $q;
							$best_s = $s;
						}
					}
				}
				else {
					if ((($inf[2]-$inf[4]<$end) && ($inf[5]-$inf[7]<$end)) || (($inf[3]<$end) && ($inf[6]<$end))) {
						if ($inf[4] - $inf[3] > $best_hit) {
							$best_hit = $inf[4] - $inf[3];
							$best_q = $q;
							$best_s = $s;
						}
					}
				}
			}
		}
		if (!$best_hit) {
			last;
		}
		else {
			$goon=1;
		}
		#join the two fragments
		my @inf = @{$align{$best_q}{$best_s}};
#		print "@inf\n";
		my $new_seq;
		if ($inf[0] eq "+") {
			if (($inf[2]-$inf[4]<$end) && ($inf[6]<$end)) {
				$new_seq = substr($seq{$best_q}, 0, $inf[4]+1);
				$new_seq .= substr($seq{$best_s}, $inf[7]+1);

			}
			elsif (($inf[3]<$end) && ($inf[5]-$inf[7]<$end)) {
				$new_seq = substr($seq{$best_s}, 0, $inf[7]+1);
				$new_seq .= substr($seq{$best_q}, $inf[4]+1);
			}
		}
		else {
			if (($inf[2]-$inf[4]<$end) && ($inf[5]-$inf[7]<$end)) {
				$new_seq = substr($seq{$best_q}, 0, $inf[4]+1);
				$new_seq .= &comp_seq(substr($seq{$best_s}, 0, $inf[6]));
			}
			elsif (($inf[3]<$end) && ($inf[6]<$end)) {
				$new_seq = &comp_seq(substr($seq{$best_s}, $inf[6]));
				$new_seq .= substr($seq{$best_q}, $inf[4]+1);
			}
		}
		delete $seq{$best_q};
		delete $seq{$best_s};
		$seq{$best_q} = $new_seq;
		$joined{$best_q} = 1;
		$joined{$best_s} = 1;
	}
}

sub comp_seq {
	my $old_seq = shift;
#	print "$old_seq\n";
	$old_seq =~ tr/ACGTacgt/TGCAtgca/;
	my $new_seq = "";
	my @a = split(//, $old_seq);
	foreach my $e (@a) {
		$new_seq = "$e$new_seq";
	}
#	print "$new_seq\n";
	return $new_seq;
}

