#!/bin/sh

if [ -z "$1" ]; then
  echo "usage: cat cmdfile | $0 jobs"
  exit 1
fi
while read cmd; do
  while [ `ls /tmp/mrun.batch* 2> /dev/null | wc -l` -ge $1 ]; do
    sleep 1
  done
  batchfile=/tmp/mrun.batch$RANDOM
  while [ -f $batchfile ]; do
    batchfile=/tmp/mrun.batch$RANDOM
  done
  echo start $batchfile
  echo "$cmd; rm $batchfile" > $batchfile
  chmod +x $batchfile
  nohup $batchfile &
done
