# Author : Li Heng
# Contact: liheng@genomics.org.cn

number=1
waitsec=60
list=
nfile=".multi_run"

which awk 2>&1 >/dev/null
if [ "$?" -ne 0 ]; then
	echo "Cannot find awk, but this script needs it. Sorry."
	exit 1
fi

usage() {
	echo "
Usage   : cat <command_file> | $0 [options]

Options : -n NUM    number of processes, default is $number
          -f STR    number file, default is .multi_run
          -w NUM    wait seconds, default is $waitsec
          -h        usage
"
	exit 2
}
set_num() {
	if [ $nfile ] && [ -f $nfile ]; then
		tmp=`head -1 $nfile`
		set $tmp
		if [ $1 -gt 0 ]; then
			[ $number -eq $1 ] || echo "Number of CPUs varies according to $nfile: $number->$1" >&2
			number=$1
		fi
	fi
}

if [ "$#" -eq 0 ]; then
	echo "'-n' or '-f' is needed. Type '$0 -h' for help."
	exit 4
fi
for opt; do
	case $opt in
		-n) shift; number=$1; shift;;
		-f) shift; nfile=$1;
			if [ ! $nfile ] || [ ! -f $nfile ]; then
				echo "File '$nfile' cannot not be found."
				exit 5
			fi
			shift;;
		-w) shift; waitsec=$1; shift;;
		-h) usage;;
		--) shift; break;;
		-*) echo "Unrecoganized option $1" >&2; exit 3;;
		!-*) break;;
	esac
done

set_num

length() { echo "$#"; } # get length of an array
make_list() { # join 'ps' output and $list, and make a new list
	ps -eo pid | tail +2 | awk -v l="$*" 'BEGIN{n=split(l,a); for(i=1;i<=n;++i)lst[a[i]]=1}
		{if($1 in lst)str=str" "$1} END{print substr(str,2)}'
}

while read comm; do
	while [ `length $list` -ge $number ]; do
		sleep $waitsec
		list=`make_list $list`
		set_num
	done
	eval $comm
	list="$list $!"
done

wait
