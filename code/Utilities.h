#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include "ReAS.h"

//input read sequences
int Rseqs(istream &, SEQ &);
//read in seq with IDs in the list
int Rlist_seq(istream & f, set<read_t> & reads, SEQ & seq);
int Rnonlist_seq (istream & f, set<read_t> & reads, SEQ & seq);
//
int InBreakpoint(BLOCK & bk_point, ifstream & fin);
//cut high-depth segment out from reads
int HighDseg(istream &, BLOCK &);
//define high-depth segment from reads
int HighDseg_2(const int size, vector<hit> & hits, BOUND & bound);
int HighDseg_3 (istream & fin, BLOCK & blocks);
//output sequence with provided boundaries
int OutBoundSeq(SEQ &, BLOCK &, ostream &);
//output breakpoint information
int OutBreakpoint(BLOCK &, ostream &);

int IDseq(set<unsigned int> &, SEQ &, SEQ &);

//convert text to binary format for pairwise alignment
int ConvertAlign_binary (istream &, FILE *);

bool ConvertAlign_binary_2 (align_inf & a, size_t & d);

//print log
void outLog(char *);

inline kmer_t ComplemMer(const kmer_t mer);
inline kmer_t DigitalMer(const string sm);
string ComplemSeq(string seq);
char * ComplemSeq(char * seq);
set<read_t> RepeatReads(FILE * fin, const int M);
//check whether the kmer is a simple kmer
inline bool isSimple_kmer(const kmer_t mer);
//read Mer-Reads index file(binary file)
int RMerIndex(FILE * fin, const set<read_t> & reads, index_kmer_read & mer_reads, index_read_kmer & read_mers);

int RandomSubset(set<read_t> & reads, const float fraction);

//randomize the order
vector<read_t> RandomOrder(set<read_t> & reads);
void RandomOrder(vector<read_t> & reads);

//calculate time used
time_t calculate_time();

int find_mask(istream & f, BLOCK & b, set<read_t> & entire_simple);
//is the aligned region almost entirely composed of simple sequence
bool isEntireSimpleAlign(BLOCK & b, align_inf & a);

read_t isChild(align_inf & a);
set<string> generate_seed(string &s);
bool matched_mers(set<string> & v, string & s, int threshold);
int read_to_seed(SEQ & seq, index_read_kmer & read_mer, index_kmer_read & mer_reads_direct, index_kmer_read & mer_reads_compl);
unsigned int cluster_link(map<read_t, unsigned int> & id_cluster, map<unsigned int, set<read_t> > & cluster_ids, FILE * fin);
int separate_link(map<unsigned int, list<size_t> > & sep_link, map<read_t, unsigned int> & id_cluster, FILE * fin);
#endif
