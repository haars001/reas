#!/usr/bin/perl -w
#*Filename: count_copies.pl
#*Description: 
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.08.24
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat <align> |$0 <stdout>
	-h	help

notes:

USAGE
#************************************************

my %opts;
GetOptions(\%opts, "h");

die $usage if defined $opts{h};

my $f = 0.9;
my %num_complete;
my %num_frag;
my %TEs;
while (<stdin>) {
	chomp;
	my @a=split(/\s+/, $_);
	my $TE = $a[1];
	my $TE_length = $a[7];
	my $TE_begin = $a[8];
	my $TE_end = $a[9];
	if (!defined $num_complete{$TE}) {
		$num_complete{$TE}=0;
	}	
	if (!defined $num_frag{$TE}) {
		$num_frag{$TE}=0;
	}
	if ($TE_end-$TE_begin+1 >= $TE_length * $f) {
		$num_complete{$TE}++;
	}
	else {
		$num_frag{$TE}++;
	}
	$TEs{$TE} = 1;
}

foreach my $i (keys %TEs) {
	print "$i\t$num_complete{$i}\t$num_frag{$i}\n";
}
