#!/usr/bin/perl
#*Copyright 2005  BGI
#*All right reserved
#*Filename: Sim-DivWGS.pl
#*Description: generate a sequence, then generate some copies each with given level of divergence, the divergent sites are randomly selected, after that, got WGS reads.
#*Version : 1.0
#*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
#*Time:    2005.06.21
#

use Getopt::Long;
use strict;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options] b
		-l	length of sequence, default=3000
		-c	number of copies, default=1000
		-d	level of divergence, default=5(%)
		-x	X coverge of WGS, default=6
		-r	size of read, default = 500
        -h	help

notes:

USAGE
#************************************************

die $usage if (@ARGV < 1);

my %opts;
GetOptions(\%opts , "l=i" , "c=i" , "d=i" , "x=i", "r=i", "h");

die $usage if defined $opts{h};

#parameters:
my $len = defined $opts{l}? $opts{l} : 3000;
my $copy = defined $opts{c} ? $opts{c} : 1000;
my $div = defined $opts{d} ? $opts{d} : 5;
my $cov = defined $opts{x} ? $opts{x} : 6;
my $sread = defined $opts{r} ? $opts{r} : 500;

#output files:

my $File_con = "Consensus.fa";
my $File_copies = "Copies.fa";
my $File_reads = "reads.fa";

open (Fcon, ">$File_con") || die;
open (Fcopy, ">$File_copies") || die;
open (Fread, ">$File_reads") || die;

#
my @C = ("A", "C", "G", "T");

#
my @con;
my @copy;

#generate the simulated consensus sequence:

for (my $i=0; $i<$len; $i++) {
	$con[$i] = (int rand(rand 100)) % 4;
}

print Fcon ">con\n";
for (my $i=0; $i < @con; $i++) {
	print Fcon "$C[$con[$i]]";
}
print Fcon "\n";
close Fcon;

#generate divergent copies:

my @t;
for (my $i=0; $i<$len; $i++) {
	$t[$i] = $i;
}

for (my $n=0; $n<$copy; $n++) {
	for (my $i=0; $i<$len; $i++) {
		my $tc;
		my $randpos = int (rand $len);
		$tc = $t[$i];
		$t[$i] = $t[$randpos];
		$t[$randpos] = $tc;
	}
	my @seq = @con;

#	my $Divgt = int (rand $div * $len/100);     # the real divergent level is randomly set from 0 to ($div)%
	my $Divgt = $div * $len/100;                # the divergent level is fixed.

	for (my $pos=0; $pos<$Divgt; $pos++) {
		$seq[$t[$pos]] = ($seq[$t[$pos]]+1) % 4;
	}
	my $S;
	for (my $tranc=0; $tranc<$len; $tranc++) {
		$S .= $C[$seq[$tranc]];
	}
	$copy[$n] = $S;
}

for (my $i=0; $i<@copy; $i++) {
	print Fcopy ">c$i\n";
	print Fcopy "$copy[$i]\n";
}
close Fcopy;

#generate WGS reads

my $Nreads = ($len * $copy * $cov) /$sread;

for (my $n=0; $n<$Nreads; $n++) {
	my $Nc = int (rand @copy);
	my $start = int (rand ($len-$sread));
	my $seq = substr($copy[$Nc], $start, $sread);
	print Fread ">r$n\n";
	print Fread "$seq\n";
}
close Fread;
