#!/usr/bin/perl -w
#*Filename: statDepth.pl
#*Description: count average, minimum and maximum depth for each query on the dataset
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.06.03
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : <align> | $0 <query.size> <stdout>

notes: align should be standard ConverAlign.pl output, with consensus as query, and reads as output

output format:
ID size average_depth  minimum_depth  maximum_depth

USAGE

(my $sizeFile) = @ARGV;
open (F_size, $sizeFile) || die $usage;

my %seqSize;
while (<F_size>) {
	if (/^(\S+)\s+(\d+)/) {
		$seqSize{$1} = $2;
	}
}
close F_size;

my %align;
#initialization
foreach my $id (keys %seqSize) {
	my @a;
	for (my $i=0; $i<$seqSize{$id}; $i++) {
		$a[$i] = 0;
	}
	$align{$id} = \@a;
}

while (<stdin>) {
	chomp;
	my @a = split(/\s+/, $_);
	if (@a<10) {
		next;
	}
	my $id = $a[0];
	my $begin = $a[5];
	my $end = $a[6];
	for (my $i=$begin; $i<=$end; $i++) {
		$align{$id}[$i]++;
	}
}

#
my $ignore_bound = 10; #ignore the fragment at boundary of sequences which maybe at low depth for alignment weakness
foreach my $id (keys %seqSize) {
	my $sum = 0;
	my $min = $align{$id}[0]+$ignore_bound;
	my $max = $align{$id}[0]+$ignore_bound;
	for (my $i=$ignore_bound; $i<$seqSize{$id}-$ignore_bound; $i++) {
		$sum += $align{$id}[$i];
		if ($align{$id}[$i] > $max) {
			$max = $align{$id}[$i];
		}
		if ($align{$id}[$i] < $min) {
			$min = $align{$id}[$i];
		}
	}
	my $avg = $sum/($seqSize{$id}-2*$ignore_bound);
	#
	print "$id\t$seqSize{$id}\t$avg\t$min\t$max\n";
}
