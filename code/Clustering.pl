#!/usr/bin/perl -w
#*Filename: Clustering.pl
#*Description: cluster segments according to overlapping information from pairwise alignment
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.04.22
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
		-s	INT	size cutoff of overlapping, default=100bp
		-i	FLOAT	identity threshold of pairwise hits, default=0.6
		-m	INT	minimal group size, default=5
		-link	STRING	alignment result file, default=seg.link
		-bk	STRING	file of repeat breakpoint information, default=seg.bk
		-seq	STRING	repeat segment file, default=seg.fa
		-h	help

notes: input should be text format of seg.link file

USAGE
#************************************************

my $ID_file = "seg.id";  #segment IDs inside cluster
my $linkage_file = "seg.link";
my $cluster_dir = "cluster_"; #prefix of directory names

my %opts;
GetOptions(\%opts , "s=i" , "i=s", "m=i", "link=s", "bk=s", "seq=s", "h");

die $usage if defined $opts{h};
my $hit_size = defined $opts{s}? $opts{s} : 100;
my $hit_ident = defined $opts{i}? $opts{i} : 0.6;
my $min_group = defined $opts{m}? $opts{m} : 5;
my $align_file = defined $opts{link}? $opts{link} : "seg.link"; #alignment file
my $bk_file = defined $opts{bk}? $opts{bk} : "seg.bk";
my $seq_file = defined $opts{seq}? $opts{seq} : "seg.fa";

open (fin_align, $align_file) || die "can not open pairwise alignment file\n";

my %align;

#read in segments overlapping information

# q_id -> s_id -> 1

while (<fin_align>) {
	chomp;
	my @a=split(/\s+/, $_);
	if (@a < 9) {
		next;
	}
	if (($a[3] < $hit_ident) || (($a[6]-$a[5]+1 < $hit_size) && ($a[9]-$a[8]+1 < $hit_size))) {
		next;
	}
	my $q_id = $a[0];
	my $s_id = $a[1];
	$align{$q_id}{$s_id} = 1;
}
close fin_align;

#clustering: single-linkage
my $num = 0;
my %cluster;  #cluster information of each segments
my @newKeys = ();

my %working;
foreach my $j (keys %align) {
	$working{$j} = 1;
}
while ((my @k = keys %working) >0) {
	if (@newKeys == 0) {
		$num++;
		$cluster{$k[0]} = $num;
		@newKeys = keys (%{$align{$k[0]}});
		delete $working{$k[0]};
	}
	else {
		my %t = ();
		foreach my $i (@newKeys) {
			if (!defined $cluster{$i}) {
				$cluster{$i} = $num;
				foreach my $e (keys %{$align{$i}}) {
					if (!defined $cluster{$e}) {
						$t{$e} = 1;
					}
				}
			}
			delete $working{$i};
		}
		@newKeys = keys %t;
	}
}

my %order_cluster;
foreach my $i (keys %cluster) {
	if (!defined $order_cluster{$cluster{$i}}) {
		my @a;
		push(@a, $i);
		$order_cluster{$cluster{$i}} = \@a;
	}
	else {
		push (@{$order_cluster{$cluster{$i}}}, $i);
	}
}

my $total = 0;
my $n_clusters = 0;
foreach my $i (keys %order_cluster) {
	#ignore small groups with number of sequences < threshold
	if ($min_group > @{$order_cluster{$i}}) {
		next;
	}
	my $dir_name = $cluster_dir.$i;
	die "$dir_name already exists\n" if (-e $dir_name);
	mkdir($dir_name);
	open (fout_id, ">$dir_name/$ID_file") || die "can not write $ID_file\n";
	foreach my $e (@{$order_cluster{$i}}) {
		print fout_id "$e\n";
	}
	close fout_id;

	#output link file
	`pickListinf.pl $dir_name/$ID_file  $align_file >$dir_name/$linkage_file`;
	#output seq file
	`cat $seq_file |pickListSeq.pl $dir_name/$ID_file >$dir_name/$seq_file`;
	#output bk file
	`pickListinf.pl $dir_name/$ID_file  $bk_file >$dir_name/$bk_file`;
	
	my $si = @{$order_cluster{$i}};
	$total += $i;
	$n_clusters++;
	print "Cluster  $i : $si seq\n";
}
print "Total :  $n_clusters clusters,   $total seq\n";
