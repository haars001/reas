/*
*Copyright 2005  BGI
*All right reserved
*
*Filename: HighDseg.cpp
*Description:  input pairwise alignment, output high-depth fragments from reads

*Version : 1.0
*Progremmer: Li ruiqiang(lirq@genomics.org.cn)
*Time:    2006.04.18
*/

#include<unistd.h>
#include "ReAS.h"
#include "Utilities.h"
#include <stdlib.h>

using namespace std;

extern int depth;
extern int hit_size;
extern float hit_ident;
extern int bk_size;

//usage:
void usage(void)
{
        cout<<"Usage:	HighDseg\n"
			<<"	-a	STRING	input pairwise alignment file, default = <stdin>\n"
			<<"	-b	STRING	output breakpoints information file, default = <stdout>\n"
            <<"	-s	INT	size threshold of pairwise hits, default=100\n"
            <<"	-i	FLOAT	identity threshold of pairwise hits, default=0.6\n"
			<<"	-d	INT	depth cutoff of defining repeat, default=6\n"
            <<"	-h	help\n"
			<<"\nNotes: format of breakpoint file\n"
			<<"ID\tsize\tbegin\tend\n"
			<<"index start from 0\n"
            <<endl;
        exit(1);
};

int main(int argc, char *argv[])
{
	char * align_file = "";
	char * bkpoint_file = "";
	//[options]
	int c;
	while((c=getopt(argc,argv,"a:b:s:i:d:h!")) != -1) {
			switch(c) {
					case 'a': align_file=optarg; break;
					case 'b': bkpoint_file=optarg; break;
					case 's': hit_size = atoi(optarg); break;
					case 'i': hit_ident = atof(optarg); break;
					case 'd': depth = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	BLOCK blocks;
	//
	ifstream fin_align(align_file);
	if (fin_align)
	{
		HighDseg(fin_align, blocks);
		fin_align.close();
	}
	else
	{
		HighDseg(cin, blocks);
	}
	ofstream fout_bk(bkpoint_file);
	if (!fout_bk)
	{
		OutBreakpoint(blocks, cout);
	}
	else
	{
		OutBreakpoint(blocks, fout_bk);
		fout_bk.close();
	}
	return 0;
}
