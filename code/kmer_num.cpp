/******************************************************************
** kmer_num.cc
** Copyright (c) BGI 2003
** Author:Li Shengting <lishengting@genomics.org.cn>
** Program Date :2003-2-5 17:40
** Modifier:Li Shengting <lishengting@genomics.org.cn>
** Last Modified:2003-2-5 17:41
** Description:
** Version:1.0 create version
******************************************************************/
#include <unistd.h>
#include <malloc.h>
#include "common.h"
#include "fasta.h"
#include "hugeBlock.h"
using namespace std;

int isText=0;

static kmer_t get_start(uchar *buf,int len,int &i,int K)
// calculate the start k-mer. this will also be called if `N' is met.
{
	static int k;
	static kmer_t t;
	static uchar tmp;
	
	for(t=0,k=i;i<K+k && i<len;i++) {
		tmp=buf[i];
		if(tmp>=4) { k=i+1;t=0;continue; } // skip `N' and restart.
		t=(t<<2)|tmp;
	}
	if(i!=K+k) return(kmer_t(~0)); // fail to construct a k-mer
	return(t);
}

static inline void inc_kmer(HugeBlock<kcount_t> &kl,kmer_t kmer) {
	static kmer_t tmp;

	if(kl[kmer]!=kcount_t(~0)) {
		kl[kmer]++;
		tmp=reverse_kmer(kmer);
		if (tmp != kmer) kl[tmp]++;
	}
}

int kmer_num(int fin,FILE * outfile)
{
	int i,len,buf_size=0;
	uchar *buf=NULL,tmp;
	kmer_t t,mask;
	HugeBlock<kcount_t> kl(BLOCK_BIT,1<<(2*K>BLOCK_BIT ? 2*K-BLOCK_BIT : 0));
	kl.fill0();
	mask=(kmer_t(~0))>>(sizeof(kmer_t)*8-K*2);
	int count=0;
	while(1) {
		read(fin,&len,sizeof(len));
		if (len==0)	continue;
		if (len<0) break;
		if (buf_size<len) {
			buf=(uchar *)realloc(buf,len*2);
			buf_size=len*2;
		}
		read(fin,buf,len);

		i=0;
		t=get_start(buf,len,i,K);
		while(i<len) {
			inc_kmer(kl,t);
			tmp=buf[i];
			if(tmp>=4) t=get_start(buf,len,i,K);
				else { t=((t<<2)|tmp)&mask;i++; }
		}
		if(t!=kmer_t(~0)) inc_kmer(kl,t);
		count++;
	}
	cal_time("Build kmers number list");
	/*
	fprintf(outfile,"%d",K);
	for (i=0;i<kl.size();i++) {
		if (!(i % 10))
			fprintf(outfile,"\n");
		fprintf(outfile,"%d\t",kl[i]);
	}
	*/
	if (isText) {
		fprintf(outfile,"%d\n",K);
		kl.outText(outfile,1);
	}else{
		fputc(K,outfile);
		kl.output(outfile,1);
	}
	cal_time("Output kmers number list");
	return count;
}

void usage (FILE *fp,int exit_code)
{
	fprintf(fp,"Program : kmer_num\n");
	fprintf(fp,"Version : %.2f\n",VERSION);
	fprintf(fp,"Contact : liheng, lishengting or zhangy@genomics.org.cn\n\n");
	fprintf(fp,"Usage : kmer_num [-o outfile] [-e] [-k K] [-b BLOCK_BIT] reads_fasta\n");
	fprintf(fp,"\t-o output file, default is stdout\n");
	fprintf(fp,"\t-e (switch,output text format)\n");
	fprintf(fp,"\t-k k-mer, default is %u\n",K);
	fprintf(fp,"\t-b number of bits of the size for a huge block, default is %u\n",BLOCK_BIT);
	fprintf(fp,"\t-h Display this usage infileormation.\n\n");

	exit (exit_code);
}

int main (int argc,char* argv[])
{
	FILE *outfile=NULL,*infile=NULL;
///////////////////////////////////////////////////////////////////////////////////////////////////
//Get options
///////////////////////////////////////////////////////////////////////////////////////////////////
	if(argc<2) usage(stderr,1);
	int c;
	while ((c=getopt (argc,argv,"hk:b:eo:"))>=0) {
		switch (c) {
			case '?':
			case 'h':
				usage (stdout,0);
			case 'k':
				K=atoi(optarg);
				break;
			case 'b':
				BLOCK_BIT=atoi(optarg);
				break;
			case 'e':
				isText=1;
				break;
			case 'o':
				outfile=fopen(optarg,"w");
				break;
			default:
				usage (stderr,1);
		}
	}

	if (optind<argc) 
		infile=fopen(argv[optind],"r");
	else 
		infile=stdin;
///////////////////////////////////////////////////////////////////////////////////////////////////
//Main
///////////////////////////////////////////////////////////////////////////////////////////////////
	if (!outfile)
		outfile=stdout;
	if (!infile)
		syserr("Can't open input file!");

	int fd[2];
	if (pipe(fd)<0)
		syserr("Pipe error!");

	pid_t pid;
	if ((pid=fork())>0) {
		cal_time("Start kmer_num");
		fprintf(stderr,"Total %d reads\n",kmer_num(fd[0],outfile));
		cal_time("Close");
	}else if (pid==0) {
		aFasta aread;
		char locus[LOCUS_LEN];
		int len;

		while ((len=read_fasta(infile,aread,locus))>=0)
		{
			write(fd[1],&len,sizeof(len));
			write(fd[1],aread.buf,len);
		}
		write(fd[1],&len,sizeof(len));
	}else{
		syserr("Can't fork()!");
	}	

	fclose(outfile);
	fclose(infile);
	return 0;
}
