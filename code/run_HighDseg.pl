#!/usr/bin/perl -w
#*Filename: run_HighDseg.pl
#*Description: split reads dataset; run pairwise alignment, get high-depth segments.
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.04.18
#

use strict;
use Getopt::Long;
use Cwd qw(cwd);

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
		-r	STRING	reads file
		-n	INT	split the dataset into number of files, default=1
		-a	INT	number of parallel processors, default=1
		-b	STRING	output breakpoints information file
		-s	INT	size threshold of pairwise hits, default=100
		-i	FLOAT	identity threshold of pairwise hits, default=0.6
		-d	INT	depth cutoff of defining repeat, default=6
		-m	INT	maximum number of hits needed to define repeat boundaries, default=all
		-t	INT	<0: cross_match |1: patternhunter |2:blastn>, pairwise alignment tool, default=0
		-h	help

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

my %opts;
GetOptions(\%opts, "r=s", "n=i", "a=i", "b=s", "s=i", "i=s", "d=i", "m=i", "t=i", "h");

my $read_file = defined $opts{r}? $opts{r} : die "no reads file\n";
die $usage if defined $opts{h};
my $Nfiles = defined $opts{n}? $opts{n} : 1;
my $Nprocess = defined $opts{a}? $opts{a} : 1;
my $outBK_file = defined $opts{b}? $opts{b} : die "no breakpoint output file\n";
my $hit_size = defined $opts{s}? $opts{s} : 100;
my $hit_ident = defined $opts{i}? $opts{i} : 0.6;
my $depth = defined $opts{d}? $opts{d} : 6;
my $max_hit = defined $opts{m}? "-max_hit $opts{m}" : "";
my $align_tool = defined $opts{t}? $opts{t} : 0;

#make working space
my $dir = "ReAS".time();
unless(-e $dir and -d $dir or mkdir $dir) {
        die "$! cannot make dir $dir for $0 in ".cwd."\n";
}
chdir($dir);

#
`split_fa_bygroup.pl ../$read_file $Nfiles 1`;

open (F_sh, ">run") || die "can not write file run\n";
if (0 == $align_tool) {
	for (my $i=1; $i<=$Nfiles; $i++) {
		print F_sh "cross_match.manyreads leaf.$i.fa ../$read_file -masklevel 101 -minmatch 17 -minscore 30 $max_hit | ConvertAlign.pl -s $hit_size -i $hit_ident -t $align_tool |HighDseg -s $hit_size -i $hit_ident -d $depth >$i.bk &\n";
	}
}
else {
	die "unsupported alignment tool\n";
}

close F_sh;
`cat run|multi_run2.sh -n $Nprocess -w 2`;
`rm -f ../$outBK_file` if (-e "../$outBK_file");
for (my $i=1; $i<=$Nfiles; $i++) {
	`cat $i.bk >>../$outBK_file`;
}

chdir("../");
`rm -rf $dir`;
