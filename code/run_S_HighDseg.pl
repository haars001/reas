#!/usr/bin/perl -w
#*Filename: run_S_HighDseg.pl
#*Description: 
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.08.30
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : $0 [options]
	-a	INT	number of parallel processors, default=1
	-r	STRING	reads file, default=reads.fa
	-b	STRING	output breakpoints information file, default=seg.bk
	-s	INT	size threshold of pairwise hits, default=100
	-i	FLOAT	identity threshold of pairwise hits, default=0.7
	-c	STRING	score matrix for pariwise alignment, default=mat70
	-d	INT	depth cutoff of defining repeat, default=6
	-n	INT	maximum number of hits needed to define repeat boundaries from reads, default=depth*20
	-h	help
notes:

USAGE

my %opts;
GetOptions(\%opts , "a:i" , "r:s" , "b:s" , "s:i" , "i:s", "c:s", "d:i", "n:i", "h");

die $usage if defined $opts{h};

my $num_cpu = defined $opts{a}? $opts{a} : 1;
my $read_file = defined $opts{r}? $opts{r} : "reads.fa";
my $bk_file = defined $opts{b}? $opts{b} : "seg.bk";
my $hit_size = defined $opts{s}? "-s $opts{s}" : "";
my $hit_ident = defined $opts{i}? "-i $opts{i}" : "";
my $matrix = defined $opts{c}? "-c ../$opts{c}" : "-c ../mat70";
my $depth = defined $opts{d}? "-d $opts{d}" : "";
my $max_hits = defined $opts{n}? "-n $opts{n}" : "";

#make working space
my $dir = "ReAS".time();
mkdir($dir) ||die "can not make dir $dir\n";

chdir($dir);

open(F, "../$read_file") || die;
my @read;
while (<F>) {
	if (/^>(\S+)/) {
		push(@read, $1);
	}
}
close F;

my $total = @read;
my $unit = $total/$num_cpu;
my $count = 0;

for (my $i=1; $i<=$num_cpu; $i++) {
	my $file = "$i.id";
	open(F_id, ">$file")||die;
	for (my $j=0; $j<$unit; $j++) {
		print F_id "$read[($i-1)*$unit+$j]\n";
	}
	if ($i == $num_cpu) {
		for (my $j=($i-1)*$unit; $j<$total; $j++) {
			print F_id "$read[$j]\n";
		}
	}
	close F_id;
}
open (F_sh, ">run")||die;
for (my $i=1; $i<=$num_cpu; $i++) {
	my $file = "$i.id";
	print F_sh "cat $file |S_HighDseg -r ../$read_file -b $i.bk $hit_size $hit_ident $matrix $depth $max_hits >$i.log &\n";
}
close F_sh;

`cat run|multi_run2.sh -n $num_cpu -w 2`;
`rm -f ../$bk_file` if (-e "../$bk_file");
for (my $i=1; $i<=$num_cpu; $i++) {
	`cat $i.bk >>../$bk_file`;
}
chdir("../");
`rm -rf $dir`;
