#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include"bio_table.h"
#include"fasta.h"

int read_fasta(FILE *fp,aFasta &buf,char *locus,bool bin)
{
	int c;
	char *p;
	uchar ch;
	while(!feof(fp) && fgetc(fp)!='>');
	if(feof(fp)) return(-1);
	p=locus;
	while((c=fgetc(fp))!=' ' && c!='\t' && !feof(fp) && c!='\n') *p++=c;
	*p='\0';
	if(c!='\n') while(fgetc(fp)!='\n' && !feof(fp));
	buf.reset();
	while((c=fgetc(fp))!='>' && !feof(fp)) {
		if (bin) {
			ch = gene_table4[c];
			if(ch < 16) buf.add(ch);
		} else if (isalpha(c)) buf.add((unsigned char)(c));
	}
	if(c=='>') ungetc(c,fp);
	return(buf.get_len());
}
int read_qual(FILE *fp, aFasta &buf, char *locus)
{
	int c;
	char *p, *q, tmp[20];

	while (!feof(fp) && fgetc(fp) != '>');
	if (feof(fp)) return -1;
	p = locus;
	while ((c = fgetc(fp)) != ' ' && c != '\t' && !feof(fp) && c != '\n')
		*p++ = c;
	*p = '\0';
	if (c != '\n')
		while (!feof(fp) && fgetc(fp) != '\n');
	buf.reset();
	q = tmp;
	while (!feof(fp) && (c = fgetc(fp)) != '>') {
		if (isdigit(c)) *q++ = c;
		else if (q != tmp && (c == '\t' || c == ' ' || c == '\n')) {
			*q = '\0';
			buf.add(uchar(atoi(tmp)));
			q = tmp;
		}
	}
	if (c == '>') ungetc(c, fp);
	return buf.get_len();
}
