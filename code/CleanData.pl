#!/usr/bin/perl -w
#*Filename: CleanData.pl
#*Description: remove description; remove 'X's and 'N's at the ends of sequences. force upper case output
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.04.07
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat input.fa | $0 [options] stdout
		-rmDes	remove description
		-rmXN	remove 'X's and 'N's at sequence ends
		-size	NUM	sequence size cutoff, remove small sequences, default=100
		-up	uppercase ouput for sequences
		-h	help

notes: default: deal with all.

USAGE
#************************************************

my %opts;
GetOptions(\%opts , "rmDes", "rmXN", "up", "size=i", "h");

die $usage if defined $opts{h};

my $A = 0;
$A = 1 if ((!defined $opts{rmDes}) && (!defined $opts{rmXN}) && (!defined $opts{up}));

my $len = defined $opts{size}? $opts{size} : 100;
my $name;
my $des;
my $seq = "";

while (<STDIN>) {
	chomp;
	if (/^(>\S+)\s*(.*)$/) {
		if ($seq ne "") {
			if ($A) {
				
				$seq = uc $seq;
				my $old_length = length($seq) + 1;
				while (length($seq) < $old_length) {
					$old_length =length($seq);
					$seq =~ s/^[ACGT]{0,5}[XN]+//;
					$seq =~ s/[XN]+[ACGT]{0,5}$//;
				}
				
				if (length $seq >= $len) {
					print "$name\n";
					print "$seq\n";
				}
			}
			else {
				if (defined $opts{rmDes}) {
					$des = "";
				}
				if (defined $opts{up}) {
					$seq = uc $seq;
				}
				if (defined $opts{rmXN}) {
					my $old_length = length($seq) + 1;
					while (length($seq) < $old_length) {
						$old_length =length($seq);
						$seq =~ s/^[ACGT]{0,5}[XN]+//;
						$seq =~ s/[XN]+[ACGT]{0,5}$//;
					}
				}
				if (defined $opts{size}) {
					$len = $opts{size};
				}
				if (length $seq >= $len) {
					print "$name\t$des\n";
					print "$seq\n";
				}
			}
		}
		$seq = "";
		$name = $1;
		$des = $2;
	}
	else {
		$seq .= $_;
	}
}
if ($seq ne "") {
	if ($A) {
		
		$seq = uc $seq;
		my $old_length = length($seq) + 1;
		while (length($seq) < $old_length) {
			$old_length =length($seq);
			$seq =~ s/^[ACGT]{0,5}[XN]+//;
			$seq =~ s/[XN]+[ACGT]{0,5}$//;
		}
		
		if (length $seq >= $len) {
			print "$name\n";
			print "$seq\n";
		}
	}
	else {
		if (defined $opts{rmDes}) {
			$des = "";
		}
		if (defined $opts{up}) {
			$seq = uc $seq;
		}
		if (defined $opts{rmXN}) {
			my $old_length = length($seq) + 1;
			while (length($seq) < $old_length) {
				$old_length =length($seq);
				$seq =~ s/^[ACGT]{0,5}[XN]+//;
				$seq =~ s/[XN]+[ACGT]{0,5}$//;
			}
		}
		if (defined $opts{size}) {
			$len = $opts{size};
		}
		if (length $seq >= $len) {
			print "$name\t$des\n";
			print "$seq\n";
		}
	}
}
