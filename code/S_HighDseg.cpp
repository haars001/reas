/*
single process program

*/

#include<unistd.h>
#include<iostream>
#include<ostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<vector>
#include<map>
#include "ReAS.h"
#include "Utilities.h"
#include "sw.h"
#include <algorithm>    // std::set_difference, std::sort

using namespace std;

extern int depth;
extern int hit_size;
extern float hit_ident;
extern int max_hits;
extern char * score_matrix;

//usage:
void usage(void)
{
	cout<<"Usage:	<stdin query ids> | Faster_HighDseg [options]\n"
		<<"	-r	STRING	reads file, default=reads.fa\n"
		<<"	-b	STRING	output breakpoints information file, default=seg.bk\n"
		<<"	-s	INT	size threshold of pairwise hits, default=100\n"
		<<"	-i	FLOAT	identity threshold of pairwise hits, default=0.7\n"
		<<"	-c	STRING	score matrix for pariwise alignment, default=mat70\n"
		<<"	-d	INT	depth cutoff of defining repeat, default=6\n"
		<<"	-n	INT	maximum number of hits needed to define repeat boundaries from reads, default=depth*20\n"
		<<"	-h	help\n"
		<<endl;
	exit(1);
};

int main(int argc, char *argv[])
{
	char * file_reads = "reads.fa";
	char * file_bk = "seg.bk";

	//[options]
	int c;
	while((c=getopt(argc,argv,"r:b:s:i:c:d:n:h")) != -1) {
			switch(c) {       
					case 'r': file_reads = optarg; break;
					case 'b': file_bk = optarg; break;
					case 's': hit_size = atoi(optarg); break;
					case 'i': hit_ident = atof(optarg); break;
					case 'c': score_matrix = optarg; break;
					case 'd': depth = atoi(optarg); break;
					case 'n': max_hits = atoi(optarg); break;
					case 'h':usage();    //usage information
					case '?':usage();    //unrecognizable input
			}
	}
	//
	ifstream fin_reads(file_reads);
	if (!fin_reads)
	{
		cerr<<"failed to open file "<<file_reads<<endl;
		exit(1);
	}
	ofstream fout_bk(file_bk);
	if (!fout_bk)
	{
		cerr<<"failed to open file "<<file_bk<<endl;
		exit(1);
	}
	//
	SEQ seq;
	BLOCK blocks;
	time_t used_time = calculate_time();
	//read in read sequences
	Rseqs(fin_reads, seq);
	int total_reads = seq.size();
	cout<<"Read in read sequences:\t"<<total_reads<<"\t"<<calculate_time()<<"sec"<<endl;
	//generate read-mers index
	index_read_kmer read_mers;
	index_kmer_read mer_reads_direct;
	index_kmer_read mer_reads_compl;
	read_to_seed(seq, read_mers, mer_reads_direct, mer_reads_compl);
	cout<<"Generated read-kmer index:\t"<<calculate_time()<<"sec"<<endl;
	
	//do the pipeline
	sw sw_a;
	sw sw_b;
	sw_a.set_params(score_matrix, 30);
	sw_b.set_params(score_matrix, 30);
	//
	int count=0;
	read_t read_id;
	while (!cin.eof())
	{	
		if (0 == count % 100)
		{
			float percent = float(count)*100/total_reads;
			cout<<"Finished:\t"<<count<<" reads,\t"<<percent<<"%\tTotal:\t"
			<<total_reads<<"\t"<<calculate_time()<<"sec"<<endl;
		}
		count++;
		cin>>read_id;
		if (cin.eof())
		{
			break;
		}
		//get the reads sharing k-mers
		set<read_t> shared_direct;
		set<read_t> shared_compl;
		for (set<kmer_t>::iterator p=read_mers[read_id].begin(); p!=read_mers[read_id].end(); p++)
		{
			shared_direct.insert(mer_reads_direct[*p].begin(), mer_reads_direct[*p].end());
			shared_compl.insert(mer_reads_compl[*p].begin(), mer_reads_compl[*p].end());
		}
		shared_direct.erase(read_id);
		shared_compl.erase(read_id);
		
		set<read_t> direct_only;
		set<read_t> compl_only;
		set<read_t> both;
		set_difference(shared_direct.begin(), shared_direct.end(), shared_compl.begin(), shared_compl.end(), inserter(direct_only, direct_only.begin()));
		set_difference(shared_compl.begin(), shared_compl.end(), shared_direct.begin(), shared_direct.end(), inserter(compl_only, compl_only.begin()));
		set_intersection(shared_direct.begin(), shared_direct.end(), shared_compl.begin(), shared_compl.end(), inserter(both, both.begin()));
		//
		if (direct_only.size()+compl_only.size()+both.size() < depth)
		{
			continue;
		}
		//
		Profile * q_profile;
		Profile * q_profile_compl;
		char * query = new char[seq[read_id].size()+1];
		char * query_compl = new char[seq[read_id].size()+1];
		strcpy(query, seq[read_id].c_str());
		strcpy(query_compl, ComplemSeq(seq[read_id]).c_str());
		q_profile = sw_a.make_profile_from_seq(query, 0);
		q_profile_compl = sw_b.make_profile_from_seq(query_compl, 0);
		
		//
		int q_size;
		int num_hits = 0;
		vector<hit> hit_this_read;
		hit a;
		int n=0;
		//try for direct only first
		for (set<read_t>::iterator p=direct_only.begin(); p!=direct_only.end(); p++)
		{
			if (num_hits >= max_hits)
			{
				break;
			}
			n++;
			read_t id = *p;
			AlignResult align_result;
			char *sbjct = new char[seq[id].size()+1];
			strcpy(sbjct, seq[id].c_str());
			align_result = sw_a.revised_swat(q_profile, sbjct);
			delete(sbjct);
			if ((align_result.iden >= hit_ident) && (align_result.q_end - align_result.q_start +1 >= hit_size))
			{
				q_size = align_result.q_len;
				a.left = align_result.q_start;
				a.right = align_result.q_end;
				hit_this_read.push_back(a);
				num_hits++;
			}
		}
		//try for complementary only
		for (set<read_t>::iterator p=compl_only.begin(); p!=compl_only.end(); p++)
		{
			if (num_hits >= max_hits)
			{
				break;
			}
			n++;
			read_t id = *p;
			AlignResult align_result;
			char * sbjct = new char[seq[id].size()+1];
			strcpy(sbjct, seq[id].c_str());
			align_result = sw_b.revised_swat(q_profile_compl, sbjct);
			delete(sbjct);
			if ((align_result.iden >= hit_ident) && (align_result.q_end - align_result.q_start +1 >= hit_size))
			{
				q_size = align_result.q_len;
				a.left = align_result.q_len-1-align_result.q_end;
				a.right = align_result.q_len-1-align_result.q_start;
				hit_this_read.push_back(a);
				num_hits++;
			}
		}
		//try for the both
		for (set<read_t>::iterator p=both.begin(); p!=both.end(); p++)
		{
			if (num_hits >= max_hits)
			{
				break;
			}
			n++;
			read_t id = *p;
			AlignResult align_result;
			char * sbjct = new char[seq[id].size()+1];
			strcpy(sbjct, seq[id].c_str());
			align_result = sw_a.revised_swat(q_profile, sbjct);
			if ((align_result.iden >= hit_ident) && (align_result.q_end - align_result.q_start +1 >= hit_size))
			{
				q_size = align_result.q_len;
				a.left = align_result.q_start;
				a.right = align_result.q_end;
				hit_this_read.push_back(a);
				num_hits++;
				continue;
			}

			align_result = sw_b.revised_swat(q_profile_compl, sbjct);
			if ((align_result.iden >= hit_ident) && (align_result.q_end - align_result.q_start +1 >= hit_size))
			{
				q_size = align_result.q_len;
				a.left = align_result.q_len-1-align_result.q_end;
				a.right = align_result.q_len-1-align_result.q_start;
				hit_this_read.push_back(a);
				num_hits++;
			}
			delete(sbjct);
		}
//		cout<<read_id<<"\t"<<n<<endl;
		sw_a.free_profile(q_profile);
		sw_b.free_profile(q_profile_compl);
		delete(query_compl);
		delete(query);
		//define high-depth boundary based on mapped hits
		if (hit_this_read.size() >= depth)
		{
			BOUND b;
			if (HighDseg_2(q_size, hit_this_read, b))
			{
				blocks[read_id] = b;
			}
		}
	}
	//
	cout<<"Finished:\t"<<calculate_time()<<"sec"<<endl;
	//output
	OutBreakpoint(blocks, fout_bk);
	fin_reads.close();
	fout_bk.close();
	return 0;
}
