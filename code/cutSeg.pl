#!/usr/bin/perl -w
#*Filename: cutSeg.pl
#*Description: cut the segment out from sequences by setting range
#*Version : 1.0
#*Li Ruiqiang(lirq@genomics.org.cn)
#*2006.04.19
#

use strict;
use Getopt::Long;

#**** USAGE *************************************
my $usage=<<"USAGE";
Usage : cat range.pos | $0 seq.infile stdout

notes:

USAGE
die $usage if (@ARGV<1);
#************************************************

open (F_seq, $ARGV[0]) || die "can not open $ARGV[0]\n";

my %s_begin;
my %s_length;

while (<STDIN>) {
	if (/^(\S+)\s+\d+\s+(\d+)\s+(\d+)/) {
		$s_begin{$1} = $2;
		$s_length{$1} = $3-$2+1;
	}
}
my $s_id = "";
my $seq;
while (<F_seq>) {
	chomp;
	if (/^>(\S+)/) {
		if (($s_id ne "") && (defined $s_begin{$s_id})) {
			my $subseq = substr($seq, $s_begin{$s_id}, $s_length{$s_id});
			print ">$s_id\n";
			print "$subseq\n";
		}
		$s_id = $1;
		$seq = "";
	}
	else {
		$seq .= $_;
	}
}
if (($s_id ne "") && (defined $s_begin{$s_id})) {
	my $subseq = substr($seq, $s_begin{$s_id}, $s_length{$s_id});
	print ">$s_id\n";
	print "$subseq\n";
}
