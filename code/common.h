/******************************************************************
** common.h
** Copyright (c) BGI 2003
** Author:Li Shengting <lishengting@genomics.org.cn>
** Program Date :2003-2-5 17:40
** Modifier:Li Shengting <lishengting@genomics.org.cn>
** Last Modified:2003-2-5 17:41
** Description:
** Version:1.0 create version
******************************************************************/
#ifndef COMMON_H_
#define COMMON_H_
#include <string>
#include "hugeBlock.h"
#include <stdlib.h>

using namespace std;

typedef unsigned long long kmer_t; // 64-bit
typedef unsigned long long slist_t; // 64-bit
typedef unsigned char kcount_t; 
typedef unsigned int read_t; // reads number < 4294967296
//typedef unsigned long read_t; // reads number < 18446744073709551616
typedef unsigned int nreads_t;

const float 
//VERSION=1.00; //create version
//VERSION=1.01; //repair sort bug
//VERSION=1.02; //add statistic function
//VERSION=1.03; //add max depth to 1M
//VERSION=2.00; //use threads
//VERSION=3.00; //less memory use
//VERSION=3.01; //fix bug
//VERSION=3.02; //less disk use
VERSION=3.03; //fix bug
const int LOCUS_LEN=255;
const size_t SWAP_SIZE=10485760;
const int READ_BLOCK=30;
extern int K,D,M,min_M,BLOCK_BIT;
extern volatile size_t mem_size;


//#define DYNAMIC_SIZE
#define LIST_SIZE 256

#define TIMEOUT_STDERR

void syserr(char *);
kmer_t reverse_kmer(kmer_t kmer);
void cal_time(char * str);
void checkLicence();

#endif
