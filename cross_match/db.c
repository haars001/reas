/*****************************************************************************
#   Copyright (C) 1993-1999 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/


/* routines for dealing with sequence files in FASTA format */

#include "swat.h"

#define ID_BUFFERSIZE 50 /* max size of ID field in database entries */
#define DESCRIP_BUFFERSIZE 100
#define SEQ_BUFFERSIZE 50000
#define MAX_NUM_BLOCKS 1000
#define BLOCK_SIZE 1000

extern Parameters *parameters;

int t_num_entries;

static int id_pos, descrip_pos, seq_pos, seq_length;
/* N.B. following needed by Bonfield code, so original "static" designation has
   been removed */
Database *head_db;
Seq_entry *seq_block_table[MAX_NUM_BLOCKS];
int num_blocks;
char comp_mat[256], alph_mat[256];

set_table_mats()
{
  int i;

  for (i = 0; i < MAX_NUM_BLOCKS; i++) seq_block_table[i] = 0;

  set_mats();
}

set_mats()
{
  int i;

  for (i = 0; i < 256; i++) alph_mat[i] = isalpha(i) ? toupper(i) : 0;

  if (parameters->DNA_flag) {
    alph_mat['-'] = 'N';

    for (i = 0; i < 256; i++) comp_mat[i] = i;

    comp_mat['A'] = 'T';
    comp_mat['a'] = 't';
    comp_mat['C'] = 'G';
    comp_mat['c'] = 'g';
    comp_mat['G'] = 'C';
    comp_mat['g'] = 'c';
    comp_mat['T'] = 'A';
    comp_mat['t'] = 'a';
    comp_mat['y'] = 'r';
    comp_mat['Y'] = 'R';
    comp_mat['r'] = 'y';
    comp_mat['R'] = 'Y';
    comp_mat['M'] = 'K';
    comp_mat['m'] = 'k';
    comp_mat['K'] = 'M';
    comp_mat['k'] = 'm';
    comp_mat['H'] = 'D';
    comp_mat['D'] = 'H';
    comp_mat['B'] = 'V';
    comp_mat['V'] = 'B';
    comp_mat['h'] = 'd';
    comp_mat['d'] = 'h';
    comp_mat['b'] = 'v';
    comp_mat['v'] = 'b';
  }
}

Database *append_db(file, in_memory, store_comps, store_aligns)
     File *file;
     int in_memory, store_comps, store_aligns;
{
  char *our_alloc();
  Database *db;
  int j, k;
  char *seq_area;

  db = (Database *)our_alloc(sizeof(Database));
  db->file = file;
  file->db = db;
  db->complements = store_comps;
  db->in_memory = in_memory;

  db->t_length = db->num_entries = 0;
  db->first_entry = -1;
  db->last_entry = -2;
  db->id_area = db->descrip_area = db->seq_area = 
    db->orig_qual_area = db->adj_qual_area = 0;
  db->id_area_size = db->descrip_area_size = db->seq_area_size = 0;
  db->align_array = 0;

  db->id_buffer_size = ID_BUFFERSIZE;
  db->descrip_buffer_size = DESCRIP_BUFFERSIZE;
  db->seq_buffer_size = SEQ_BUFFERSIZE;

  db->id_buffer = (char *)our_alloc((db->id_buffer_size + 1) * sizeof(char));
  db->descrip_buffer = (char *)our_alloc((db->descrip_buffer_size + 1) * sizeof(char));
  db->seq_buffer = (char *)our_alloc((db->seq_buffer_size + 1) * sizeof(char));

  db->id_buffer[0] = db->descrip_buffer[0] = db->seq_buffer[0] = 0;

  db->next = head_db;
  head_db = db;

  if (in_memory) {
    notify("Reading query file into memory ...");
    alloc_mem(db, store_comps);
    file_to_mem(db);
    notify(" Done\n");

    if (store_comps) { 
      notify("Complementing ...");
      seq_area = db->seq_area;
      for (j = 0, k = db->seq_area_size - 1; j < k; j++, k--) 
	seq_area[k] = comp_mat[seq_area[j]];
      notify(" Done\n");
   }
    if (store_aligns) {
      notify("Allocating align_entries ...");

      init_aligns(db);
      init_qual_areas(db);

      notify(" Done\n");
    }
  }

  return db;
}


init_aligns(db)
     Database *db;
{
  char *our_alloc();
  int i;
  Align_info *align_array;

  align_array = db->align_array 
    = (Align_info *)our_alloc(db->num_entries * sizeof(Align_info)) - db->first_entry;
  for (i = db->first_entry; i <= db->last_entry; i++) {

    align_array[i].seq_entry = align_array[i].equiv_class = i;

    align_array[i].template_start = align_array[i].template_end = 0;

    align_array[i].orig_qual = align_array[i].adj_qual = 0;

    align_array[i].segments = 0;

    align_array[i].first_start = align_array[i].rev_first_start = align_array[i].first_vec 
      = align_array[i].qual_start = get_seq_length(i); 
    align_array[i].last_end = align_array[i].rev_last_end = 0; /* should be -1 ?? */
    align_array[i].last_vec = align_array[i].qual_end = -1;

    align_array[i].anomalies = 0;
    align_array[i].blocked = 0;
    align_array[i].bypassed = 0;
    align_array[i].chemistry = 0;
    align_array[i].direction = 0;
    align_array[i].chimera_bits = 0;
    align_array[i].tags = 0;
  }
}

init_qual_areas(db)
     Database *db;
{
  char *our_alloc();
  Align_info *align_entry;
  int i, j;

  db->orig_qual_area = (char *)our_alloc(db->t_length * sizeof(char));
  for (i = 0; i < db->t_length; i++) db->orig_qual_area[i] = parameters->default_qual;

/* with cross_match, adj_qual should be identical with orig_qual; otherwise, 
   make it separate */
  if (strcmp(parameters->calling_program, "cross_match")) {
    db->adj_qual_area = (char *)our_alloc(db->t_length * sizeof(char));
    for (i = 0; i < db->t_length; i++) db->adj_qual_area[i] = 0;
  }
  else db->adj_qual_area = db->orig_qual_area;

  for (i = db->first_entry, j = 0; i <= db->last_entry; i++) {
    align_entry = db->align_array + i;
    align_entry->orig_qual = db->orig_qual_area + j;
    align_entry->adj_qual = db->adj_qual_area + j;
    j += get_seq_length(i);
  }
  if (j != db->t_length) fatalError("qual_area size");
}

reset_file(db)
     Database *db;
{
  rewind(db->file->fp);
  fgetc(db->file->fp);
}

/* NB: following assumes that on entry fp is always pointing at the
   character directly behind the '>' at the beginning of the entry. On
   return, points to the appropriate
   buffer; thus to retain information when next entry is accessed, space
   must be allocated in the calling program. */

get_next_file_entry(db)
     Database *db;
{
  int i, c;
  char d;
  char *make_new_buffer();
  FILE *fp;
  char *id_buffer, *descrip_buffer, *seq_buffer;
  int id_buffer_size, descrip_buffer_size, seq_buffer_size;

  fp = db->file->fp;
  id_buffer = db->id_buffer;
  descrip_buffer = db->descrip_buffer;
  seq_buffer = db->seq_buffer;
  id_buffer_size = db->id_buffer_size;
  descrip_buffer_size = db->descrip_buffer_size;
  seq_buffer_size = db->seq_buffer_size;

  id_pos = ftell(fp); 

  i = 0;
  while (!isspace(c = fgetc(fp)) && c != EOF) {
    if (i >= id_buffer_size) {
      fprintf(stderr,"\nid_buffer expanded");
      db->id_buffer = id_buffer = make_new_buffer(id_buffer, &id_buffer_size);
      db->id_buffer_size = id_buffer_size;
    }
    id_buffer[i++] = c; 
  }
  
/*  if (i >= ID_BUFFERSIZE) fatalError("id_buffer overflow"); */
  id_buffer[i] = 0;
  if (c == EOF) return 0;

  for (; isspace(c) && c != '\n'; c = fgetc(fp));
  descrip_pos = ftell(fp) - 1;
  for (i = 0; c != '\n' && c != EOF; c = fgetc(fp)) {
    if (i >= descrip_buffer_size)  {
      fprintf(stderr,"\ndescrip_buffer expanded");
      db->descrip_buffer = descrip_buffer = make_new_buffer(descrip_buffer, &descrip_buffer_size);
      db->descrip_buffer_size = descrip_buffer_size;
    }
    descrip_buffer[i++] = c; 
  }
  descrip_buffer[i] = 0;
/*  if (i >= DESCRIP_BUFFERSIZE) fatalError("descrip_buffer overflow"); */

  seq_pos = ftell(fp);
  i = 0;
  while ((c = fgetc(fp)) != '>' && c != EOF) {
    if (d = alph_mat[c]) {
      if (i >= seq_buffer_size)  {
	fprintf(stderr,"\nseq_buffer expanded");
	db->seq_buffer = seq_buffer = make_new_buffer(seq_buffer, &seq_buffer_size);
	db->seq_buffer_size = seq_buffer_size;
      }
      seq_buffer[i++] = d;
    }
  }
  seq_buffer[i] = 0;
  db->seq_length = seq_length = i;
/*  if (i >= SEQ_BUFFERSIZE) fatalError("seq_buffer overflow"); */

  return 1;
}

char *make_new_buffer(buffer, add_size)
     char *buffer;
     int *add_size;
{
  int i;
  char *new_buffer;
  char *our_alloc();

  new_buffer = (char *)our_alloc((*add_size * 2 + 1) * sizeof(char));
  for (i = 0; i < *add_size; i++) new_buffer[i] = buffer[i];

  our_free(buffer);
  *add_size *= 2;
  return new_buffer;
}

/* initial read of database files to determine size needed, allocate space 
 store_comps is flag indicating whether complements are needed */
alloc_mem(db, store_comps)
     Database *db;
     int store_comps;
{
  FILE *fp;
  File *file;
  char *our_alloc();
  int c, n_entries, n_residues, n_id_chars, n_descrip_chars;
  int residues[256];

  n_id_chars = n_descrip_chars = 0;

  file = db->file; 
  for (c = 0; c < 256; c++) residues[c] = 0;
  n_entries = 0;
  fp = file->fp;
  do {
    n_entries++;
    while (!isspace(c = fgetc(fp)) && c != EOF) n_id_chars++;
    for (; isspace(c) && c != '\n'; c = fgetc(fp));
    for (; c != '\n' && c != EOF; c = fgetc(fp)) n_descrip_chars++;
    while ((c = fgetc(fp)) != '>' && c != EOF) residues[c] += 1;
  } while (c != EOF);
  printf("\n\nSequence file: %s    %d entries\nResidue counts:", 
	 file->name, n_entries);
  for (c = n_residues = 0; c < 256; c++) {
    if (alph_mat[c] && residues[c]) {
      n_residues += residues[c];
      printf("\n  %c    %5d", (char)c, residues[c]);
    }
  }
  printf("\nTotal  %5d", n_residues);
  rewind(fp);
  fgetc(fp);

  db->t_length = n_residues;
  db->id_area_size = n_id_chars + n_entries + 1;
  db->id_area = (char *)our_alloc(db->id_area_size * sizeof(char));
  db->descrip_area_size = n_descrip_chars + n_entries + 1;
  db->descrip_area = (char *)our_alloc(db->descrip_area_size * sizeof(char));

  if (store_comps) {
/* allow space for complements of entries in first file */
    n_entries *= 2;
    n_residues *= 2;
  }
  db->seq_area_size = n_residues + n_entries + 1;
  db->seq_area = (char *)our_alloc(db->seq_area_size * sizeof(char));
}


file_to_mem(db) 
     Database *db;
{
  char *our_alloc();
  FILE *fp;
  int c, len, i_id, i_descrip, i_seq, max_length;
  char d;
  char *temp_ptr;
  char *seq_area; /* array containing all residue sequences; first char is 0, 
		     which is followed by each sequence in turn, each being 
		     terminated by 0 */
  char *id_area, *descrip_area; /* arrays containing ids and descriptions */

  seq_area = db->seq_area;
  id_area = db->id_area;
  descrip_area = db->descrip_area;

  id_area[0] = descrip_area[0] = seq_area[0] = 0;
  i_id = i_descrip = i_seq = 1;

  max_length = 0;
  
  fp = db->file->fp;
  do { 
    id_pos = i_id;
    while (!isspace(c = fgetc(fp)) && c != EOF) id_area[i_id++] = c;
    id_area[i_id++] = 0;
    
    for (; isspace(c) && c != '\n'; c = fgetc(fp));
    
    descrip_pos = i_descrip;
    for (; c != '\n' && c != EOF; c = fgetc(fp)) descrip_area[i_descrip++] = c;
    descrip_area[i_descrip++] = 0;
    
    seq_pos = i_seq;
    
    while ((c = fgetc(fp)) != '>' && c != EOF) 
      if (d = alph_mat[c]) seq_area[i_seq++] = d; 
    
    seq_length = i_seq - seq_pos;
    seq_area[i_seq++] = 0;
    
    append_seq_entry(db);
    
    if (seq_length > max_length) max_length = seq_length;
    
    temp_ptr = id_area + id_pos;
    len = strlen(temp_ptr);
    if (!strcmp(temp_ptr + len - 4, ".seq")) {
      len -= 4;
      temp_ptr[len] = 0;
    }
  } while (c != EOF);
  rewind(fp);
  fgetc(fp);
  
  if (i_id != db->id_area_size) fatalError("id_area_size");
  if (i_descrip != db->descrip_area_size) fatalError("descrip_area_size");
  if (i_seq != (db->complements ? 1 + db->seq_area_size / 2 : db->seq_area_size)) {
    fprintf(stderr, "\n%d %d %d",db->complements, i_seq,  db->seq_area_size);
    fatalError("seq_area_size");
  }

  if (max_length > db->seq_buffer_size) { /* make sure buffer is adequate to store complements */
    db->seq_buffer_size = max_length;
    our_free(db->seq_buffer);
    db->seq_buffer = (char *)our_alloc((db->seq_buffer_size + 1) * sizeof(char));
    db->seq_buffer[0] = 0;
  }
/*  fprintf(stderr,"\n%s", seq_area + 1); */

/* following test probably not in correct location */
  if (!strcmp(parameters->calling_program, "phrap") && max_length > 64000 && sizeof(Read_position) < 4)
    fatalError("Longest read sequence exceeds current limit (64000)\n -- use program phrap.longreads instead (create using make manyreads)");
}

set_score(entry_num, score) 
     int entry_num, score;
{
  Seq_entry *get_seq_entry();

  get_seq_entry(entry_num)->score = score;
}
 
Seq_entry *get_seq_entry(entry_num)
     int entry_num;
{
  return seq_block_table[entry_num / BLOCK_SIZE] + entry_num % BLOCK_SIZE;
}

Database *get_db(entry_num)
     int entry_num;
{
  Database *db;

  for (db = head_db; db->first_entry > entry_num || db->first_entry < 0; db = db->next);
  return db; 
}

/* N.B. FOLLOWING ASSUME THAT id_buffer, descrip_buffer, AND seq_buffer ARE ALL LARGE ENOUGH */
char *get_id(entry_num)
     int entry_num;
{
  Seq_entry *seq_entry;
  Seq_entry *get_seq_entry();
  Database *db;
  Database *get_db();
  FILE *fp;
  int j, c;
  char *id_buffer;

  seq_entry = get_seq_entry(entry_num);
  db = get_db(entry_num);

  if (db->in_memory) return db->id_area + seq_entry->id_pos;

  fp = db->file->fp;
  fseek(fp, (long int)seq_entry->id_pos, 0);
  id_buffer = db->id_buffer;
  for (j = 0; !isspace(c = fgetc(fp)) && c != EOF;) id_buffer[j++] = c;
  id_buffer[j] = 0;
  return id_buffer;
}

char *get_descrip(entry_num)
     int entry_num;
{
  Seq_entry *seq_entry;
  Seq_entry *get_seq_entry();
  Database *db;
  Database *get_db();
  FILE *fp;
  int j, c;
  char *descrip_buffer;

  seq_entry = get_seq_entry(entry_num);
  db = get_db(entry_num);

  if (db->in_memory) return db->descrip_area + seq_entry->descrip_pos;

  fp = db->file->fp;
  fseek(fp, (long int)seq_entry->descrip_pos, 0);
  descrip_buffer = db->descrip_buffer;
  for (j = 0; (c = fgetc(fp)) != '\n' && c != EOF; ) descrip_buffer[j++] = c;
  descrip_buffer[j] = 0;
  return descrip_buffer;
}

char *get_seq(entry_num)
     int entry_num;
{
  Seq_entry *seq_entry;
  Seq_entry *get_seq_entry();
  Database *db;
  Database *get_db();
  FILE *fp;
  int j, c, length;
  char *seq_buffer;

  seq_entry = get_seq_entry(entry_num);
  db = get_db(entry_num);
  length = seq_entry->seq_length;

  if (db->in_memory) return db->seq_area + seq_entry->seq_pos;

  seq_buffer = db->seq_buffer;
  fp = db->file->fp;
  fseek(fp, (long int)seq_entry->seq_pos, 0);
  for (j = 0; j < length; ) {
    if (c = alph_mat[fgetc(fp)]) seq_buffer[j++] = c;
  }
  seq_buffer[j] = 0;
  return seq_buffer;
}

char *get_comp_seq(entry_num)
     int entry_num;
{
  Seq_entry *seq_entry;
  Seq_entry *get_seq_entry();
  Database *db;
  Database *get_db();
  FILE *fp;
  int j, c, length, seq_pos;
  char *seq;
  char *seq_buffer;

  seq_entry = get_seq_entry(entry_num);
  seq_pos = seq_entry->seq_pos;
  db = get_db(entry_num);
  length = seq_entry->seq_length;
  seq_buffer = db->seq_buffer;
  seq_buffer[length] = 0;

  if (db->in_memory) {
    if (db->complements)
      return db->seq_area + (db->seq_area_size - seq_pos - length);
    seq = db->seq_area + seq_pos;
    for (j = length - 1; j >= 0; j--, seq++) 
      seq_buffer[j] = comp_mat[*seq];
  }
  else { /* read from file into seq_buffer */
    fp = db->file->fp;
    fseek(fp, (long int)seq_pos, 0);
    for (j = length - 1; j >= 0; ) {
      if (c = alph_mat[fgetc(fp)]) seq_buffer[j--] = comp_mat[c];
    }
  }
  return seq_buffer;
}

int get_seq_length(entry_num)
     int entry_num;
{
  Seq_entry *get_seq_entry();

  return get_seq_entry(entry_num)->seq_length;
}

Cand_pair *get_cand_pairs(entry_num)
     int entry_num;
{
  Seq_entry *get_seq_entry();

  return get_seq_entry(entry_num)->cand_pairs;
}

Aligned_pair *get_aligned_pairs(entry_num)
     int entry_num;
{
  Seq_entry *get_seq_entry();

  return get_seq_entry(entry_num)->aligned_pairs;
}

//Add for ReAS
int get_aligned_num(entry_num)
     int entry_num;
{	
  Seq_entry *get_seq_entry();

  return get_seq_entry(entry_num)->aligned_num;
}
	
Align_info *get_align_entry(entry_num)
     int entry_num;
{
  Database *get_db();

  return get_db(entry_num)->align_array + entry_num;
}

char *get_orig_qual(entry_num)
     int entry_num;
{
  Database *get_db();

  return get_db(entry_num)->align_array[entry_num].orig_qual;
}

int get_equiv_class(entry_num)
     int entry_num;
{
  Database *get_db();

  return get_db(entry_num)->align_array[entry_num].equiv_class;
}

int set_equiv_class(entry_num, class)
     int entry_num, class;
{
  Database *get_db();

  get_db(entry_num)->align_array[entry_num].equiv_class = class;
}

char *get_adj_qual(entry_num)
     int entry_num;
{
  Database *get_db();

  return get_db(entry_num)->align_array[entry_num].adj_qual;
}

get_chemistry(entry_num)
     int entry_num;
{
  Database *get_db();

  return (int)(get_db(entry_num)->align_array[entry_num].chemistry);
}

get_read_direction(entry_num)
     int entry_num;
{
  Database *get_db();

  return (int)(get_db(entry_num)->align_array[entry_num].direction);
}

set_chemistry(entry_num, value)
     int entry_num, value;
{
  Database *get_db();

  get_db(entry_num)->align_array[entry_num].chemistry = value;
}

set_direction(entry_num, value)
     int entry_num, value;
{
  Database *get_db();

  get_db(entry_num)->align_array[entry_num].direction = value;
}

int append_seq_entry(db)
     Database *db;
{
  Seq_entry *seq_entry;
  int i_entry;

  i_entry = t_num_entries % BLOCK_SIZE;

  if (!i_entry) alloc_seq_block();

  seq_entry = seq_block_table[num_blocks - 1] + i_entry;
  seq_entry->id_pos = id_pos;
  seq_entry->descrip_pos = descrip_pos;
  seq_entry->seq_pos = seq_pos;
  seq_entry->seq_length = seq_length;
  seq_entry->aligned_pairs = 0;
  seq_entry->aligned_num = 0;  //Revised for ReAS
  seq_entry->cand_pairs = 0;
  if (db->first_entry < 0) db->first_entry = t_num_entries;
  db->last_entry = t_num_entries;
  db->num_entries += 1;
  t_num_entries++;
  return t_num_entries - 1;
}

remove_seq_entry(db)
     Database *db;
{
  int i_entry;

  db->last_entry -= 1;
  db->num_entries -= 1;

  t_num_entries--;

  i_entry = t_num_entries % BLOCK_SIZE;

  if (!i_entry) remove_seq_block();
}

alloc_seq_block()
{
  char *our_alloc();

  if (num_blocks >= MAX_NUM_BLOCKS) fatalError("MAX_NUM_BLOCKS exceeded");
  if (sizeof(Entry_index) < 4 && (num_blocks + 1) * BLOCK_SIZE > 64000)
    fatalError("Number of sequences exceeds current limit (64000)\n -- use .manyreads version instead (create using make manyreads)");

  seq_block_table[num_blocks++] = (Seq_entry *)our_alloc(BLOCK_SIZE * sizeof(Seq_entry));

}

remove_seq_block()
{
  num_blocks--;
  our_free(seq_block_table[num_blocks]);
}

write_entry(fp, id, descrip, seq)
     FILE *fp;
     char *id, *descrip, *seq;
{
  int j;

  fprintf(fp,">%s", id);
  if (descrip) fprintf(fp,"  %s", descrip);
  for (j = 0; ; j++) {
    if (!(j % 50)) fprintf(fp,"\n");
    if (!seq[j]) break;
    fprintf(fp,"%c", seq[j]);
  }
  fprintf(fp,"\n");
}
