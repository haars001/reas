/*****************************************************************************
#   Copyright (C) 1994-1996 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

#include "swat.h"

extern Parameters *parameters;

char *make_reversed_diffs(diffs, rev)
     char *diffs;
     int rev;
{
  char *our_alloc();     
  char *diff, *diff2, *diffs2;
  int n_diffs;
  int d, d_next, g;

  if (!diffs) return 0;
  n_diffs = strlen(diffs) + 1;
  diffs2 = (char *)our_alloc(n_diffs * sizeof(char));

  if (rev) {
    diffs2[n_diffs - 1] = 0;
    d = 'S';
    for (diff = diffs, diff2 = diffs2 + n_diffs - 2; *diff; diff2--, diff++) {
      d_next = diff_type(*diff); /* d and d_next are the types of discrepancies
				    which flank the interval */
      g = diff_gap2(*diff);
      if (d_next == 'D') g++;
      if (d == 'D') {
	g--;
	d = 'I';
      }
      else if (d == 'I') d = 'D';
      *diff2 = set_diff(g, d);
      d = d_next;
    }
  }
  else {
    for (diff = diffs, diff2 = diffs2; *diff; diff2++, diff++) {
      d = diff_type(*diff);
      g = diff_gap2(*diff);
      if (d == 'I') *diff2 = set_diff(g, 'D');
      else if (d == 'D') *diff2 = set_diff(g, 'I');
      else *diff2 = *diff;
    }
    *diff2 = 0;
  }
  return diffs2;
}
/*
char *make_reversed_diffs(pair, diffs)
     Aligned_pair *pair;
     char *diffs;
{
  char *our_alloc();     
  Diff *diff, *diff2, *pair2_diffs;
  int i, length1, length2;
  int n_diffs;

  n_diffs = strlen(diffs) + 1;
  diff2 = pair2_diffs = (Diff *)our_alloc(n_diffs * sizeof(Diff));
  if (is_reverse(pair)) {
    length1 = get_seq_length(pair->entry1) - 1;
    length2 = get_seq_length(pair->entry2) - 1;
    diff = diffs + n_diffs - 1;
    for (i = 0; i < n_diffs; i++, diff2++, diff--) {
      diff2->site1 = length2 - diff->site2;
      diff2->site2 = length1 - diff->site1;
      if (diff->type == 'I') {
	diff2->type = 'D';
	diff2->site2 -= 1;
      }
      else if (diff->type == 'D') {
	diff2->type = 'I';
	diff2->site1 -= 1;
      }
      else diff2->type = diff->type;
    }
    if ((diff2 - 1)->type != 'B') fatalError("Diff count");
    (diff2 - 1)->type = 'E';
    if (pair2_diffs->type != 'E') fatalError("Diff count");
    pair2_diffs->type = 'B';
  }
  else {
    diff = diffs;
    for (i = 0; i < n_diffs; i++, diff2++, diff++) {
      diff2->site1 = diff->site2;
      diff2->site2 = diff->site1;
      if (diff->type == 'I') diff2->type = 'D';
      else if (diff->type == 'D') diff2->type = 'I';
      else diff2->type = diff->type;
    }
    if ((diff2 - 1)->type != 'E') fatalError("Diff count");
    if (pair2_diffs->type != 'B') fatalError("Diff count");
  }
  return pair2_diffs;
}
*/

print_alignment_from_diffs(pair)
     Aligned_pair *pair;
{
  int i, j, cc, i_start, j_start;
  int g_init, g_tot, last_i_init, last_j_init;
  double transitions, transversions, length;
  char *diff_i, *diff_j, *diff_i_start, *diff_j_start;
  char print_char;
  char *strchr();
  char *diffs;
  int site1, site2, site1_start, site2_start;
  char *get_id(), *get_seq(), *get_comp_seq();
  char *id1, *id2, *seq1, *seq2;
  int length1, length2, mark_matches;

  diffs = pair->diffs;

  /* N.B. This could be a problem for cross_match -- may need to construct complement! */

  mark_matches = !parameters->DNA_flag;
  transitions = transversions = 0;
  i = pair->start2;
  j = pair->start1; 
  diff_i = diff_j = diffs;
  site1 = j - 1 + diff_gap1(*diff_j);
  site2 = i - 1 + diff_gap2(*diff_i);

  id1 = get_id(pair->entry1);
  id2 = get_id(pair->entry2);
  seq1 = get_seq(pair->entry1);
  seq2 = is_reverse(pair) ? get_comp_seq(pair->entry2) : get_seq(pair->entry2);
  length1 = get_seq_length(pair->entry1);
  length2 = get_seq_length(pair->entry2);

  g_init = g_tot = 0;
  last_i_init = last_j_init = -1;
  for ( ; i <= pair->end2 && j <= pair->end1; ) {
    i_start = i;
    j_start = j;
    diff_i_start = diff_i;
    diff_j_start = diff_j;
    site1_start = site1;
    site2_start = site2;

    printf("\n\n%c %-15.15s %6d ", is_reverse(pair) ? 'C' : ' ', id2, 
	   is_reverse(pair) ? length2 - i : i + 1);
    for (cc = 0; cc < 50 && i <= pair->end2; cc++) {
      while (*(diff_i + 1) && (site2 < i - 1 || diff_type(*diff_i) != 'D')) {
	diff_i++;
	site2 += diff_gap2(*diff_i);
      }
      if (i <= site2) {
	printf("%c", seq2[i]);
	i++;
      }
      else {
	g_tot++;
	if (i != last_i_init) {
	  g_init++;
	  last_i_init = i;
	}
	printf("-");
	diff_i++;
	site2 += diff_gap2(*diff_i);
      }
    }
    printf(" %d\n%15.15s          ", is_reverse(pair) ? length2 - i + 1 : i, "");

    diff_i = diff_i_start;
    site2 = site2_start;
    for (cc = 0, i = i_start; cc < 50 && i <= pair->end2; cc++) {
      while (*(diff_i + 1) && (site2 < i - 1 || diff_type(*diff_i) != 'D'))  {
	diff_i++;
	site2 += diff_gap2(*diff_i);
      }
      while (*(diff_j + 1) && (site1 < j - 1 || diff_type(*diff_j) != 'I'))  {
	diff_j++;
	site1 += diff_gap1(*diff_j);
      }

      if (i <= site2 && j <= site1) {
	if (mark_matches) 
	  print_char = seq2[i] == seq1[j] ? seq2[i] : ' '; 
	else {
	  print_char = ' ';
	  if (seq2[i] != seq1[j]) {
	    if (!strchr("ACGTYR", seq1[j]) || !strchr("ACGTYR", seq2[i]))
	      print_char = '?';
	    else if (strchr("CTY", seq1[j]) && strchr("CTY", seq2[i])
		     || strchr("AGR", seq1[j]) && strchr("AGR", seq2[i])) {
	      transitions++;
	      print_char = 'i';
	    }
	    else {
	      transversions++;
	      print_char = 'v';
	    }
	  }
	}
	i++;
	j++;
      }
      else {
	print_char = '-';
	if (j <= site1) {
	  j++;
	  diff_i++;
	  site2 += diff_gap2(*diff_i);
	}
	else {
	  i++;
	  diff_j++;
	  site1 += diff_gap1(*diff_j);
	}
      }
      printf("%c", print_char);
    }

    j = j_start;
    diff_j = diff_j_start;
    site1 = site1_start;
    printf("\n  %-15.15s %6d ", id1, j + 1);
    for (cc = 0; cc < 50 && j <= pair->end1; cc++) {
      while (*(diff_j + 1) && (site1 < j - 1 || diff_type(*diff_j) != 'I'))  {
	diff_j++;
	site1 += diff_gap1(*diff_j);
      }
      if (j <= site1) {
	printf("%c", seq1[j]);
	j++;
      }
      else {
	g_tot++;
	if (j != last_j_init) {
	  g_init++;
	  last_j_init = j;
	}
	printf("-");
	diff_j++;
	site1 += diff_gap1(*diff_j);
      }
    }
    printf(" %d", j);
  }
  length = pair->end1 - pair->start1 + 1;
  printf("\n\nTransitions / transversions = %.2f (%.0f / %.0f)", transversions ? transitions / transversions : 1.0,
	 transitions, transversions);
  printf("\nGap_init rate = %.2f (%d / %.0f), avg. gap size = %.2f (%d / %d)  \n", 
	 g_init / length, g_init, length, g_init ? g_tot / (double)g_init : 0.0, g_tot, g_init);
	 
}

