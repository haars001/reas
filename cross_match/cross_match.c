/*****************************************************************************
#   Copyright (C) 1994-1996 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

#include "swat.h"

extern Parameters *parameters; /* from get_parameters() */
extern Database *query_db;

main(argc,argv)
     int argc;
     char *argv[];
{
      
  get_parameters(argc, argv, "cross_match");

/* read in sequence files, initialize array of aligned read info, and log file of ancillary information,
 and find pairwise matches */
  readin_and_match();

  print_matches();
/*  print_coverage(query_db); */
/*  analyze_discreps(query_db); Needs revision*/

  if (parameters->screen) screen_seqs(query_db);

  check_pairs();
  check_alloc_list();
  print_t_n_pairs();
  print_t_n_segments();
  print_t_n_diffs();
  notify("\n");
  return 0;
}

