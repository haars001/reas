/*****************************************************************************
#   Copyright (C) 1994-1996 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

#include <stdio.h>

FILE *fp_log; /* log file */

init_log_file(file)
     char *file;
{
  char *our_alloc();
  char *log_file;
  FILE *fopenWrite();

  log_file = (char *)our_alloc(strlen(file) + 10);
  strcpy(log_file, file);
  strcat(log_file,".log");
  fp_log = fopenWrite(log_file);
  our_free(log_file);
}

