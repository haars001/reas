/*****************************************************************************
#   Copyright (C) 1994-1998 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

#include "swat.h"

extern Parameters *parameters;
extern int num_pairs;

static int num_swat, num_good_scores;
static int n_successes, n_failures;

reset_n_successes()
{
  n_successes = n_failures = 0;
}

reset_swat_statics()
{
  num_swat = num_good_scores = 0;
}

get_num_swat()
{
  return num_swat;
}

get_num_good_scores()
{
  return num_good_scores;
}

print_n_swats()
{
  fprintf(stderr, "Quickalign: %d successes, %d failures\n", n_successes, n_failures);
  fprintf(stderr, "%d SWAT alignments performed. %d pairs have score >= %d\n",
	  num_swat, num_pairs, parameters->minscore);
}

recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
	       q_left, q_right, s_left, s_right)
     int minscore;
     Cand_pair *pair;
     Profile *q_profile;
     char *seq;
     int seq_length;
     int l_edge, r_edge, q_left, q_right, s_left, s_right;
{
  int quick_full_smith_waterman();
  Aligned_pair *append_pair();
  int start1, end1, start2, end2, score;
  int mismatches, deletions, insertions;
  char *make_diffs();
  int orig_score;
  Aligned_pair *match_pair;
  int success;

  score = quick_full_smith_waterman(q_profile, seq, seq_length, l_edge, r_edge, 
				    q_left, q_right, s_left, s_right, minscore, &orig_score, &success);

  num_swat++;
  if (!(num_swat % 1000)) notify(".");
  if (orig_score >= minscore) {
    get_stats(&start1, &end1, &start2, &end2, &mismatches, &insertions, &deletions);
    success ? n_successes++ : n_failures++;

    if (start1 < q_left || end1 > q_right || start1 >= end1 ||
	start2 < s_left || end2 > s_right || start2 >= end2)
      fprintf(stderr, "\nWARNING: %d %d %d start/end out of bounds %d %d %d %d / %d %d %d %d",
	      orig_score, score, minscore, start1, end1, q_left, q_right, start2, end2, s_left, s_right);
    if (score >= minscore) {
      num_good_scores++;
      num_pairs++; 
      match_pair = append_pair(pair->entry1, pair->entry2);
      set_reverse_flag(match_pair, pair->reverse);
      match_pair->score = score;
      match_pair->diffs = make_diffs();
      match_pair->start1 = start1 - 1;
      match_pair->start2 = start2 - 1;
      match_pair->end1 = end1 - 1;
      match_pair->end2 = end2 - 1;
    } 
    
/* N.B. FOLLOWING ASSUMES REQUIRE EACH SEQUENCE NEEDS TO BE AT LEAST MINSCORE
   IN LENGTH TO HAVE POSSIBLE SCORE OF MINSCORE -- NOT TRUE WITH MATRIX */

/*    fprintf(stderr, "*%d %d %d %d\n", start1, end1, start2, end2); */
#if defined(OLDVERSION)
    if (l_edge <= r_edge) {
      if (q_left <= start1 - minscore) {
	if (s_left <= start2 - minscore)
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 q_left, start1 - 1, s_left, start2 - 1);
	if (s_right >= end2 + minscore)
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 q_left, start1 - 1, end2 + 1, s_right);
      }
      if (q_right >= end1 + minscore) {
	if (s_left <= start2 - minscore)
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 end1 + 1, q_right, s_left, start2 - 1);
	if (s_right >= end2 + minscore)
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 end1 + 1, q_right, end2 + 1, s_right);
      }
    }
    else { /* perhaps this should be the generic case */
      if (s_left < start2)
	recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		       q_left, q_right, s_left, start2 - 1);
      if (s_right > end2)
	recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		       q_left, q_right, end2 + 1, s_right);
      if (!parameters->subject_files) {
/* ignore overlapping matches in first input file --- */
	if (q_left < start1) 
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 q_left, start1 - 1, s_left, s_right);
	if (q_right > end1) 
	  recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
			 end1 + 1, q_right, s_left, s_right);
      }
    }
#else
    if (s_left < start2)
      recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		     q_left, q_right, s_left, start2 - 1);
    if (s_right > end2)
      recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		     q_left, q_right, end2 + 1, s_right);

/* NOTE NON-SYMMETRIC CONDITIONS -- BUT WANT TO AVOID REDUNDANCY */

/*
    if (q_left < start1) 
      recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		     q_left, start1 - 1, s_left, s_right);
    if (q_right > end1) 
      recursive_swat(minscore, pair, q_profile, seq, seq_length, l_edge, r_edge, 
		     end1 + 1, q_right, s_left, s_right);
*/
#endif
  }
}
