/*****************************************************************************
#   Copyright (C) 1993-1998 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

#include "swat.h"

extern Parameters *parameters;

static char alphabet[256];
static int alphsize;
static char *row_labels;
static int n_rows, n_places;
static int **score_mat;
static int gap_init, gap_ext, ins_gap_ext, del_gap_ext, end_gap;
static int row_convert[256], col_convert[256];
static int profile_flag;
static double *freqs, *log_freqs;
static double lambda;
static int freq_flag;

/* Following routine reads in score matrix, and sets gap penalties; if file_name is 0 or the empty
 string, it is assumed that the matrix is for nucleotides (upper or lower
 case) and mismatch penalties are set to mismatch, except for N and X (which
 have penalties equal to 0 and -1 respectively). mismatch is ignored when
 file_name is not empty */

get_alphsize()
{
  return alphsize;
}

get_gap_init()
{
  return gap_init;
}

get_gap_ext()
{
  return gap_ext;
}

get_ins_gap_ext()
{
  return ins_gap_ext;
}

get_del_gap_ext()
{
  return del_gap_ext;
}

set_gap_penalties(gap_init0, gap_ext0, ins_gap_ext0, del_gap_ext0, end_gap0)
     int gap_init0, gap_ext0, ins_gap_ext0, del_gap_ext0, end_gap0;
{
  gap_init = gap_init0;
  gap_ext = gap_ext0;
  ins_gap_ext = ins_gap_ext0;
  del_gap_ext = del_gap_ext0;
  end_gap = end_gap0;
}

set_score_mat(file_name, mismatch)
     char *file_name;
     int mismatch;
{
  FILE *fp;
  FILE *fopenRead();
  char buffer[501];
  char *our_alloc();
  int i, j, k, n_lines, alph_flag, file_flag, gap_flag;
  char letter;
  float frequency;
  double total;
  double pre_freqs[256];

  file_flag = file_name && file_name[0];
  if (score_mat) {
    for (i = 0; i < n_rows; i++) our_free(score_mat[i]);
    our_free(score_mat);
  }
  gap_flag = 0;
  if (row_labels) our_free(row_labels);
  if (file_flag) {
    freqs = 0;
    freq_flag = 0;
    for (i = 0; i < 256; i++) pre_freqs[i] = 0;
    fp = fopenRead(file_name);
    n_rows = alph_flag = n_lines = 0;
    while (fgets(buffer, 500, fp)) {
      if (!alph_flag) n_lines++;
      for (i = 0; buffer[i] && isspace(buffer[i]); i++);
      if (!buffer[i] || buffer[i] == '#') continue;
      if (!strncmp(buffer + i, "GAP", 3)) {
	gap_flag = 1;
	if (2 != sscanf(buffer + i + 3, "%d %d", &gap_init, &gap_ext))
	  fatalError("Score matrix format: GAP line incorrect");
      }
      else if (!strncmp(buffer + i, "FREQS", 5)) {
	freq_flag = 1;
	k = i + 5; 
	do {
	  while (buffer[k] && isspace(buffer[k])) k++;
	  if (!buffer[k]) break;
	  if (1 != sscanf(buffer + k, "%c ", &letter))
	    fatalError("Incorrect score matrix format -- FREQS line");
	  for (k++; buffer[k] && isspace(buffer[k]); k++);
	  if (1 != sscanf(buffer + k, "%f", &frequency))
	    fatalError("Incorrect score matrix format -- FREQS line");
	  while (buffer[k] && !isspace(buffer[k])) k++;
	  pre_freqs[letter] = frequency;
	} while (buffer[k]);
      }
      else if (!alph_flag) {
	for (alphsize = 0; buffer[i]; i++) 
	  if (!isspace(buffer[i])) {
	    if ((isalpha(buffer[i]) || buffer[i] == '*') && alphsize < 256)
	      alphabet[alphsize++] = buffer[i]; /* toupper(buffer[i]); */
	    else fatalError("Incorrect score matrix format-- reading column labels");
	  }
	alph_flag = 1;
      }
      else n_rows++;
    }
    rewind(fp);
    for (i = 0; i < n_lines; i++) fgets(buffer, 500, fp);
  }
  else {
    alphsize = n_rows = 6;
    strcpy(alphabet,"ACGTNX");
    gap_flag = 1;
    gap_init = mismatch - 2;
    gap_ext = mismatch - 1;
  }

  for (i = 1; i < alphsize; i++)
    for (j = 0; j < i; j++)
      if (alphabet[i] == alphabet[j])
	fatalError("Incorrect score matrix format -- duplicate column labels");

  for (i = 0; i < 256; i++) col_convert[i] = alphsize - 1;
  for (i = 0; i < alphsize; i++) col_convert[alphabet[i]] = i;
 
  if (!freqs) {
    freqs = (double *)our_alloc(256 * sizeof(double));
    log_freqs = (double *)our_alloc(256 * sizeof(double));
    for (i = 0; i < 256; i++) freqs[i] = 0;

    if (freq_flag) {
      for (i = 0; i < 256; i++) {
	freqs[col_convert[i]] = pre_freqs[i];
      }
    }
    else if (alphsize < 20) {
      freqs[col_convert['A']] = .25;
      freqs[col_convert['C']] = .25;
      freqs[col_convert['G']] = .25;
      freqs[col_convert['T']] = .25;
    }
    else {
      freqs[col_convert['A']] = .0756;
      freqs[col_convert['C']] = .0171;
      freqs[col_convert['D']] = .0531;
      freqs[col_convert['E']] = .0633;
      freqs[col_convert['F']] = .0408;
      freqs[col_convert['G']] = .0688;
      freqs[col_convert['H']] = .0224;
      freqs[col_convert['I']] = .0574;
      freqs[col_convert['K']] = .0596;
      freqs[col_convert['L']] = .0934;
      freqs[col_convert['M']] = .0214;
      freqs[col_convert['N']] = .0456;
      freqs[col_convert['P']] = .0493;
      freqs[col_convert['Q']] = .0404;
      freqs[col_convert['R']] = .0517;
      freqs[col_convert['S']] = .0721;
      freqs[col_convert['T']] = .0578;
      freqs[col_convert['V']] = .0654;
      freqs[col_convert['W']] = .0127;
      freqs[col_convert['Y']] = .0322;
    }
    for (i = total = 0; i < 256; i++) total += freqs[i];
    if (total) {
      for (i = 0; i < 256; i++) {
	freqs[i] /= total;
      }
    }
    
    for (i = 0; i < 256; i++) log_freqs[i] = freqs[i] ? log(freqs[i]) : 0.0;
  }


/* N.B. NEED CODE FOR PROTEINS; AND FOR PROFILES !!!  */

  score_mat = (int **)our_alloc(n_rows * sizeof(int *));
  row_labels = (char *)our_alloc(n_rows * sizeof(char));
  for (i = 0; i < n_rows; i++) {
    row_labels[i] = alphabet[i]; /* default */
    score_mat[i] = (int *)our_alloc(alphsize * sizeof(int));
    if (file_flag) {
      do {
	fgets(buffer, 500, fp);
	for (k = 0; buffer[k] && isspace(buffer[k]); k++);
      } while (!buffer[k] || buffer[k] == '#');
      if (isalpha(buffer[k]) || buffer[k] == '*') {
	row_labels[i] = buffer[k];
	k++;
      }
      else if (i >= alphsize) 
	fatalError("Incorrect score matrix format -- too many rows");
      for (j = 0; j < alphsize; j++) {
	while (buffer[k] && isspace(buffer[k])) k++;
	if (!buffer[k] || 1 != sscanf(buffer + k,"%d",&score_mat[i][j]))
	  fatalError("Incorrect score matrix format -- while reading rows");
	while (buffer[k] && !isspace(buffer[k])) k++;
      }
    }
    else {
/*      printf("\n"); */
      for (j = 0; j < alphsize; j++) {
	if (toupper(alphabet[j]) == 'N' || toupper(alphabet[i]) == 'N')
	  score_mat[i][j] = 0;
	else if (alphabet[j] == 'X' || alphabet[i] == 'X')
	  score_mat[i][j] = mismatch - 1; /* was 1 -- is this right? */
	else if (toupper(alphabet[i]) == toupper(alphabet[j]))
	  score_mat[i][j] = 1;
/*	else if (islower(alphabet[i]) || islower(alphabet[j]))
	  score_mat[i][j] = gap_ext / 2; */
	else score_mat[i][j] = mismatch;
/*	printf("%3d ",score_mat[i][j]); */
      }
    }
  }
  if (file_flag) fclose(fp); 

  profile_flag = 0;
  for (i = 1; i < n_rows && !profile_flag; i++)
    for (j = 0; j < i && !profile_flag; j++)
      if (row_labels[i] == row_labels[j]) profile_flag = 1;

  if (!profile_flag) {
    for (i = 0; i < 256; i++) row_convert[i] = n_rows - 1;
    for (i = 0; i < n_rows; i++) row_convert[row_labels[i]] = i;
  }   
  n_places = alphsize + 1 + 10;

  get_lambda();

/*  printf("\n%s\n",alphabet); */
  return gap_flag;
}

print_score_mat()
{
  int i, j;

  printf("\n ");
  for (i = 0; i < alphsize; i++) printf("   %c",  alphabet[i]);
  for (i = 0; i < n_rows; i++) {
    printf("\n%c",  row_labels[i]);
    for (j = 0; j < alphsize; j++) {
      printf(" %3d", score_mat[i][j]);
    }
  } 
}

print_background_freqs()
{
  int i;

  printf(" Assumed background frequencies:\n ");
  for (i = 0; i < alphsize; i++) printf("%c: %.3f  ",  alphabet[i], freqs[i]);
}

Profile *make_profile_from_seq(seq, nw_flag)
     char *seq;
     int nw_flag;
{
  char *our_alloc();
  Profile *profile;
  int i, j, k, length;
  char *strchr();
  int offset, max_score_cutoff, packed_length;

  for (length = 0; seq[length]; length++);

  if (profile_flag) {
    if (length != n_rows)
	fatalError("Query sequence does not match profile matrix row labels");
    for (i = 0; i < n_rows; i++)
      if (seq[i] != row_labels[i])
	fatalError("Query sequence does not match profile matrix row labels");
  }

  profile = (Profile *)our_alloc(sizeof(Profile));
  profile->length = length;
  profile->seq = (char *)our_alloc((length + 1) * sizeof(char));
  strcpy(profile->seq, seq);
  profile->maxstu_vec = (int *)our_alloc((length + 1) * sizeof(int));
  profile->t_vec = (int *)our_alloc((length + 1) * sizeof(int));
  for (j = 0; j < length; j++) profile->maxstu_vec[j] = profile->t_vec[j] = 0; 
  profile->maxstu_vec[length] = -1; /* sentinel for end of vector in smith-waterman
				  algorithm -- ; can never occur
				  with actual scores */
  profile->t_vec[length] = 0; /* sentinel (in comb w/ maxstu_vec[j] in n-w algorthm,
			    since never should exceed maxstu_vec with actual
			    scores */
  profile->convert = (int *)our_alloc(256 * sizeof(int));
  for (i = 0; i < 256; i++) profile->convert[i] = col_convert[i];

/*
  for (i = 1, j = 0; i < 256; i++) N.B. strchr(alphabet,0) is non-null !
    profile->convert[i] = strchr(alphabet, i) ?
      strchr(alphabet, i) - alphabet : alphsize - 1;
*/
  /* previously used toupper(i) instead of i */
  profile->scores = (int **)our_alloc(alphsize * sizeof(int *));
/*  profile->score_pos = (int **)our_alloc(alphsize * sizeof(int *)); */
  profile->max_entry = profile->min_entry = 0;
  for (i = 0; i < alphsize; i++) {
    profile->scores[i] = (int *)our_alloc((1 + length) * sizeof(int));
/*    profile->score_pos[i] = (int *)our_alloc((1 + length) * sizeof(int)); */
    for (j = k = 0; j < length; j++) {
      profile->scores[i][j] = 
	profile_flag ? score_mat[j][i] : score_mat[row_convert[seq[j]]][i];

      if (nw_flag) profile->scores[i][j] -= gap_ext + gap_ext;
/* last term is adjustment for needleman-wunsch algorithm, to allow assumption
   that gap_ext = 0 */
/* N.B. ADJUSTMENTS NEEDED FOR INS_GAP_EXT, DEL_GAP_EXT */
      if (profile->scores[i][j] > profile->max_entry)
	profile->max_entry = profile->scores[i][j];
      if (profile->scores[i][j] < profile->min_entry)
	profile->min_entry = profile->scores[i][j];
/*      if (profile->scores[i][j] > 0) profile->score_pos[i][k++] = j; */
    }
    profile->scores[i][length] = 0;
/*    profile->score_pos[i][k] = length; */
  }
  if (parameters->query_histograms) {
    profile->hist = (int **)our_alloc(length * sizeof(int *));
    for (i = 0; i < length; i++) {
      profile->hist[i] = (int *)our_alloc(n_places * sizeof(int));
      for (j = 0; j < n_places; j++) profile->hist[i][j] = 0;
    }
  }

  profile->lambda = lambda;
  profile->log_freqs = log_freqs;
  profile->gap_init = gap_init;
  profile->gap_ext = gap_ext;
  profile->ins_gap_ext = ins_gap_ext;
  profile->del_gap_ext = del_gap_ext;
  /* temporary settings */
  profile->qbegin_gap_init = end_gap;
  profile->qbegin_gap_ext = end_gap;
  profile->qend_gap_init = end_gap;
  profile->qend_gap_ext = end_gap;

  profile->sbegin_gap_init = end_gap;
  profile->sbegin_gap_ext = end_gap;
  profile->send_gap_init = end_gap;
  profile->send_gap_ext = end_gap;
/*  profile->score_mat = score_mat; */
 
  profile->packed_length = packed_length = (length + 8) / 8; /* +8, rather than +7, to allow
								for shift of last byte to next
								iteration (in fast_smith_wat) */
  offset = 1;
  if (profile->min_entry < 0 && offset < -profile->min_entry) offset = -profile->min_entry;
  if (profile->gap_init < 0 && offset < -profile->gap_init) offset = -profile->gap_init;
  if (profile->gap_ext < 0 && offset < -8 * profile->gap_ext) offset = -8 * profile->gap_ext;
  profile->poly_offset = profile->poly_one = 0;
  profile->max_score_cutoff = 0;
  profile->poly_gap_init = 0;
  profile->poly_gap_ext = 0;
  max_score_cutoff = 127 - profile->max_entry;
/* N.B. COULD BE 255 - profile->max_entry -- IF WEREN'T USING 1 BIT FOR A POSITIVE T
   INDICATOR */
  for (i = 0; i < 8; i++) {
    profile->poly_offset <<= 8;
    profile->poly_offset += offset;
    profile->poly_one <<= 8;
    profile->poly_one += 1;
    profile->max_score_cutoff <<= 8;
    profile->max_score_cutoff += max_score_cutoff;
    profile->poly_gap_init <<= 8;
    profile->poly_gap_init += -gap_init;
    profile->poly_gap_ext <<= 8;
    profile->poly_gap_ext += -gap_ext;
  }
/* N.B. ASSUMING gap_init, gap_ext are NON-POSITIVE */
  profile->packed_maxstu_vec = (unsigned long int *)our_alloc((2 * packed_length + 2) * sizeof(unsigned long int));
  profile->packed_scores = (unsigned long int **)our_alloc(alphsize * sizeof(unsigned long int *));
  for (i = 0; i < alphsize; i++) {
    profile->packed_scores[i] = (unsigned long int *)our_alloc((1 + packed_length) * sizeof(unsigned long int));

    for (j = 0; j < packed_length * 8; j++) {
      profile->packed_scores[i][j / 8] <<= 8;
      profile->packed_scores[i][j / 8] += offset + (j < length ? profile->scores[i][j] : 0);
    }
    profile->packed_scores[i][packed_length] = 0;
  }
/*
  fprintf(stderr, "\n%d %d %d %d %d\n", gap_init, gap_ext, offset, packed_length, max_score_cutoff);
  fprintf(stderr, "\n%ld %ld %ld %d %ld\n", profile->poly_gap_init & 255, profile->poly_gap_ext & 255, profile->poly_offset & 255, profile->packed_length & 255, profile->max_score_cutoff & 255);
*/
  return profile;
}

maximal_profile_segments(seq, minscore, id, screen)
     char *seq, *id;
     int minscore, screen;
{
  int i;
  Profile *profile;
  Profile *make_profile_from_seq();

  profile = make_profile_from_seq(seq, 0);

  for (i = 0; i < alphsize; i++)
    find_segs(profile, i, 0, profile->length - 1, minscore, seq, id, screen);
  free_profile(profile);
}

find_segs(profile, i_alph, j_start, j_end, minscore, seq, id, screen)
     Profile *profile;
     int i_alph, j_start, j_end, minscore, screen;
     char *seq, *id;
{
  int j;
  int max, cum, min, j_min, j_max, j_min_best;

  max = min = cum = 0;
  j_min = j_start - 1;
  for (j = j_start; j <= j_end; j++) {
    cum += profile->scores[i_alph][j];
    if (cum < min) {
      min = cum;
      j_min = j;
    }
    else if (cum - min > max) {
      max = cum - min;
      j_min_best = j_min + 1;
      j_max = j;
    }
  }
  if (max >= minscore) {
    printf("\n%s   %c   Score: %4d   Residues: %5d - %5d", 
	   id, alphabet[i_alph], max, j_min_best + 1, j_max + 1);
    if (screen) {
      for (j = j_min_best; j <= j_max; j++) seq[j] = 'X';
    }
    find_segs(profile, i_alph, j_start, j_min_best, minscore, seq, id, screen);
    find_segs(profile, i_alph, j_max + 1, j_end, minscore, seq, id, screen);
  }
}

show_query_hist(profile)
  Profile *profile;
{
  int i, j, r_total, n_match, match_pos;

  printf("\n\nQuery discrepancy histogram:\n              ");
  for (i = 0; i < alphsize; i++) printf(" %c ", alphabet[i]);
  printf("    -  I1 I2 ...");
  for (i = 0; i < profile->length; i++) {
    r_total = 0;
    for (j = 0; j < n_places; j++) {
      r_total += profile->hist[i][j];
    }
    match_pos = profile->convert[profile->seq[i]];
    n_match = profile->hist[i][match_pos];
    profile->hist[i][match_pos] = 0;
    printf("\n%c %4d %4d  ", profile->seq[i], r_total, n_match);
    for (j = 0; j < alphsize; j++) printf(" %2d", profile->hist[i][j]);
    printf("   ");
    for (; j < n_places; j++) printf(" %2d", profile->hist[i][j]);
  }
}  


free_profile(profile)
  Profile *profile;
{
  int i;

  if (parameters->query_histograms) {
    for (i = 0; i < profile->length; i++) our_free(profile->hist[i]);
    our_free(profile->hist);
  }
  for (i = 0; i < alphsize; i++) {
    our_free(profile->scores[i]);
    our_free(profile->packed_scores[i]);
  }
  our_free(profile->scores); 
  our_free(profile->convert); 
  our_free(profile->t_vec); 
  our_free(profile->maxstu_vec); 
  our_free(profile->seq); 
  our_free(profile->packed_maxstu_vec);
  our_free(profile->packed_scores);
  our_free(profile); 
}

/* generate random sequence -- not needed at present 
char *get_random_seq(length)
     int length;
{
  char *our_alloc();
  char *seq;
  int i;

  seq = (char *)our_alloc((length + 1) * sizeof(char));
  for (i = 0; i < length; i++)
    seq[i] = alphabet[random() % alphsize];
  seq[length] = 0;
  return seq;
}
*/
      
get_lambda()
{
  double get_sum();
  double sum, lambda_lower, lambda_upper;

  lambda_lower = 0.0;
  lambda = 0.5;
  for (;;) {
    sum = get_sum();
    if (sum >= 1.0) break;
    lambda_lower = lambda;
    lambda *= 2.0; 
  } 
  lambda_upper = lambda;
  while (lambda_upper - lambda_lower > .00001) {
    lambda = (lambda_lower + lambda_upper) / 2.0;
    sum = get_sum();
    if (sum >= 1.0) lambda_upper = lambda;
    else lambda_lower = lambda;
  } 
/*
  fprintf(stderr,"\n lambda: %f  sum: %f", lambda, sum);
*/
}

double get_sum()
{
  int i, j;
  double total;
  double check;

  check = 0;
  for (i = total = 0; i < alphsize; i++)
    for (j = 0; j < alphsize; j++)
      if (freqs[i] && freqs[j]) {
	total += freqs[i] * freqs[j] * exp(lambda * score_mat[i][j]);
	check += freqs[i] * freqs[j];
      }
  if (check > 1.001 || check < .999) fatalError("frequency sums");
  return total;
}
