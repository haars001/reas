/*****************************************************************************
#   Copyright (C) 1994-1999 by Phil Green.                          
#   All rights reserved.                           
#                                                                           
#   This software is part of a beta-test version of the swat/cross_match/phrap 
#   package.  It should not be redistributed, or
#   used for any commercial purpose, including commercially funded
#   sequencing, without written permission from the author and the
#   University of Washington.
#   
#   This software is provided ``AS IS'' and any express or implied
#   warranties, including, but not limited to, the implied warranties of
#   merchantability and fitness for a particular purpose, are disclaimed.
#   In no event shall the authors or the University of Washington be
#   liable for any direct, indirect, incidental, special, exemplary, or
#   consequential damages (including, but not limited to, procurement of
#   substitute goods or services; loss of use, data, or profits; or
#   business interruption) however caused and on any theory of liability,
#   whether in contract, strict liability, or tort (including negligence
#   or otherwise) arising in any way out of the use of this software, even
#   if advised of the possibility of such damage.
#                                                                         
#*****************************************************************************/

  /* TAKE OUT FOLLOWING LINE IF time.h COMPILATION ERROR */
#include <time.h>

#include "swat.h"

#define VERSION "0.990329"
 
/* swat defaults */
#define PROTMATRIX "BLOSUM50" /* default protein score matrix */
#define GAP_INIT -12  /* default penalty for first residue in gap */
#define GAP_EXT -2 /* default penalty for each succeeding residue in gap */
#define END_GAP -1 /* gap penalty for gaps at ends of sequences
		      (for Needleman-Wunsch algorithm) */
#define E_CUTOFF 1.0 /* default E value (upper) cutoff */
#define Z_CUTOFF 6.0 /* default z-value (lower) cutoff */
#define MAX_NUM_ALIGNMENTS 20 /* max no. displayed alignments */

/* phrap, cross_match defaults */
#define PENALTY  -2 /* mismatch penalty (match is +1, gap is penalty - 2) for primary matching */
#define DNA_INDEXWORDSIZE 10 /* word size for indexing (partial hashing), in words.c */
#define PROT_INDEXWORDSIZE 4 /* word size for indexing (partial hashing), in words.c */
#define DNA_MINMATCH 14  /* minimum length perfect matching word (with no
			ambiguity chars) */
#define PROT_MINMATCH 4  /* minimum length perfect matching word (with no
			ambiguity chars) */
#define DNA_MAXMATCH 30  /* maximum length perfect matching word (with no
			ambiguity chars) */
#define PROT_MAXMATCH 4  /* maximum length perfect matching word (with no
			ambiguity chars) */
#define MAX_GROUP_SIZE 20
#define MINSCORE 30 /* minimum SWAT score (using PENALTY) for defining read matches */
#define MAXGAP 30  /* maximum gap size */
#define BANDWIDTH 14 /* 1/2 band size for banded swat searches */
#define TRIM_START 0 /* number of bases to be converted to 'N' at the start of each
                        read */
#define VECTOR_BOUND 80 /* no. of bases at beginning of read, matches lying entirely within which
			   should be considered vector */
#define TRIM_PENALTY -2 /* mismatch penalty for scoring degenerate end sequence */
#define TRIM_SCORE 20    /* minimum score for converting degenerate end sequence to N */
#define TRIM_QUAL 13 /* quality used in trimming reads (modified R. Mott method) */
#define DEFAULT_QUAL 15 /* quality value to be used when no .qual file is provided */
#define CONFIRM_LENGTH 8  /* minimum size of confirming segment (starts at first occurrence
			   of 3d distinct nuc following discrepancy) */
#define CONFIRM_TRIM 1 /* amount by which confirming segments are "trimmed" on
			  either side */
#define CONFIRM_PENALTY -5 /* penalty for confirming matches */
#define CONFIRM_SCORE 30 /* minimum SWAT score (using confirm_penalty) for confirming matches */

#define REPEAT_STRINGENCY .95 /* assumed % identity of repeats in computing LLR scores;
				 must be < 1.0 -- the closer to 1, the higher the stringency */

#define QUAL_SHOW 20 /* LLR cutoff for displaying discrepancies, low-quality regions */
#define MASKLEVEL 80 /* maximum percentage of aligned bases in query allowed to
			overlap a higher scoring match */
#define NODE_SEG 8
#define NODE_SPACE 4

#define SCREEN 0 
#define OLD_ACE 0
#define NEW_ACE 0
#define VIEW 0
#define ALIGNMENTS 0
#define DISCREP_LISTS 0
#define DISCREP_TABLES 0
#define REVISE_GREEDY 0
#define SHATTER_GREEDY 0
#define PREASSEMBLE 0
#define TAGS 0
#define RAW 0 /* raw Smith-Waterman scores */
#define WORD_RAW 0 /* raw word length, for minmatch */
#define CREATE_TEST_FILE 0 /* create file for testing validity of qualities */
#define PRINT_EXTRANEOUS_MATCHES 0 /* in phrap, print non-local matches between contig regions */
#define FORCELEVEL 0 /* stringency for a final "forcing" merge pass */
#define BYPASSLEVEL 1 /* stringency for bypassing reads */
#define FORCE_HIGH 0 /*  Allows ignoring high-quality discrancies during final "forcing" merge pass */
#define RETAIN_DUPLICATES 0 /*  retain exact duplicate reads, rather than eliminating
			     them  */
#define SUBCLONE_DELIM '.'
#define N_DELIM 1
#define GROUP_DELIM '_'
#define MAX_SUBCLONE_SIZE 5000 /* maximum possible size of a subclone */
#define EXP_INPUT 0  /* flag indicating whether to take experiment files as input */
#define EXP_OUTPUT 0 /* flag indicating whether to produce experiment files as output */


Parameters *parameters; /* exported */ 

get_parameters(argc, argv, calling_program)
     int argc;
     char **argv;
     char *calling_program;
{
  int i, penalty_flag, init_flag, extend_flag, ins_extend_flag, del_extend_flag, gap_flag, index_flag, minmatch_flag, maxmatch_flag;
  int num_files, swat_flag, phrap_flag, gcphrap_flag, cross_flag, cluster_flag;
  char *our_alloc();
  FILE *fp;
  FILE *fopenRead();
  File *file;
  float x;
  int nw(), full_nw(), smith_waterman(), quick_full_smith_waterman(), 
     full_smith_waterman();
  /* remove following if compile error */
  time_t timee;

  for (i = 0; i < argc; i++) printf("%s ", argv[i]);
  fprintf(stderr, "\n");
  for (i = 0; i < argc; i++) fprintf(stderr, "%s ", argv[i]);
  printf("\n%s version %s\n", calling_program, VERSION);
  fprintf(stderr, "\n%s version %s\n", calling_program, VERSION);

  notify("Reading parameters ... ");

  if (sizeof(int) < 4)
    fatalError("Ints are less than 4 bytes; use a different C compiler.");

  parameters = (Parameters *)our_alloc(sizeof(Parameters));
 
  strcpy(parameters->calling_program, calling_program);
  strcpy(parameters->version, VERSION);

  strcpy(parameters->date, "999999:999999");

  /* TAKE OUT FOLLOWING 4 LINES IF time.h COMPILATION ERROR */
  time(&timee);
  strftime(parameters->date, 20, "%y%m%d:%H%M%S", localtime( &timee ) );
  printf("\nRun date:time  %s", parameters->date);
  fprintf(stderr, "Run date:time  %s\n", parameters->date);


  parameters->argc = argc;
  parameters->argv = argv;

  swat_flag = !strcmp(calling_program, "swat");
  phrap_flag = !strcmp(calling_program, "phrap");
  gcphrap_flag = !strcmp(calling_program, "gcphrap");
  cross_flag = !strcmp(calling_program, "cross_match");
  cluster_flag = !strcmp(calling_program, "cluster");

  parameters->query_histograms = swat_flag;

  parameters->penalty = PENALTY;
  parameters->minscore = MINSCORE;
  parameters->maxgap = MAXGAP;
  parameters->bandwidth = BANDWIDTH;
  parameters->trim_start = TRIM_START;
  parameters->vector_bound = phrap_flag || gcphrap_flag || cluster_flag ? VECTOR_BOUND : 0;
  parameters->trim_penalty = TRIM_PENALTY;
  parameters->trim_score = TRIM_SCORE;
  parameters->trim_qual = TRIM_QUAL;
  parameters->default_qual = DEFAULT_QUAL;
  parameters->confirm_length = CONFIRM_LENGTH;
  parameters->confirm_trim = CONFIRM_TRIM;
  parameters->confirm_penalty = CONFIRM_PENALTY;
  parameters->confirm_score = CONFIRM_SCORE;
  parameters->repeat_stringency = REPEAT_STRINGENCY;

  parameters->max_group_size = MAX_GROUP_SIZE;
  parameters->qual_show = QUAL_SHOW;
  parameters->masklevel = MASKLEVEL;
  parameters->node_seg = NODE_SEG;
  parameters->node_space = NODE_SPACE;

  parameters->screen = SCREEN;
  parameters->old_ace = OLD_ACE; 
  parameters->new_ace = NEW_ACE; 
  parameters->view = VIEW; 
  parameters->alignments = ALIGNMENTS;
  parameters->discrep_lists = DISCREP_LISTS;
  parameters->discrep_tables = DISCREP_TABLES;
  parameters->revise_greedy = REVISE_GREEDY;
  parameters->shatter_greedy = SHATTER_GREEDY;
  parameters->preassemble = PREASSEMBLE;
  parameters->tags = TAGS;
  parameters->raw = RAW;
  parameters->word_raw = WORD_RAW;
  parameters->forcelevel = FORCELEVEL;
  parameters->bypasslevel = BYPASSLEVEL;
  parameters->force_high = FORCE_HIGH;
  parameters->retain_duplicates = RETAIN_DUPLICATES;
  parameters->print_extraneous_matches = PRINT_EXTRANEOUS_MATCHES;
  parameters->subclone_delim = SUBCLONE_DELIM;
  parameters->n_delim = N_DELIM;
  parameters->group_delim = GROUP_DELIM;
  parameters->max_hit_num = 50000000;   //Revised for ReAS

  parameters->matrix = 0;

  parameters->query_files = 0;
  parameters->subject_files = 0;

/* swat specific values */
  parameters->nw_flag = parameters->use_e = parameters->use_z = parameters->use_n = parameters->file_flag = 0;
  parameters->find_z_flag = 1;  /* 1;  NOTE CHANGE IN DEFAULT */
  parameters->end_gap = END_GAP;
  parameters->e_cutoff = E_CUTOFF;
  parameters->z_cutoff = Z_CUTOFF;
  parameters->max_num_alignments = MAX_NUM_ALIGNMENTS; 
  parameters->truncatedb = 0;
  parameters->align = smith_waterman;
  parameters->full_align = swat_flag ? full_smith_waterman : quick_full_smith_waterman;
  parameters->create_test_file = CREATE_TEST_FILE;
  parameters->max_subclone_size = MAX_SUBCLONE_SIZE;
  parameters->exp_input = EXP_INPUT;
  parameters->exp_output = EXP_OUTPUT;

  num_files = 0;
  penalty_flag = init_flag = extend_flag = ins_extend_flag = del_extend_flag = index_flag = minmatch_flag = maxmatch_flag = 0;

  /* N.B. filenames must occur first; anything not a parameter is assumed to be a filename */
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-penalty")) {
      if (!sscanf(argv[++i],"%d", &parameters->penalty)) errorMessage();
      penalty_flag = 1;
    }
    else if (!strcmp(argv[i], "-max_hit")) {
      if (!sscanf(argv[++i],"%d", &parameters->max_hit_num)) errorMessage();
    }
    else if (!strcmp(argv[i], "-gap_init")) {
      if (!sscanf(argv[++i],"%d", &parameters->gap_init)) errorMessage();
      init_flag = 1;
    }
    else if (!strcmp(argv[i], "-gap_ext")) {
      if (!sscanf(argv[++i],"%d", &parameters->gap_ext)) errorMessage();
      extend_flag = 1;
    }
    else if (!strcmp(argv[i], "-ins_gap_ext")) {
      if (!sscanf(argv[++i],"%d", &parameters->ins_gap_ext)) errorMessage();
      ins_extend_flag = 1;
    }
    else if (!strcmp(argv[i], "-del_gap_ext")) {
      if (!sscanf(argv[++i],"%d", &parameters->del_gap_ext)) errorMessage();
      del_extend_flag = 1;
    }
    else if (!strcmp(argv[i], "-minmatch")) {
      minmatch_flag = 1;
      if (!sscanf(argv[++i],"%d", &parameters->minmatch)) errorMessage();
    }
    else if (!strcmp(argv[i], "-maxmatch")) {
      maxmatch_flag = 1;
      if (!sscanf(argv[++i],"%d", &parameters->maxmatch)) errorMessage();
    }
    else if (!strcmp(argv[i], "-max_group_size")) {
     if (!sscanf(argv[++i],"%d", &parameters->max_group_size)) errorMessage();
    }
    else if (!strcmp(argv[i], "-indexwordsize")) {
      index_flag = 1;
      if (!sscanf(argv[++i],"%d", &parameters->indexwordsize)) errorMessage();
    }
    else if (!strcmp(argv[i], "-minscore")) {   
      if (!sscanf(argv[++i],"%d", &parameters->minscore)) errorMessage();
    }
    else if (!strcmp(argv[i], "-maxgap")) {  
      if (!sscanf(argv[++i],"%d", &parameters->maxgap)) errorMessage();
    }
    else if (!strcmp(argv[i], "-bandwidth")) {  
      if (!sscanf(argv[++i],"%d", &parameters->bandwidth)) errorMessage();
    }
    else if (!strcmp(argv[i], "-trim_start")) { 
      if (!sscanf(argv[++i],"%d", &parameters->trim_start)) errorMessage();
    }
    else if (!strcmp(argv[i], "-vector_bound")) { 
      if (!sscanf(argv[++i],"%d", &parameters->vector_bound)) errorMessage();
    }
    else if (!strcmp(argv[i], "-trim_penalty")) { 
      if (!sscanf(argv[++i],"%d", &parameters->trim_penalty)) errorMessage();
    }
    else if (!strcmp(argv[i], "-trim_score")) { 
      if (!sscanf(argv[++i],"%d", &parameters->trim_score)) errorMessage();
    }
    else if (!strcmp(argv[i], "-trim_qual")) { 
      if (!sscanf(argv[++i],"%d", &parameters->trim_qual)) errorMessage();
    }
    else if (!strcmp(argv[i], "-default_qual")) { 
      if (!sscanf(argv[++i],"%d", &parameters->default_qual)) errorMessage();
    }
    else if (!strcmp(argv[i], "-confirm_length")) { 
      if (!sscanf(argv[++i],"%d", &parameters->confirm_length)) errorMessage();
    }
    else if (!strcmp(argv[i], "-confirm_trim")) { 
      if (!sscanf(argv[++i],"%d", &parameters->confirm_trim)) errorMessage();
    }
    else if (!strcmp(argv[i], "-confirm_penalty")) { 
      if (!sscanf(argv[++i],"%d", &parameters->confirm_penalty)) errorMessage();
    }
    else if (!strcmp(argv[i], "-confirm_score")) { 
      if (!sscanf(argv[++i],"%d", &parameters->confirm_score)) errorMessage();
    }
    else if (!strcmp(argv[i], "-repeat_stringency")) { 
      if (!sscanf(argv[++i],"%f", &parameters->repeat_stringency)) errorMessage();

    }
    else if (!strcmp(argv[i], "-qual_show")) {
      if (!sscanf(argv[++i],"%f", &x)) errorMessage();
      parameters->qual_show = x;
    }
    else if (!strcmp(argv[i], "-masklevel")) { 
      if (!sscanf(argv[++i],"%d", &parameters->masklevel)) errorMessage();
    }
    else if (!strcmp(argv[i], "-node_seg")) { 
      if (!sscanf(argv[++i],"%d", &parameters->node_seg)) errorMessage();
    }
    else if (!strcmp(argv[i], "-node_space")) { 
      if (!sscanf(argv[++i],"%d", &parameters->node_space)) errorMessage();
    }
    else if (!strcmp(argv[i], "-max_subclone_size")) { 
      if (!sscanf(argv[++i],"%d", &parameters->max_subclone_size)) errorMessage();
    }
    else if (!strcmp(argv[i], "-M") || !strcmp(argv[i], "-matrix")) { 
      parameters->matrix = (char *)our_alloc(strlen(argv[++i]) + 1);
      strcpy(parameters->matrix, argv[i]);
    }
    else if (!strcmp(argv[i], "-screen")) parameters->screen = 1;
    else if (!strcmp(argv[i], "-old_ace")) parameters->old_ace = 1;
    else if (!strcmp(argv[i], "-new_ace")
	     || !strcmp(argv[i], "-ace")) parameters->new_ace = 1;
    else if (!strcmp(argv[i], "-view") || !strcmp(argv[i], "-viewfile")) parameters->view = 1;
    else if (!strcmp(argv[i], "-exp") || !strcmp(argv[i], "-expfile")) {
      if (!gcphrap_flag) 
	fatalError("option -exp is only available with gcphrap, not phrap -- see documentation");
      parameters->exp_output = 1;
      parameters->exp_dir = (char *)our_alloc((strlen(argv[++i]+1)));
      strcpy(parameters->exp_dir, argv[i]);
    }
    else if (!strcmp(argv[i], "-alignments")) parameters->alignments = 1;
    else if (!strcmp(argv[i], "-discrep_lists")) parameters->discrep_lists = 1;
    else if (!strcmp(argv[i], "-discrep_tables")) parameters->discrep_tables = 1;
    else if (!strcmp(argv[i], "-revise_greedy")) parameters->revise_greedy = 1;
    else if (!strcmp(argv[i], "-shatter_greedy")) parameters->shatter_greedy = 1;
    else if (!strcmp(argv[i], "-preassemble")) parameters->preassemble = 1;
    else if (!strcmp(argv[i], "-tags")) parameters->tags = 1;
    else if (!strcmp(argv[i], "-create_test_file")) { 
      if (!sscanf(argv[++i],"%d", &parameters->create_test_file)) errorMessage();
    }
    else if (!strcmp(argv[i], "-forcelevel")) { 
      if (!sscanf(argv[++i],"%d", &parameters->forcelevel)) errorMessage();
    }
    else if (!strcmp(argv[i], "-bypasslevel")) { 
      if (!sscanf(argv[++i],"%d", &parameters->bypasslevel)) errorMessage();
    }
    else if (!strcmp(argv[i], "-subclone_delim")) { 
      if (!sscanf(argv[++i],"%c", &parameters->subclone_delim)) errorMessage();
    }
    else if (!strcmp(argv[i], "-n_delim")) { 
      if (!sscanf(argv[++i],"%d", &parameters->n_delim)) errorMessage();
    }
    else if (!strcmp(argv[i], "-group_delim")) { 
      if (!sscanf(argv[++i],"%c", &parameters->group_delim)) errorMessage();
    }
    else if (!strcmp(argv[i], "-force_high")) 
      parameters->force_high = 1;
    else if (!strcmp(argv[i], "-retain_duplicates")) 
      parameters->retain_duplicates = 1;
    else if (!strcmp(argv[i], "-raw")) {
      if (swat_flag) parameters->find_z_flag = 0;
      else parameters->raw = 1;
    }
    else if (!strcmp(argv[i], "-word_raw")) parameters->word_raw = 1;
    else if (!strcmp(argv[i], "-print_extraneous_matches")) 
      parameters->print_extraneous_matches = 1;
    
    else if (!strcmp(argv[i], "-nw")) parameters->nw_flag = 1;
    else if (!strcmp(argv[i], "-file")) parameters->file_flag = 1;
    else if (!strcmp(argv[i], "-end_gap")) { 
      if (!sscanf(argv[++i],"%d",&parameters->end_gap)) errorMessage();
    }
    else if (!strcmp(argv[i], "-E")) {
      parameters->use_e = 1;
      if (!sscanf(argv[++i],"%f",&parameters->e_cutoff)) errorMessage();
    }
    else if (!strcmp(argv[i], "-z")) {
      parameters->use_z = 1;
      if (!sscanf(argv[++i],"%f",&parameters->z_cutoff)) errorMessage();
    }
    else if (!strcmp(argv[i], "-N")) {
      parameters->use_n = 1;
      if (!sscanf(argv[++i],"%d",&parameters->max_num_alignments)) errorMessage();
    }
    else if (!strcmp(argv[i], "-truncatedb")) { 
      if (!sscanf(argv[++i],"%d",&parameters->truncatedb)) errorMessage();
    }

    else if (argv[i][0] == '-') {
      fprintf(stderr, "\nFATAL ERROR: Command line option %s not recognized\n", argv[i]);
      exit(1);
    }
    else {
      file = (File *)our_alloc(sizeof(File));
      file->name = (char *)our_alloc((strlen(argv[i]) + 1) * sizeof(char));
      strcpy(file->name, argv[i]);
      file->fp = fopenRead(file->name);
      if (fgetc(file->fp) != '>') {
	if (gcphrap_flag) {
 	  parameters->exp_input = 1;
	}
	else {
	  fprintf(stderr, "\nERROR: file %s not in FASTA format -- leading character not >",
		  file->name);
	  exit(1);
	}
      }
      if (!parameters->query_files) {
	file->next = 0;
	parameters->query_files = file;
      }
      else { /* note order is reversed */
	
	file->next = parameters->subject_files;
	parameters->subject_files = file;
      }
    }
  }

  if (!parameters->query_files) 
    fatalError("Sequence files must be specified on command line. See documentation.");

  if ((phrap_flag || gcphrap_flag) && parameters->subject_files)
    fatalError("With current phrap version only one input sequence file may be specified");

  printf("\nQuery file(s): ");

  for (file = parameters->query_files; file; file = file->next)
    printf(" %s", file->name);

  if (parameters->subject_files) {
    printf("\nSubject file(s): ");
    for (file = parameters->subject_files; file; file = file->next)
      printf("  %s", file->name);
  }

  if (parameters->confirm_penalty > parameters->penalty)
    parameters->confirm_penalty = parameters->penalty;

  if (!parameters->matrix && !penalty_flag) {
    if (swat_flag) { 
      parameters->matrix = (char *)our_alloc(strlen(PROTMATRIX) + 1);
      strcpy(parameters->matrix, PROTMATRIX);
    }
  }
  gap_flag = set_score_mat(parameters->matrix, parameters->penalty);

  if (!init_flag) parameters->gap_init = gap_flag ? get_gap_init() : GAP_INIT;
  if (!extend_flag) parameters->gap_ext = gap_flag ? get_gap_ext() : GAP_EXT;
  if (!ins_extend_flag) parameters->ins_gap_ext = parameters->gap_ext;
  if (!del_extend_flag) parameters->del_gap_ext = parameters->gap_ext;

  set_gap_penalties(parameters->gap_init, parameters->gap_ext, 
		    parameters->ins_gap_ext, parameters->del_gap_ext, parameters->end_gap);
/* N.B. parameters->DNA_flag gets set in above call */

  parameters->DNA_flag = get_alphsize() < 20; /* NOT A GOOD CRITERION !!! */
  printf("\nPresumed sequence type: %s", parameters->DNA_flag ? "DNA" : "protein");

  set_table_mats();

  if (parameters->forcelevel < 0) parameters->forcelevel = 0;
  if (parameters->forcelevel > 10) parameters->forcelevel = 10;
  if (parameters->bypasslevel < 0) parameters->bypasslevel = 0;
  if (parameters->bypasslevel > 10) parameters->bypasslevel = 10;

  printf("\n\nPairwise comparison algorithm: %s %s", swat_flag ? "" : "banded",
	   parameters->nw_flag ? "Needleman-Wunsch" : "Smith-Waterman");

  printf("\n\nScore matrix ");
  if (parameters->matrix) printf("%s", parameters->matrix);
  else {
    printf("(set by value of penalty: %d)", parameters->penalty);
    if (parameters->penalty >= 0)
      fatalError("Penalty must be negative");
  }
  if (parameters->DNA_flag) {
    print_score_mat();
  }
  
  printf("\n\nGap penalties: gap_init: %d, gap_ext: %d, ins_gap_ext: %d, del_gap_ext: %d, ", 
	 parameters->gap_init, parameters->gap_ext, 
	 parameters->ins_gap_ext, parameters->del_gap_ext); 
  if (parameters->gap_init >= 0)
      fatalError("gap_init must be negative");

  if (!index_flag) 
    parameters->indexwordsize = parameters->DNA_flag ? DNA_INDEXWORDSIZE : PROT_INDEXWORDSIZE;
  if (!minmatch_flag) 
    parameters->minmatch = parameters->DNA_flag ? DNA_MINMATCH : PROT_MINMATCH;
  if (!maxmatch_flag) {
    parameters->maxmatch = cross_flag ? parameters->minmatch :
      (parameters->DNA_flag ? DNA_MAXMATCH : PROT_MAXMATCH);
  }

  if (parameters->maxmatch > 127) parameters->maxmatch = 127;
  if (parameters->minmatch > 127) parameters->minmatch = 127;
/* necessary because of use of characters to represent match length,
   in words.c */

  if (parameters->maxmatch < parameters->minmatch) 
    parameters->maxmatch = parameters->minmatch;
  if (parameters->indexwordsize > parameters->minmatch) 
    parameters->indexwordsize = parameters->minmatch;

  if (swat_flag) {
    if (parameters->nw_flag) {
      printf(", terminal: %d", parameters->end_gap);
      parameters->find_z_flag = 0; /* no valid E or z-values in this case */
      parameters->align = nw;
      parameters->full_align = full_nw;
    }
    if (!parameters->find_z_flag) {
      printf("\nz-scores and E-values will not be computed.");
      parameters->use_n = 1;
      parameters->use_z = parameters->use_e = 0;
    }
    else if (!(parameters->use_e || parameters->use_z || parameters->use_n)) 
      parameters->use_e = parameters->use_z = parameters->use_n = 1;
    printf("\n");
    if (parameters->use_z) printf("z cutoff: %.4g   ",  parameters->z_cutoff);
    if (parameters->use_e) printf("E cutoff: %.4g   ",  parameters->e_cutoff);
    if (parameters->use_n) printf("Max. no. displayed alignments: %d", parameters->max_num_alignments);  
    printf("\n");
  }

  if (phrap_flag || gcphrap_flag || cross_flag || cluster_flag) {
    
    if (!parameters->raw && parameters->DNA_flag) {
      printf("\nUsing complexity-adjusted scores.");
      print_background_freqs();
      set_complexity_adjust(.25);
    }
    else printf("\nUsing raw scores.");
    printf("\n\nminmatch: %d, maxmatch: %d, max_group_size: %d, minscore: %d, bandwidth: %d, indexwordsize: %d", 
	   parameters->minmatch, parameters->maxmatch, parameters->max_group_size, parameters->minscore, parameters->bandwidth,
	   parameters->indexwordsize);
    printf("\nvector_bound: %d", parameters->vector_bound); 
    printf("\nword_raw: %d", parameters->word_raw);
  }    
  if (phrap_flag || gcphrap_flag) {
    if (parameters->trim_start) 
      printf("\n%d characters trimmed from start of each read", parameters->trim_start);
    printf("\ntrim_penalty: %d, trim_score: %d, trim_qual: %d, maxgap: %d", 
	   parameters->trim_penalty, parameters->trim_score, parameters->trim_qual, parameters->maxgap);
    printf("\nrepeat_stringency: %f", parameters->repeat_stringency);
    printf("\nqual_show: %d", parameters->qual_show);
    
    printf("\nconfirm_length: %d, confirm_trim: %d, confirm_penalty: %d, confirm_score: %d",
	   parameters->confirm_length, parameters->confirm_trim, parameters->confirm_penalty, 
	   parameters->confirm_score);
    printf("\nnode_seg: %d, node_space: %d", 
	   parameters->node_seg, parameters->node_space); 
    printf("\nforcelevel: %d, bypasslevel: %d", parameters->forcelevel, parameters->bypasslevel);
    printf("\nmax_subclone_size: %d", parameters->max_subclone_size);
  }
    
  if (cross_flag) {
    printf("\nmasklevel: %d", parameters->masklevel);
  }
  notify(" Done\n");
}

errorMessage()
{	
  fatalError("Missing or misspecified numerical value on command line");
}
